.PHONY: install uninstall clean sbit bin/sbitc bin/5b1t test test-offline smoke analyze-coverage badges public shared dictionaries

default: documentation/documentation.pdf bin/sbitc sbit bin/5b1t

nodoc: bin/sbitc sbit bin/5b1t

docs: documentation/documentation.pdf

ifdef VERBOSE
INSTALL_FLAGS=-v
else
INSTALL_FLAGS=-q
endif

# Known MODEs: Release, Debug, Coverage, Profiling
ifdef MODE
MODE_FLAG=-XBUILD_MODE=$(MODE)
SBIT_MODE_FLAG=-B$(MODE)

ifeq ($(MODE),Release)
SBIT_TEST_CMD=5b1t -r 26
endif

ifeq ($(MODE),Debug)
SBIT_TEST_CMD=5b1t -r 26 -v -g
INSTALL_FLAGS+=--side-debug
endif

ifeq ($(MODE),Coverage)
SBIT_TEST_CMD=./bin/5b1t -L . $(SBIT_MODE_FLAG) -O build -r 26 -v
INSTALL_FLAGS+=--side-debug
endif

else
MODE_FLAG=
SBIT_MODE_FLAG=
SBIT_TEST_CMD=5b1t -r 26 -v
endif

PREFIX ?= /usr/local
SAFEPREFIX=$(subst @,\\@,$(subst \",\\\",$(PREFIX)))
SAFEVERSION=$(subst @,\\@,$(subst \",\\\",$(shell git log -n 1 --format='%h %ai %s')))
install: bin/sbitc bin/sbite bin/sbitd bin/5b1t
	-gprinstall -f -p --prefix=$(PREFIX) $(INSTALL_FLAGS) --mode=usage sbitc.gpr $(MODE_FLAG)
	-gprinstall -f -p --prefix=$(PREFIX) $(INSTALL_FLAGS) --install-name=sbit sbit.gpr $(MODE_FLAG)
	cp -r bin $(PREFIX)
	cp -r share $(PREFIX)
	cp -r tools $(PREFIX)/share
	sed -i 's@SBIT_PREFIX="."@SBIT_PREFIX="$(SAFEPREFIX)"@g' $(PREFIX)/bin/5b1t
	sed -i 's@SBIT_VERSION="local version"@SBIT_VERSION="$(SAFEVERSION)"@g' $(PREFIX)/bin/5b1t

uninstall:
	-gprinstall --uninstall --prefix=$(PREFIX) --mode=usage sbitc.gpr
	-gprinstall --uninstall --prefix=$(PREFIX) sbit.gpr
	-rm $(PREFIX)/bin/sbite
	-rm $(PREFIX)/bin/sbitd
	-rm $(PREFIX)/bin/5b1t
	-rm $(PREFIX)/share/types

documentation/documentation.pdf: documentation/operations.tex

documentation/operations.tex: share/operations
	./tools/ops_to_latex.awk < $^ > $@

%.pdf: %.tex
	xelatex -output-directory $(@D) -interaction=batchmode $< || xelatex -output-directory $(@D) $<
	xelatex -output-directory $(@D) -interaction=batchmode $< # again for TOC

bin/sbitc:
	gprbuild -j0 -gnatef -p -P sbitc.gpr $(MODE_FLAG)

clean:
	gprclean -P sbitc.gpr
	gprclean -P sbit.gpr
	find documentation -name '*.aux' -delete
	find documentation -name '*.lof' -delete
	find documentation -name '*.log' -delete
	find documentation -name '*.lot' -delete
	find documentation -name '*.toc' -delete
	-rm build/*
	-rm html/*
	-rm lib/*
	-rm documentation/operations.tex

sbit:
	gprbuild -j0 -gnatef -p -P sbit.gpr $(MODE_FLAG)

test:
	@./test/lstest.sh test | ./test/run_tests.sh $(SBIT_TEST_CMD)

# Runs tests that don't use any internet (equivalent to test_net)
test-offline:
	@export GREP_5B1T_TEST_FILTER='net'; ./test/lstest.sh test | ./test/run_tests.sh $(SBIT_TEST_CMD)

# Runs tests that don't match the given pattern
test-%:
	@./test/lstest.sh test | grep -v '$*' | ./test/run_tests.sh $(SBIT_TEST_CMD)

# Runs tests that match the given pattern
test.%:
	@./test/lstest.sh test | grep '$*' | ./test/run_tests.sh $(SBIT_TEST_CMD)

# Runs tests that match the given tag pattern
test@%:
	@export GREP_5B1T_TEST_SELECT='$*'; ./test/lstest.sh test | ./test/run_tests.sh $(SBIT_TEST_CMD)

# Runs tests that don't match the given tag pattern
test_%:
	@export GREP_5B1T_TEST_FILTER='$*'; ./test/lstest.sh test | ./test/run_tests.sh $(SBIT_TEST_CMD)

shared: dictionaries

dictionaries:
	./tools/get_all_dicts.sh

smoke:
	@echo -n "~" > smoke.sbit
	@./bin/5b1t -L . $(SBIT_MODE_FLAG) -e smoke.sbit > smoke
	@mkdir smokeobj
	@./bin/5b1t -L . $(SBIT_MODE_FLAG) -i S smoke -O smokeobj -s smoke.adb -c -o smoke.out
	@echo "sMOKE tEST..." | ./smoke.out > smoke.txt
	@echo "Smoke Test..." > expected.txt
	@if ! cmp smoke.txt expected.txt; then echo "--- SMOKE TEST FAILED! ---"; SUCCESS=false; else echo "Smoke test passed."; SUCCESS=true; fi; rm smoke.sbit smoke smoke.out smoke.txt expected.txt smoke.adb; rm -rf smokeobj; $$SUCCESS

analyze-coverage:
	mkdir -p public
	touch build/*.gcno
	touch build/*.gcda
	lcov -d build -d src --no-external -c -o coverage.all
	lcov -r coverage.all '*__*.adb' -o coverage.info
	genhtml -o public/html coverage.info
	lcov --list coverage.info > coverage.summary
	-rm coverage.all coverage.info

public:
	./tools/make_badges.sh results.json
	./tools/report2html.sh results.json > public/results.html
