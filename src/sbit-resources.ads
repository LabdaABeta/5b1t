-- This file provides access to static resources that may be used by the runtime
package Sbit.Resources is
    No_Resource : exception;

    procedure Initialize (Share : String);

    procedure Access_Resource (
        Name : String;
        Process_Line : access procedure (Line : String));
end Sbit.Resources;
