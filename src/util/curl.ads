-- Implements Curl functionality
package Curl is
    Curl_Error : exception;

    -- Invokes Callback for each line
    procedure Curl (
        Url : String;
        Callback : access procedure (Line : String));
end Curl;
