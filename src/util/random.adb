with Ada.Numerics.Discrete_Random;

package body Random is
    type Random_Result is mod 2 ** 32;
    Max_Result : constant Random_Result := 2 ** 32 - 1;

    -- Current GCC uses MT 19937 over 32-bit words
    package Random is new Ada.Numerics.Discrete_Random (Random_Result);

    Generator : Random.Generator;

    procedure Initialize is
    begin
        Random.Reset (Generator);
    end Initialize;

    procedure Initialize (Seed : Integer) is
    begin
        Random.Reset (Generator, Seed);
    end Initialize;

    function Next return Result is
        -- How many values the type can hold
        Type_Size : constant Random_Result :=
            Result'Pos (Result'Last) - Result'Pos (Result'First) + 1;

        -- How many copies of each value can be stored
        Total_Fit : constant Random_Result := Max_Result / Type_Size;

        -- The maximum usable result from the RNG
        Max_Valid : constant Random_Result := Type_Size * Total_Fit;

        Yield : Random_Result := Random.Random (Generator);
    begin
        while Yield >= Max_Valid loop
            Yield := Random.Random (Generator);
        end loop;

        return Result'Val (Result'Pos (Result'First) + Yield mod Type_Size);
    end Next;
end Random;
