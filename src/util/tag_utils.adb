package body Tag_Utils is
    function Common_Ancestor (Left, Right : Ada.Tags.Tag) return Ada.Tags.Tag is
        use type Ada.Tags.Tag;

        Parent : Ada.Tags.Tag := Left;
    begin
        if Left = Right then
            return Left;
        end if;

        while not Ada.Tags.Is_Descendant_At_Same_Level (Right, Parent) loop
            Parent := Ada.Tags.Parent_Tag (Parent);
        end loop;

        return Parent;
    end Common_Ancestor;
end Tag_Utils;
