-- This package maintains the state of each random number generator used
package Random is
    procedure Initialize; -- Using time as seed
    procedure Initialize (Seed : Integer); -- Using integer as seed

    generic
        type Result is (<>);
    function Next return Result;
end Random;
