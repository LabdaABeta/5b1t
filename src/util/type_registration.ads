with Ada.Tags;

package Type_Registration is
    pragma Elaborate_Body;
    procedure Register (
        From : String; -- Package name
        To : Ada.Tags.Tag);
    function Reduce (
        From : String;
        Default : Ada.Tags.Tag)
        return Ada.Tags.Tag;
end Type_Registration;
