with Ada.Tags;
with Sbit;

package Set_Registration is
    function Register (From, To : Ada.Tags.Tag) return Sbit.Nothing;
    function Reduce (Root, Default : Ada.Tags.Tag) return Ada.Tags.Tag;
end Set_Registration;
