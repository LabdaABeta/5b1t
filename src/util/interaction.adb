with Ada.Text_IO;
with Interfaces.C;
with Interfaces.C.Strings;

package body Interaction is
    use type Interfaces.C.int;

    function System (
        Command : Interfaces.C.Strings.chars_ptr)
        return Interfaces.C.int;
    pragma Import (C, System, "system");

    Input_Check_Command : aliased Interfaces.C.char_array :=
        Interfaces.C.To_C ("[ -t 0 ]");
    Output_Check_Command : aliased Interfaces.C.char_array :=
        Interfaces.C.To_C ("[ -t 1 ]");

    function Is_Input_TTY return Boolean is (
        System (
            Command =>
                Interfaces.C.Strings.To_Chars_Ptr (Input_Check_Command'Access)
        ) = 0);

    function Is_Output_TTY return Boolean is (
        System (
            Command =>
                Interfaces.C.Strings.To_Chars_Ptr (Output_Check_Command'Access)
        ) = 0);

    function Get_Natural (From : Inputter) return Natural is
    begin
        if From.Direct then
            Ada.Text_IO.Put (
                File => Ada.Text_IO.Standard_Error,
                Item => "Enter a natural number: ");
        end if;

        return Natural'Value (Ada.Text_IO.Get_Line);
    end Get_Natural;

    function Get_Int (From : Inputter) return Big_Integers.Big_Integer is
    begin
        if From.Direct then
            Ada.Text_IO.Put (
                File => Ada.Text_IO.Standard_Error,
                Item => "Enter an integer: ");
        end if;

        return Big_Integers.Create (Ada.Text_IO.Get_Line);
    end Get_Int;

    function Get_Line (From : Inputter) return String is
    begin
        if From.Direct then
            Ada.Text_IO.Put (
                File => Ada.Text_IO.Standard_Error,
                Item => "Enter a line of text: ");
        end if;

        return Ada.Text_IO.Get_Line;
    end Get_Line;

    procedure Put_Int (To : Outputter; Value : Big_Integers.Big_Integer) is
    begin
        if To.Collections.Is_Empty then
            Ada.Text_IO.Put_Line (Big_Integers.Image (Value));
        else
            Ada.Text_IO.Put (Big_Integers.Image (Value));
        end if;
    end Put_Int;

    procedure Put_String (To : Outputter; Value : String) is
        Data : constant String := (
            if To.Direct then
                """" & Value & """"
            else
                Value);
    begin
        if To.Collections.Is_Empty then
            Ada.Text_IO.Put_Line (Data);
        else
            Ada.Text_IO.Put (Data);
        end if;
    end Put_String;

    procedure Start_Collection (To : in out Outputter; Kind : Collection_Kind)
    is
    begin
        if To.Direct then
            Ada.Text_IO.Put (
                File => Ada.Text_IO.Standard_Error,
                Item => "Collection: ");
        end if;

        Ada.Text_IO.Put (
            case Kind is
                when COLLECTION_TUPLE => '(',
                when COLLECTION_SET => '{',
                when COLLECTION_LIST => '[');

        To.Collections.Append (Kind);
    end Start_Collection;

    procedure End_Collection (To : in out Outputter) is
    begin
        Ada.Text_IO.Put_Line (
            case To.Collections.Last_Element is
                when COLLECTION_TUPLE => ")",
                when COLLECTION_SET => "}",
                when COLLECTION_LIST => "]");

        To.Collections.Delete_Last;
    end End_Collection;

    procedure Separator (To : Outputter) is
    begin
        if To.Collections.Is_Empty then
            return;
        end if;

        Ada.Text_IO.Put (", ");
    end Separator;
begin
    Input_Instance.Direct := Is_Input_TTY;
    Output_Instance.Direct := Is_Output_TTY;
end Interaction;
