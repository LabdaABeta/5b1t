with Ada.Text_IO;
with Big_Integers; use Big_Integers;
with Ada.Containers.Ordered_Maps;
with Curl;
with Ada.Strings.Fixed;

package body OEIS is

    type Memo_Datum is
        record
            List : Positive;
            Index : Natural;
        end record;

    function "<" (Left, Right : Memo_Datum) return Boolean is
    begin
        return Left.List < Right.List or else Left.Index < Right.Index;
    end "<";

    package Memo_Maps is new Ada.Containers.Ordered_Maps (
        Memo_Datum, Big_Integer);

    Memos : Memo_Maps.Map := Memo_Maps.Empty_Map;

    function Get_OEIS (List, Index : Natural) return Big_Integers.Big_Integer is
        Found : Boolean := False;

        procedure Add_To_List (Line : String) is
        begin
            if Line'Length > 0 and then
                Line (Line'First) in '0' .. '9'
            then
                declare
                    Space_Index : constant Natural :=
                        Ada.Strings.Fixed.Index (Line, " ", 1);
                    Idx : constant Natural := Natural'Value (
                        Line (Line'First .. Space_Index - 1));
                    Val : constant Big_Integer := Create (
                        Line (Space_Index .. Line'Last));
                begin
                    Memos.Insert ((List, Idx), Val);
                    if Idx = Index then
                        Found := True;
                    end if;
                end;
            end if;
        end Add_To_List;

        Zero_Padded : String (1 .. 6);
        R_List : constant String := Natural'Image (List);
    begin
        Ada.Strings.Fixed.Move (
            Source => R_List (R_List'First + 1 .. R_List'Last),
            Target => Zero_Padded,
            Justify => Ada.Strings.Right,
            Pad => '0');

        Curl.Curl (
            "http://oeis.org/A" & Zero_Padded &
            "/b" & Zero_Padded & ".txt",
            Add_To_List'Access);

        if not Found then
            Ada.Text_IO.Put_Line (Ada.Text_IO.Standard_Error,
                "Unknown, unimplemented, or out of range " &
                "OEIS sequence:" &
                Integer'Image (List) & " @" &
                Integer'Image (Index));
            return Create (0);
        end if;

        return Memos.Element ((List, Index));
    end Get_OEIS;

    function Get (List, Index : Natural) return Big_Integers.Big_Integer is
    begin
        if not Memos.Contains ((List, Index)) then
            declare
                Memoized : constant Big_Integer := Get_OEIS (List, Index);
            begin
                -- Get_OEIS might have generated the answer itself
                if not Memos.Contains ((List, Index)) then
                    Memos.Insert ((List, Index), Memoized);
                end if;
            end;
        end if;

        return Memos.Element ((List, Index));
    end Get;
end OEIS;
