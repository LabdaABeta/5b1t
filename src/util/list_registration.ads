with Ada.Tags;

package List_Registration is
    pragma Elaborate_Body;
    procedure Register (From, To : Ada.Tags.Tag);
    function Reduce (Root, Default : Ada.Tags.Tag) return Ada.Tags.Tag;
end List_Registration;
