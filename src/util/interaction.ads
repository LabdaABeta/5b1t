with Big_Integers;
with Ada.Containers.Doubly_Linked_Lists;

package Interaction is
    pragma Elaborate_Body;

    type Inputter is tagged limited private;
    type Outputter is tagged limited private;

    type Collection_Kind is (COLLECTION_TUPLE, COLLECTION_SET, COLLECTION_LIST);

    -- TODO: Add Get_Boolean/Get_Rational/etc
    -- TODO: Add argument for plurality (SET/LIST/SINGULAR/ETC)
    function Get_Natural (From : Inputter) return Natural;
    function Get_Int (From : Inputter) return Big_Integers.Big_Integer;
    function Get_Line (From : Inputter) return String;

    procedure Put_Int (To : Outputter; Value : Big_Integers.Big_Integer);
    procedure Put_String (To : Outputter; Value : String);
    procedure Start_Collection (To : in out Outputter; Kind : Collection_Kind);
    procedure End_Collection (To : in out Outputter);
    procedure Separator (To : Outputter);

    Input : constant not null access Inputter;
    Output : constant not null access Outputter;
private
    package Collection_Lists is new Ada.Containers.Doubly_Linked_Lists (
        Element_Type => Collection_Kind);

    type Boolean_Access is access all Boolean;

    type Inputter is tagged limited
        record
            Direct : Boolean := False;
        end record;

    type Outputter is tagged limited
        record
            Direct : Boolean := False;
            Collections : Collection_Lists.List;
        end record;

    Input_Instance : aliased Inputter;
    Output_Instance : aliased Outputter;

    Input : constant not null access Inputter := Input_Instance'Access;
    Output : constant not null access Outputter := Output_Instance'Access;
end Interaction;
