with Ada.Text_IO;
with Sbit.Blockcats;
with Opinfo; use Opinfo;
with Ada.Strings.Unbounded; use Ada.Strings.Unbounded;

package body Debug is
    function "-" (Right : Unbounded_String) return String renames To_String;
    function Debug (
        This : Sbit.Datatype'Class;
        Op : Sbit.Code_Point;
        Verbose : Debug_Verbosity;
        Block : access function (
            This : Sbit.Datatype'Class)
            return Sbit.Datatype'Class := null)
        return Sbit.Datatype'Class
    is
        Ops : constant Sbit_Type_Maps.Map := Get_Operations;
        Source_Rep : constant Sbit_Representation := +This.Type_Image;

        procedure Err_Line (Item : String) is
        begin
            Ada.Text_IO.Put_Line (Ada.Text_IO.Standard_Error, Item);
        end Err_Line;

        procedure Err (Item : String) is
        begin
            Ada.Text_IO.Put (Ada.Text_IO.Standard_Error, Item);
        end Err;
    begin
        Err (This.Image);
        Err ("." & String (Sbit.Canonical_Name (Op)));

        if Block /= null then
            Err ("[" & Block.all (Sbit.Blockcats.Create).Image & "]");
        end if;

        if not Ops.Contains (Source_Rep) then
            Err_Line ("?");
            Err_Line ("Could not find type '" & (-Source_Rep) & "'!");
            Err_Line ("Cannot debug this operation. Sorry.");
            return This;
        end if;

        if not Ops.Element (Source_Rep).Operations (
            (if Block /= null then Blocked else No_Block), Op).Exists
        then
            Err_Line ("?");
            Err_Line ("This operator is undocumented!");
            Err_Line ("Cannot debug this operation. Sorry.");
            return This;
        end if;

        declare
            Source : constant Sbit_Type := Ops.Element (Source_Rep);
            The_Op : constant Present_Operator := Source.Operations (
                (if Block /= null then Blocked else No_Block), Op);
            Start : Boolean := True;
        begin
            case Verbose is
                when Low =>
                    Err_Line (" => ");
                when Medium =>
                    Err (" (" & (-The_Op.Name) & ") => (");

                    for Ret of The_Op.Return_Types loop
                        if not Start then
                            Err (", ");
                        end if;
                        Start := False;

                        if Ops.Contains (Ret) then
                            Err (-Ops.Element (Ret).Name);
                        else
                            Err ("???");
                        end if;
                    end loop;

                    Err_Line (")");
                when High =>
                    Err ((-The_Op.Name) & ": " & (-Source_Rep) & " -> ");

                    for Ret of The_Op.Return_Types loop
                        if not Start then
                            Err (", ");
                        end if;
                        Start := False;

                        Err (-Ret);
                    end loop;

                    Err_Line (-The_Op.Description);

                    for Ret of The_Op.Return_Types loop
                        if Ops.Contains (Ret) then
                            Err_Line (" => " & (-Ops.Element (Ret).Name));
                            Err_Line (-Ops.Element (Ret).Description);
                        else
                            Err_Line (" => Unknown Type");
                            Err_Line ("This type is undocumented.");
                            Err_Line ("No information is available for it.");
                        end if;
                    end loop;

                    Err_Line ("");
            end case;
        end;

        return This;
    end Debug;
end Debug;
