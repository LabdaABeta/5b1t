-- Implements PCRE regex matching using the GNAT.Regpat library
package Regex is
    type Match is
        record
            Start, Finish : Natural;
        end record;
    type Match_List is array (Positive range <>) of Match;

    -- See GNAT.Regpat to understand these better
    type Reg_Mode is
        record
            Case_Insensitive : Boolean;
            Single_Line : Boolean;
            Multi_Line : Boolean;
        end record;

    Case_Insensitive : constant Reg_Mode := (True, False, False);

    function Quote (Input : String) return String;

    -- Returns if the pattern matches the input
    function Matches (
        Input, Pattern : String;
        Mode : Reg_Mode := (False, False, False))
        return Boolean;

    -- Returns each group of the match of pattern in the input
    function Get_Matches (
        Input, Pattern : String;
        Mode : Reg_Mode := (False, False, False))
        return Match_List;

    -- Returns each subsequence of the input that is separated by the pattern
    function Tokenize (
        Input, Pattern : String;
        Mode : Reg_Mode := (False, False, False))
        return Match_List;

    -- Returns the input with all occurrences of the input removed
    function Remove (
        Input, Pattern : String;
        Mode : Reg_Mode := (False, False, False))
        return String;
end Regex;
