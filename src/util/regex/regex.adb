with GNAT.Regpat;

package body Regex is

    function Quote (Input : String) return String is
    begin
        return GNAT.Regpat.Quote (Input);
    end Quote;

    function To_Flags (From : Reg_Mode) return GNAT.Regpat.Regexp_Flags is
        use type GNAT.Regpat.Regexp_Flags;
        Result : GNAT.Regpat.Regexp_Flags := GNAT.Regpat.No_Flags;
    begin
        if From.Case_Insensitive then
            Result := Result or GNAT.Regpat.Case_Insensitive;
        end if;

        if From.Single_Line then
            Result := Result or GNAT.Regpat.Single_Line;
        end if;

        if From.Multi_Line then
            Result := Result or GNAT.Regpat.Multiple_Lines;
        end if;

        return Result;
    end To_Flags;

    function To_List (From : GNAT.Regpat.Match_Array) return Match_List is
        Result : Match_List (1 .. From'Length);
    begin
        for I in From'Range loop
            Result (I - From'First + 1) := (
                Start => From (I).First,
                Finish => From (I).Last);
        end loop;

        return Result;
    end To_List;

    function Matches (
        Input, Pattern : String;
        Mode : Reg_Mode := (False, False, False))
        return Boolean
    is
        Compiled : constant GNAT.Regpat.Pattern_Matcher := GNAT.Regpat.Compile (
            Pattern, To_Flags (Mode));
    begin
        return GNAT.Regpat.Match (Compiled, Input) >= Input'First;
    end Matches;

    function Get_Matches (
        Input, Pattern : String;
        Mode : Reg_Mode := (False, False, False))
        return Match_List
    is
        Compiled : constant GNAT.Regpat.Pattern_Matcher := GNAT.Regpat.Compile (
            Pattern, To_Flags (Mode));
        Groups : constant GNAT.Regpat.Match_Count :=
            GNAT.Regpat.Paren_Count (Compiled);
        Match_Array : GNAT.Regpat.Match_Array (0 .. Groups);
        use type GNAT.Regpat.Match_Location;
    begin
        GNAT.Regpat.Match (Compiled, Input, Match_Array);

        for I in Match_Array'Range loop
            if Match_Array (I) = GNAT.Regpat.No_Match then
                return To_List (Match_Array (Match_Array'First .. I));
            end if;
        end loop;

        return To_List (Match_Array);
    end Get_Matches;

    function Tokenize (
        Input, Pattern : String;
        Mode : Reg_Mode := (False, False, False))
        return Match_List
    is
        Compiled : constant GNAT.Regpat.Pattern_Matcher := GNAT.Regpat.Compile (
            Pattern, To_Flags (Mode));

        function Recursive_Tokenize (Start_From : Natural) return Match_List is
            Matches : GNAT.Regpat.Match_Array (0 .. 1);
            Result : Match;

            use type GNAT.Regpat.Match_Location;
        begin
            GNAT.Regpat.Match (
                Self => Compiled,
                Data => Input (Start_From .. Input'Last),
                Matches => Matches);

            if Matches (0) = GNAT.Regpat.No_Match then
                Result.Start := Start_From;
                Result.Finish := Input'Last;
                return (1 => Result);
            end if;

            Result.Start := Start_From;
            Result.Finish := Matches (0).First - 1;
            return Result & Recursive_Tokenize (Matches (0).Last + 1);
        end Recursive_Tokenize;
    begin
        return Recursive_Tokenize (Input'First);
    end Tokenize;

    function Remove (
        Input, Pattern : String;
        Mode : Reg_Mode := (False, False, False))
        return String
    is
        Compiled : constant GNAT.Regpat.Pattern_Matcher := GNAT.Regpat.Compile (
            Pattern, To_Flags (Mode));

        function Recursive_Remove (Current : String) return String is
            Matches : GNAT.Regpat.Match_Array (0 .. 1);

            use type GNAT.Regpat.Match_Location;
        begin
            GNAT.Regpat.Match (Compiled, Current, Matches);

            if Matches (0) = GNAT.Regpat.No_Match then
                return Current;
            end if;

            return Recursive_Remove (
                Current (Current'First .. Matches (0).First - 1) &
                Current (Matches (0).Last + 1 .. Current'Last));
        end Recursive_Remove;
    begin
        return Recursive_Remove (Input);
    end Remove;
end Regex;
