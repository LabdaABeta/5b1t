with Ada.Tags;

package Tuple_Registration is
    pragma Elaborate_Body;

    -- A tuple T will match the From registration if:
    --  - T has the same number of elements as From
    --  - Each element in T is a descendant of the corresponding element in From
    procedure Register (From : Ada.Tags.Tag_Array; To : Ada.Tags.Tag);
    function Reduce (
        From : Ada.Tags.Tag_Array;
        Default : Ada.Tags.Tag)
        return Ada.Tags.Tag;
end Tuple_Registration;
