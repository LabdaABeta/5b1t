with Ada.Containers.Indefinite_Vectors;

package body Type_Registration is
    type Name_Tag (Size : Natural) is
        record
            Name : String (1 .. Size);
            To : Ada.Tags.Tag;
        end record;

    package Name_Tag_Vectors is new Ada.Containers.Indefinite_Vectors (
        Positive, Name_Tag);
    Reductions : Name_Tag_Vectors.Vector := Name_Tag_Vectors.Empty_Vector;

    procedure Register (
        From : String; -- Package name
        To : Ada.Tags.Tag) is
    begin
        Reductions.Append ((From'Length, From, To));
    end Register;

    function Reduce (
        From : String;
        Default : Ada.Tags.Tag)
        return Ada.Tags.Tag
    is
    begin
        for Name of Reductions loop
            if Name.Name = From then
                return Name.To;
            end if;
        end loop;

        return Default;
    end Reduce;
end Type_Registration;
