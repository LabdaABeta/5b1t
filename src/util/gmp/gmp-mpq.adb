package body GMP.MPQ is
    -- Not exposed as a symbol, implementation copied from source
    function Sign (Item : access constant MPQ_T) return C_Int is
    begin
        return GMP.MPZ.Sign (Item.MP_Num'Access);
    end Sign;
end GMP.MPQ;
