with Interfaces.C.Strings;
with GMP.MPZ;

package body Big_Rationals is
    use type GMP.C_Int;
    use type GMP.Polarity;

    procedure Initialize (This : in out Big_Rational) is
    begin
        GMP.MPQ.Initialize (This.Value'Access);
    end Initialize;

    procedure Finalize (This : in out Big_Rational) is
    begin
        GMP.MPQ.Finalize (This.Value'Access);
    end Finalize;

    procedure Adjust (This : in out Big_Rational) is
        Result : aliased GMP.MPQ.MPQ_T;
    begin
        GMP.MPQ.Initialize (Result'Access);
        GMP.MPQ.Set (Result'Access, This.Value'Access);
        This.Value := Result;
    end Adjust;

    function Create (
        From : String;
        Base : GMP.Representation_Base := 0)
        return Big_Rational
    is
        Result : Big_Rational;
        Num : constant access GMP.MPZ.MPZ_T := Result.Value.MP_Num'Access;
        Den : constant access GMP.MPZ.MPZ_T := Result.Value.MP_Den'Access;
        Error : GMP.C_Int;

        function String_Base return GMP.Representation_Base is
            use type GMP.Representation_Base;
        begin
            if Base /= 0 then
                return Base;
            end if;

            if From'Length < 2 then
                return 10;
            end if;

            if From (From'First) /= '0' then
                return 10;
            end if;

            case From (From'First + 1) is
                when 'x' | 'X' => return 16;
                when 'b' | 'B' => return 2;
                when others => return 8;
            end case;
        end String_Base;
    begin
        for I in From'Range loop
            if From (I) = '.' or From (I) = ',' then
                declare
                    Input : GMP.C_String := Interfaces.C.Strings.New_String (
                        From (From'First .. I - 1) & From (I + 1 .. From'Last));
                begin
                    Error := GMP.MPZ.Set_String (
                        This => Num,
                        From => Input,
                        Base => GMP.C_Int (Base));

                    Interfaces.C.Strings.Free (Input);

                    if Error /= 0 then
                        raise Big_Rational_Exception;
                    end if;
                end;

                declare
                    Base_MPZ : aliased GMP.MPZ.MPZ_T;
                begin
                    GMP.MPZ.Init_Unsigned (
                        This => Base_MPZ'Access,
                        From => GMP.Unsigned (String_Base));

                    GMP.MPZ.Power (Den, Base_MPZ'Access,
                        GMP.Unsigned (From'Last - I));

                    GMP.MPZ.Finalize (Base_MPZ'Access);
                end;

                GMP.MPQ.Canonicalize (Result.Value'Access);
                return Result;
            end if;
        end loop;

        -- No ,/. found, so must be normal GMP format
        declare
            Input : GMP.C_String := Interfaces.C.Strings.New_String (From);
        begin
            Error := GMP.MPQ.Set_String (
                This => Result.Value'Access,
                From => Input,
                Base => GMP.C_Int (Base));

            Interfaces.C.Strings.Free (Input);

            if Error /= 0 then
                raise Big_Rational_Exception;
            end if;
        end;

        GMP.MPQ.Canonicalize (Result.Value'Access);
        return Result;
    end Create;

    function Create (From : Big_Integers.Big_Integer) return Big_Rational is
        Result : Big_Rational;
    begin
        GMP.MPQ.Set_MPZ (Result.Value'Access, From.Value'Access);
        return Result;
    end Create;

    function Create (Num, Den : Big_Integers.Big_Integer) return Big_Rational is
        Result : Big_Rational;
    begin
        GMP.MPZ.Set (Result.Value.MP_Num'Access, Num.Value'Access);
        GMP.MPZ.Set (Result.Value.MP_Den'Access, Den.Value'Access);
        GMP.MPQ.Canonicalize (Result.Value'Access);
        return Result;
    end Create;

    function "=" (Left, Right : Big_Rational) return Boolean is
        Result : GMP.C_Int;
    begin
        Result := GMP.MPQ.Compare (Left.Value'Access, Right.Value'Access);
        return Result = 0;
    end "=";

    function ">" (Left, Right : Big_Rational) return Boolean is
        Result : GMP.C_Int;
    begin
        Result := GMP.MPQ.Compare (Left.Value'Access, Right.Value'Access);
        return Result > 0;
    end ">";

    function "<" (Left, Right : Big_Rational) return Boolean is
        Result : GMP.C_Int;
    begin
        Result := GMP.MPQ.Compare (Left.Value'Access, Right.Value'Access);
        return Result < 0;
    end "<";

    function ">=" (Left, Right : Big_Rational) return Boolean is
        Result : GMP.C_Int;
    begin
        Result := GMP.MPQ.Compare (Left.Value'Access, Right.Value'Access);
        return Result >= 0;
    end ">=";

    function "<=" (Left, Right : Big_Rational) return Boolean is
        Result : GMP.C_Int;
    begin
        Result := GMP.MPQ.Compare (Left.Value'Access, Right.Value'Access);
        return Result <= 0;
    end "<=";

    function "+" (Left, Right : Big_Rational) return Big_Rational is
        Result : Big_Rational;
    begin
        GMP.MPQ.Add (
            Result => Result.Value'Access,
            X => Left.Value'Access,
            Y => Right.Value'Access);
        return Result;
    end "+";

    function "-" (Left, Right : Big_Rational) return Big_Rational is
        Result : Big_Rational;
    begin
        GMP.MPQ.Sub (
            Result => Result.Value'Access,
            X => Left.Value'Access,
            Y => Right.Value'Access);
        return Result;
    end "-";

    function "-" (This : Big_Rational) return Big_Rational is
        Result : Big_Rational;
    begin
        GMP.MPQ.Negate (Result.Value'Access, This.Value'Access);
        return Result;
    end "-";

    function "*" (Left, Right : Big_Rational) return Big_Rational is
        Result : Big_Rational;
    begin
        GMP.MPQ.Mul (
            Result => Result.Value'Access,
            X => Left.Value'Access,
            Y => Right.Value'Access);
        return Result;
    end "*";

    function "/" (Left, Right : Big_Rational) return Big_Rational is
        Result : Big_Rational;
    begin
        GMP.MPQ.Div (
            Result => Result.Value'Access,
            X => Left.Value'Access,
            Y => Right.Value'Access);
        return Result;
    end "/";

    function "abs" (This : Big_Rational) return Big_Rational is
        Result : Big_Rational;
    begin
        GMP.MPQ.Absolute (Result.Value'Access, This.Value'Access);
        return Result;
    end "abs";

    function Sign (This : Big_Rational) return GMP.Polarity is
        Result : GMP.C_Int;
    begin
        Result := GMP.MPQ.Sign (This.Value'Access);

        if Result > 0 then
            return GMP.Positive;
        elsif Result < 0 then
            return GMP.Negative;
        else
            return GMP.Neutral;
        end if;
    end Sign;

    function Shift_Left (
        This : Big_Rational;
        By : Positive)
        return Big_Rational is
        Result : Big_Rational;
    begin
        GMP.MPQ.Shift_Left (
            Result => Result.Value'Access,
            X => This.Value'Access,
            Y => GMP.Unsigned (By));

        return Result;
    end Shift_Left;

    function Shift_Right (
        This : Big_Rational;
        By : Positive)
        return Big_Rational is
        Result : Big_Rational;
    begin
        GMP.MPQ.Shift_Right (
            Result => Result.Value'Access,
            X => This.Value'Access,
            Y => GMP.Unsigned (By));

        return Result;
    end Shift_Right;

    function Inverse (This : Big_Rational) return Big_Rational is
        Result : Big_Rational;
    begin
        GMP.MPQ.Inverse (Result.Value'Access, This.Value'Access);

        return Result;
    end Inverse;

    function Numerator (From : Big_Rational) return Big_Integers.Big_Integer is
        Result : Big_Integers.Big_Integer;
        R_MPZ : constant access GMP.MPZ.MPZ_T := Result.Value'Access;
    begin
        GMP.MPZ.Set (R_MPZ, From.Value.MP_Num'Access);
        return Result;
    end Numerator;

    function Denominator (From : Big_Rational) return Big_Integers.Big_Integer
    is
        Result : Big_Integers.Big_Integer;
        R_MPZ : constant access GMP.MPZ.MPZ_T := Result.Value'Access;
    begin
        GMP.MPZ.Set (R_MPZ, From.Value.MP_Den'Access);
        return Result;
    end Denominator;

    function Set_Numerator (
        This : Big_Rational;
        To : Big_Integers.Big_Integer)
        return Big_Rational is
        Result : Big_Rational := This;
    begin
        GMP.MPZ.Set (Result.Value.MP_Num'Access, To.Value'Access);

        GMP.MPQ.Canonicalize (Result.Value'Access);
        return Result;
    end Set_Numerator;

    function Set_Denominator (
        This : Big_Rational;
        To : Big_Integers.Big_Integer)
        return Big_Rational is
        Result : Big_Rational := This;
    begin
        GMP.MPZ.Set (Result.Value.MP_Den'Access, To.Value'Access);

        GMP.MPQ.Canonicalize (Result.Value'Access);
        return Result;
    end Set_Denominator;

    function Image (
        This : Big_Rational;
        Base : GMP.Output_Base := 10)
        return String
    is
        use type GMP.Size;

        -- Add 3 for -, / and null terminator
        Digit_Count : constant GMP.Size :=
            GMP.MPZ.Base_Size (This.Value.MP_Num'Access, GMP.C_Int (Base)) +
            GMP.MPZ.Base_Size (This.Value.MP_Den'Access, GMP.C_Int (Base)) + 3;

        Buffer : String (1 .. Integer (Digit_Count) + 2);

        Result : Interfaces.C.Strings.chars_ptr;
    begin
        Result := GMP.MPQ.To_String (
            Memory => Buffer'Address,
            Base => GMP.C_Int (Base),
            This => This.Value'Access);
        return Interfaces.C.Strings.Value (Result);
    end Image;
end Big_Rationals;
