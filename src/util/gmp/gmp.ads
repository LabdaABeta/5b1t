with Interfaces.C;
with Interfaces.C.Strings;
with System;

-- Low-level Bindings for GMP
-- See the GMP manual for descriptions of functions
-- Actual bindings are in child packages
package GMP is
    pragma Preelaborate;
    subtype Unsigned is Interfaces.C.unsigned_long;
    subtype Signed is Interfaces.C.long;
    subtype C_String is Interfaces.C.Strings.chars_ptr;
    subtype C_Int is Interfaces.C.int;
    subtype Size is Interfaces.C.size_t;
    subtype Address is System.Address;
    type Representation_Base is range 0 .. 62;
    type Output_Base is range -36 .. 62;
    type Polarity is (Positive, Neutral, Negative);
end GMP;
