with GMP;
with GMP.MPZ;
with Ada.Finalization;

package Big_Integers is
    pragma Preelaborate;
    type Big_Integer is new Ada.Finalization.Controlled with
        record
            Value : aliased GMP.MPZ.MPZ_T;
        end record;

    Big_Integer_Exception : exception;

    function Create (
        From : String;
        Base : GMP.Representation_Base := 0)
        return Big_Integer;

    function Create (From : Integer) return Big_Integer;

    function "=" (Left, Right : Big_Integer) return Boolean;
    function ">" (Left, Right : Big_Integer) return Boolean;
    function "<" (Left, Right : Big_Integer) return Boolean;
    function ">=" (Left, Right : Big_Integer) return Boolean;
    function "<=" (Left, Right : Big_Integer) return Boolean;
    function "+" (Left, Right : Big_Integer) return Big_Integer;
    function "-" (Left, Right : Big_Integer) return Big_Integer;
    function "-" (This : Big_Integer) return Big_Integer;
    function "*" (Left, Right : Big_Integer) return Big_Integer;
    function "/" (Left, Right : Big_Integer) return Big_Integer;
    function "mod" (Left, Right : Big_Integer) return Big_Integer;
    function "rem" (Left, Right : Big_Integer) return Big_Integer;
    function "**" (Left : Big_Integer; Right : Natural) return Big_Integer;
    function "abs" (This : Big_Integer) return Big_Integer;

    function "=" (Left : Big_Integer; Right : Integer) return Boolean;
    function ">" (Left : Big_Integer; Right : Integer) return Boolean;
    function "<" (Left : Big_Integer; Right : Integer) return Boolean;
    function ">=" (Left : Big_Integer; Right : Integer) return Boolean;
    function "<=" (Left : Big_Integer; Right : Integer) return Boolean;
    function "+" (Left : Big_Integer; Right : Integer) return Big_Integer;
    function "-" (Left : Big_Integer; Right : Integer) return Big_Integer;
    function "*" (Left : Big_Integer; Right : Integer) return Big_Integer;
    function "/" (Left : Big_Integer; Right : Integer) return Big_Integer;
    function "mod" (Left : Big_Integer; Right : Integer) return Big_Integer;
    function "rem" (Left : Big_Integer; Right : Integer) return Big_Integer;

    function Sign (This : Big_Integer) return GMP.Polarity;
    function Shift_Left (This : Big_Integer; By : Positive) return Big_Integer;
    function Shift_Right (This : Big_Integer; By : Positive) return Big_Integer;
    function Is_Divisible_By (This, That : Big_Integer) return Boolean;
    function Is_Shiftable_By (This : Big_Integer; By : Positive) return Boolean;
    function Is_Congruent_To (This, That, Mod_By : Big_Integer) return Boolean;
    function Power_Mod (
        Base, Exponent, Modulus : Big_Integer)
        return Big_Integer;
    function Root (This : Big_Integer; By : Positive) return Big_Integer;
    function Root_Remainder (
        This : Big_Integer;
        By : Positive)
        return Big_Integer;
    function Is_Perfect_Power (This : Big_Integer) return Boolean;
    function Is_Perfect_Square (This : Big_Integer) return Boolean;

    function Is_Prime (This : Big_Integer) return Boolean;
    function Next_Prime (This : Big_Integer) return Big_Integer;
    function GCD (A, B : Big_Integer) return Big_Integer;
    procedure GCDx (A, B : Big_Integer; R, S, T : out Big_Integer);
    function LCM (A, B : Big_Integer) return Big_Integer;
    function Modular_Inverse (A, Mod_B : Big_Integer) return Big_Integer;
    function Jacobi (A, B : Big_Integer) return GMP.Polarity;
    function Legendre (A, B : Big_Integer) return GMP.Polarity;
    function Kronecker (A, B : Big_Integer) return GMP.Polarity;
    function Factorial (From : Natural) return Big_Integer;
    function Double_Factorial (From : Natural) return Big_Integer;
    function Multi_Factorial (From, By : Natural) return Big_Integer;
    function Primorial (From : Natural) return Big_Integer;
    function Binomial (
        From : Big_Integer;
        Choose : Natural)
        return Big_Integer;
    function Fibonacci (From : Natural) return Big_Integer;
    function Lucas (From : Natural) return Big_Integer;

    function Bit_And (A, B : Big_Integer) return Big_Integer;
    function Bit_Or (A, B : Big_Integer) return Big_Integer;
    function Bit_Xor (A, B : Big_Integer) return Big_Integer;
    function Bit_Compliment (B : Big_Integer) return Big_Integer;
    function Population (This : Big_Integer) return Natural;
    function Hamming (A, B : Big_Integer) return Natural;
    function Scan_0 (This : Big_Integer; From : Natural) return Natural;
    function Scan_1 (This : Big_Integer; From : Natural) return Natural;
    procedure Set (This : in out Big_Integer; Bit : Natural);
    procedure Clear (This : in out Big_Integer; Bit : Natural);
    procedure Toggle (This : in out Big_Integer; Bit : Natural);
    function Test (This : Big_Integer; Bit : Natural) return Boolean;

    function Image (
        This : Big_Integer;
        Base : GMP.Output_Base := 10)
        return String;
    function Fits_Integer (This : Big_Integer) return Boolean;
    function As_Integer (This : Big_Integer) return Integer;
private
    procedure Initialize (This : in out Big_Integer);
    procedure Finalize (This : in out Big_Integer);
    procedure Adjust (This : in out Big_Integer);
end Big_Integers;
