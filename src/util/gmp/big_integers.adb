with Interfaces.C.Strings;

package body Big_Integers is
    use type GMP.C_Int;
    use type GMP.Polarity;

    procedure Initialize (This : in out Big_Integer) is
    begin
        GMP.MPZ.Initialize (This.Value'Access);
    end Initialize;

    procedure Finalize (This : in out Big_Integer) is
    begin
        GMP.MPZ.Finalize (This.Value'Access);
    end Finalize;

    procedure Adjust (This : in out Big_Integer) is
        Result : aliased GMP.MPZ.MPZ_T;
    begin
        GMP.MPZ.Initialize (Result'Access);
        GMP.MPZ.Set (Result'Access, This.Value'Access);
        This.Value := Result;
    end Adjust;

    function Create (
        From : String;
        Base : GMP.Representation_Base := 0)
        return Big_Integer
    is
        Result : Big_Integer;
        Error : GMP.C_Int;
        Input : GMP.C_String := Interfaces.C.Strings.New_String (From);
    begin
        Error := GMP.MPZ.Set_String (
            This => Result.Value'Access,
            From => Input,
            Base => GMP.C_Int (Base));

        Interfaces.C.Strings.Free (Input);

        if Error /= 0 then
            raise Big_Integer_Exception;
        end if;

        return Result;
    end Create;

    function Create (From : Integer) return Big_Integer is
        Result : Big_Integer;
    begin
        GMP.MPZ.Set_Signed (Result.Value'Access, GMP.Signed (From));
        return Result;
    end Create;

    function "=" (Left, Right : Big_Integer) return Boolean is
        Result : GMP.C_Int;
    begin
        Result := GMP.MPZ.Compare (Left.Value'Access, Right.Value'Access);
        return Result = 0;
    end "=";

    function ">" (Left, Right : Big_Integer) return Boolean is
        Result : GMP.C_Int;
    begin
        Result := GMP.MPZ.Compare (Left.Value'Access, Right.Value'Access);
        return Result > 0;
    end ">";

    function "<" (Left, Right : Big_Integer) return Boolean is
        Result : GMP.C_Int;
    begin
        Result := GMP.MPZ.Compare (Left.Value'Access, Right.Value'Access);
        return Result < 0;
    end "<";

    function ">=" (Left, Right : Big_Integer) return Boolean is
        Result : GMP.C_Int;
    begin
        Result := GMP.MPZ.Compare (Left.Value'Access, Right.Value'Access);
        return Result >= 0;
    end ">=";

    function "<=" (Left, Right : Big_Integer) return Boolean is
        Result : GMP.C_Int;
    begin
        Result := GMP.MPZ.Compare (Left.Value'Access, Right.Value'Access);
        return Result <= 0;
    end "<=";

    function "+" (Left, Right : Big_Integer) return Big_Integer is
        Result : Big_Integer;
    begin
        GMP.MPZ.Add (
            Result => Result.Value'Access,
            X => Left.Value'Access,
            Y => Right.Value'Access);
        return Result;
    end "+";

    function "-" (Left, Right : Big_Integer) return Big_Integer is
        Result : Big_Integer;
    begin
        GMP.MPZ.Sub (
            Result => Result.Value'Access,
            X => Left.Value'Access,
            Y => Right.Value'Access);
        return Result;
    end "-";

    function "-" (This : Big_Integer) return Big_Integer is
        Result : Big_Integer;
    begin
        GMP.MPZ.Negate (Result.Value'Access, This.Value'Access);
        return Result;
    end "-";

    function "*" (Left, Right : Big_Integer) return Big_Integer is
        Result : Big_Integer;
    begin
        GMP.MPZ.Mul (
            Result => Result.Value'Access,
            X => Left.Value'Access,
            Y => Right.Value'Access);
        return Result;
    end "*";

    function "/" (Left, Right : Big_Integer) return Big_Integer is
        Result : Big_Integer;
    begin
        GMP.MPZ.Quotient_Truncate (
            Result => Result.Value'Access,
            Numerator => Left.Value'Access,
            Denominator => Right.Value'Access);
        return Result;
    end "/";

    function "mod" (Left, Right : Big_Integer) return Big_Integer is
        Result : Big_Integer;
        Num : Big_Integer := abs Left;
        Den : Big_Integer := abs Right;
    begin
        if GMP.MPZ.Compare_Unsigned (Right.Value'Access, 0) = 0 then
            raise Constraint_Error;
        end if;

        if Sign (Left) /= GMP.Negative and Sign (Right) /= GMP.Negative then
            GMP.MPZ.Modulo (
                Result => Result.Value'Access,
                Numerator => Left.Value'Access,
                Denominator => Right.Value'Access);
            return Result;
        end if;

        GMP.MPZ.Absolute (Num.Value'Access, Left.Value'Access);
        GMP.MPZ.Absolute (Den.Value'Access, Right.Value'Access);

        GMP.MPZ.Modulo (
            Result => Result.Value'Access,
            Numerator => Left.Value'Access,
            Denominator => Right.Value'Access);

        if Sign (Left) = GMP.Negative then
            if Sign (Right) = GMP.Negative then
                return -Result;
            else
                return Den - Result;
            end if;
        else
            if Sign (Right) = GMP.Negative then
                return Result - Den;
            else
                return Result;
            end if;
        end if;
    end "mod";

    function "rem" (Left, Right : Big_Integer) return Big_Integer is
        Result : Big_Integer;
    begin
        if GMP.MPZ.Compare_Unsigned (Right.Value'Access, 0) = 0 then
            raise Constraint_Error;
        end if;

        GMP.MPZ.Remainder_Truncate (
            Result => Result.Value'Access,
            Numerator => Left.Value'Access,
            Denominator => Right.Value'Access);

        if Sign (Left) /= Sign (Result) then
            return -Result;
        else
            return Result;
        end if;
    end "rem";

    function "**" (Left : Big_Integer; Right : Natural) return Big_Integer is
        Result : Big_Integer;
    begin
        GMP.MPZ.Power (
            Result => Result.Value'Access,
            Base => Left.Value'Access,
            Exponent => GMP.Unsigned (Right));
        return Result;
    end "**";

    function "abs" (This : Big_Integer) return Big_Integer is
        Result : Big_Integer;
    begin
        GMP.MPZ.Absolute (Result.Value'Access, This.Value'Access);
        return Result;
    end "abs";

    function "=" (Left : Big_Integer; Right : Integer) return Boolean is
    begin
        return GMP.MPZ.Compare_Signed (
            A => Left.Value'Access,
            B => GMP.Signed (Right)) = 0;
    end "=";

    function ">" (Left : Big_Integer; Right : Integer) return Boolean is
    begin
        return GMP.MPZ.Compare_Signed (
            A => Left.Value'Access,
            B => GMP.Signed (Right)) > 0;
    end ">";

    function "<" (Left : Big_Integer; Right : Integer) return Boolean is
    begin
        return GMP.MPZ.Compare_Signed (
            A => Left.Value'Access,
            B => GMP.Signed (Right)) < 0;
    end "<";

    function ">=" (Left : Big_Integer; Right : Integer) return Boolean is
    begin
        return GMP.MPZ.Compare_Signed (
            A => Left.Value'Access,
            B => GMP.Signed (Right)) >= 0;
    end ">=";

    function "<=" (Left : Big_Integer; Right : Integer) return Boolean is
    begin
        return GMP.MPZ.Compare_Signed (
            A => Left.Value'Access,
            B => GMP.Signed (Right)) <= 0;
    end "<=";

    function "+" (Left : Big_Integer; Right : Integer) return Big_Integer is
        Result : Big_Integer;
    begin
        if Right > 0 then
            GMP.MPZ.Add_Unsigned (
                Result => Result.Value'Access,
                X => Left.Value'Access,
                Y => GMP.Unsigned (Right));
        else
            GMP.MPZ.Sub_Unsigned (
                Result => Result.Value'Access,
                X => Left.Value'Access,
                Y => GMP.Unsigned (abs Right));
        end if;

        return Result;
    end "+";

    function "-" (Left : Big_Integer; Right : Integer) return Big_Integer is
        Result : Big_Integer;
    begin
        if Right < 0 then
            GMP.MPZ.Add_Unsigned (
                Result => Result.Value'Access,
                X => Left.Value'Access,
                Y => GMP.Unsigned (abs Right));
        else
            GMP.MPZ.Sub_Unsigned (
                Result => Result.Value'Access,
                X => Left.Value'Access,
                Y => GMP.Unsigned (Right));
        end if;

        return Result;
    end "-";

    function "*" (Left : Big_Integer; Right : Integer) return Big_Integer is
        Result : Big_Integer;
    begin
        GMP.MPZ.Mul_Signed (
            Result => Result.Value'Access,
            X => Left.Value'Access,
            Y => GMP.Signed (Right));
        return Result;
    end "*";

    function "/" (Left : Big_Integer; Right : Integer) return Big_Integer is
        Result : Big_Integer;
        Ignored : GMP.Unsigned;
    begin
        Ignored := GMP.MPZ.Quotient_Truncate_Unsigned (
            Result => Result.Value'Access,
            Numerator => Left.Value'Access,
            Denominator => GMP.Unsigned (abs Right));

        if Right > 0 then
            return Result;
        else
            return -Result;
        end if;
    end "/";

    function "mod" (Left : Big_Integer; Right : Integer) return Big_Integer is
    begin
        return Left mod Create (Right);
    end "mod";

    function "rem" (Left : Big_Integer; Right : Integer) return Big_Integer is
        Result : Big_Integer;
        Ignored : GMP.Unsigned;
    begin
        if Right = 0 then
            raise Constraint_Error;
        end if;

        Ignored := GMP.MPZ.Remainder_Truncate_Unsigned (
            Result => Result.Value'Access,
            Numerator => Left.Value'Access,
            Denominator => GMP.Unsigned (abs Right));

        if Sign (Left) /= Sign (Result) then
            return -Result;
        else
            return Result;
        end if;
    end "rem";

    function Sign (This : Big_Integer) return GMP.Polarity is
        Result : GMP.C_Int;
    begin
        Result := GMP.MPZ.Sign (This.Value'Access);

        if Result > 0 then
            return GMP.Positive;
        elsif Result < 0 then
            return GMP.Negative;
        else
            return GMP.Neutral;
        end if;
    end Sign;

    function Shift_Left (
        This : Big_Integer;
        By : Positive)
        return Big_Integer
    is
        Result : Big_Integer;
    begin
        GMP.MPZ.Shift_Left (
            Result => Result.Value'Access,
            X => This.Value'Access,
            Y => GMP.Unsigned (By));
        return Result;
    end Shift_Left;

    function Shift_Right (
        This : Big_Integer;
        By : Positive)
        return Big_Integer
    is
        Result : Big_Integer;
    begin
        GMP.MPZ.Shift_Right_Truncate (
            Result => Result.Value'Access,
            This => This.Value'Access,
            Shift => GMP.Unsigned (By));
        return Result;
    end Shift_Right;

    function Is_Divisible_By (This, That : Big_Integer) return Boolean is
    begin
        return GMP.MPZ.Is_Divisible_By (
            This => This.Value'Access,
            That => That.Value'Access) /= 0;
    end Is_Divisible_By;

    function Is_Shiftable_By (
        This : Big_Integer;
        By : Positive)
        return Boolean
    is begin
        return GMP.MPZ.Is_Shiftable_By (
            This => This.Value'Access,
            That => GMP.Unsigned (By)) /= 0;
    end Is_Shiftable_By;

    function Is_Congruent_To (
        This, That, Mod_By : Big_Integer)
        return Boolean
    is begin
        return GMP.MPZ.Is_Congruent_To (
            This => This.Value'Access,
            That => That.Value'Access,
            Mod_D => Mod_By.Value'Access) /= 0;
    end Is_Congruent_To;

    function Power_Mod (
        Base, Exponent, Modulus : Big_Integer)
        return Big_Integer
    is
        Result : Big_Integer;
    begin
        GMP.MPZ.To_The_Power_Of (
            Result => Result.Value'Access,
            Base => Base.Value'Access,
            Exponent => Exponent.Value'Access,
            Modulus => Modulus.Value'Access);
        return Result;
    end Power_Mod;

    function Root (This : Big_Integer; By : Positive) return Big_Integer is
        Result : Big_Integer;
        Ignored : GMP.C_Int;
    begin
        Ignored := GMP.MPZ.Root (
            Result => Result.Value'Access,
            From => This.Value'Access,
            By => GMP.Unsigned (By));
        return Result;
    end Root;

    function Root_Remainder (
        This : Big_Integer;
        By : Positive)
        return Big_Integer
    is
        Result : Big_Integer;
        Ignored : Big_Integer;
    begin
        GMP.MPZ.Root_Remainder (
            Root_Result => Ignored.Value'Access,
            Remainder_Result => Result.Value'Access,
            From => This.Value'Access,
            By => GMP.Unsigned (By));
        return Result;
    end Root_Remainder;

    function Is_Perfect_Power (This : Big_Integer) return Boolean is
    begin
        return GMP.MPZ.Is_Perfect_Power (This.Value'Access) /= 0;
    end Is_Perfect_Power;

    function Is_Perfect_Square (This : Big_Integer) return Boolean is
    begin
        return GMP.MPZ.Is_Perfect_Square (This.Value'Access) /= 0;
    end Is_Perfect_Square;

    function Is_Prime (This : Big_Integer) return Boolean is
    begin
        return GMP.MPZ.Is_Probably_Prime (This.Value'Access, 26) /= 0;
    end Is_Prime;

    function Next_Prime (This : Big_Integer) return Big_Integer is
        Result : Big_Integer;
    begin
        GMP.MPZ.Next_Prime (Result.Value'Access, This.Value'Access);
        return Result;
    end Next_Prime;

    function GCD (A, B : Big_Integer) return Big_Integer is
        Result : Big_Integer;
    begin
        GMP.MPZ.GCD (Result.Value'Access, A.Value'Access, B.Value'Access);
        return Result;
    end GCD;

    procedure GCDx (A, B : Big_Integer; R, S, T : out Big_Integer) is
    begin
        GMP.MPZ.GCD_Extended (
            Result => R.Value'Access,
            S => S.Value'Access,
            T => T.Value'Access,
            A => A.Value'Access,
            B => B.Value'Access);
    end GCDx;

    function LCM (A, B : Big_Integer) return Big_Integer is
        Result : Big_Integer;
    begin
        GMP.MPZ.LCM (Result.Value'Access, A.Value'Access, B.Value'Access);
        return Result;
    end LCM;

    function Modular_Inverse (A, Mod_B : Big_Integer) return Big_Integer is
        Result : Big_Integer;
    begin
        if GMP.MPZ.Invert (
            Result => Result.Value'Access,
            A => A.Value'Access,
            Mod_B => Mod_B.Value'Access) = 0
        then
            raise Big_Integer_Exception;
        end if;

        return Result;
    end Modular_Inverse;

    function Jacobi (A, B : Big_Integer) return GMP.Polarity is
        Result : GMP.C_Int;
    begin
        Result := GMP.MPZ.Jacobi (A.Value'Access, B.Value'Access);

        if Result > 0 then
            return GMP.Positive;
        elsif Result < 0 then
            return GMP.Negative;
        else
            return GMP.Neutral;
        end if;
    end Jacobi;

    function Legendre (A, B : Big_Integer) return GMP.Polarity is
        Result : GMP.C_Int;
    begin
        Result := GMP.MPZ.Legendre (A.Value'Access, B.Value'Access);

        if Result > 0 then
            return GMP.Positive;
        elsif Result < 0 then
            return GMP.Negative;
        else
            return GMP.Neutral;
        end if;
    end Legendre;

    function Kronecker (A, B : Big_Integer) return GMP.Polarity is
        Result : GMP.C_Int;
    begin
        Result := GMP.MPZ.Kronecker (A.Value'Access, B.Value'Access);

        if Result > 0 then
            return GMP.Positive;
        elsif Result < 0 then
            return GMP.Negative;
        else
            return GMP.Neutral;
        end if;
    end Kronecker;

    function Factorial (From : Natural) return Big_Integer is
        Result : Big_Integer;
    begin
        GMP.MPZ.Factorial (Result.Value'Access, GMP.Unsigned (From));
        return Result;
    end Factorial;

    function Double_Factorial (From : Natural) return Big_Integer is
        Result : Big_Integer;
    begin
        GMP.MPZ.Double_Factorial (Result.Value'Access, GMP.Unsigned (From));
        return Result;
    end Double_Factorial;

    function Multi_Factorial (From, By : Natural) return Big_Integer is
        Result : Big_Integer;
    begin
        GMP.MPZ.Multi_Factorial (
            Result => Result.Value'Access,
            From => GMP.Unsigned (From),
            By => GMP.Unsigned (By));
        return Result;
    end Multi_Factorial;

    function Primorial (From : Natural) return Big_Integer is
        Result : Big_Integer;
    begin
        GMP.MPZ.Primorial (Result.Value'Access, GMP.Unsigned (From));
        return Result;
    end Primorial;

    function Binomial (
        From : Big_Integer;
        Choose : Natural)
        return Big_Integer
    is
        Result : Big_Integer;
    begin
        GMP.MPZ.Binomial (
            Result => Result.Value'Access,
            From => From.Value'Access,
            Choose => GMP.Unsigned (Choose));
        return Result;
    end Binomial;

    function Fibonacci (From : Natural) return Big_Integer is
        Result : Big_Integer;
    begin
        GMP.MPZ.Fibonacci (Result.Value'Access, GMP.Unsigned (From));
        return Result;
    end Fibonacci;

    function Lucas (From : Natural) return Big_Integer is
        Result : Big_Integer;
    begin
        GMP.MPZ.Lucas (Result.Value'Access, GMP.Unsigned (From));
        return Result;
    end Lucas;

    function Bit_And (A, B : Big_Integer) return Big_Integer is
        Result : Big_Integer;
    begin
        GMP.MPZ.Bit_And (Result.Value'Access, A.Value'Access, B.Value'Access);
        return Result;
    end Bit_And;

    function Bit_Or (A, B : Big_Integer) return Big_Integer is
        Result : Big_Integer;
    begin
        GMP.MPZ.Bit_Or (Result.Value'Access, A.Value'Access, B.Value'Access);
        return Result;
    end Bit_Or;

    function Bit_Xor (A, B : Big_Integer) return Big_Integer is
        Result : Big_Integer;
    begin
        GMP.MPZ.Bit_Xor (Result.Value'Access, A.Value'Access, B.Value'Access);
        return Result;
    end Bit_Xor;

    function Bit_Compliment (B : Big_Integer) return Big_Integer is
        Result : Big_Integer;
    begin
        GMP.MPZ.Bit_Compliment (Result.Value'Access, B.Value'Access);
        return Result;
    end Bit_Compliment;

    function Population (This : Big_Integer) return Natural is
    begin
        return Natural (GMP.MPZ.Population (This.Value'Access));
    end Population;

    function Hamming (A, B : Big_Integer) return Natural is
    begin
        return Natural (GMP.MPZ.Hamming (A.Value'Access, B.Value'Access));
    end Hamming;

    function Scan_0 (This : Big_Integer; From : Natural) return Natural is
    begin
        return Natural (
            GMP.MPZ.Scan_0 (This.Value'Access, GMP.Unsigned (From)));
    end Scan_0;

    function Scan_1 (This : Big_Integer; From : Natural) return Natural is
    begin
        return Natural (
            GMP.MPZ.Scan_1 (This.Value'Access, GMP.Unsigned (From)));
    end Scan_1;

    procedure Set (This : in out Big_Integer; Bit : Natural) is
    begin
        GMP.MPZ.Set_Bit (This.Value'Access, GMP.Unsigned (Bit));
    end Set;

    procedure Clear (This : in out Big_Integer; Bit : Natural) is
    begin
        GMP.MPZ.Clear_Bit (This.Value'Access, GMP.Unsigned (Bit));
    end Clear;

    procedure Toggle (This : in out Big_Integer; Bit : Natural) is
    begin
        GMP.MPZ.Toggle_Bit (This.Value'Access, GMP.Unsigned (Bit));
    end Toggle;

    function Test (This : Big_Integer; Bit : Natural) return Boolean is
    begin
        return GMP.MPZ.Test_Bit (This.Value'Access, GMP.Unsigned (Bit)) /= 0;
    end Test;

    function Image (
        This : Big_Integer;
        Base : GMP.Output_Base := 10)
        return String
    is
        Digit_Count : constant GMP.Size :=
            GMP.MPZ.Base_Size (This.Value'Access, GMP.C_Int (Base));

        Buffer : String (1 .. Integer (Digit_Count) + 2);

        Result : Interfaces.C.Strings.chars_ptr;
    begin
        Result := GMP.MPZ.To_String (
            Memory => Buffer'Address,
            Base => GMP.C_Int (Base),
            This => This.Value'Access);
        return Interfaces.C.Strings.Value (Result);
    end Image;

    function Fits_Integer (This : Big_Integer) return Boolean is
    begin
        return This <= Create (Integer'Last) and This >= Create (Integer'First);
    end Fits_Integer;

    function As_Integer (This : Big_Integer) return Integer is
    begin
        return Integer (GMP.MPZ.To_Signed (This.Value'Access));
    end As_Integer;
end Big_Integers;
