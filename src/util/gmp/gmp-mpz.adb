package body GMP.MPZ is

    -- Not exposed as a symbol, implementation copied from source
    function Sign (Item : access constant MPZ_T) return C_Int is
        use type Interfaces.C.int;
    begin
        if Item.MP_Size < 0 then
            return C_Int (-1);
        elsif Item.MP_Size > 0 then
            return C_Int (1);
        else
            return C_Int (0);
        end if;
    end Sign;
end GMP.MPZ;
