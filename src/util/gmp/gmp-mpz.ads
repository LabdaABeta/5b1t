with System;

package GMP.MPZ is
    pragma Preelaborate;
    type MPZ_T is private;

    -- Initializing
    procedure Initialize (This : access MPZ_T);
    procedure Finalize (This : access MPZ_T);

    -- Assigning
    procedure Set (This : access MPZ_T; From : access constant MPZ_T);
    procedure Set_Unsigned (This : access MPZ_T; From : Unsigned);
    procedure Set_Signed (This : access MPZ_T; From : Signed);
    function Set_String (
        This : access MPZ_T;
        From : C_String;
        Base : C_Int)
        return C_Int;
    procedure Swap (This, Other : access MPZ_T);

    -- Init and Assign
    procedure Init_Set (This : access MPZ_T; From : access constant MPZ_T);
    procedure Init_Unsigned (This : access MPZ_T; From : Unsigned);
    procedure Init_Signed (This : access MPZ_T; From : Signed);
    function Init_String (
        This : access MPZ_T;
        From : C_String;
        Base : C_Int)
        return C_Int;

    -- Converting
    function To_Unsigned (From : access constant MPZ_T) return Unsigned;
    function To_Signed (From : access constant MPZ_T) return Signed;
    function To_String (
        Memory : System.Address;
        Base : C_Int;
        This : access constant MPZ_T)
        return C_String;

    -- Arithmetic
    procedure Add (Result : access MPZ_T; X, Y : access constant MPZ_T);
    procedure Add_Unsigned (
        Result : access MPZ_T;
        X : access constant MPZ_T;
        Y : Unsigned);
    procedure Sub (Result : access MPZ_T; X, Y : access constant MPZ_T);
    procedure Sub_Unsigned (
        Result : access MPZ_T;
        X : access constant MPZ_T;
        Y : Unsigned);
    procedure Unsigned_Sub (
        Result : access MPZ_T;
        X : Unsigned;
        Y : access constant MPZ_T);
    procedure Mul (Result : access MPZ_T; X, Y : access constant MPZ_T);
    procedure Mul_Unsigned (
        Result : access MPZ_T;
        X : access constant MPZ_T;
        Y : Unsigned);
    procedure Mul_Signed (
        Result : access MPZ_T;
        X : access constant MPZ_T;
        Y : Signed);
    procedure Add_Mul (Result : access MPZ_T; X, Y : access constant MPZ_T);
    procedure Add_Mul_Unsigned (
        Result : access MPZ_T;
        X : access constant MPZ_T;
        Y : Unsigned);
    procedure Sub_Mul (Result : access MPZ_T; X, Y : access constant MPZ_T);
    procedure Sub_Mul_Unsigned (
        Result : access MPZ_T;
        X : access constant MPZ_T;
        Y : Unsigned);
    procedure Shift_Left (
        Result : access MPZ_T;
        X : access constant MPZ_T;
        Y : Unsigned);
    procedure Negate (Result : access MPZ_T; This : access constant MPZ_T);
    procedure Absolute (Result : access MPZ_T; This : access constant MPZ_T);

    -- Division
    procedure Quotient_Ceiling (
        Result : access MPZ_T;
        Numerator : access constant MPZ_T;
        Denominator : access constant MPZ_T);
    procedure Remainder_Ceiling (
        Result : access MPZ_T;
        Numerator : access constant MPZ_T;
        Denominator : access constant MPZ_T);
    procedure Divide_Ceiling (
        Quotient : access MPZ_T;
        Remainder : access MPZ_T;
        Numerator : access constant MPZ_T;
        Denominator : access constant MPZ_T);
    function Quotient_Ceiling_Unsigned (
        Result : access MPZ_T;
        Numerator : access constant MPZ_T;
        Denominator : Unsigned)
        return Unsigned;
    function Remainder_Ceiling_Unsigned (
        Result : access MPZ_T;
        Numerator : access constant MPZ_T;
        Denominator : Unsigned)
        return Unsigned;
    function Divide_Ceiling_Unsigned (
        Quotient : access MPZ_T;
        Remainder : access MPZ_T;
        Numerator : access constant MPZ_T;
        Denominator : Unsigned)
        return Unsigned;
    function Unsigned_Divide_Ceiling (
        Numerator : access constant MPZ_T;
        Denominator : Unsigned)
        return Unsigned;
    procedure Shift_Right_Ceiling (
        Result : access MPZ_T;
        This : access constant MPZ_T;
        Shift : Unsigned);
    procedure Shift_Right_Remainder_Ceiling (
        Result : access MPZ_T;
        This : access constant MPZ_T;
        Shift : Unsigned);
    procedure Quotient_Floor (
        Result : access MPZ_T;
        Numerator : access constant MPZ_T;
        Denominator : access constant MPZ_T);
    procedure Remainder_Floor (
        Result : access MPZ_T;
        Numerator : access constant MPZ_T;
        Denominator : access constant MPZ_T);
    procedure Divide_Floor (
        Quotient : access MPZ_T;
        Remainder : access MPZ_T;
        Numerator : access constant MPZ_T;
        Denominator : access constant MPZ_T);
    function Quotient_Floor_Unsigned (
        Result : access MPZ_T;
        Numerator : access constant MPZ_T;
        Denominator : Unsigned)
        return Unsigned;
    function Remainder_Floor_Unsigned (
        Result : access MPZ_T;
        Numerator : access constant MPZ_T;
        Denominator : Unsigned)
        return Unsigned;
    function Divide_Floor_Unsigned (
        Quotient : access MPZ_T;
        Remainder : access MPZ_T;
        Numerator : access constant MPZ_T;
        Denominator : Unsigned)
        return Unsigned;
    function Unsigned_Divide_Floor (
        Numerator : access constant MPZ_T;
        Denominator : Unsigned)
        return Unsigned;
    procedure Shift_Right_Floor (
        Result : access MPZ_T;
        This : access constant MPZ_T;
        Shift : Unsigned);
    procedure Shift_Right_Remainder_Floor (
        Result : access MPZ_T;
        This : access constant MPZ_T;
        Shift : Unsigned);
    procedure Quotient_Truncate (
        Result : access MPZ_T;
        Numerator : access constant MPZ_T;
        Denominator : access constant MPZ_T);
    procedure Remainder_Truncate (
        Result : access MPZ_T;
        Numerator : access constant MPZ_T;
        Denominator : access constant MPZ_T);
    procedure Divide_Truncate (
        Quotient : access MPZ_T;
        Remainder : access MPZ_T;
        Numerator : access constant MPZ_T;
        Denominator : access constant MPZ_T);
    function Quotient_Truncate_Unsigned (
        Result : access MPZ_T;
        Numerator : access constant MPZ_T;
        Denominator : Unsigned)
        return Unsigned;
    function Remainder_Truncate_Unsigned (
        Result : access MPZ_T;
        Numerator : access constant MPZ_T;
        Denominator : Unsigned)
        return Unsigned;
    function Divide_Truncate_Unsigned (
        Quotient : access MPZ_T;
        Remainder : access MPZ_T;
        Numerator : access constant MPZ_T;
        Denominator : Unsigned)
        return Unsigned;
    function Unsigned_Divide_Truncate (
        Numerator : access constant MPZ_T;
        Denominator : Unsigned)
        return Unsigned;
    procedure Shift_Right_Truncate (
        Result : access MPZ_T;
        This : access constant MPZ_T;
        Shift : Unsigned);
    procedure Shift_Right_Remainder_Truncate (
        Result : access MPZ_T;
        This : access constant MPZ_T;
        Shift : Unsigned);
    procedure Modulo (
        Result : access MPZ_T;
        Numerator : access constant MPZ_T;
        Denominator : access constant MPZ_T);
    procedure Divide_Exactly (
        Result : access MPZ_T;
        Numerator : access constant MPZ_T;
        Denominator : access constant MPZ_T);
    procedure Divide_Unsigned_Exactly (
        Result : access MPZ_T;
        Numerator : access constant MPZ_T;
        Denominator : Unsigned);
    function Is_Divisible_By (This, That : access constant MPZ_T) return C_Int;
    function Is_Divisible_By_Unsigned (
        This : access constant MPZ_T;
        That : Unsigned)
        return C_Int;
    function Is_Shiftable_By (
        This : access constant MPZ_T;
        That : Unsigned)
        return C_Int;
    function Is_Congruent_To (
        This, That, Mod_D : access constant MPZ_T)
        return C_Int;
    function Is_Congruent_To_Unsigned (
        This : access constant MPZ_T;
        That, Mod_D : Unsigned)
        return C_Int;
    function Is_Shift_Congruent_To (
        This, That : access constant MPZ_T;
        Mod_2B : Unsigned)
        return C_Int;

    -- Exponentiation
    procedure To_The_Power_Of (
        Result : access MPZ_T;
        Base, Exponent, Modulus : access constant MPZ_T);
    procedure To_The_Power_Of_Unsigned (
        Result : access MPZ_T;
        Base : access constant MPZ_T;
        Exponent : Unsigned;
        Modulus : access constant MPZ_T);
    procedure To_The_Power_Of_Side_Channel_Safely (
        Result : access MPZ_T;
        Base, Exponent, Modulus : access constant MPZ_T);
    procedure Power (
        Result : access MPZ_T;
        Base : access constant MPZ_T;
        Exponent : Unsigned);
    procedure Power_Unsigned (
        Result : access MPZ_T;
        Base, Exponent : Unsigned);

    -- Roots
    function Root (
        Result : access MPZ_T;
        From : access constant MPZ_T;
        By : Unsigned)
        return C_Int;
    procedure Root_Remainder (
        Root_Result : access MPZ_T;
        Remainder_Result : access MPZ_T;
        From : access constant MPZ_T;
        By : Unsigned);
    function Is_Perfect_Power (Item : access constant MPZ_T) return C_Int;
    function Is_Perfect_Square (Item : access constant MPZ_T) return C_Int;

    -- Number Theoretic
    function Is_Probably_Prime (
        Item : access constant MPZ_T;
        Reps : C_Int)
        return C_Int;
    procedure Next_Prime (Result : access MPZ_T; After : access constant MPZ_T);
    procedure GCD (Result : access MPZ_T; A, B : access constant MPZ_T);
    function GCD_Unsigned (
        Result : access MPZ_T;
        A : access constant MPZ_T;
        B : Unsigned)
        return Unsigned;
    procedure GCD_Extended (
        Result, S, T : access MPZ_T;
        A, B : access constant MPZ_T);
    procedure LCM (Result : access MPZ_T; A, B : access constant MPZ_T);
    procedure LCM_Unsigned (
        Result : access MPZ_T;
        A : access constant MPZ_T;
        B : Unsigned);
    function Invert (
        Result : access MPZ_T;
        A, Mod_B : access constant MPZ_T)
        return C_Int;
    function Jacobi (A, B : access constant MPZ_T) return C_Int;
    function Legendre (A, B : access constant MPZ_T) return C_Int;
    function Kronecker (A, B : access constant MPZ_T) return C_Int;
    function Kronecker_Signed (
        A : access constant MPZ_T;
        B : Signed)
        return C_Int;
    function Kronecker_Unsigned (
        A : access constant MPZ_T;
        B : Unsigned)
        return C_Int;
    function Signed_Kronecker (
        A : Signed;
        B : access constant MPZ_T)
        return C_Int;
    function Unsigned_Kronecker (
        A : Unsigned;
        B : access constant MPZ_T)
        return C_Int;
    function Remove (
        Result : access MPZ_T;
        From, Factor : access constant MPZ_T)
        return Unsigned;
    procedure Factorial (Result : access MPZ_T; From : Unsigned);
    procedure Double_Factorial (Result : access MPZ_T; From : Unsigned);
    procedure Multi_Factorial (Result : access MPZ_T; From, By : Unsigned);
    procedure Primorial (Result : access MPZ_T; From : Unsigned);
    procedure Binomial (
        Result : access MPZ_T;
        From : access constant MPZ_T;
        Choose : Unsigned);
    procedure Binomial_Unsigned (
        Result : access MPZ_T;
        From, Choose : Unsigned);
    procedure Fibonacci (Result : access MPZ_T; From : Unsigned);
    procedure Fibonacci_Pair (Result, Prev : access MPZ_T; From : Unsigned);
    procedure Lucas (Result : access MPZ_T; From : Unsigned);
    procedure Lucas_Pair (Result, Prev : access MPZ_T; From : Unsigned);

    -- Comparisons
    function Compare (A, B : access constant MPZ_T) return C_Int;
    function Compare_Signed (
        A : access constant MPZ_T;
        B : Signed)
        return C_Int;
    function Compare_Unsigned (
        A : access constant MPZ_T;
        B : Unsigned)
        return C_Int;
    function Compare_Abs (A, B : access constant MPZ_T) return C_Int;
    function Compare_Abs_Unsigned (
        A : access constant MPZ_T;
        B : Unsigned)
        return C_Int;
    function Sign (Item : access constant MPZ_T) return C_Int;

    -- Logic and Bit Fiddling
    procedure Bit_And (Result : access MPZ_T; A, B : access constant MPZ_T);
    procedure Bit_Or (Result : access MPZ_T; A, B : access constant MPZ_T);
    procedure Bit_Xor (Result : access MPZ_T; A, B : access constant MPZ_T);
    procedure Bit_Compliment (Result : access MPZ_T; X : access constant MPZ_T);
    function Population (Item : access constant MPZ_T) return Unsigned;
    function Hamming (A, B : access constant MPZ_T) return Unsigned;
    function Scan_0 (A : access constant MPZ_T; S : Unsigned) return Unsigned;
    function Scan_1 (A : access constant MPZ_T; S : Unsigned) return Unsigned;
    procedure Set_Bit (This : access MPZ_T; Bit : Unsigned);
    procedure Clear_Bit (This : access MPZ_T; Bit : Unsigned);
    procedure Toggle_Bit (This : access MPZ_T; Bit : Unsigned);
    function Test_Bit (
        This : access constant MPZ_T;
        Bit : Unsigned)
        return C_Int;

    -- Import/Export
    procedure Import (
        Result : access MPZ_T;
        Count : Size;
        Order : C_Int;
        C_Size : Size;
        Endian : C_Int;
        Nails : Size;
        Op : Address);
    function Export (
        Result : Address;
        Count : access Size;
        Order : C_Int;
        C_Size : Size;
        Endian : C_Int;
        Nails : Size;
        Op : access constant MPZ_T)
        return Address;

    -- Miscellaneous functions
    function Base_Size (This : access constant MPZ_T; Base : C_Int) return Size;

    pragma Import (C, Initialize, "__gmpz_init");
    pragma Import (C, Finalize, "__gmpz_clear");
    pragma Import (C, Set, "__gmpz_set");
    pragma Import (C, Set_Unsigned, "__gmpz_set_ui");
    pragma Import (C, Set_Signed, "__gmpz_set_si");
    pragma Import (C, Set_String, "__gmpz_set_str");
    pragma Import (C, Swap, "__gmpz_swap");
    pragma Import (C, Init_Set, "__gmpz_init_set");
    pragma Import (C, Init_Unsigned, "__gmpz_init_set_ui");
    pragma Import (C, Init_Signed, "__gmpz_init_set_si");
    pragma Import (C, Init_String, "__gmpz_init_set_str");
    pragma Import (C, To_Unsigned, "__gmpz_get_ui");
    pragma Import (C, To_Signed, "__gmpz_get_si");
    pragma Import (C, To_String, "__gmpz_get_str");
    pragma Import (C, Add, "__gmpz_add");
    pragma Import (C, Add_Unsigned, "__gmpz_add_ui");
    pragma Import (C, Sub, "__gmpz_sub");
    pragma Import (C, Sub_Unsigned, "__gmpz_sub_ui");
    pragma Import (C, Unsigned_Sub, "__gmpz_ui_sub");
    pragma Import (C, Mul, "__gmpz_mul");
    pragma Import (C, Mul_Unsigned, "__gmpz_mul_ui");
    pragma Import (C, Mul_Signed, "__gmpz_mul_si");
    pragma Import (C, Add_Mul, "__gmpz_addmul");
    pragma Import (C, Add_Mul_Unsigned, "__gmpz_addmul_ui");
    pragma Import (C, Sub_Mul, "__gmpz_submul");
    pragma Import (C, Sub_Mul_Unsigned, "__gmpz_submul_ui");
    pragma Import (C, Shift_Left, "__gmpz_mul_2exp");
    pragma Import (C, Negate, "__gmpz_neg");
    pragma Import (C, Absolute, "__gmpz_abs");
    pragma Import (C, Quotient_Ceiling, "__gmpz_cdiv_q");
    pragma Import (C, Remainder_Ceiling, "__gmpz_cdiv_r");
    pragma Import (C, Divide_Ceiling, "__gmpz_cdiv_qr");
    pragma Import (C, Quotient_Ceiling_Unsigned, "__gmpz_cdiv_q_ui");
    pragma Import (C, Remainder_Ceiling_Unsigned, "__gmpz_cdiv_r_ui");
    pragma Import (C, Divide_Ceiling_Unsigned, "__gmpz_cdiv_qr_ui");
    pragma Import (C, Unsigned_Divide_Ceiling, "__gmpz_cdiv_ui");
    pragma Import (C, Shift_Right_Ceiling, "__gmpz_cdiv_q_2exp");
    pragma Import (C, Shift_Right_Remainder_Ceiling, "__gmpz_cdiv_r_2exp");
    pragma Import (C, Quotient_Floor, "__gmpz_fdiv_q");
    pragma Import (C, Remainder_Floor, "__gmpz_fdiv_r");
    pragma Import (C, Divide_Floor, "__gmpz_fdiv_qr");
    pragma Import (C, Quotient_Floor_Unsigned, "__gmpz_fdiv_q_ui");
    pragma Import (C, Remainder_Floor_Unsigned, "__gmpz_fdiv_r_ui");
    pragma Import (C, Divide_Floor_Unsigned, "__gmpz_fdiv_qr_ui");
    pragma Import (C, Unsigned_Divide_Floor, "__gmpz_fdiv_ui");
    pragma Import (C, Shift_Right_Floor, "__gmpz_fdiv_q_2exp");
    pragma Import (C, Shift_Right_Remainder_Floor, "__gmpz_fdiv_r_2exp");
    pragma Import (C, Quotient_Truncate, "__gmpz_tdiv_q");
    pragma Import (C, Remainder_Truncate, "__gmpz_tdiv_r");
    pragma Import (C, Divide_Truncate, "__gmpz_tdiv_qr");
    pragma Import (C, Quotient_Truncate_Unsigned, "__gmpz_tdiv_q_ui");
    pragma Import (C, Remainder_Truncate_Unsigned, "__gmpz_tdiv_r_ui");
    pragma Import (C, Divide_Truncate_Unsigned, "__gmpz_tdiv_qr_ui");
    pragma Import (C, Unsigned_Divide_Truncate, "__gmpz_tdiv_ui");
    pragma Import (C, Shift_Right_Truncate, "__gmpz_tdiv_q_2exp");
    pragma Import (C, Shift_Right_Remainder_Truncate, "__gmpz_tdiv_r_2exp");
    pragma Import (C, Modulo, "__gmpz_mod");
    pragma Import (C, Divide_Exactly, "__gmpz_divexact");
    pragma Import (C, Divide_Unsigned_Exactly, "__gmpz_divexact_ui");
    pragma Import (C, Is_Divisible_By, "__gmpz_divisible_p");
    pragma Import (C, Is_Divisible_By_Unsigned, "__gmpz_divisible_ui_p");
    pragma Import (C, Is_Shiftable_By, "__gmpz_divisible_2exp_p");
    pragma Import (C, Is_Congruent_To, "__gmpz_congruent_p");
    pragma Import (C, Is_Congruent_To_Unsigned, "__gmpz_congruent_ui_p");
    pragma Import (C, Is_Shift_Congruent_To, "__gmpz_congruent_2exp_p");
    pragma Import (C, To_The_Power_Of, "__gmpz_powm");
    pragma Import (C, To_The_Power_Of_Unsigned, "__gmpz_powm_ui");
    pragma Import (C, To_The_Power_Of_Side_Channel_Safely, "__gmpz_powm_sec");
    pragma Import (C, Power, "__gmpz_pow_ui");
    pragma Import (C, Power_Unsigned, "__gmpz_ui_pow_ui");
    pragma Import (C, Root, "__gmpz_root");
    pragma Import (C, Root_Remainder, "__gmpz_rootrem");
    pragma Import (C, Is_Perfect_Power, "__gmpz_perfect_power_p");
    pragma Import (C, Is_Perfect_Square, "__gmpz_perfect_square_p");
    pragma Import (C, Is_Probably_Prime, "__gmpz_probab_prime_p");
    pragma Import (C, Next_Prime, "__gmpz_nextprime");
    pragma Import (C, GCD, "__gmpz_gcd");
    pragma Import (C, GCD_Unsigned, "__gmpz_gcd_ui");
    pragma Import (C, GCD_Extended, "__gmpz_gcdext");
    pragma Import (C, LCM, "__gmpz_lcm");
    pragma Import (C, LCM_Unsigned, "__gmpz_lcm_ui");
    pragma Import (C, Invert, "__gmpz_invert");
    pragma Import (C, Jacobi, "__gmpz_jacobi");
    pragma Import (C, Legendre, "__gmpz_jacobi");
    pragma Import (C, Kronecker, "__gmpz_jacobi");
    pragma Import (C, Kronecker_Signed, "__gmpz_kronecker_si");
    pragma Import (C, Kronecker_Unsigned, "__gmpz_kronecker_ui");
    pragma Import (C, Signed_Kronecker, "__gmpz_si_kronecker");
    pragma Import (C, Unsigned_Kronecker, "__gmpz_ui_kronecker");
    pragma Import (C, Remove, "__gmpz_remove");
    pragma Import (C, Factorial, "__gmpz_fac_ui");
    pragma Import (C, Double_Factorial, "__gmpz_2fac_ui");
    pragma Import (C, Multi_Factorial, "__gmpz_mfac_uiui");
    pragma Import (C, Primorial, "__gmpz_primorial_ui");
    pragma Import (C, Binomial, "__gmpz_bin_ui");
    pragma Import (C, Binomial_Unsigned, "__gmpz_bin_uiui");
    pragma Import (C, Fibonacci, "__gmpz_fib_ui");
    pragma Import (C, Fibonacci_Pair, "__gmpz_fib2_ui");
    pragma Import (C, Lucas, "__gmpz_lucnum_ui");
    pragma Import (C, Lucas_Pair, "__gpmz_lucnum2_ui");
    pragma Import (C, Compare, "__gmpz_cmp");
    pragma Import (C, Compare_Signed, "__gmpz_cmp_si");
    pragma Import (C, Compare_Unsigned, "__gmpz_cmp_ui");
    pragma Import (C, Compare_Abs, "__gmpz_cmpabs");
    pragma Import (C, Compare_Abs_Unsigned, "__gmpz_cmpabs_ui");
    pragma Import (C, Bit_And, "__gmpz_and");
    pragma Import (C, Bit_Or, "__gmpz_ior");
    pragma Import (C, Bit_Xor, "__gmpz_xor");
    pragma Import (C, Bit_Compliment, "__gmpz_com");
    pragma Import (C, Population, "__gmpz_popcount");
    pragma Import (C, Hamming, "__gmpz_hamdist");
    pragma Import (C, Scan_0, "__gmpz_scan0");
    pragma Import (C, Scan_1, "__gmpz_scan1");
    pragma Import (C, Set_Bit, "__gmpz_setbit");
    pragma Import (C, Clear_Bit, "__gmpz_clrbit");
    pragma Import (C, Toggle_Bit, "__gmpz_combit");
    pragma Import (C, Test_Bit, "__gmpz_tstbit");
    pragma Import (C, Import, "__gmpz_import");
    pragma Import (C, Export, "__gmpz_export");
    pragma Import (C, Base_Size, "__gmpz_sizeinbase");
private
    -- Determined from gmp sources
    type MPZ_T is record
        MP_Alloc : Interfaces.C.int;
        MP_Size : Interfaces.C.int;
        MP_D : System.Address;
    end record;

    pragma Convention (C, MPZ_T);
end GMP.MPZ;
