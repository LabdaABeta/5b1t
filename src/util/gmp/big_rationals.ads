with GMP;
with GMP.MPQ;
with Big_Integers;
with Ada.Finalization;

package Big_Rationals is
    type Big_Rational is tagged private;

    Big_Rational_Exception : exception;

    -- Accepts A/B A.B or A,B formats (but not A/B.C or similar)
    function Create (
        From : String;
        Base : GMP.Representation_Base := 0)
        return Big_Rational;

    function Create (From : Big_Integers.Big_Integer) return Big_Rational;

    function Create (Num, Den : Big_Integers.Big_Integer) return Big_Rational;

    function "=" (Left, Right : Big_Rational) return Boolean;
    function ">" (Left, Right : Big_Rational) return Boolean;
    function "<" (Left, Right : Big_Rational) return Boolean;
    function ">=" (Left, Right : Big_Rational) return Boolean;
    function "<=" (Left, Right : Big_Rational) return Boolean;
    function "+" (Left, Right : Big_Rational) return Big_Rational;
    function "-" (Left, Right : Big_Rational) return Big_Rational;
    function "-" (This : Big_Rational) return Big_Rational;
    function "*" (Left, Right : Big_Rational) return Big_Rational;
    function "/" (Left, Right : Big_Rational) return Big_Rational;
    function "abs" (This : Big_Rational) return Big_Rational;

    function Sign (This : Big_Rational) return GMP.Polarity;
    function Shift_Left (
        This : Big_Rational;
        By : Positive)
        return Big_Rational;
    function Shift_Right (
        This : Big_Rational;
        By : Positive)
        return Big_Rational;
    function Inverse (This : Big_Rational) return Big_Rational;

    function Numerator (From : Big_Rational) return Big_Integers.Big_Integer;
    function Denominator (From : Big_Rational) return Big_Integers.Big_Integer;
    function Set_Numerator (
        This : Big_Rational;
        To : Big_Integers.Big_Integer)
        return Big_Rational;
    function Set_Denominator (
        This : Big_Rational;
        To : Big_Integers.Big_Integer)
        return Big_Rational;

    function Image (
        This : Big_Rational;
        Base : GMP.Output_Base := 10)
        return String;
private
    type Big_Rational is new Ada.Finalization.Controlled with
        record
            Value : aliased GMP.MPQ.MPQ_T;
        end record;

    procedure Initialize (This : in out Big_Rational);
    procedure Finalize (This : in out Big_Rational);
    procedure Adjust (This : in out Big_Rational);
end Big_Rationals;
