with System;
with GMP.MPZ;

package GMP.MPQ is
    -- Determined from gmp sources
    type MPQ_T is record
        MP_Num : aliased GMP.MPZ.MPZ_T;
        MP_Den : aliased GMP.MPZ.MPZ_T;
    end record;
    pragma Convention (C, MPQ_T);

    -- Canonicalize
    procedure Canonicalize (This : access MPQ_T);

    -- Initializing
    procedure Initialize (This : access MPQ_T);
    procedure Finalize (This : access MPQ_T);
    procedure Swap (This, Other : access MPQ_T);

    -- Init and Assign
    procedure Set (This : access MPQ_T; From : access constant MPQ_T);
    procedure Set_MPZ (
        This : access MPQ_T;
        From : access constant GMP.MPZ.MPZ_T);
    procedure Set_Unsigned (
        This : access MPQ_T;
        Num : Unsigned;
        Den : Unsigned);
    procedure Set_Signed (
        This : access MPQ_T;
        Num : Signed;
        Den : Unsigned);
    function Set_String (
        This : access MPQ_T;
        From : C_String;
        Base : C_Int)
        return C_Int;

    -- Converting
    function To_String (
        Memory : System.Address;
        Base : C_Int;
        This : access constant MPQ_T)
        return C_String;

    -- Arithmetic
    procedure Add (Result : access MPQ_T; X, Y : access constant MPQ_T);
    procedure Sub (Result : access MPQ_T; X, Y : access constant MPQ_T);
    procedure Mul (Result : access MPQ_T; X, Y : access constant MPQ_T);
    procedure Shift_Left (
        Result : access MPQ_T;
        X : access constant MPQ_T;
        Y : Unsigned);
    procedure Div (Result : access MPQ_T; X, Y : access constant MPQ_T);
    procedure Shift_Right (
        Result : access MPQ_T;
        X : access constant MPQ_T;
        Y : Unsigned);
    procedure Negate (Result : access MPQ_T; This : access constant MPQ_T);
    procedure Absolute (Result : access MPQ_T; This : access constant MPQ_T);
    procedure Inverse (Result : access MPQ_T; This : access constant MPQ_T);

    -- Comparisons
    function Compare (A, B : access constant MPQ_T) return C_Int;
    function Compare_MPZ (
        A : access constant MPQ_T;
        B : access constant GMP.MPZ.MPZ_T)
        return C_Int;
    function Sign (Item : access constant MPQ_T) return C_Int;
    function Equal (A, B : access constant MPQ_T) return C_Int;

    pragma Import (C, Canonicalize, "__gmpq_canonicalize");
    pragma Import (C, Initialize, "__gmpq_init");
    pragma Import (C, Finalize, "__gmpq_clear");
    pragma Import (C, Swap, "__gmpq_swap");
    pragma Import (C, Set, "__gmpq_set");
    pragma Import (C, Set_MPZ, "__gmpq_set_z");
    pragma Import (C, Set_Unsigned, "__gmpq_set_ui");
    pragma Import (C, Set_Signed, "__gmpq_set_si");
    pragma Import (C, Set_String, "__gmpq_set_str");
    pragma Import (C, To_String, "__gmpq_get_str");
    pragma Import (C, Add, "__gmpq_add");
    pragma Import (C, Sub, "__gmpq_sub");
    pragma Import (C, Mul, "__gmpq_mul");
    pragma Import (C, Shift_Left, "__gmpq_mul_2exp");
    pragma Import (C, Div, "__gmpq_div");
    pragma Import (C, Shift_Right, "__gmpq_div_2exp");
    pragma Import (C, Negate, "__gmpq_neg");
    pragma Import (C, Absolute, "__gmpq_abs");
    pragma Import (C, Inverse, "__gmpq_inv");
    pragma Import (C, Compare, "__gmpq_cmp");
    pragma Import (C, Compare_MPZ, "__gmpq_cmp_z");
    pragma Import (C, Equal, "__gmpq_equal");
end GMP.MPQ;
