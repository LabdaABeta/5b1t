with Sbit.Resources;
with Regex;
with Ada.Strings.Unbounded; use Ada.Strings.Unbounded;

package body Opinfo is
    Types : Sbit_Type_Maps.Map;
    NL : constant Character := Character'Val (10);

    function Get_Operations return Sbit_Type_Maps.Map is
        Cur_Kind : Type_Kind := Unknown;
        In_Operator : Boolean := False;
        Type_Count : Natural := 0;
        Cur_Repr : Sbit_Representation;
        Cur_Op : Sbit.Code_Point := Sbit.Code_Point'Last;
        Cur_Arg : Operator_Kind := No_Block;
        Temp : Sbit_Type;

        procedure Process (Line : String) is
            Tokens : constant Regex.Match_List := Regex.Tokenize (Line, "\t");

            function Token (Index : Natural) return String is
                M : constant Regex.Match := Tokens (Index);
            begin
                return Line (M.Start .. M.Finish);
            end Token;

            function Char_To_Mode (C : Character) return Operator_Mode is (
                case C is
                    when '?' => Unimplemented,
                    when '!' => Obsolete,
                    when '.' => Partial,
                    when others => Normal);
        begin
            case Tokens'Length is
                when 1 => -- Comment / Section
                    if Regex.Matches (Line, "^##* Single Types ##*$") then
                        Cur_Kind := Single;
                    elsif Regex.Matches (Line, "^##* List Types ##*$") then
                        Cur_Kind := List;
                    elsif Regex.Matches (Line, "^##* Tuple Types ##*$") then
                        Cur_Kind := Tuple;
                    elsif Regex.Matches (Line, "^##* Set Types ##*$") then
                        Cur_Kind := Set;
                    elsif Regex.Matches (Line, "^##* Group Types ##*$") then
                        Cur_Kind := Group;
                    else
                        Cur_Kind := Unknown;
                    end if;
                when 2 => -- Description
                    Temp := Types.Element (Cur_Repr);
                    if In_Operator then
                        Append (
                            Temp.Operations (Cur_Arg, Cur_Op).Description,
                            NL & Token (2));
                    else
                        Append (Temp.Description, NL & Token (2));
                    end if;
                    Types.Replace (Cur_Repr, Temp);
                when 3 => -- Operator Definition
                    declare
                        T : constant String := Token (1);
                        T2 : constant String := Token (2);
                        Literal : Boolean := False;
                        Mode : Operator_Mode := Normal;
                        Returns : Representation_Lists.List;

                        RTs : constant Regex.Match_List :=
                            Regex.Tokenize (T2, "∪");
                    begin
                        if T'Length < 1 then
                            return;
                        end if;

                        for M of RTs loop
                            Returns.Append (+(T2 (M.Start .. M.Finish)));
                        end loop;

                        Cur_Op := Sbit.To_Code (T (T'First));
                        Cur_Arg := No_Block;

                        case T'Length is
                            when 2 =>
                                Mode := Char_To_Mode (T (T'Last));
                            when 3 =>
                                if T (T'First + 1 .. T'Last) = "Λ" then
                                    Cur_Arg := Blocked;
                                else
                                    Literal := True;
                                end if;
                            when 4 =>
                                if T (T'First + 1 .. T'Last - 1) = "Λ" then
                                    Cur_Arg := Blocked;
                                else
                                    Literal := True;
                                end if;

                                Mode := Char_To_Mode (T (T'Last));
                            when 5 =>
                                Cur_Arg := Blocked;
                                Literal := True;
                            when 6 =>
                                Cur_Arg := Blocked;
                                Literal := True;
                                Mode := Char_To_Mode (T (T'Last));
                            when others =>
                                null;
                        end case;

                        Temp := Types.Element (Cur_Repr);
                        Temp.Operations (Cur_Arg, Cur_Op) := (
                            Exists => True,
                            Return_Types => Returns,
                            Name => +Token (3),
                            Description => +"",
                            Source => Cur_Repr,
                            Mode => Mode,
                            Has_Literal => Literal);
                        Types.Replace (Cur_Repr, Temp);

                        In_Operator := True;
                    end;
                when 4 => -- Type Definition
                    Cur_Repr := +Token (1);

                    declare
                        Parent_Repr : constant Sbit_Representation :=
                            +Token (2);
                        This_Name : constant Raw_Text := +Token (3);
                        This_Plural : constant Raw_Text := +Token (4);
                    begin
                        if Types.Contains (Cur_Repr) then
                            return;
                        end if;

                        -- Append this type to its parent's children list
                        if Types.Contains (Parent_Repr) then
                            Temp := Types.Element (Parent_Repr);
                            Temp.Children.Append (Cur_Repr);
                            Types.Replace (Parent_Repr, Temp);
                        end if;

                        Temp := (
                            Representation => Cur_Repr,
                            Name => This_Name,
                            Plural => This_Plural,
                            Kind => Cur_Kind,
                            Parent => Parent_Repr,
                            Children => Representation_Lists.Empty_List,
                            Description => +"",
                            Operations => (
                                if Types.Contains (Parent_Repr) then
                                    Types.Element (Parent_Repr).Operations
                                else
                                    No_Operations),
                            Order => Type_Count);
                        Type_Count := Type_Count + 1;

                        Types.Insert (Cur_Repr, Temp);
                    end;

                    In_Operator := False;
                when others =>
                    null;
            end case;
        end Process;
    begin
        if Types.Is_Empty then
            -- Load the types
            Sbit.Resources.Access_Resource ("operations", Process'Access);
        end if;

        return Types;
    end Get_Operations;
end Opinfo;
