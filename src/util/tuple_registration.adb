with Ada.Containers.Indefinite_Doubly_Linked_Lists;

package body Tuple_Registration is
    use type Ada.Tags.Tag_Array;
    use type Ada.Tags.Tag;

    type Tags_Pair (Size : Natural) is
        record
            From : Ada.Tags.Tag_Array (1 .. Size);
            To : Ada.Tags.Tag;
        end record;

    package Tags_Pair_Lists is new Ada.Containers.Indefinite_Doubly_Linked_Lists
        (Tags_Pair);
    Reductions : Tags_Pair_Lists.List := Tags_Pair_Lists.Empty_List;

    package Tag_List_Lists is new Ada.Containers.Indefinite_Doubly_Linked_Lists
        (Ada.Tags.Tag_Array);

    procedure Register (From : Ada.Tags.Tag_Array; To : Ada.Tags.Tag) is begin
        Reductions.Append ((From'Length, From, To));
    end Register;

    function Reduce (
        From : Ada.Tags.Tag_Array;
        Default : Ada.Tags.Tag)
        return Ada.Tags.Tag
    is
        Open : Tag_List_Lists.List := Tag_List_Lists.Empty_List;
    begin
        Open.Append (From);

        while not Open.Is_Empty loop
            declare
                Candidate : Ada.Tags.Tag_Array := Open.First_Element;
            begin
                for Pair of Reductions loop
                    if Candidate = Pair.From then
                        return Pair.To;
                    end if;
                end loop;

                for I in Candidate'Range loop
                    declare
                        Saved : constant Ada.Tags.Tag := Candidate (I);
                        Parent : constant Ada.Tags.Tag :=
                            Ada.Tags.Parent_Tag (Saved);
                    begin
                        if Parent /= Ada.Tags.No_Tag then
                            Candidate (I) := Parent;
                            Open.Append (Candidate);
                            Candidate (I) := Saved;
                        end if;
                    end;
                end loop;
            end;

            Open.Delete_First;
        end loop;

        return Default;
    end Reduce;
end Tuple_Registration;
