with Ada.Containers.Vectors;

package body List_Registration is
    type Tag_Pair is
        record
            From, To : Ada.Tags.Tag;
        end record;

    package Tag_Pair_Vectors is new Ada.Containers.Vectors (Positive, Tag_Pair);
    Reductions : Tag_Pair_Vectors.Vector := Tag_Pair_Vectors.Empty_Vector;

    procedure Register (From, To : Ada.Tags.Tag) is begin
        Reductions.Append ((From, To));
    end Register;

    function Reduce (Root, Default : Ada.Tags.Tag) return Ada.Tags.Tag is
        use type Ada.Tags.Tag;
    begin
        for Pair of Reductions loop
            if Pair.From = Root then
                return Pair.To;
            end if;
        end loop;

        return Default;
    end Reduce;
end List_Registration;
