with System;
with Interfaces.C;
with Interfaces.C.Strings;
with Ada.Strings.Unbounded;

package body Curl is
    function Curl_Easy_Init return System.Address;
    pragma Import (C, Curl_Easy_Init, "curl_easy_init");

    function Curl_Easy_SetOpt (
        Handle : System.Address;
        Option : Interfaces.C.int;
        Param : System.Address)
        return Interfaces.C.int;
    pragma Import (C, Curl_Easy_SetOpt, "curl_easy_setopt");

    function Curl_Easy_SetOpt_String (
        Handle : System.Address;
        Option : Interfaces.C.int;
        Param : Interfaces.C.Strings.chars_ptr)
        return Interfaces.C.int;
    pragma Import (C, Curl_Easy_SetOpt_String, "curl_easy_setopt");

    function Curl_Easy_Perform (
        Handle : System.Address)
        return Interfaces.C.int;
    pragma Import (C, Curl_Easy_Perform, "curl_easy_perform");

    procedure Curl_Easy_Cleanup (Handle : System.Address);
    pragma Import (C, Curl_Easy_Cleanup, "curl_easy_cleanup");

    function Curl_Easy_StrError (
        ErrorNum : Interfaces.C.int)
        return Interfaces.C.Strings.chars_ptr;
    pragma Import (C, Curl_Easy_StrError, "curl_easy_strerror");

    -- Constants determined from examination
    CURLOPT_WRITEFUNCTION : constant := 20011;
    CURLOPT_URL : constant := 10002;

    procedure Curl (
        Url : String;
        Callback : access procedure (Line : String))
    is
        C_URL : Interfaces.C.Strings.chars_ptr :=
            Interfaces.C.Strings.New_String (Url);
        Handle : constant System.Address := Curl_Easy_Init;
        Error : Interfaces.C.int;
        use type Interfaces.C.int;

        Line_Buf : Ada.Strings.Unbounded.Unbounded_String :=
            Ada.Strings.Unbounded.Null_Unbounded_String;

        function Write_Callback (
            Ptr : Interfaces.C.Strings.chars_ptr; -- NOT null terminated
            Size : Interfaces.C.size_t;
            Nmemb : Interfaces.C.size_t;
            UserData : System.Address)
            return Interfaces.C.size_t;
        pragma Convention (C, Write_Callback);

        function Write_Callback (
            Ptr : Interfaces.C.Strings.chars_ptr; -- NOT null terminated
            Size : Interfaces.C.size_t;
            Nmemb : Interfaces.C.size_t;
            UserData : System.Address)
            return Interfaces.C.size_t
        is
            use type Interfaces.C.char;
            use type Interfaces.C.size_t;
            pragma Unreferenced (UserData);

            Data : constant Interfaces.C.char_array :=
                Interfaces.C.Strings.Value (Ptr, Nmemb * Size);
            New_Line : constant Interfaces.C.char := Interfaces.C.char'Val (10);
        begin
            for Datum of Data loop
                if Datum = New_Line then
                    Callback.all (Ada.Strings.Unbounded.To_String (Line_Buf));
                    Line_Buf := Ada.Strings.Unbounded.Null_Unbounded_String;
                else
                    Ada.Strings.Unbounded.Append (Line_Buf, Character (Datum));
                end if;
            end loop;

            return Nmemb;
        end Write_Callback;
    begin
        Error := Curl_Easy_SetOpt (
            Handle => Handle,
            Option => CURLOPT_WRITEFUNCTION,
            Param => Write_Callback'Address);

        if Error /= 0 then
            Interfaces.C.Strings.Free (C_URL);
            raise Curl_Error with
                Interfaces.C.Strings.Value (Curl_Easy_StrError (Error));
        end if;

        Error := Curl_Easy_SetOpt_String (
            Handle => Handle,
            Option => CURLOPT_URL,
            Param => C_URL);

        if Error /= 0 then
            Interfaces.C.Strings.Free (C_URL);
            raise Curl_Error with
                Interfaces.C.Strings.Value (Curl_Easy_StrError (Error));
        end if;

        Error := Curl_Easy_Perform (Handle);

        if Error /= 0 then
            Interfaces.C.Strings.Free (C_URL);
            raise Curl_Error with
                Interfaces.C.Strings.Value (Curl_Easy_StrError (Error));
        end if;

        if Ada.Strings.Unbounded.Length (Line_Buf) > 0 then
            Callback.all (Ada.Strings.Unbounded.To_String (Line_Buf));
        end if;

        Curl_Easy_Cleanup (Handle);
        Interfaces.C.Strings.Free (C_URL);
    end Curl;
end Curl;
