with Big_Integers;

package OEIS is
    function Get (List, Index : Natural) return Big_Integers.Big_Integer;
end OEIS;
