with Sbit;

package Debug is
    type Debug_Verbosity is (
        Low,    -- val.OP_OF[block] =>
        Medium, -- val.OP_OF[block] (Operator Name) => (Type Name, Other Type)
        High);  -- val.OP_OF[block] Operator Name: SrcRep -> DstRep
                --     Operator description spanning
                --     multiple lines.
                -- => Type Name
                --     Type description spanning
                --     multiple lines.
                -- => Other Type
                --     Type description spanning
                --     multiple lines.

    -- Prints information to stderr, returning the input type
    -- If Block is null then the operator isn't taking a block
    function Debug (
        This : Sbit.Datatype'Class;
        Op : Sbit.Code_Point;
        Verbose : Debug_Verbosity;
        Block : access function (
            This : Sbit.Datatype'Class)
            return Sbit.Datatype'Class := null)
        return Sbit.Datatype'Class;
end Debug;
