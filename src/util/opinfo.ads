with Sbit;
with Ada.Strings.Unbounded.Hash;
with Ada.Strings.Unbounded;
with Ada.Containers.Doubly_Linked_Lists;
with Ada.Containers.Hashed_Maps;

package Opinfo is
    use type Ada.Strings.Unbounded.Unbounded_String;
    type Type_Kind is (Unknown, Single, List, Tuple, Set, Group);

    subtype Sbit_Representation is Ada.Strings.Unbounded.Unbounded_String;
    subtype Raw_Text is Ada.Strings.Unbounded.Unbounded_String;

    function "+" (Source : String) return Ada.Strings.Unbounded.Unbounded_String
        renames Ada.Strings.Unbounded.To_Unbounded_String;

    package Representation_Lists is new Ada.Containers.Doubly_Linked_Lists (
        Element_Type => Sbit_Representation);

    type Operator_Kind is (No_Block, Blocked);
    type Operator_Mode is (Normal, Unimplemented, Obsolete, Partial);

    type Sbit_Operator (Exists : Boolean := True) is
        record
            case Exists is
                when True =>
                    Return_Types : Representation_Lists.List;
                    Name : Raw_Text;
                    Description : Raw_Text;
                    Source : Sbit_Representation;
                    Mode : Operator_Mode;
                    Has_Literal : Boolean;
                when False =>
                    null;
            end case;
        end record;
    subtype Present_Operator is Sbit_Operator (Exists => True);

    type Sbit_Operations is array (Operator_Kind, Sbit.Code_Point) of
        Sbit_Operator;

    No_Operations : constant Sbit_Operations := (
        others => (others => (Exists => False)));

    type Sbit_Type is
        record
            Representation : Sbit_Representation;
            Name : Raw_Text;
            Plural : Raw_Text;
            Kind : Type_Kind;
            Parent : Sbit_Representation;
            Children : Representation_Lists.List;
            Description : Raw_Text;
            Operations : Sbit_Operations;
            Order : Natural; -- Order in which declared
        end record;

    package Sbit_Type_Maps is new Ada.Containers.Hashed_Maps (
        Key_Type => Sbit_Representation,
        Element_Type => Sbit_Type,
        Hash => Ada.Strings.Unbounded.Hash,
        Equivalent_Keys => "=");

    -- First time called it generates, subsequent calls return the same object.
    function Get_Operations return Sbit_Type_Maps.Map;
end Opinfo;
