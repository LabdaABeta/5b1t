with Ada.Tags;

package Tag_Utils is
    function Common_Ancestor (Left, Right : Ada.Tags.Tag) return Ada.Tags.Tag;
end Tag_Utils;
