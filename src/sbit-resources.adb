with Ada.Text_IO;
with Ada.Strings.Unbounded;

package body Sbit.Resources is
    Share_Dir : Ada.Strings.Unbounded.Unbounded_String :=
        Ada.Strings.Unbounded.Null_Unbounded_String;

    procedure Initialize (Share : String) is
    begin
        Share_Dir := Ada.Strings.Unbounded.To_Unbounded_String (Share);
    end Initialize;

    procedure Access_Resource (
        Name : String;
        Process_Line : access procedure (Line : String))
    is
        File : Ada.Text_IO.File_Type;
    begin
        Ada.Text_IO.Open (
            File => File,
            Mode => Ada.Text_IO.In_File,
            Name => Ada.Strings.Unbounded.To_String (Share_Dir) & "/" & Name);

        if not Ada.Text_IO.Is_Open (File) then
            raise No_Resource with "Resource not found: " & Name;
        end if;

        while not Ada.Text_IO.End_Of_File (File) loop
            Process_Line.all (Ada.Text_IO.Get_Line (File));
        end loop;

        Ada.Text_IO.Close (File);
    end Access_Resource;
end Sbit.Resources;
