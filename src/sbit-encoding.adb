package body Sbit.Encoding is
    ln3_by_ln2 : constant := 1.5849625007211561814538; -- Intentionally high
    ln2_by_ln3 : constant := 0.6309297535714574370995; -- Intentionally low
    ln96_by_ln2 : constant := 5.0 + ln3_by_ln2;

    type Bit is mod 2;
    type Trit is mod 3;

    type Bits is array (Positive range <>) of Bit;
    type Trits is array (Positive range <>) of Trit;

    procedure Triple (On : in out Bits) is
        Twice : constant Bits (On'Range) := On (2 .. On'Last) & 0;

        Carry : Bit := 0;
        Sum : Integer;
    begin
        for I in reverse On'Range loop
            Sum := Integer (Carry) + Integer (On (I)) + Integer (Twice (I));
            Carry := Bit (Sum / 2);
            On (I) := Bit (Sum mod 2);
        end loop;
    end Triple;

    procedure Double (On : in out Trits) is
        Carry : Trit := 0;
        Sum : Integer;
    begin
        for I in reverse On'Range loop
            Sum := Integer (Carry) + 2 * Integer (On (I));
            Carry := Trit (Sum / 3);
            On (I) := Trit (Sum mod 3);
        end loop;
    end Double;

    procedure Add_To (On : in out Bits; Value : Trit) is
        Carry : Integer := Integer (Value);
        Sum : Integer;
        I : Positive := On'Last;
    begin
        while Carry /= 0 loop
            Sum := Carry + Integer (On (I));
            Carry := Sum / 2;
            On (I) := Bit (Sum mod 2);
            I := I - 1;
        end loop;
    end Add_To;

    procedure Increment (On : in out Trits) is
        Carry : Trit := 1;
        Sum : Integer;
        I : Positive := On'Last;
    begin
        while Carry /= 0 loop
            Sum := Integer (Carry) + Integer (On (I));
            Carry := Trit (Sum / 3);
            On (I) := Trit (Sum mod 3);
            I := I - 1;
        end loop;
    end Increment;

    function Ternary_To_Binary (From : Trits) return Bits is
        Necessary_Bits : constant Positive := Positive (
            Long_Float'Ceiling (Long_Float (From'Length) * ln3_by_ln2));
        Result : Bits (1 .. Necessary_Bits) := (others => 0);
    begin
        for I in From'Range loop
            Triple (Result);
            Add_To (Result, From (I));
        end loop;

        return Result;
    end Ternary_To_Binary;

    function Binary_To_Ternary (From : Bits) return Trits is
        Necessary_Trits : constant Positive := Positive (
            Long_Float'Floor (Long_Float (From'Length) * ln2_by_ln3));
        Result : Trits (1 .. Necessary_Trits) := (others => 0);
    begin
        for I in From'Range loop
            Double (Result);
            if From (I) = 1 then
                Increment (Result);
            end if;
        end loop;

        return Result;
    end Binary_To_Ternary;

    function Pack (From : Bits) return Byte_List is
        Result : Byte_List (1 .. ((From'Length - 1) / 8) + 1) := (others => 0);

        function Get (Which : Positive) return Byte is
        begin
            if Which <= From'Last then
                return Byte (From (Which));
            else
                return 0;
            end if;
        end Get;

        function Extract (Which : Positive) return Byte is
        begin
            return Get (Which * 8 - 7) * 128 + Get (Which * 8 - 6) * 64 +
                Get (Which * 8 - 5) * 32 + Get (Which * 8 - 4) * 16 +
                Get (Which * 8 - 3) * 8  + Get (Which * 8 - 2) * 4 +
                Get (Which * 8 - 1) * 2  + Get (Which * 8);
        end Extract;
    begin
        for I in Result'Range loop
            Result (I) := Extract (I);
        end loop;

        return Result;
    end Pack;

    function Unpack (From : Byte_List) return Bits is
        Result : Bits (1 .. 8 * From'Length) := (others => 0);
    begin
        for I in From'Range loop
            Result (I * 8 - 7) := Bit (From (I) / 128);
            Result (I * 8 - 6) := Bit ((From (I) / 64) mod 2);
            Result (I * 8 - 5) := Bit ((From (I) / 32) mod 2);
            Result (I * 8 - 4) := Bit ((From (I) / 16) mod 2);
            Result (I * 8 - 3) := Bit ((From (I) / 8) mod 2);
            Result (I * 8 - 2) := Bit ((From (I) / 4) mod 2);
            Result (I * 8 - 1) := Bit ((From (I) / 2) mod 2);
            Result (I * 8) := Bit (From (I) mod 2);
        end loop;

        return Result;
    end Unpack;

    function Encode (From : Listing) return Byte_List is
        Raw_Bits : Bits (1 .. From'Length * 5);
        Raw_Trits : Trits (1 .. From'Length);

        function Padding (Size : Natural) return Bits is
            Result : constant Bits (1 .. Size) := (others => 0);
        begin
            return Result;
        end Padding;
    begin
        for I in From'Range loop
            Raw_Trits (I) := Trit (From (I) / 32);
            Raw_Bits (I * 5 - 4) := Bit ((From (I) / 16) mod 2);
            Raw_Bits (I * 5 - 3) := Bit ((From (I) / 8) mod 2);
            Raw_Bits (I * 5 - 2) := Bit ((From (I) / 4) mod 2);
            Raw_Bits (I * 5 - 1) := Bit ((From (I) / 2) mod 2);
            Raw_Bits (I * 5) := Bit (From (I) mod 2);
        end loop;

        declare
            Tern_Part : constant Bits := Ternary_To_Binary (Raw_Trits);
        begin
            return Pack (
                Raw_Bits &
                Padding (8 - ((Raw_Bits'Length + Tern_Part'Length) mod 8)) &
                Tern_Part);
        end;
    end Encode;

    -- ISSUE: XXX: Size can't uniquely determine length due to 'free' bytes
    -- Need to parse the ternary - if it 'fits' then its good, if there are
    -- extra trits its too big. Try the bigger size first.
    function Decode (From : Byte_List) return Listing is
        Raw_Bits : constant Bits := Unpack (From);
        Size : constant Positive := Positive (
            Long_Float (From'Length) * 8.0 / ln96_by_ln2);
        Result : Listing (1 .. Size) := (others => 0);
        Raw_Trits : constant Trits :=
            Binary_To_Ternary (Raw_Bits (Size * 5 + 1 .. Raw_Bits'Last));
    begin
        -- Extract the binary part
        for I in Result'Range loop
            Result (I) :=
                Code_Point (Raw_Bits (I * 5 - 4)) * 16 +
                Code_Point (Raw_Bits (I * 5 - 3)) * 8 +
                Code_Point (Raw_Bits (I * 5 - 2)) * 4 +
                Code_Point (Raw_Bits (I * 5 - 1)) * 2 +
                Code_Point (Raw_Bits (I * 5)) +
                Code_Point (Raw_Trits (I)) * 32;
        end loop;

        return Result;
    end Decode;

end Sbit.Encoding;
