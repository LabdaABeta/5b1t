# 5b1t styleguide

Since this language will involve a lot of code that will evolve over time it is
important to have a consistent styleguide. This styleguide is enforced via
compilation switches seen in the shared project file. A few additional
enforcements must be made manually.

## Subprogram Headings

If the full subprogram with arguments and `is` fit on one line, then they should
be placed on one line. Otherwise the declaration up to and including the open
bracket for the parameter list shall appear on one line, with each parameter on
a subsequent line (possibly grouped by type with commas) and the return type on
another line. These extra lines are indented by one level. The `is` shall be on
its own line and not indented past the declaration.

Examples:

```
function Short_Enough (X : Integer) return String is
    -- ...

procedure Also_Short_Enough (X : Integer; Y : String; Z : out Float) is
    -- ...

function Too_Long_For_Just_One_Line (
    X : Integer;
    Y : String;
    W,Z : Float)
    return Natural
is
    -- ...

procedure Also_Too_Long_For_Just_One_Line (
    X : Integer;
    Y : String;
    W,Z : Float;
    Result : out Natural)
is
    -- ...
```

## Parameter Lists

If all parameters in the call fit on one line and are all positional then names
can be omitted. Otherwise all names must be present and each shall be on
a separate line with an increased level of indentation.

Examples:

```
Short_Enough_Call (First_Argument, Second_Argument, Third_Argument);

Not_Short_Enough (
    First_Parameter => First_Argument,
    Second_Parameter => Second_Argument,
    Third_Parameter => Third_Argument,
    Fourth_Parameter => Fourth_Argument);

Not_Positional (
    First_Parameter => First_Argument,
    Third_Parameter => Third_Argument);
```

## External Libraries

When including an external library one should use the lowest-level interface
available. If possible, a direct binding to a static C library is preferred. In
any case, the library should be wrapped in two layers. The first is to rename
the imported functions without changing their typing or signatures. This rename
should include all functions of the library, except possibly duplicates, even if
only a subset will be used. This is to allow future use of the library without
modification. The second wrapping is to implement a proper Ada type with
syntactic sugar and cleaner syntax for the underlying operations.

When an external library is included, be sure to give it its own directory under
the `util` directory. Also ensure to put a GPR file for its inclusion there, and
add that GPR as an include in the `setup.gpr` file in the root of the source
directory.

If the external library is hard to get a hold of, then a copy of it should be
placed in its `util` directory. Otherwise installation instructions should be
provided in a `INSTALL.md` file also in its `util` directory.
