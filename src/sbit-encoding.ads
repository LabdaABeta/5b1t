package Sbit.Encoding is
    type Byte is mod 256;

    type Byte_List is array (Positive range <>) of Byte;

    function Encode (From : Listing) return Byte_List;
    function Decode (From : Byte_List) return Listing;
end Sbit.Encoding;
