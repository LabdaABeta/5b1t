with Big_Integers; use Big_Integers;
with Sbit.Naturals;
with Sbit.Booleans;
with Sbit.Integer_Lists;
with Type_Registration;

package body Sbit.Integers is
    function Create (Ignore : not null access Nothing) return Integer is
        Result : constant Integer := Create (Create (0));
    begin
        return Result;
    end Create;

    function Create (From : Big_Integers.Big_Integer) return Integer is
        Result : constant Integer := (Empty_Datum with Value => From);
    begin
        return Result;
    end Create;

    function Get_Value (From : Integer) return Big_Integers.Big_Integer is
    begin
        return From.Value;
    end Get_Value;

    function D_MAJ (This : Integer) return Datatype'Class is
        Result : Integer := This;
    begin
        Result.Value := This.Value * Create (2);
        return Result;
    end D_MAJ;

    function E_MAJ (This : Integer) return Datatype'Class is
        Result : constant Booleans.Boolean := Booleans.Create (
            not Test (This.Get_Value, 0));
    begin
        return Result;
    end E_MAJ;

    function R_MAJ (This : Integer) return Datatype'Class is
        Result : Integer_Lists.Integer_List := Integer_Lists.Empty_List;
        One : constant Big_Integer := Create (1);
        I : Big_Integer := Create (0);
    begin
        if This.Get_Value >= Create (0) then
            while I <= This.Get_Value loop
                Result.Append (Naturals.Create (I));
                I := I + One;
            end loop;
        else
            while I >= This.Get_Value loop
                Result.Append (Create (I));
                I := I - One;
            end loop;
        end if;

        return Result.Child_Reduce;
    end R_MAJ;

    function R_MIN (This : Integer) return Datatype'Class is
        Result : Integer_Lists.Integer_List := Integer_Lists.Empty_List;
        One : constant Big_Integer := Create (1);
        I : Big_Integer := Create (0);
    begin
        if This.Get_Value >= Create (0) then
            while I < This.Get_Value loop
                Result.Append (Naturals.Create (I));
                I := I + One;
            end loop;
        else
            while I > This.Get_Value loop
                Result.Append (Create (I));
                I := I - One;
            end loop;
        end if;

        return Result.Child_Reduce;
    end R_MIN;

    function S_MIN (This : Integer) return Datatype'Class is
        Result : constant Integer := Create (Create (
            if This.Get_Value > Create (0) then 1
            elsif This.Get_Value < Create (0) then -1
            else 0));
    begin
        return Result;
    end S_MIN;

    function ADDIT (This : Integer) return Datatype'Class is
        Result : constant Sbit.Naturals.Natural := Sbit.Naturals.Create (
            abs This.Value);
    begin
        return Result;
    end ADDIT;

    function MINUS (This : Integer) return Datatype'Class is
        Result : constant Integer := Create (-This.Get_Value);
    begin
        return Result;
    end MINUS;

    function Read (From : not null access Inputter) return Integer is
    begin
        return (Empty_Datum with Value => From.Get_Int);
    end Read;

    procedure Write (This : Integer; To : in out Outputter) is
    begin
        To.Put_Int (This.Value);
    end Write;

    function Image (This : Integer) return String is
    begin
        return Big_Integers.Image (This.Value);
    end Image;

    function Type_Image (This : Integer) return String is
        pragma Unreferenced (This);
    begin
        return "i";
    end Type_Image;
begin
    Type_Registration.Register ("Integers", Integer'Tag);
end Sbit.Integers;
