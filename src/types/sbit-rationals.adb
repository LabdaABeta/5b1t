with Big_Rationals; use Big_Rationals;
with Big_Integers;
with Sbit.Integers;
with Sbit.Naturals;
with Type_Registration;

package body Sbit.Rationals is
    function Create (Ignore : not null access Nothing) return Rational is
        Result : constant Rational := (Empty_Datum with
            Value => Create (Big_Integers.Create (0)));
    begin
        return Result;
    end Create;

    function Create (From : Big_Rationals.Big_Rational) return Rational is
        Result : constant Rational := (Empty_Datum with
            Value => From);
    begin
        return Result;
    end Create;

    -- Direct operators
    function D_MIN (This : Rational) return Datatype'Class is (
        Naturals.Create (This.Value.Denominator));

    function N_MIN (This : Rational) return Datatype'Class is (
        Integers.Create (This.Value.Numerator));

    function Read (From : not null access Inputter) return Rational is
    begin
        return (Empty_Datum with
            Value => Big_Rationals.Create (From.Get_Line));
    end Read;

    procedure Write (This : Rational; To : in out Outputter) is
    begin
        To.Put_String (Big_Rationals.Image (This.Value));
    end Write;

    function Image (This : Rational) return String is
    begin
        return Big_Rationals.Image (This.Value);
    end Image;

    function Type_Image (This : Rational) return String is
        pragma Unreferenced (This);
    begin
        return "Q";
    end Type_Image;
begin
    Type_Registration.Register ("Rationals", Rational'Tag);
end Sbit.Rationals;
