with Big_Integers; use Big_Integers;
with Big_Rationals;
with Sbit.Booleans;
with Sbit.Literals;
with Sbit.Rationals;
with Sbit.Sets;
with OEIS;
with Type_Registration;

package body Sbit.Naturals is
    function Create (Ignore : not null access Nothing) return Natural is
        Result : constant Natural := Create (Create (0));
    begin
        return Result;
    end Create;

    function Create (From : Big_Integers.Big_Integer) return Natural is
        Result : constant Natural := (
            Sbit.Integers.Create (From) with null record);
    begin
        if From < Create (0) then
            raise Constraint_Error;
        end if;
        return Result;
    end Create;

    function Get_Value (From : Natural) return Big_Integers.Big_Integer is
    begin
        return Sbit.Integers.Get_Value (Sbit.Integers.Integer (From));
    end Get_Value;

    function D_MAJ (This : Natural) return Datatype'Class is (
        Create (This.Get_Value * Create (2)));

    function E_MAJ (This : Natural) return Datatype'Class is (
        Booleans.Create (not Test (This.Get_Value, 0)));

    function F_MAJ (This : Natural) return Datatype'Class is (
        Create (Fibonacci (This.Get_Value.As_Integer)));

    function H_MAJ (This : Natural) return Datatype'Class is (
        if This.Get_Value mod 2 = 0 then
            Create (This.Get_Value / 2)
        else
            Rationals.Create (
                From => Big_Rationals.Create (This.Get_Value, Create (2))));

    function L_MAJ (This : Natural) return Datatype'Class is (
        Create (Lucas (This.Get_Value.As_Integer)));

    function Apply_OEIS (
        This : Natural;
        Argument : Big_Integers.Big_Integer)
        return Datatype'Class is (
        Create (OEIS.Get (As_Integer (Argument), This.Get_Value.As_Integer)));

    package OEIS_Literals is new Sbit.Literals (Natural, Apply_OEIS);

    function O_MAJ (This : Natural) return Datatype'Class is (
        OEIS_Literals.Empty_Literal (This));

    function P_MAJ (This : Natural) return Datatype'Class is (
        Booleans.Create (Is_Prime (This.Get_Value)));

    function S_MAJ (This : Natural) return Datatype'Class is (
        Create (This.Get_Value ** 2));

    function Z_MAJ (This : Natural) return Datatype'Class is
        function Recursive_Wrap (Size : Big_Integer) return Sets.Set is
            Result : Sets.Set := Sets.Empty_Set;
        begin
            if Size > 0 then
                Result.Insert (Recursive_Wrap (Size - 1));
            end if;

            return Result;
        end Recursive_Wrap;
    begin
        return Recursive_Wrap (This.Get_Value);
    end Z_MAJ;

    function A_MIN (This : Natural) return Datatype'Class is (
        Create (This.Get_Value + Create (1)));

    function D_MIN (This : Natural) return Datatype'Class is
    begin
        if This.Get_Value = Create (0) then
            return Integers.Create (This.Get_Value - Create (1));
        else
            return Create (This.Get_Value - Create (1));
        end if;
    end D_MIN;

    function N_MIN (This : Natural) return Datatype'Class is (
        Create (Next_Prime (This.Get_Value)));

    function P_MIN (This : Natural) return Datatype'Class is
        Result : Big_Integer := Create (2);
        Count : Big_Integer := Create (0);
        One : constant Big_Integer := Create (1);
    begin
        while Count < This.Get_Value loop
            Result := Result.Next_Prime;
            Count := Count + One;
        end loop;

        return Create (Result);
    end P_MIN;

    function Z_MIN (This : Natural) return Datatype'Class is (
        Booleans.Create (This.Get_Value = Create (0)));

    function Apply_Addition (
        This : Natural;
        Argument : Big_Integers.Big_Integer)
        return Datatype'Class is (
        Create (This.Get_Value + Argument));

    package Addition_Literals is new Sbit.Literals (Natural, Apply_Addition);

    function ADDIT (This : Natural) return Datatype'Class is (
        Addition_Literals.Empty_Literal (This));

    function SLASH (This : Natural) return Datatype'Class is (
        Rationals.Create (Big_Rationals.Create (
            Num => Create (1),
            Den => This.Get_Value)));

    function MINUS (This : Natural) return Datatype'Class is (
        Integers.Create (-This.Get_Value));

    function EXCLA (This : Natural) return Datatype'Class is
        One : constant Big_Integer := Create (1);

        function Factorial_Of (N : Big_Integer) return Big_Integer is
        begin
            if Fits_Integer (N) then
                return Factorial (As_Integer (N));
            else
                return Factorial_Of (N - One) * N;
            end if;
        end Factorial_Of;
    begin
        return Create (Factorial_Of (This.Get_Value));
    end EXCLA;

    function POUND (This : Natural) return Datatype'Class is
        One : constant Big_Integer := Create (1);

        function Primorial_Of (N : Big_Integer) return Big_Integer is
        begin
            if Fits_Integer (N) then
                return Primorial (As_Integer (N));
            else
                if Is_Prime (N) then
                    return Primorial_Of (N - One) * N;
                else
                    return Primorial_Of (N - One);
                end if;
            end if;
        end Primorial_Of;
    begin
        return Create (Primorial_Of (This.Get_Value));
    end POUND;

    function Read (From : not null access Inputter) return Natural is
    begin
        return Create (From.Get_Int);
    end Read;

    procedure Write (This : Natural; To : in out Outputter) is
    begin
        To.Put_Int (This.Get_Value);
    end Write;

    function Image (This : Natural) return String is
    begin
        return Big_Integers.Image (This.Get_Value);
    end Image;

    function Type_Image (This : Natural) return String is
        pragma Unreferenced (This);
    begin
        return "n";
    end Type_Image;
begin
    Type_Registration.Register ("Naturals", Natural'Tag);
end Sbit.Naturals;
