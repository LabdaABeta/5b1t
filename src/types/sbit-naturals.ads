with Big_Integers;
with Sbit.Integers;

package Sbit.Naturals is
    type Natural is new Sbit.Integers.Integer with private;

    function Create (Ignore : not null access Nothing) return Natural;
    function Create (From : Big_Integers.Big_Integer) return Natural;
    function Get_Value (From : Natural) return Big_Integers.Big_Integer;

    -- Direct operators
    function D_MAJ (This : Natural) return Datatype'Class;
    function E_MAJ (This : Natural) return Datatype'Class;
    function F_MAJ (This : Natural) return Datatype'Class;
    function H_MAJ (This : Natural) return Datatype'Class;
    function L_MAJ (This : Natural) return Datatype'Class;
    function O_MAJ (This : Natural) return Datatype'Class;
    function P_MAJ (This : Natural) return Datatype'Class;
    function S_MAJ (This : Natural) return Datatype'Class;
    function Z_MAJ (This : Natural) return Datatype'Class;
    function A_MIN (This : Natural) return Datatype'Class;
    function D_MIN (This : Natural) return Datatype'Class;
    function N_MIN (This : Natural) return Datatype'Class;
    function P_MIN (This : Natural) return Datatype'Class;
    function Z_MIN (This : Natural) return Datatype'Class;
    function ADDIT (This : Natural) return Datatype'Class;
    function SLASH (This : Natural) return Datatype'Class;
    function MINUS (This : Natural) return Datatype'Class;
    function EXCLA (This : Natural) return Datatype'Class;
    function POUND (This : Natural) return Datatype'Class;

    function Read (From : not null access Inputter) return Natural;
    procedure Write (This : Natural; To : in out Outputter);

    -- Debugging and utility functions
    function Image (This : Natural) return String;
    function Type_Image (This : Natural) return String;

private
    type Natural is new Sbit.Integers.Integer with null record;
end Sbit.Naturals;
