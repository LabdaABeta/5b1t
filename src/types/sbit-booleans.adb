with Regex;
with Type_Registration;

package body Sbit.Booleans is
    function Create (Ignore : not null access Nothing) return Boolean is
    begin
        return Create (False);
    end Create;

    function Create (From : Standard.Boolean) return Boolean is
        Result : constant Boolean := (Empty_Datum with Value => From);
    begin
        return Result;
    end Create;

    function Get_Value (From : Boolean) return Standard.Boolean is
    begin
        return From.Value;
    end Get_Value;

    function "not" (This : Boolean) return Boolean is
        Result : Boolean := This;
    begin
        Result.Value := not This.Value;
        return Result;
    end "not";

    function EXCLA (This : Boolean) return Sbit.Datatype'Class is
        Result : Boolean := This;
    begin
        Result.Value := not This.Value;
        return Result;
    end EXCLA;

    function Read (From : not null access Inputter) return Boolean
    is
        Line : constant String := From.Get_Line;
    begin
        return Create (not (
            Regex.Matches (Line, "false", Regex.Case_Insensitive) or
            Regex.Matches (Line, "no.*", Regex.Case_Insensitive) or
            Regex.Matches (Line, "^0*$", Regex.Case_Insensitive) or
            Regex.Matches (Line, "ne.*", Regex.Case_Insensitive)));
    end Read;

    procedure Write (This : Boolean; To : in out Outputter) is
    begin
        To.Put_String (Standard.Boolean'Image (This.Value));
    end Write;

    function Image (This : Boolean) return String is
    begin
        return Standard.Boolean'Image (This.Value);
    end Image;

    function Type_Image (This : Boolean) return String is
        pragma Unreferenced (This);
    begin
        return "B";
    end Type_Image;
begin
    Type_Registration.Register ("Booleans", Boolean'Tag);
end Sbit.Booleans;
