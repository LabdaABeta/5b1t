package body Sbit.Blockcats is
    New_Line : constant String (1 .. 1) := (1 => Character'Val (10));

    function Create (Ignore : not null access Nothing) return Blockcat is
    begin
        return Create;
    end Create;

    function Create return Blockcat is
        Result : constant Blockcat := (Empty_Datum with
            Length => 0,
            Code => "");
    begin
        return Result;
    end Create;

    function Extract (From : Blockcat) return String is
    begin
        return From.Code;
    end Extract;

    -- Direct operators
    function NUM_0 (This : Blockcat) return Datatype'Class is
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + 1,
            Code => This.Code & "0");
    begin
        return Result;
    end NUM_0;

    function NUM_0 (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Child : constant Blockcat := Blockcat (Block.all (Create));
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + Child.Length + 3,
            Code => This.Code & "0 " & Child.Code & New_Line);
    begin
        return Result;
    end NUM_0;

    function NUM_1 (This : Blockcat) return Datatype'Class is
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + 1,
            Code => This.Code & "1");
    begin
        return Result;
    end NUM_1;

    function NUM_1 (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Child : constant Blockcat := Blockcat (Block.all (Create));
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + Child.Length + 3,
            Code => This.Code & "1 " & Child.Code & New_Line);
    begin
        return Result;
    end NUM_1;

    function NUM_2 (This : Blockcat) return Datatype'Class is
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + 1,
            Code => This.Code & "2");
    begin
        return Result;
    end NUM_2;

    function NUM_2 (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Child : constant Blockcat := Blockcat (Block.all (Create));
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + Child.Length + 3,
            Code => This.Code & "2 " & Child.Code & New_Line);
    begin
        return Result;
    end NUM_2;

    function NUM_3 (This : Blockcat) return Datatype'Class is
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + 1,
            Code => This.Code & "3");
    begin
        return Result;
    end NUM_3;

    function NUM_3 (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Child : constant Blockcat := Blockcat (Block.all (Create));
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + Child.Length + 3,
            Code => This.Code & "3 " & Child.Code & New_Line);
    begin
        return Result;
    end NUM_3;

    function NUM_4 (This : Blockcat) return Datatype'Class is
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + 1,
            Code => This.Code & "4");
    begin
        return Result;
    end NUM_4;

    function NUM_4 (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Child : constant Blockcat := Blockcat (Block.all (Create));
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + Child.Length + 3,
            Code => This.Code & "4 " & Child.Code & New_Line);
    begin
        return Result;
    end NUM_4;

    function NUM_5 (This : Blockcat) return Datatype'Class is
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + 1,
            Code => This.Code & "5");
    begin
        return Result;
    end NUM_5;

    function NUM_5 (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Child : constant Blockcat := Blockcat (Block.all (Create));
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + Child.Length + 3,
            Code => This.Code & "5 " & Child.Code & New_Line);
    begin
        return Result;
    end NUM_5;

    function NUM_6 (This : Blockcat) return Datatype'Class is
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + 1,
            Code => This.Code & "6");
    begin
        return Result;
    end NUM_6;

    function NUM_6 (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Child : constant Blockcat := Blockcat (Block.all (Create));
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + Child.Length + 3,
            Code => This.Code & "6 " & Child.Code & New_Line);
    begin
        return Result;
    end NUM_6;

    function NUM_7 (This : Blockcat) return Datatype'Class is
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + 1,
            Code => This.Code & "7");
    begin
        return Result;
    end NUM_7;

    function NUM_7 (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Child : constant Blockcat := Blockcat (Block.all (Create));
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + Child.Length + 3,
            Code => This.Code & "7 " & Child.Code & New_Line);
    begin
        return Result;
    end NUM_7;

    function NUM_8 (This : Blockcat) return Datatype'Class is
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + 1,
            Code => This.Code & "8");
    begin
        return Result;
    end NUM_8;

    function NUM_8 (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Child : constant Blockcat := Blockcat (Block.all (Create));
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + Child.Length + 3,
            Code => This.Code & "8 " & Child.Code & New_Line);
    begin
        return Result;
    end NUM_8;

    function NUM_9 (This : Blockcat) return Datatype'Class is
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + 1,
            Code => This.Code & "9");
    begin
        return Result;
    end NUM_9;

    function NUM_9 (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Child : constant Blockcat := Blockcat (Block.all (Create));
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + Child.Length + 3,
            Code => This.Code & "9 " & Child.Code & New_Line);
    begin
        return Result;
    end NUM_9;

    function A_MAJ (This : Blockcat) return Datatype'Class is
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + 1,
            Code => This.Code & "A");
    begin
        return Result;
    end A_MAJ;

    function A_MAJ (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Child : constant Blockcat := Blockcat (Block.all (Create));
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + Child.Length + 3,
            Code => This.Code & "A " & Child.Code & New_Line);
    begin
        return Result;
    end A_MAJ;

    function B_MAJ (This : Blockcat) return Datatype'Class is
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + 1,
            Code => This.Code & "B");
    begin
        return Result;
    end B_MAJ;

    function B_MAJ (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Child : constant Blockcat := Blockcat (Block.all (Create));
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + Child.Length + 3,
            Code => This.Code & "B " & Child.Code & New_Line);
    begin
        return Result;
    end B_MAJ;

    function C_MAJ (This : Blockcat) return Datatype'Class is
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + 1,
            Code => This.Code & "C");
    begin
        return Result;
    end C_MAJ;

    function C_MAJ (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Child : constant Blockcat := Blockcat (Block.all (Create));
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + Child.Length + 3,
            Code => This.Code & "C " & Child.Code & New_Line);
    begin
        return Result;
    end C_MAJ;

    function D_MAJ (This : Blockcat) return Datatype'Class is
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + 1,
            Code => This.Code & "D");
    begin
        return Result;
    end D_MAJ;

    function D_MAJ (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Child : constant Blockcat := Blockcat (Block.all (Create));
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + Child.Length + 3,
            Code => This.Code & "D " & Child.Code & New_Line);
    begin
        return Result;
    end D_MAJ;

    function E_MAJ (This : Blockcat) return Datatype'Class is
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + 1,
            Code => This.Code & "E");
    begin
        return Result;
    end E_MAJ;

    function E_MAJ (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Child : constant Blockcat := Blockcat (Block.all (Create));
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + Child.Length + 3,
            Code => This.Code & "E " & Child.Code & New_Line);
    begin
        return Result;
    end E_MAJ;

    function F_MAJ (This : Blockcat) return Datatype'Class is
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + 1,
            Code => This.Code & "F");
    begin
        return Result;
    end F_MAJ;

    function F_MAJ (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Child : constant Blockcat := Blockcat (Block.all (Create));
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + Child.Length + 3,
            Code => This.Code & "F " & Child.Code & New_Line);
    begin
        return Result;
    end F_MAJ;

    function G_MAJ (This : Blockcat) return Datatype'Class is
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + 1,
            Code => This.Code & "G");
    begin
        return Result;
    end G_MAJ;

    function G_MAJ (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Child : constant Blockcat := Blockcat (Block.all (Create));
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + Child.Length + 3,
            Code => This.Code & "G " & Child.Code & New_Line);
    begin
        return Result;
    end G_MAJ;

    function H_MAJ (This : Blockcat) return Datatype'Class is
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + 1,
            Code => This.Code & "H");
    begin
        return Result;
    end H_MAJ;

    function H_MAJ (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Child : constant Blockcat := Blockcat (Block.all (Create));
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + Child.Length + 3,
            Code => This.Code & "H " & Child.Code & New_Line);
    begin
        return Result;
    end H_MAJ;

    function I_MAJ (This : Blockcat) return Datatype'Class is
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + 1,
            Code => This.Code & "I");
    begin
        return Result;
    end I_MAJ;

    function I_MAJ (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Child : constant Blockcat := Blockcat (Block.all (Create));
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + Child.Length + 3,
            Code => This.Code & "I " & Child.Code & New_Line);
    begin
        return Result;
    end I_MAJ;

    function J_MAJ (This : Blockcat) return Datatype'Class is
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + 1,
            Code => This.Code & "J");
    begin
        return Result;
    end J_MAJ;

    function J_MAJ (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Child : constant Blockcat := Blockcat (Block.all (Create));
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + Child.Length + 3,
            Code => This.Code & "J " & Child.Code & New_Line);
    begin
        return Result;
    end J_MAJ;

    function K_MAJ (This : Blockcat) return Datatype'Class is
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + 1,
            Code => This.Code & "K");
    begin
        return Result;
    end K_MAJ;

    function K_MAJ (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Child : constant Blockcat := Blockcat (Block.all (Create));
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + Child.Length + 3,
            Code => This.Code & "K " & Child.Code & New_Line);
    begin
        return Result;
    end K_MAJ;

    function L_MAJ (This : Blockcat) return Datatype'Class is
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + 1,
            Code => This.Code & "L");
    begin
        return Result;
    end L_MAJ;

    function L_MAJ (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Child : constant Blockcat := Blockcat (Block.all (Create));
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + Child.Length + 3,
            Code => This.Code & "L " & Child.Code & New_Line);
    begin
        return Result;
    end L_MAJ;

    function M_MAJ (This : Blockcat) return Datatype'Class is
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + 1,
            Code => This.Code & "M");
    begin
        return Result;
    end M_MAJ;

    function M_MAJ (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Child : constant Blockcat := Blockcat (Block.all (Create));
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + Child.Length + 3,
            Code => This.Code & "M " & Child.Code & New_Line);
    begin
        return Result;
    end M_MAJ;

    function N_MAJ (This : Blockcat) return Datatype'Class is
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + 1,
            Code => This.Code & "N");
    begin
        return Result;
    end N_MAJ;

    function N_MAJ (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Child : constant Blockcat := Blockcat (Block.all (Create));
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + Child.Length + 3,
            Code => This.Code & "N " & Child.Code & New_Line);
    begin
        return Result;
    end N_MAJ;

    function O_MAJ (This : Blockcat) return Datatype'Class is
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + 1,
            Code => This.Code & "O");
    begin
        return Result;
    end O_MAJ;

    function O_MAJ (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Child : constant Blockcat := Blockcat (Block.all (Create));
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + Child.Length + 3,
            Code => This.Code & "O " & Child.Code & New_Line);
    begin
        return Result;
    end O_MAJ;

    function P_MAJ (This : Blockcat) return Datatype'Class is
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + 1,
            Code => This.Code & "P");
    begin
        return Result;
    end P_MAJ;

    function P_MAJ (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Child : constant Blockcat := Blockcat (Block.all (Create));
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + Child.Length + 3,
            Code => This.Code & "P " & Child.Code & New_Line);
    begin
        return Result;
    end P_MAJ;

    function Q_MAJ (This : Blockcat) return Datatype'Class is
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + 1,
            Code => This.Code & "Q");
    begin
        return Result;
    end Q_MAJ;

    function Q_MAJ (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Child : constant Blockcat := Blockcat (Block.all (Create));
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + Child.Length + 3,
            Code => This.Code & "Q " & Child.Code & New_Line);
    begin
        return Result;
    end Q_MAJ;

    function R_MAJ (This : Blockcat) return Datatype'Class is
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + 1,
            Code => This.Code & "R");
    begin
        return Result;
    end R_MAJ;

    function R_MAJ (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Child : constant Blockcat := Blockcat (Block.all (Create));
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + Child.Length + 3,
            Code => This.Code & "R " & Child.Code & New_Line);
    begin
        return Result;
    end R_MAJ;

    function S_MAJ (This : Blockcat) return Datatype'Class is
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + 1,
            Code => This.Code & "S");
    begin
        return Result;
    end S_MAJ;

    function S_MAJ (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Child : constant Blockcat := Blockcat (Block.all (Create));
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + Child.Length + 3,
            Code => This.Code & "S " & Child.Code & New_Line);
    begin
        return Result;
    end S_MAJ;

    function T_MAJ (This : Blockcat) return Datatype'Class is
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + 1,
            Code => This.Code & "T");
    begin
        return Result;
    end T_MAJ;

    function T_MAJ (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Child : constant Blockcat := Blockcat (Block.all (Create));
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + Child.Length + 3,
            Code => This.Code & "T " & Child.Code & New_Line);
    begin
        return Result;
    end T_MAJ;

    function U_MAJ (This : Blockcat) return Datatype'Class is
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + 1,
            Code => This.Code & "U");
    begin
        return Result;
    end U_MAJ;

    function U_MAJ (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Child : constant Blockcat := Blockcat (Block.all (Create));
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + Child.Length + 3,
            Code => This.Code & "U " & Child.Code & New_Line);
    begin
        return Result;
    end U_MAJ;

    function V_MAJ (This : Blockcat) return Datatype'Class is
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + 1,
            Code => This.Code & "V");
    begin
        return Result;
    end V_MAJ;

    function V_MAJ (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Child : constant Blockcat := Blockcat (Block.all (Create));
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + Child.Length + 3,
            Code => This.Code & "V " & Child.Code & New_Line);
    begin
        return Result;
    end V_MAJ;

    function W_MAJ (This : Blockcat) return Datatype'Class is
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + 1,
            Code => This.Code & "W");
    begin
        return Result;
    end W_MAJ;

    function W_MAJ (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Child : constant Blockcat := Blockcat (Block.all (Create));
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + Child.Length + 3,
            Code => This.Code & "W " & Child.Code & New_Line);
    begin
        return Result;
    end W_MAJ;

    function X_MAJ (This : Blockcat) return Datatype'Class is
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + 1,
            Code => This.Code & "X");
    begin
        return Result;
    end X_MAJ;

    function X_MAJ (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Child : constant Blockcat := Blockcat (Block.all (Create));
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + Child.Length + 3,
            Code => This.Code & "X " & Child.Code & New_Line);
    begin
        return Result;
    end X_MAJ;

    function Y_MAJ (This : Blockcat) return Datatype'Class is
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + 1,
            Code => This.Code & "Y");
    begin
        return Result;
    end Y_MAJ;

    function Y_MAJ (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Child : constant Blockcat := Blockcat (Block.all (Create));
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + Child.Length + 3,
            Code => This.Code & "Y " & Child.Code & New_Line);
    begin
        return Result;
    end Y_MAJ;

    function Z_MAJ (This : Blockcat) return Datatype'Class is
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + 1,
            Code => This.Code & "Z");
    begin
        return Result;
    end Z_MAJ;

    function Z_MAJ (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Child : constant Blockcat := Blockcat (Block.all (Create));
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + Child.Length + 3,
            Code => This.Code & "Z " & Child.Code & New_Line);
    begin
        return Result;
    end Z_MAJ;

    function A_MIN (This : Blockcat) return Datatype'Class is
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + 1,
            Code => This.Code & "a");
    begin
        return Result;
    end A_MIN;

    function A_MIN (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Child : constant Blockcat := Blockcat (Block.all (Create));
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + Child.Length + 3,
            Code => This.Code & "a " & Child.Code & New_Line);
    begin
        return Result;
    end A_MIN;

    function B_MIN (This : Blockcat) return Datatype'Class is
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + 1,
            Code => This.Code & "b");
    begin
        return Result;
    end B_MIN;

    function B_MIN (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Child : constant Blockcat := Blockcat (Block.all (Create));
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + Child.Length + 3,
            Code => This.Code & "b " & Child.Code & New_Line);
    begin
        return Result;
    end B_MIN;

    function C_MIN (This : Blockcat) return Datatype'Class is
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + 1,
            Code => This.Code & "c");
    begin
        return Result;
    end C_MIN;

    function C_MIN (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Child : constant Blockcat := Blockcat (Block.all (Create));
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + Child.Length + 3,
            Code => This.Code & "c " & Child.Code & New_Line);
    begin
        return Result;
    end C_MIN;

    function D_MIN (This : Blockcat) return Datatype'Class is
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + 1,
            Code => This.Code & "d");
    begin
        return Result;
    end D_MIN;

    function D_MIN (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Child : constant Blockcat := Blockcat (Block.all (Create));
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + Child.Length + 3,
            Code => This.Code & "d " & Child.Code & New_Line);
    begin
        return Result;
    end D_MIN;

    function E_MIN (This : Blockcat) return Datatype'Class is
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + 1,
            Code => This.Code & "e");
    begin
        return Result;
    end E_MIN;

    function E_MIN (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Child : constant Blockcat := Blockcat (Block.all (Create));
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + Child.Length + 3,
            Code => This.Code & "e " & Child.Code & New_Line);
    begin
        return Result;
    end E_MIN;

    function F_MIN (This : Blockcat) return Datatype'Class is
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + 1,
            Code => This.Code & "f");
    begin
        return Result;
    end F_MIN;

    function F_MIN (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Child : constant Blockcat := Blockcat (Block.all (Create));
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + Child.Length + 3,
            Code => This.Code & "f " & Child.Code & New_Line);
    begin
        return Result;
    end F_MIN;

    function G_MIN (This : Blockcat) return Datatype'Class is
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + 1,
            Code => This.Code & "g");
    begin
        return Result;
    end G_MIN;

    function G_MIN (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Child : constant Blockcat := Blockcat (Block.all (Create));
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + Child.Length + 3,
            Code => This.Code & "g " & Child.Code & New_Line);
    begin
        return Result;
    end G_MIN;

    function H_MIN (This : Blockcat) return Datatype'Class is
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + 1,
            Code => This.Code & "h");
    begin
        return Result;
    end H_MIN;

    function H_MIN (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Child : constant Blockcat := Blockcat (Block.all (Create));
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + Child.Length + 3,
            Code => This.Code & "h " & Child.Code & New_Line);
    begin
        return Result;
    end H_MIN;

    function I_MIN (This : Blockcat) return Datatype'Class is
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + 1,
            Code => This.Code & "i");
    begin
        return Result;
    end I_MIN;

    function I_MIN (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Child : constant Blockcat := Blockcat (Block.all (Create));
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + Child.Length + 3,
            Code => This.Code & "i " & Child.Code & New_Line);
    begin
        return Result;
    end I_MIN;

    function J_MIN (This : Blockcat) return Datatype'Class is
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + 1,
            Code => This.Code & "j");
    begin
        return Result;
    end J_MIN;

    function J_MIN (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Child : constant Blockcat := Blockcat (Block.all (Create));
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + Child.Length + 3,
            Code => This.Code & "j " & Child.Code & New_Line);
    begin
        return Result;
    end J_MIN;

    function K_MIN (This : Blockcat) return Datatype'Class is
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + 1,
            Code => This.Code & "k");
    begin
        return Result;
    end K_MIN;

    function K_MIN (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Child : constant Blockcat := Blockcat (Block.all (Create));
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + Child.Length + 3,
            Code => This.Code & "k " & Child.Code & New_Line);
    begin
        return Result;
    end K_MIN;

    function L_MIN (This : Blockcat) return Datatype'Class is
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + 1,
            Code => This.Code & "l");
    begin
        return Result;
    end L_MIN;

    function L_MIN (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Child : constant Blockcat := Blockcat (Block.all (Create));
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + Child.Length + 3,
            Code => This.Code & "l " & Child.Code & New_Line);
    begin
        return Result;
    end L_MIN;

    function M_MIN (This : Blockcat) return Datatype'Class is
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + 1,
            Code => This.Code & "m");
    begin
        return Result;
    end M_MIN;

    function M_MIN (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Child : constant Blockcat := Blockcat (Block.all (Create));
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + Child.Length + 3,
            Code => This.Code & "m " & Child.Code & New_Line);
    begin
        return Result;
    end M_MIN;

    function N_MIN (This : Blockcat) return Datatype'Class is
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + 1,
            Code => This.Code & "n");
    begin
        return Result;
    end N_MIN;

    function N_MIN (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Child : constant Blockcat := Blockcat (Block.all (Create));
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + Child.Length + 3,
            Code => This.Code & "n " & Child.Code & New_Line);
    begin
        return Result;
    end N_MIN;

    function O_MIN (This : Blockcat) return Datatype'Class is
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + 1,
            Code => This.Code & "o");
    begin
        return Result;
    end O_MIN;

    function O_MIN (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Child : constant Blockcat := Blockcat (Block.all (Create));
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + Child.Length + 3,
            Code => This.Code & "o " & Child.Code & New_Line);
    begin
        return Result;
    end O_MIN;

    function P_MIN (This : Blockcat) return Datatype'Class is
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + 1,
            Code => This.Code & "p");
    begin
        return Result;
    end P_MIN;

    function P_MIN (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Child : constant Blockcat := Blockcat (Block.all (Create));
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + Child.Length + 3,
            Code => This.Code & "p " & Child.Code & New_Line);
    begin
        return Result;
    end P_MIN;

    function Q_MIN (This : Blockcat) return Datatype'Class is
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + 1,
            Code => This.Code & "q");
    begin
        return Result;
    end Q_MIN;

    function Q_MIN (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Child : constant Blockcat := Blockcat (Block.all (Create));
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + Child.Length + 3,
            Code => This.Code & "q " & Child.Code & New_Line);
    begin
        return Result;
    end Q_MIN;

    function R_MIN (This : Blockcat) return Datatype'Class is
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + 1,
            Code => This.Code & "r");
    begin
        return Result;
    end R_MIN;

    function R_MIN (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Child : constant Blockcat := Blockcat (Block.all (Create));
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + Child.Length + 3,
            Code => This.Code & "r " & Child.Code & New_Line);
    begin
        return Result;
    end R_MIN;

    function S_MIN (This : Blockcat) return Datatype'Class is
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + 1,
            Code => This.Code & "s");
    begin
        return Result;
    end S_MIN;

    function S_MIN (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Child : constant Blockcat := Blockcat (Block.all (Create));
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + Child.Length + 3,
            Code => This.Code & "s " & Child.Code & New_Line);
    begin
        return Result;
    end S_MIN;

    function T_MIN (This : Blockcat) return Datatype'Class is
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + 1,
            Code => This.Code & "t");
    begin
        return Result;
    end T_MIN;

    function T_MIN (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Child : constant Blockcat := Blockcat (Block.all (Create));
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + Child.Length + 3,
            Code => This.Code & "t " & Child.Code & New_Line);
    begin
        return Result;
    end T_MIN;

    function U_MIN (This : Blockcat) return Datatype'Class is
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + 1,
            Code => This.Code & "u");
    begin
        return Result;
    end U_MIN;

    function U_MIN (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Child : constant Blockcat := Blockcat (Block.all (Create));
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + Child.Length + 3,
            Code => This.Code & "u " & Child.Code & New_Line);
    begin
        return Result;
    end U_MIN;

    function V_MIN (This : Blockcat) return Datatype'Class is
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + 1,
            Code => This.Code & "v");
    begin
        return Result;
    end V_MIN;

    function V_MIN (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Child : constant Blockcat := Blockcat (Block.all (Create));
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + Child.Length + 3,
            Code => This.Code & "v " & Child.Code & New_Line);
    begin
        return Result;
    end V_MIN;

    function W_MIN (This : Blockcat) return Datatype'Class is
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + 1,
            Code => This.Code & "w");
    begin
        return Result;
    end W_MIN;

    function W_MIN (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Child : constant Blockcat := Blockcat (Block.all (Create));
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + Child.Length + 3,
            Code => This.Code & "w " & Child.Code & New_Line);
    begin
        return Result;
    end W_MIN;

    function X_MIN (This : Blockcat) return Datatype'Class is
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + 1,
            Code => This.Code & "x");
    begin
        return Result;
    end X_MIN;

    function X_MIN (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Child : constant Blockcat := Blockcat (Block.all (Create));
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + Child.Length + 3,
            Code => This.Code & "x " & Child.Code & New_Line);
    begin
        return Result;
    end X_MIN;

    function Y_MIN (This : Blockcat) return Datatype'Class is
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + 1,
            Code => This.Code & "y");
    begin
        return Result;
    end Y_MIN;

    function Y_MIN (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Child : constant Blockcat := Blockcat (Block.all (Create));
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + Child.Length + 3,
            Code => This.Code & "y " & Child.Code & New_Line);
    begin
        return Result;
    end Y_MIN;

    function Z_MIN (This : Blockcat) return Datatype'Class is
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + 1,
            Code => This.Code & "z");
    begin
        return Result;
    end Z_MIN;

    function Z_MIN (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Child : constant Blockcat := Blockcat (Block.all (Create));
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + Child.Length + 3,
            Code => This.Code & "z " & Child.Code & New_Line);
    begin
        return Result;
    end Z_MIN;

    function ADDIT (This : Blockcat) return Datatype'Class is
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + 1,
            Code => This.Code & "+");
    begin
        return Result;
    end ADDIT;

    function ADDIT (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Child : constant Blockcat := Blockcat (Block.all (Create));
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + Child.Length + 3,
            Code => This.Code & "+ " & Child.Code & New_Line);
    begin
        return Result;
    end ADDIT;

    function SLASH (This : Blockcat) return Datatype'Class is
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + 1,
            Code => This.Code & "/");
    begin
        return Result;
    end SLASH;

    function SLASH (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Child : constant Blockcat := Blockcat (Block.all (Create));
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + Child.Length + 3,
            Code => This.Code & "/ " & Child.Code & New_Line);
    begin
        return Result;
    end SLASH;

    function MINUS (This : Blockcat) return Datatype'Class is
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + 1,
            Code => This.Code & "-");
    begin
        return Result;
    end MINUS;

    function MINUS (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Child : constant Blockcat := Blockcat (Block.all (Create));
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + Child.Length + 3,
            Code => This.Code & "- " & Child.Code & New_Line);
    begin
        return Result;
    end MINUS;

    function TIMES (This : Blockcat) return Datatype'Class is
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + 1,
            Code => This.Code & "*");
    begin
        return Result;
    end TIMES;

    function TIMES (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Child : constant Blockcat := Blockcat (Block.all (Create));
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + Child.Length + 3,
            Code => This.Code & "* " & Child.Code & New_Line);
    begin
        return Result;
    end TIMES;

    function VPIPE (This : Blockcat) return Datatype'Class is
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + 1,
            Code => This.Code & "|");
    begin
        return Result;
    end VPIPE;

    function VPIPE (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Child : constant Blockcat := Blockcat (Block.all (Create));
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + Child.Length + 3,
            Code => This.Code & "| " & Child.Code & New_Line);
    begin
        return Result;
    end VPIPE;

    function CARET (This : Blockcat) return Datatype'Class is
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + 1,
            Code => This.Code & "^");
    begin
        return Result;
    end CARET;

    function CARET (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Child : constant Blockcat := Blockcat (Block.all (Create));
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + Child.Length + 3,
            Code => This.Code & "^ " & Child.Code & New_Line);
    begin
        return Result;
    end CARET;

    function AMPER (This : Blockcat) return Datatype'Class is
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + 1,
            Code => This.Code & "&");
    begin
        return Result;
    end AMPER;

    function AMPER (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Child : constant Blockcat := Blockcat (Block.all (Create));
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + Child.Length + 3,
            Code => This.Code & "& " & Child.Code & New_Line);
    begin
        return Result;
    end AMPER;

    function PRCNT (This : Blockcat) return Datatype'Class is
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + 1,
            Code => This.Code & "%");
    begin
        return Result;
    end PRCNT;

    function PRCNT (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Child : constant Blockcat := Blockcat (Block.all (Create));
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + Child.Length + 3,
            Code => This.Code & "% " & Child.Code & New_Line);
    begin
        return Result;
    end PRCNT;

    function LESST (This : Blockcat) return Datatype'Class is
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + 1,
            Code => This.Code & "<");
    begin
        return Result;
    end LESST;

    function LESST (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Child : constant Blockcat := Blockcat (Block.all (Create));
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + Child.Length + 3,
            Code => This.Code & "< " & Child.Code & New_Line);
    begin
        return Result;
    end LESST;

    function MORET (This : Blockcat) return Datatype'Class is
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + 1,
            Code => This.Code & ">");
    begin
        return Result;
    end MORET;

    function MORET (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Child : constant Blockcat := Blockcat (Block.all (Create));
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + Child.Length + 3,
            Code => This.Code & "> " & Child.Code & New_Line);
    begin
        return Result;
    end MORET;

    function PAR_L (This : Blockcat) return Datatype'Class is
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + 1,
            Code => This.Code & "(");
    begin
        return Result;
    end PAR_L;

    function PAR_L (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Child : constant Blockcat := Blockcat (Block.all (Create));
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + Child.Length + 3,
            Code => This.Code & "( " & Child.Code & New_Line);
    begin
        return Result;
    end PAR_L;

    function PAR_R (This : Blockcat) return Datatype'Class is
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + 1,
            Code => This.Code & ")");
    begin
        return Result;
    end PAR_R;

    function PAR_R (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Child : constant Blockcat := Blockcat (Block.all (Create));
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + Child.Length + 3,
            Code => This.Code & ") " & Child.Code & New_Line);
    begin
        return Result;
    end PAR_R;

    function BRA_L (This : Blockcat) return Datatype'Class is
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + 1,
            Code => This.Code & "[");
    begin
        return Result;
    end BRA_L;

    function BRA_L (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Child : constant Blockcat := Blockcat (Block.all (Create));
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + Child.Length + 3,
            Code => This.Code & "[ " & Child.Code & New_Line);
    begin
        return Result;
    end BRA_L;

    function BRA_R (This : Blockcat) return Datatype'Class is
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + 1,
            Code => This.Code & "]");
    begin
        return Result;
    end BRA_R;

    function BRA_R (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Child : constant Blockcat := Blockcat (Block.all (Create));
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + Child.Length + 3,
            Code => This.Code & "] " & Child.Code & New_Line);
    begin
        return Result;
    end BRA_R;

    function CUR_L (This : Blockcat) return Datatype'Class is
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + 1,
            Code => This.Code & "{");
    begin
        return Result;
    end CUR_L;

    function CUR_L (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Child : constant Blockcat := Blockcat (Block.all (Create));
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + Child.Length + 3,
            Code => This.Code & "{ " & Child.Code & New_Line);
    begin
        return Result;
    end CUR_L;

    function CUR_R (This : Blockcat) return Datatype'Class is
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + 1,
            Code => This.Code & "}");
    begin
        return Result;
    end CUR_R;

    function CUR_R (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Child : constant Blockcat := Blockcat (Block.all (Create));
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + Child.Length + 3,
            Code => This.Code & "} " & Child.Code & New_Line);
    begin
        return Result;
    end CUR_R;

    function O_DOT (This : Blockcat) return Datatype'Class is
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + 1,
            Code => This.Code & ".");
    begin
        return Result;
    end O_DOT;

    function O_DOT (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Child : constant Blockcat := Blockcat (Block.all (Create));
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + Child.Length + 3,
            Code => This.Code & ". " & Child.Code & New_Line);
    begin
        return Result;
    end O_DOT;

    function COMMA (This : Blockcat) return Datatype'Class is
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + 1,
            Code => This.Code & ",");
    begin
        return Result;
    end COMMA;

    function COMMA (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Child : constant Blockcat := Blockcat (Block.all (Create));
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + Child.Length + 3,
            Code => This.Code & ", " & Child.Code & New_Line);
    begin
        return Result;
    end COMMA;

    function SEMIC (This : Blockcat) return Datatype'Class is
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + 1,
            Code => This.Code & ";");
    begin
        return Result;
    end SEMIC;

    function SEMIC (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Child : constant Blockcat := Blockcat (Block.all (Create));
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + Child.Length + 3,
            Code => This.Code & "; " & Child.Code & New_Line);
    begin
        return Result;
    end SEMIC;

    function COLON (This : Blockcat) return Datatype'Class is
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + 1,
            Code => This.Code & ":");
    begin
        return Result;
    end COLON;

    function COLON (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Child : constant Blockcat := Blockcat (Block.all (Create));
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + Child.Length + 3,
            Code => This.Code & ": " & Child.Code & New_Line);
    begin
        return Result;
    end COLON;

    function APOST (This : Blockcat) return Datatype'Class is
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + 1,
            Code => This.Code & "'");
    begin
        return Result;
    end APOST;

    function APOST (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Child : constant Blockcat := Blockcat (Block.all (Create));
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + Child.Length + 3,
            Code => This.Code & "' " & Child.Code & New_Line);
    begin
        return Result;
    end APOST;

    function QUOTE (This : Blockcat) return Datatype'Class is
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + 1,
            Code => This.Code & """");
    begin
        return Result;
    end QUOTE;

    function QUOTE (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Child : constant Blockcat := Blockcat (Block.all (Create));
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + Child.Length + 3,
            Code => This.Code & """ " & Child.Code & New_Line);
    begin
        return Result;
    end QUOTE;

    function BTICK (This : Blockcat) return Datatype'Class is
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + 1,
            Code => This.Code & "`");
    begin
        return Result;
    end BTICK;

    function BTICK (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Child : constant Blockcat := Blockcat (Block.all (Create));
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + Child.Length + 3,
            Code => This.Code & "` " & Child.Code & New_Line);
    begin
        return Result;
    end BTICK;

    function TILDE (This : Blockcat) return Datatype'Class is
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + 1,
            Code => This.Code & "~");
    begin
        return Result;
    end TILDE;

    function TILDE (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Child : constant Blockcat := Blockcat (Block.all (Create));
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + Child.Length + 3,
            Code => This.Code & "~ " & Child.Code & New_Line);
    begin
        return Result;
    end TILDE;

    function EXCLA (This : Blockcat) return Datatype'Class is
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + 1,
            Code => This.Code & "!");
    begin
        return Result;
    end EXCLA;

    function EXCLA (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Child : constant Blockcat := Blockcat (Block.all (Create));
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + Child.Length + 3,
            Code => This.Code & "! " & Child.Code & New_Line);
    begin
        return Result;
    end EXCLA;

    function ATSGN (This : Blockcat) return Datatype'Class is
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + 1,
            Code => This.Code & "@");
    begin
        return Result;
    end ATSGN;

    function ATSGN (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Child : constant Blockcat := Blockcat (Block.all (Create));
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + Child.Length + 3,
            Code => This.Code & "@ " & Child.Code & New_Line);
    begin
        return Result;
    end ATSGN;

    function POUND (This : Blockcat) return Datatype'Class is
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + 1,
            Code => This.Code & "#");
    begin
        return Result;
    end POUND;

    function POUND (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Child : constant Blockcat := Blockcat (Block.all (Create));
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + Child.Length + 3,
            Code => This.Code & "# " & Child.Code & New_Line);
    begin
        return Result;
    end POUND;

    function MONEY (This : Blockcat) return Datatype'Class is
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + 1,
            Code => This.Code & "$");
    begin
        return Result;
    end MONEY;

    function MONEY (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Child : constant Blockcat := Blockcat (Block.all (Create));
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + Child.Length + 3,
            Code => This.Code & "$ " & Child.Code & New_Line);
    begin
        return Result;
    end MONEY;

    function QUERY (This : Blockcat) return Datatype'Class is
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + 1,
            Code => This.Code & "?");
    begin
        return Result;
    end QUERY;

    function QUERY (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Child : constant Blockcat := Blockcat (Block.all (Create));
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + Child.Length + 3,
            Code => This.Code & "? " & Child.Code & New_Line);
    begin
        return Result;
    end QUERY;

    function UNDER (This : Blockcat) return Datatype'Class is
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + 1,
            Code => This.Code & "_");
    begin
        return Result;
    end UNDER;

    function UNDER (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Child : constant Blockcat := Blockcat (Block.all (Create));
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + Child.Length + 3,
            Code => This.Code & "_ " & Child.Code & New_Line);
    begin
        return Result;
    end UNDER;

    function EQUAL (This : Blockcat) return Datatype'Class is
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + 1,
            Code => This.Code & "=");
    begin
        return Result;
    end EQUAL;

    function EQUAL (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Child : constant Blockcat := Blockcat (Block.all (Create));
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + Child.Length + 3,
            Code => This.Code & "= " & Child.Code & New_Line);
    begin
        return Result;
    end EQUAL;

    function HSALS (This : Blockcat) return Datatype'Class is
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + 1,
            Code => This.Code & "\");
    begin
        return Result;
    end HSALS;

    function HSALS (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Child : constant Blockcat := Blockcat (Block.all (Create));
        Result : constant Blockcat := (Empty_Datum with
            Length => This.Length + Child.Length + 3,
            Code => This.Code & "\ " & Child.Code & New_Line);
    begin
        return Result;
    end HSALS;

    function Read (From : not null access Inputter) return Blockcat is
        pragma Unreferenced (From);
    begin
        return Create;
    end Read;

    procedure Write (This : Blockcat; To : in out Outputter) is
    begin
        To.Put_String (This.Extract);
    end Write;

    -- Debugging and utility functions
    function Image (This : Blockcat) return String is
    begin
        return This.Extract;
    end Image;

    function Type_Image (This : Blockcat) return String is
    begin
        return "(lambda " & This.Image & ")";
    end Type_Image;
end Sbit.Blockcats;
