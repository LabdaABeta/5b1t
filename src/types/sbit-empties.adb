with Sbit.Naturals;
with Big_Integers;
with Type_Registration;
with Ada.Tags.Generic_Dispatching_Constructor;
with Sbit.Blockcats;
with Sbit.Typenames;

package body Sbit.Empties is
    function Create (Ignore : not null access Nothing) return Empty is
    begin
        return (Empty_Datum with null record);
    end Create;

    function Instance return Empty is
    begin
        return (Empty_Datum with null record);
    end Instance;

    -- Direct operators
    function NUM_0 (This : Empty) return Datatype'Class is
        pragma Unreferenced (This);
    begin
        return Naturals.Create (Big_Integers.Create (0));
    end NUM_0;

    function I_MIN (
        This : Empty;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class is
        pragma Unreferenced (This);

        function Constructor is new Ada.Tags.Generic_Dispatching_Constructor (
            T => Datatype,
            Parameters => Inputter,
            Constructor => Read);

        Type_String : constant String :=
            Block.all (Sbit.Blockcats.Create).Image;
        Type_Listing : Listing (Type_String'Range);
    begin
        for I in Type_String'Range loop
            Type_Listing (I) := To_Code (Type_String (I));
        end loop;

        return Constructor (
            The_Tag => Type_Registration.Reduce (
                From => Typenames.To_Nearest_Package (Type_Listing),
                Default => Empty'Tag),
            Params => Input);
    end I_MIN;

    function Read (From : not null access Inputter) return Empty is
        pragma Unreferenced (From);
    begin
        return (Empty_Datum with null record);
    end Read;

    procedure Write (This : Empty; To : in out Outputter) is
        pragma Unreferenced (This);
        pragma Unreferenced (To);
    begin
        null;
    end Write;

    -- Debugging and utility functions
    function Image (This : Empty) return String is
        pragma Unreferenced (This);
    begin
        return "";
    end Image;

    function Type_Image (This : Empty) return String is
        pragma Unreferenced (This);
    begin
        return "0";
    end Type_Image;
begin
    Type_Registration.Register ("Empties", Empty'Tag);
end Sbit.Empties;
