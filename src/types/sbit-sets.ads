with Ada.Tags;

package Sbit.Sets is
    type Set is new Collection with private;

    function Create (Ignore : not null access Nothing) return Set;
    function Extract (From : not null access Set) return Set;

    function Is_Set (From : Ada.Tags.Tag) return Boolean;

    -- May return any child class of set
    function Child_Reduce (From : Set) return Set'Class;

    function Empty_Set return Set;

    procedure Insert (Into : in out Set; Item : Datatype'Class);
    function Size (From : Set) return Natural;

    -- Direct operators
    function F_MAJ (This : Set) return Datatype'Class;
    function F_MIN (This : Set) return Datatype'Class;

    function Read (From : not null access Inputter) return Set;
    procedure Write (This : Set; To : in out Outputter);

    -- Debugging and utility functions
    function Image (This : Set) return String;
    function Type_Image (This : Set) return String;

    function Elements (From : Set) return Data_Lists.List;
private
    type Set is new Collection with
        record
            Value : Data_Sets.Set;
        end record;
end Sbit.Sets;
