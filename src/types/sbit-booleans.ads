package Sbit.Booleans is
    type Boolean is new Datatype with private;

    function Create (Ignore : not null access Nothing) return Boolean;
    function Create (From : Standard.Boolean) return Boolean;

    function Get_Value (From : Boolean) return Standard.Boolean;

    function "not" (This : Boolean) return Boolean;

    -- Direct operators
    function EXCLA (This : Boolean) return Sbit.Datatype'Class;

    function Read (From : not null access Inputter) return Boolean;
    procedure Write (This : Boolean; To : in out Outputter);

    -- Debugging and utility functions
    function Image (This : Boolean) return String;
    function Type_Image (This : Boolean) return String;

private
    type Boolean is new Datatype with
        record
            Value : Standard.Boolean;
        end record;
end Sbit.Booleans;
