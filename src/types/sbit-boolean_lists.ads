with Sbit.Lists;

package Sbit.Boolean_Lists is
    type Boolean_List is new Lists.List with private;

    function Create (Ignore : not null access Nothing) return Boolean_List;
    function Extract (From : not null access Lists.List) return Boolean_List;
    function From_List (From : Lists.List) return Boolean_List;

    function Empty_List return Boolean_List;

    -- Direct operators
    function VPIPE (This : Boolean_List) return Datatype'Class;
    function CARET (This : Boolean_List) return Datatype'Class;
    function AMPER (This : Boolean_List) return Datatype'Class;

    function Read (From : not null access Inputter) return Boolean_List;
    procedure Write (This : Boolean_List; To : in out Outputter);

    -- Debugging and utility functions
    function Image (This : Boolean_List) return String;
    function Type_Image (This : Boolean_List) return String;
private
    type Boolean_List is new Lists.List with null record;
end Sbit.Boolean_Lists;
