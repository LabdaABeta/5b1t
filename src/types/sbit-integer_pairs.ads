with Sbit.Tuples;
with Sbit.Integers;

package Sbit.Integer_Pairs is
    type Integer_Pair is new Tuples.Tuple with private;

    function Create (Ignore : not null access Nothing) return Integer_Pair;
    function Extract (From : not null access Tuples.Tuple) return Integer_Pair;
    function From_Tuple (From : Tuples.Tuple) return Integer_Pair;

    function Create (Left, Right : Integers.Integer) return Integer_Pair;
    function Get_Left (From : Integer_Pair) return Integers.Integer;
    function Get_Right (From : Integer_Pair) return Integers.Integer;

    -- Direct operators
    function ADDIT (This : Integer_Pair) return Datatype'Class;

    function Read (From : not null access Inputter) return Integer_Pair;
    procedure Write (This : Integer_Pair; To : in out Outputter);

    -- Debugging and utility functions
    function Image (This : Integer_Pair) return String;
    function Type_Image (This : Integer_Pair) return String;
private
    type Integer_Pair is new Tuples.Tuple with null record;
end Sbit.Integer_Pairs;
