with Big_Integers;

package Sbit.Integers is
    type Integer is new Datatype with private;

    function Create (Ignore : not null access Nothing) return Integer;
    function Create (From : Big_Integers.Big_Integer) return Integer;
    function Get_Value (From : Integer) return Big_Integers.Big_Integer;

    -- Direct operators
    function D_MAJ (This : Integer) return Datatype'Class;
    function E_MAJ (This : Integer) return Datatype'Class;
    function R_MAJ (This : Integer) return Datatype'Class;
    function R_MIN (This : Integer) return Datatype'Class;
    function S_MIN (This : Integer) return Datatype'Class;
    function ADDIT (This : Integer) return Datatype'Class;
    function MINUS (This : Integer) return Datatype'Class;

    function Read (From : not null access Inputter) return Integer;
    procedure Write (This : Integer; To : in out Outputter);

    -- Debugging and utility functions
    function Image (This : Integer) return String;
    function Type_Image (This : Integer) return String;

private
    type Integer is new Datatype with
        record
            Value : Big_Integers.Big_Integer;
        end record;
end Sbit.Integers;
