with Big_Integers;

package Sbit.BNFs is
    type BNF is new Datatype with private;

    function Create (Ignore : not null access Nothing) return BNF;
    function Zero_BNF return BNF;

    function Get_Value (From : BNF) return Big_Integers.Big_Integer;

    -- Direct operators
    function NUM_0 (This : BNF) return Datatype'Class;
    function NUM_0 (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function NUM_1 (This : BNF) return Datatype'Class;
    function NUM_1 (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function NUM_2 (This : BNF) return Datatype'Class;
    function NUM_2 (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function NUM_3 (This : BNF) return Datatype'Class;
    function NUM_3 (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function NUM_4 (This : BNF) return Datatype'Class;
    function NUM_4 (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function NUM_5 (This : BNF) return Datatype'Class;
    function NUM_5 (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function NUM_6 (This : BNF) return Datatype'Class;
    function NUM_6 (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function NUM_7 (This : BNF) return Datatype'Class;
    function NUM_7 (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function NUM_8 (This : BNF) return Datatype'Class;
    function NUM_8 (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function NUM_9 (This : BNF) return Datatype'Class;
    function NUM_9 (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function A_MAJ (This : BNF) return Datatype'Class;
    function A_MAJ (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function B_MAJ (This : BNF) return Datatype'Class;
    function B_MAJ (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function C_MAJ (This : BNF) return Datatype'Class;
    function C_MAJ (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function D_MAJ (This : BNF) return Datatype'Class;
    function D_MAJ (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function E_MAJ (This : BNF) return Datatype'Class;
    function E_MAJ (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function F_MAJ (This : BNF) return Datatype'Class;
    function F_MAJ (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function G_MAJ (This : BNF) return Datatype'Class;
    function G_MAJ (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function H_MAJ (This : BNF) return Datatype'Class;
    function H_MAJ (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function I_MAJ (This : BNF) return Datatype'Class;
    function I_MAJ (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function J_MAJ (This : BNF) return Datatype'Class;
    function J_MAJ (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function K_MAJ (This : BNF) return Datatype'Class;
    function K_MAJ (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function L_MAJ (This : BNF) return Datatype'Class;
    function L_MAJ (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function M_MAJ (This : BNF) return Datatype'Class;
    function M_MAJ (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function N_MAJ (This : BNF) return Datatype'Class;
    function N_MAJ (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function O_MAJ (This : BNF) return Datatype'Class;
    function O_MAJ (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function P_MAJ (This : BNF) return Datatype'Class;
    function P_MAJ (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function Q_MAJ (This : BNF) return Datatype'Class;
    function Q_MAJ (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function R_MAJ (This : BNF) return Datatype'Class;
    function R_MAJ (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function S_MAJ (This : BNF) return Datatype'Class;
    function S_MAJ (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function T_MAJ (This : BNF) return Datatype'Class;
    function T_MAJ (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function U_MAJ (This : BNF) return Datatype'Class;
    function U_MAJ (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function V_MAJ (This : BNF) return Datatype'Class;
    function V_MAJ (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function W_MAJ (This : BNF) return Datatype'Class;
    function W_MAJ (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function X_MAJ (This : BNF) return Datatype'Class;
    function X_MAJ (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function Y_MAJ (This : BNF) return Datatype'Class;
    function Y_MAJ (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function Z_MAJ (This : BNF) return Datatype'Class;
    function Z_MAJ (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function A_MIN (This : BNF) return Datatype'Class;
    function A_MIN (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function B_MIN (This : BNF) return Datatype'Class;
    function B_MIN (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function C_MIN (This : BNF) return Datatype'Class;
    function C_MIN (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function D_MIN (This : BNF) return Datatype'Class;
    function D_MIN (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function E_MIN (This : BNF) return Datatype'Class;
    function E_MIN (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function F_MIN (This : BNF) return Datatype'Class;
    function F_MIN (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function G_MIN (This : BNF) return Datatype'Class;
    function G_MIN (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function H_MIN (This : BNF) return Datatype'Class;
    function H_MIN (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function I_MIN (This : BNF) return Datatype'Class;
    function I_MIN (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function J_MIN (This : BNF) return Datatype'Class;
    function J_MIN (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function K_MIN (This : BNF) return Datatype'Class;
    function K_MIN (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function L_MIN (This : BNF) return Datatype'Class;
    function L_MIN (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function M_MIN (This : BNF) return Datatype'Class;
    function M_MIN (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function N_MIN (This : BNF) return Datatype'Class;
    function N_MIN (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function O_MIN (This : BNF) return Datatype'Class;
    function O_MIN (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function P_MIN (This : BNF) return Datatype'Class;
    function P_MIN (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function Q_MIN (This : BNF) return Datatype'Class;
    function Q_MIN (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function R_MIN (This : BNF) return Datatype'Class;
    function R_MIN (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function S_MIN (This : BNF) return Datatype'Class;
    function S_MIN (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function T_MIN (This : BNF) return Datatype'Class;
    function T_MIN (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function U_MIN (This : BNF) return Datatype'Class;
    function U_MIN (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function V_MIN (This : BNF) return Datatype'Class;
    function V_MIN (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function W_MIN (This : BNF) return Datatype'Class;
    function W_MIN (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function X_MIN (This : BNF) return Datatype'Class;
    function X_MIN (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function Y_MIN (This : BNF) return Datatype'Class;
    function Y_MIN (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function Z_MIN (This : BNF) return Datatype'Class;
    function Z_MIN (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function ADDIT (This : BNF) return Datatype'Class;
    function ADDIT (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function SLASH (This : BNF) return Datatype'Class;
    function SLASH (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function MINUS (This : BNF) return Datatype'Class;
    function MINUS (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function TIMES (This : BNF) return Datatype'Class;
    function TIMES (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function VPIPE (This : BNF) return Datatype'Class;
    function VPIPE (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function CARET (This : BNF) return Datatype'Class;
    function CARET (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function AMPER (This : BNF) return Datatype'Class;
    function AMPER (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function PRCNT (This : BNF) return Datatype'Class;
    function PRCNT (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function LESST (This : BNF) return Datatype'Class;
    function LESST (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function MORET (This : BNF) return Datatype'Class;
    function MORET (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function PAR_L (This : BNF) return Datatype'Class;
    function PAR_L (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function PAR_R (This : BNF) return Datatype'Class;
    function PAR_R (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function BRA_L (This : BNF) return Datatype'Class;
    function BRA_L (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function BRA_R (This : BNF) return Datatype'Class;
    function BRA_R (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function CUR_L (This : BNF) return Datatype'Class;
    function CUR_L (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function CUR_R (This : BNF) return Datatype'Class;
    function CUR_R (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function O_DOT (This : BNF) return Datatype'Class;
    function O_DOT (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function COMMA (This : BNF) return Datatype'Class;
    function COMMA (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function SEMIC (This : BNF) return Datatype'Class;
    function SEMIC (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function COLON (This : BNF) return Datatype'Class;
    function COLON (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function APOST (This : BNF) return Datatype'Class;
    function APOST (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function QUOTE (This : BNF) return Datatype'Class;
    function QUOTE (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function BTICK (This : BNF) return Datatype'Class;
    function BTICK (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function TILDE (This : BNF) return Datatype'Class;
    function TILDE (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function EXCLA (This : BNF) return Datatype'Class;
    function EXCLA (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function ATSGN (This : BNF) return Datatype'Class;
    function ATSGN (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function POUND (This : BNF) return Datatype'Class;
    function POUND (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function MONEY (This : BNF) return Datatype'Class;
    function MONEY (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function QUERY (This : BNF) return Datatype'Class;
    function QUERY (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function UNDER (This : BNF) return Datatype'Class;
    function UNDER (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function EQUAL (This : BNF) return Datatype'Class;
    function EQUAL (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function HSALS (This : BNF) return Datatype'Class;
    function HSALS (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;

    function Read (From : not null access Inputter) return BNF;
    procedure Write (This : BNF; To : in out Outputter);

    -- Debugging and utility functions
    function Image (This : BNF) return String;
    function Type_Image (This : BNF) return String;

private
    type BNF is new Datatype with
        record
            Value : Big_Integers.Big_Integer;
        end record;
end Sbit.BNFs;
