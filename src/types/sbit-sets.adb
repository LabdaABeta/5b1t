with Ada.Tags.Generic_Dispatching_Constructor;
with Ada.Strings.Unbounded;
with Set_Registration;
with Tag_Utils;
with Type_Registration;

package body Sbit.Sets is
    function Create (Ignore : not null access Nothing) return Set is
    begin
        return Empty_Set;
    end Create;

    function Extract (From : not null access Set) return Set is
        Result : constant Set := From.all;
    begin
        return Result;
    end Extract;

    function Base_Tag_From (From : Set) return Ada.Tags.Tag is
        use type Ada.Tags.Tag;

        Result : Ada.Tags.Tag := Ada.Tags.No_Tag;
    begin
        if From.Value.Is_Empty then
            return Datatype'Tag;
        end if;

        for I of From.Value loop
            if Result = Ada.Tags.No_Tag then
                Result := I'Tag;
            else
                Result := Tag_Utils.Common_Ancestor (Result, I'Tag);
            end if;
        end loop;

        return Result;
    end Base_Tag_From;

    function Is_Set (From : Ada.Tags.Tag) return Boolean is
        use type Ada.Tags.Tag;
    begin
        return Tag_Utils.Common_Ancestor (Set'Tag, From) = Set'Tag;
    end Is_Set;

    function Child_Reduce (From : Set) return Set'Class is
        function Constructor is new Ada.Tags.Generic_Dispatching_Constructor (
            T => Set,
            Parameters => Set,
            Constructor => Extract);
        Copy : aliased Set := From;
        Root_Tag : constant Ada.Tags.Tag := Base_Tag_From (From);
    begin
        return Constructor (
            The_Tag => Set_Registration.Reduce (Root_Tag, Set'Tag),
            Params => Copy'Access);
    end Child_Reduce;

    function Empty_Set return Set is
        Result : constant Set := (Empty_Datum with
            Value => Data_Sets.Empty_Set);
    begin
        return Result;
    end Empty_Set;

    procedure Insert (Into : in out Set; Item : Datatype'Class) is
    begin
        Into.Value.Insert (Item);
    end Insert;

    function Size (From : Set) return Natural is
    begin
        return Natural (From.Value.Length);
    end Size;

    function F_MAJ (This : Set) return Datatype'Class is
        function Flatten (From : Data_Lists.List) return Data_Lists.List is
            Result : Data_Lists.List := Data_Lists.Empty_List;
            Was_Flat : Boolean := True;
        begin
            for Item of From loop
                if Item in Collection'Class then
                    declare
                        Item_Collection : constant Collection'Class :=
                            Collection (Item);
                    begin
                        for Part of Item_Collection.Elements loop
                            Result.Append (Part);
                        end loop;
                    end;

                    Was_Flat := False;
                else
                    Result.Append (Item);
                end if;
            end loop;

            if Was_Flat then
                return Result;
            else
                return Flatten (Result);
            end if;
        end Flatten;

        Result : Set := Empty_Set;
    begin
        for Item of Flatten (This.Elements) loop
            Result.Insert (Item);
        end loop;

        return Result;
    end F_MAJ;

    function F_MIN (This : Set) return Datatype'Class is
        Result : Set := Empty_Set;
    begin
        for Item of This.Value loop
            if Item in Collection'Class then
                declare
                    Item_Collection : constant Collection'Class :=
                        Collection (Item);
                begin
                    for Part of Item_Collection.Elements loop
                        Result.Insert (Part);
                    end loop;
                end;
            else
                Result.Insert (Item);
            end if;
        end loop;

        return Result;
    end F_MIN;

    function Read (From : not null access Inputter) return Set is
        pragma Unreferenced (From);
        Result : constant Set := Empty_Set;
    begin
        return Result;
    end Read;

    procedure Write (This : Set; To : in out Outputter) is
        First : Boolean := True;
    begin
        To.Start_Collection (COLLECTION_SET);

        for Item of This.Value loop
            if not First then
                To.Separator;
            end if;

            First := False;

            Item.Write (To);
        end loop;

        To.End_Collection;
    end Write;

    -- Debugging and utility functions
    function Image (This : Set) return String is
        Result : Ada.Strings.Unbounded.Unbounded_String :=
            Ada.Strings.Unbounded.Null_Unbounded_String;
        First : Boolean := True;
    begin
        Ada.Strings.Unbounded.Append (Result, "{");

        for Item of This.Value loop
            if not First then
                Ada.Strings.Unbounded.Append (Result, ", ");
            end if;

            First := False;

            Ada.Strings.Unbounded.Append (Result, Item.Image);
        end loop;

        Ada.Strings.Unbounded.Append (Result, "}");
        return Ada.Strings.Unbounded.To_String (Result);
    end Image;

    function Type_Image (This : Set) return String is
        Root_Tag : constant Ada.Tags.Tag := Base_Tag_From (This);
    begin
        return "{" & Type_Image (Root_Tag) & "}";
    end Type_Image;

    function Elements (From : Set) return Data_Lists.List is
        Result : Data_Lists.List := Data_Lists.Empty_List;
    begin
        for Item of From.Value loop
            Result.Append (Item);
        end loop;

        return Result;
    end Elements;
begin
    Type_Registration.Register ("Sets", Set'Tag);
end Sbit.Sets;
