package Sbit.Strings is
    type String (<>) is new Datatype with private;

    function Create (Ignore : not null access Nothing) return String;
    function Create (From : Standard.String) return String;

    -- Direct operators
    function TILDE (This : String) return Datatype'Class;

    function Read (From : not null access Inputter) return String;
    procedure Write (This : String; To : in out Outputter);

    -- Debugging and utility functions
    function Image (This : String) return Standard.String;
    function Type_Image (This : String) return Standard.String;

private
    type String (Size : Natural) is new Datatype with
        record
            Value : Standard.String (1 .. Size);
        end record;
end Sbit.Strings;
