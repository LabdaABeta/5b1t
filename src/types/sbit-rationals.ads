with Big_Rationals;

package Sbit.Rationals is
    type Rational is new Datatype with private;

    function Create (Ignore : not null access Nothing) return Rational;
    function Create (From : Big_Rationals.Big_Rational) return Rational;

    -- Direct operators
    function D_MIN (This : Rational) return Datatype'Class;
    function N_MIN (This : Rational) return Datatype'Class;

    function Read (From : not null access Inputter) return Rational;
    procedure Write (This : Rational; To : in out Outputter);

    -- Debugging and utility functions
    function Image (This : Rational) return String;
    function Type_Image (This : Rational) return String;

private
    type Rational is new Datatype with
        record
            Value : Big_Rationals.Big_Rational;
        end record;
end Sbit.Rationals;
