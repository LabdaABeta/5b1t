with Ada.Strings.Unbounded;
with Ada.Tags;
with Ada.Tags.Generic_Dispatching_Constructor;
with Tuple_Registration;
with Type_Registration;
with Sbit.Literals;
with Big_Integers;

package body Sbit.Tuples is
    use type Ada.Containers.Count_Type;

    function Create (Ignore : not null access Nothing) return Tuple is
    begin
        return Empty_Tuple;
    end Create;

    function Extract (From : not null access Tuple) return Tuple is
        Result : constant Tuple := From.all;
    begin
        return Result;
    end Extract;

    function Child_Reduce (From : Tuple) return Tuple'Class is
        function Constructor is new Ada.Tags.Generic_Dispatching_Constructor (
            T => Tuple,
            Parameters => Tuple,
            Constructor => Extract);
        Copy : aliased Tuple := From;
        Tag_List : Ada.Tags.Tag_Array (1 .. Natural (From.Value.Length));
        Next : Positive := 1;
    begin
        for Item of From.Value loop
            Tag_List (Next) := Item'Tag;
            Next := Next + 1;
        end loop;

        return Constructor (
            The_Tag => Tuple_Registration.Reduce (Tag_List, Tuple'Tag),
            Params => Copy'Access);
    end Child_Reduce;

    function Empty_Tuple return Tuple is
        Result : constant Tuple := (Empty_Datum with
            Value => Data_Vectors.Empty_Vector);
    begin
        return Result;
    end Empty_Tuple;

    procedure Append (Into : in out Tuple; Item : Datatype'Class) is
    begin
        Into.Value.Append (Item);
    end Append;

    procedure Prepend (Into : in out Tuple; Item : Datatype'Class) is
    begin
        Into.Value.Prepend (Item);
    end Prepend;

    function Get (
        From : Tuple;
        Index : Positive)
        return Datatype'Class
    is
    begin
        return From.Value.Element (Index);
    end Get;

    function Size (From : Tuple) return Natural is
    begin
        return Natural (From.Value.Length);
    end Size;

    -- Direct operators
    function Apply_0 (
        Self : Tuple;
        Argument : Big_Integers.Big_Integer)
        return Datatype'Class
    is
        Big_Length : constant Big_Integers.Big_Integer :=
            Big_Integers.Create (Natural (Self.Value.Length));
        use type Big_Integers.Big_Integer;
    begin
        if Argument < Big_Integers.Create (0) then
            return Self;
        end if;

        if Argument >= Big_Length then
            return Self;
        end if;

        return Self.Value.Element (Big_Integers.As_Integer (Argument));
    end Apply_0;

    package Apply_0_Literals is new Sbit.Literals (Tuple, Apply_0);

    function NUM_0 (This : Tuple) return Datatype'Class is (
        Apply_0_Literals.Empty_Literal (This));

    function NUM_1 (This : Tuple) return Datatype'Class is
    begin
        if This.Value.Length < 1 then
            return This;
        else
            return This.Value.Element (1);
        end if;
    end NUM_1;

    function NUM_1 (
        This : Tuple;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : Tuple := This;
    begin
        while Result.Value.Length < 1 loop
            Result.Append (This);
        end loop;

        Result.Value.Replace_Element (1, Block.all (Result.Get (1)));

        return Child_Reduce (Result);
    end NUM_1;

    function NUM_2 (This : Tuple) return Datatype'Class is
    begin
        if This.Value.Length < 2 then
            return This;
        else
            return This.Value.Element (2);
        end if;
    end NUM_2;

    function NUM_2 (
        This : Tuple;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : Tuple := This;
    begin
        while Result.Value.Length < 2 loop
            Result.Append (This);
        end loop;

        Result.Value.Replace_Element (2, Block.all (Result.Get (2)));

        return Child_Reduce (Result);
    end NUM_2;

    function NUM_3 (This : Tuple) return Datatype'Class is
    begin
        if This.Value.Length < 3 then
            return This;
        else
            return This.Value.Element (3);
        end if;
    end NUM_3;

    function NUM_3 (
        This : Tuple;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : Tuple := This;
    begin
        while Result.Value.Length < 3 loop
            Result.Append (This);
        end loop;

        Result.Value.Replace_Element (3, Block.all (Result.Get (3)));

        return Child_Reduce (Result);
    end NUM_3;

    function NUM_4 (This : Tuple) return Datatype'Class is
    begin
        if This.Value.Length < 4 then
            return This;
        else
            return This.Value.Element (4);
        end if;
    end NUM_4;

    function NUM_4 (
        This : Tuple;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : Tuple := This;
    begin
        while Result.Value.Length < 4 loop
            Result.Append (This);
        end loop;

        Result.Value.Replace_Element (4, Block.all (Result.Get (4)));

        return Child_Reduce (Result);
    end NUM_4;

    function NUM_5 (This : Tuple) return Datatype'Class is
    begin
        if This.Value.Length < 5 then
            return This;
        else
            return This.Value.Element (5);
        end if;
    end NUM_5;

    function NUM_5 (
        This : Tuple;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : Tuple := This;
    begin
        while Result.Value.Length < 5 loop
            Result.Append (This);
        end loop;

        Result.Value.Replace_Element (5, Block.all (Result.Get (5)));

        return Child_Reduce (Result);
    end NUM_5;

    function NUM_6 (This : Tuple) return Datatype'Class is
    begin
        if This.Value.Length < 6 then
            return This;
        else
            return This.Value.Element (6);
        end if;
    end NUM_6;

    function NUM_6 (
        This : Tuple;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : Tuple := This;
    begin
        while Result.Value.Length < 6 loop
            Result.Append (This);
        end loop;

        Result.Value.Replace_Element (6, Block.all (Result.Get (6)));

        return Child_Reduce (Result);
    end NUM_6;

    function NUM_7 (This : Tuple) return Datatype'Class is
    begin
        if This.Value.Length < 7 then
            return This;
        else
            return This.Value.Element (7);
        end if;
    end NUM_7;

    function NUM_7 (
        This : Tuple;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : Tuple := This;
    begin
        while Result.Value.Length < 7 loop
            Result.Append (This);
        end loop;

        Result.Value.Replace_Element (7, Block.all (Result.Get (7)));

        return Child_Reduce (Result);
    end NUM_7;

    function NUM_8 (This : Tuple) return Datatype'Class is
    begin
        if This.Value.Length < 8 then
            return This;
        else
            return This.Value.Element (8);
        end if;
    end NUM_8;

    function NUM_8 (
        This : Tuple;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : Tuple := This;
    begin
        while Result.Value.Length < 8 loop
            Result.Append (This);
        end loop;

        Result.Value.Replace_Element (8, Block.all (Result.Get (8)));

        return Child_Reduce (Result);
    end NUM_8;

    function NUM_9 (This : Tuple) return Datatype'Class is
    begin
        if This.Value.Length < 9 then
            return This;
        else
            return This.Value.Element (9);
        end if;
    end NUM_9;

    function NUM_9 (
        This : Tuple;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : Tuple := This;
    begin
        while Result.Value.Length < 9 loop
            Result.Append (This);
        end loop;

        Result.Value.Replace_Element (9, Block.all (Result.Get (9)));

        return Child_Reduce (Result);
    end NUM_9;

    function Read (From : not null access Inputter) return Tuple is
        pragma Unreferenced (From);
    begin
        return Empty_Tuple;
    end Read;

    procedure Write (This : Tuple; To : in out Outputter) is
        First : Boolean := True;
    begin
        To.Start_Collection (COLLECTION_TUPLE);

        for Item of This.Value loop
            if not First then
                To.Separator;
            end if;

            First := False;

            Item.Write (To);
        end loop;

        To.End_Collection;
    end Write;

    -- Debugging and utility functions
    function Image (This : Tuple) return String is
        Result : Ada.Strings.Unbounded.Unbounded_String :=
            Ada.Strings.Unbounded.Null_Unbounded_String;
        First : Boolean := True;
    begin
        Ada.Strings.Unbounded.Append (Result, "(");

        for Item of This.Value loop
            if not First then
                Ada.Strings.Unbounded.Append (Result, ", ");
            end if;

            First := False;

            Ada.Strings.Unbounded.Append (Result, Item.Image);
        end loop;

        Ada.Strings.Unbounded.Append (Result, ")");
        return Ada.Strings.Unbounded.To_String (Result);
    end Image;

    function Type_Image (This : Tuple) return String is
        Result : Ada.Strings.Unbounded.Unbounded_String :=
            Ada.Strings.Unbounded.Null_Unbounded_String;
        First : Boolean := True;
    begin
        Ada.Strings.Unbounded.Append (Result, "(");

        for Item of This.Value loop
            if not First then
                Ada.Strings.Unbounded.Append (Result, ", ");
            end if;

            First := False;

            Ada.Strings.Unbounded.Append (Result, Item.Type_Image);
        end loop;

        Ada.Strings.Unbounded.Append (Result, ")");
        return Ada.Strings.Unbounded.To_String (Result);
    end Type_Image;
begin
    Type_Registration.Register ("Tuples", Tuple'Tag);
end Sbit.Tuples;
