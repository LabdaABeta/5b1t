with Big_Integers; use Big_Integers;
with Type_Registration;

package body Sbit.Radigits is
    function Create (Ignore : not null access Nothing) return Radigit is
    begin
        return (Naturals.Natural with Base => 0);
    end Create;

    function Create (From : Big_Integers.Big_Integer) return Radigit is
        Result : constant Radigit := (Naturals.Create (From) with Base => 0);
    begin
        return Result;
    end Create;

    function Create (Digit, Base : Natural) return Radigit is
        Result : constant Radigit := (Naturals.Create (Create (Digit)) with
            Base => Base);
    begin
        return Result;
    end Create;

    function Read (From : not null access Inputter) return Radigit is
    begin
        return (Naturals.Read (From) with Base => From.Get_Natural);
    end Read;

    procedure Write (This : Radigit; To : in out Outputter) is
        Base_Rep : String := Natural'Image (This.Base);
    begin
        Base_Rep (Base_Rep'First) := '_';
        To.Put_String (Big_Integers.Image (This.Get_Value) & Base_Rep);
    end Write;

    -- Debugging and utility functions
    function Image (This : Radigit) return String is
        Base_Rep : String := Natural'Image (This.Base);
    begin
        Base_Rep (Base_Rep'First) := '_';
        return Naturals.Image (Naturals.Natural (This)) & Base_Rep;
    end Image;

    function Type_Image (This : Radigit) return String is
        Base_Rep : String := Natural'Image (This.Base);
    begin
        Base_Rep (Base_Rep'First) := '_';
        return "b" & Base_Rep;
    end Type_Image;
begin
    Type_Registration.Register ("Radigits", Radigit'Tag);
end Sbit.Radigits;
