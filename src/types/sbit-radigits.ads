with Big_Integers;
with Sbit.Naturals;

package Sbit.Radigits is
    type Radigit is new Naturals.Natural with private;

    function Create (Ignore : not null access Nothing) return Radigit;
    function Create (From : Big_Integers.Big_Integer) return Radigit;
    function Create (Digit, Base : Natural) return Radigit;

    -- Direct operators

    function Read (From : not null access Inputter) return Radigit;
    procedure Write (This : Radigit; To : in out Outputter);

    -- Debugging and utility functions
    function Image (This : Radigit) return String;
    function Type_Image (This : Radigit) return String;
private
    type Radigit is new Naturals.Natural with
        record
            -- Underlying Natural is the digit
            Base : Natural;
        end record;

end Sbit.Radigits;
