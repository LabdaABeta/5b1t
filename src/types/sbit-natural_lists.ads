with Sbit.Lists;
with Sbit.Integer_Lists;

package Sbit.Natural_Lists is
    type Natural_List is new Integer_Lists.Integer_List with private;

    function Create (Ignore : not null access Nothing) return Natural_List;
    function Extract (From : not null access Lists.List) return Natural_List;
    function From_List (From : Lists.List) return Natural_List;

    function Empty_List return Natural_List;

    -- Direct operators
    function P_MAJ (This : Natural_List) return Datatype'Class;
    function ADDIT (This : Natural_List) return Datatype'Class;

    function Read (From : not null access Inputter) return Natural_List;
    procedure Write (This : Natural_List; To : in out Outputter);

    -- Debugging and utility functions
    function Image (This : Natural_List) return String;
    function Type_Image (This : Natural_List) return String;

private
    type Natural_List is new Integer_Lists.Integer_List with null record;
end Sbit.Natural_Lists;
