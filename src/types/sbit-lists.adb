with Ada.Tags.Generic_Dispatching_Constructor;
with Ada.Tags;
with Ada.Strings.Unbounded;
with Sbit.Naturals;
with Big_Integers;
with Random;
with List_Registration;
with Type_Registration;
with Sbit.Literals;

package body Sbit.Lists is
    function Create (Ignore : not null access Nothing) return List is
    begin
        return Empty_List;
    end Create;

    function Extract (From : not null access List) return List is
        Result : constant List := From.all;
    begin
        return Result;
    end Extract;

    function Common_Ancestor (Left, Right : Ada.Tags.Tag) return Ada.Tags.Tag is
        use type Ada.Tags.Tag;

        Parent : Ada.Tags.Tag := Left;
    begin
        if Left = Right then
            return Left;
        end if;

        while not Ada.Tags.Is_Descendant_At_Same_Level (Right, Parent) loop
            Parent := Ada.Tags.Parent_Tag (Parent);
        end loop;

        return Parent;
    end Common_Ancestor;

    function Base_Tag_From (From : List) return Ada.Tags.Tag is
        Result : Ada.Tags.Tag;
    begin
        if From.Value.Is_Empty then
            return Datatype'Tag;
        end if;

        Result := From.Value.First_Element'Tag;

        for I of From.Value loop
            Result := Common_Ancestor (Result, I'Tag);
        end loop;

        return Result;
    end Base_Tag_From;

    function Child_Reduce (From : List) return List'Class is
        function Constructor is new Ada.Tags.Generic_Dispatching_Constructor (
            T => List,
            Parameters => List,
            Constructor => Extract);
        Copy : aliased List := From;
        Root_Tag : constant Ada.Tags.Tag := Base_Tag_From (From);
    begin
        return Constructor (
            The_Tag => List_Registration.Reduce (Root_Tag, List'Tag),
            Params => Copy'Access);
    end Child_Reduce;

    function Empty_List return List is
        Result : constant List := (Empty_Datum with
            Value => Data_Lists.Empty_List);
    begin
        return Result;
    end Empty_List;

    procedure Append (
        Into : in out List;
        Item : Datatype'Class)
    is
    begin
        Into.Value.Append (Item);
    end Append;

    function Get (
        From : List;
        Index : Positive)
        return Datatype'Class
    is
        Current : Positive := 1;
        Cursor : Data_Lists.Cursor := From.Value.First;
    begin
        while Current < Index loop
            Data_Lists.Next (Cursor);
            Current := Current + 1;
        end loop;

        if Data_Lists.Has_Element (Cursor) then
            return Data_Lists.Element (Cursor);
        else
            return Empty_Datum;
        end if;
    end Get;

    function Length (From : List) return Natural is
    begin
        return Natural (From.Value.Length);
    end Length;

    function Get_Value (From : List) return Data_Lists.List is
    begin
        return From.Value;
    end Get_Value;

    function S_MAJ (This : List) return Datatype'Class is
        Result : List := This;
        procedure Swap (A, B : Positive) is
            Current : Positive := 1;
            Cursor : Data_Lists.Cursor := This.Value.First;
            A_Cursor : Data_Lists.Cursor;
        begin
            while Current < A loop
                Data_Lists.Next (Cursor);
                Current := Current + 1;
            end loop;

            A_Cursor := Cursor;

            while Current < B loop
                Data_Lists.Next (Cursor);
                Current := Current + 1;
            end loop;

            Result.Value.Swap (A_Cursor, Cursor);
        end Swap;
    begin
        for I in reverse Positive range 1 .. Positive (Result.Value.Length) loop
            declare
                subtype Valid_Subrange is Positive range 1 .. I;
                function Next_Index is new Random.Next (Valid_Subrange);
            begin
                Swap (Next_Index, I);
            end;
        end loop;

        return Child_Reduce (Result);
    end S_MAJ;

    function Apply_Index (
        This : List;
        Argument : Big_Integers.Big_Integer)
        return Datatype'Class is
    begin
        return Get (This, Argument.As_Integer + 1);
    end Apply_Index;

    package Index_Literals is new Sbit.Literals (List, Apply_Index);

    function I_MIN (This : List) return Datatype'Class is
        Result : constant Index_Literals.Literal :=
            Index_Literals.Empty_Literal (This);
    begin
        return Result;
    end I_MIN;

    function L_MIN (This : List) return Datatype'Class is
        Result : constant Sbit.Naturals.Natural :=
            Sbit.Naturals.Create (Big_Integers.Create (This.Length));
    begin
        return Result;
    end L_MIN;

    function M_MIN (
        This : List;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : List := Empty_List;
    begin
        for Item of This.Value loop
            Result.Append (Block.all (Item));
        end loop;

        return Child_Reduce (Result);
    end M_MIN;

    function Read (From : not null access Inputter) return List is
        pragma Unreferenced (From);
        Result : constant List := Empty_List;
    begin
        return Result;
    end Read;

    procedure Write (This : List; To : in out Outputter) is
        First : Boolean := True;
    begin
        To.Start_Collection (COLLECTION_LIST);

        for Item of This.Value loop
            if not First then
                To.Separator;
            end if;

            First := False;

            Item.Write (To);
        end loop;

        To.End_Collection;
    end Write;

    -- Debugging and utility functions
    function Image (This : List) return String is
        Result : Ada.Strings.Unbounded.Unbounded_String :=
            Ada.Strings.Unbounded.Null_Unbounded_String;
        First : Boolean := True;
    begin
        Ada.Strings.Unbounded.Append (Result, "[");

        for Item of This.Value loop
            if not First then
                Ada.Strings.Unbounded.Append (Result, ", ");
            end if;

            First := False;

            Ada.Strings.Unbounded.Append (Result, Item.Image);
        end loop;

        Ada.Strings.Unbounded.Append (Result, "]");
        return Ada.Strings.Unbounded.To_String (Result);
    end Image;

    function Type_Image (This : List) return String is
        Root_Tag : constant Ada.Tags.Tag := Base_Tag_From (This);
    begin
        return "[" & Type_Image (Root_Tag) & "]";
    end Type_Image;

    function As_List (From : Data_Lists.List) return List is
    begin
        return (Empty_Datum with Value => From);
    end As_List;

    function Elements (From : List) return Data_Lists.List is
        Result : Data_Lists.List := Data_Lists.Empty_List;
    begin
        for Item of From.Value loop
            Result.Append (Item);
        end loop;

        return Result;
    end Elements;
begin
    Type_Registration.Register ("Lists", List'Tag);
end Sbit.Lists;
