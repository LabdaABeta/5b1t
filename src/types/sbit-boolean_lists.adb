with Sbit.Booleans;
with List_Registration;
with Type_Registration;

package body Sbit.Boolean_Lists is
    function Create (Ignore : not null access Nothing) return Boolean_List is
    begin
        return Empty_List;
    end Create;

    function Extract (From : not null access Lists.List) return Boolean_List is
    begin
        return From_List (From.all);
    end Extract;

    function From_List (From : Lists.List) return Boolean_List is
        Result : constant Boolean_List := (From with null record);
    begin
        return Result;
    end From_List;

    function Empty_List return Boolean_List is
        Result : constant Boolean_List :=
            (Sbit.Lists.Empty_List with null record);
    begin
        return Result;
    end Empty_List;

    function VPIPE (This : Boolean_List) return Datatype'Class is
    begin
        for I of This.Get_Value loop
            if Booleans.Get_Value (Booleans.Boolean (I)) then
                return Booleans.Create (True);
            end if;
        end loop;

        return Booleans.Create (False);
    end VPIPE;

    function CARET (This : Boolean_List) return Datatype'Class is
        Result : Booleans.Boolean := Booleans.Create (False);
        use type Booleans.Boolean;
    begin
        for I of This.Get_Value loop
            if Booleans.Get_Value (Booleans.Boolean (I)) then
                Result := not Result;
            end if;
        end loop;

        return Result;
    end CARET;

    function AMPER (This : Boolean_List) return Datatype'Class is
    begin
        for I of This.Get_Value loop
            if not Booleans.Get_Value (Booleans.Boolean (I)) then
                return Booleans.Create (False);
            end if;
        end loop;

        return Booleans.Create (True);
    end AMPER;

    function Read (From : not null access Inputter) return Boolean_List is
        pragma Unreferenced (From);
    begin
        return Empty_List;
    end Read;

    procedure Write (This : Boolean_List; To : in out Outputter) is
    begin
        Sbit.Lists.Write (Sbit.Lists.List (This), To);
    end Write;

    -- Debugging and utility functions
    function Image (This : Boolean_List) return String is
    begin
        return Sbit.Lists.Image (Sbit.Lists.List (This));
    end Image;

    function Type_Image (This : Boolean_List) return String is
    begin
        return Sbit.Lists.Type_Image (Sbit.Lists.List (This));
    end Type_Image;
begin
    List_Registration.Register (Booleans.Boolean'Tag, Boolean_List'Tag);
    Type_Registration.Register ("Boolean_Lists", Boolean_List'Tag);
end Sbit.Boolean_Lists;
