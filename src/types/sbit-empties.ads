package Sbit.Empties is
    type Empty is new Datatype with private;

    function Create (Ignore : not null access Nothing) return Empty;
    function Instance return Empty;

    -- Direct operators
    function NUM_0 (This : Empty) return Datatype'Class;
    function I_MIN (
        This : Empty;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;

    function Read (From : not null access Inputter) return Empty;
    procedure Write (This : Empty; To : in out Outputter);

    -- Debugging and utility functions
    function Image (This : Empty) return String;
    function Type_Image (This : Empty) return String;
private
    type Empty is new Datatype with null record;
end Sbit.Empties;
