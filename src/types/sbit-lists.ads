package Sbit.Lists is
    type List is new Collection with private;

    function Create (Ignore : not null access Nothing) return List;
    function Extract (From : not null access List) return List;

    -- May return any child class of list, coerces to most specific type
    function Child_Reduce (From : List) return List'Class;

    function Empty_List return List;

    procedure Append (Into : in out List; Item : Datatype'Class);
    function Get (
        From : List;
        Index : Positive)
        return Datatype'Class;
    function Length (From : List) return Natural;
    function Get_Value (From : List) return Data_Lists.List;
    function As_List (From : Data_Lists.List) return List;

    -- Direct operators
    function S_MAJ (This : List) return Datatype'Class;
    function I_MIN (This : List) return Datatype'Class;
    function L_MIN (This : List) return Datatype'Class;
    function M_MIN (
        This : List;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;

    function Read (From : not null access Inputter) return List;
    procedure Write (This : List; To : in out Outputter);

    -- Debugging and utility functions
    function Image (This : List) return String;
    function Type_Image (This : List) return String;

    function Elements (From : List) return Data_Lists.List;
private
    type List is new Collection with
        record
            Value : Data_Lists.List;
        end record;
end Sbit.Lists;
