with Big_Integers; use Big_Integers;
with GMP;

package body Sbit.BNFs is
    use type GMP.Polarity;

    function Create (Ignore : not null access Nothing) return BNF is
        Result : BNF;
    begin
        Result.Value := Create (0);
        return Result;
    end Create;

    function Zero_BNF return BNF is
        Result : constant BNF := (Empty_Datum with
            Value => Create (0));
    begin
        return Result;
    end Zero_BNF;

    function Get_Value (From : BNF) return Big_Integers.Big_Integer is
    begin
        return From.Value;
    end Get_Value;

    -- LCOV_EXCL_START: Don't compute coverage here {{{
    function NUM_0 (This : BNF) return Datatype'Class is
        Result : BNF := This;
    begin
        Result.Value := Result.Value * 94 + 0;

        return Result;
    end NUM_0;

    function NUM_0 (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : BNF := This;
        Counter : constant BNF := BNF (Block.all (Zero_BNF));
        Repeats : Big_Integer := Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 0;
            Repeats := Repeats - 1;
        end loop;

        return Result;
    end NUM_0;

    function NUM_1 (This : BNF) return Datatype'Class is
        Result : BNF := This;
    begin
        Result.Value := Result.Value * 94 + 1;

        return Result;
    end NUM_1;

    function NUM_1 (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : BNF := This;
        Counter : constant BNF := BNF (Block.all (Zero_BNF));
        Repeats : Big_Integer := Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 1;
            Repeats := Repeats - 1;
        end loop;

        return Result;
    end NUM_1;

    function NUM_2 (This : BNF) return Datatype'Class is
        Result : BNF := This;
    begin
        Result.Value := Result.Value * 94 + 2;

        return Result;
    end NUM_2;

    function NUM_2 (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : BNF := This;
        Counter : constant BNF := BNF (Block.all (Zero_BNF));
        Repeats : Big_Integer := Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 2;
            Repeats := Repeats - 1;
        end loop;

        return Result;
    end NUM_2;

    function NUM_3 (This : BNF) return Datatype'Class is
        Result : BNF := This;
    begin
        Result.Value := Result.Value * 94 + 3;

        return Result;
    end NUM_3;

    function NUM_3 (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : BNF := This;
        Counter : constant BNF := BNF (Block.all (Zero_BNF));
        Repeats : Big_Integer := Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 3;
            Repeats := Repeats - 1;
        end loop;

        return Result;
    end NUM_3;

    function NUM_4 (This : BNF) return Datatype'Class is
        Result : BNF := This;
    begin
        Result.Value := Result.Value * 94 + 4;

        return Result;
    end NUM_4;

    function NUM_4 (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : BNF := This;
        Counter : constant BNF := BNF (Block.all (Zero_BNF));
        Repeats : Big_Integer := Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 4;
            Repeats := Repeats - 1;
        end loop;

        return Result;
    end NUM_4;

    function NUM_5 (This : BNF) return Datatype'Class is
        Result : BNF := This;
    begin
        Result.Value := Result.Value * 94 + 5;

        return Result;
    end NUM_5;

    function NUM_5 (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : BNF := This;
        Counter : constant BNF := BNF (Block.all (Zero_BNF));
        Repeats : Big_Integer := Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 5;
            Repeats := Repeats - 1;
        end loop;

        return Result;
    end NUM_5;

    function NUM_6 (This : BNF) return Datatype'Class is
        Result : BNF := This;
    begin
        Result.Value := Result.Value * 94 + 6;

        return Result;
    end NUM_6;

    function NUM_6 (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : BNF := This;
        Counter : constant BNF := BNF (Block.all (Zero_BNF));
        Repeats : Big_Integer := Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 6;
            Repeats := Repeats - 1;
        end loop;

        return Result;
    end NUM_6;

    function NUM_7 (This : BNF) return Datatype'Class is
        Result : BNF := This;
    begin
        Result.Value := Result.Value * 94 + 7;

        return Result;
    end NUM_7;

    function NUM_7 (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : BNF := This;
        Counter : constant BNF := BNF (Block.all (Zero_BNF));
        Repeats : Big_Integer := Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 7;
            Repeats := Repeats - 1;
        end loop;

        return Result;
    end NUM_7;

    function NUM_8 (This : BNF) return Datatype'Class is
        Result : BNF := This;
    begin
        Result.Value := Result.Value * 94 + 8;

        return Result;
    end NUM_8;

    function NUM_8 (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : BNF := This;
        Counter : constant BNF := BNF (Block.all (Zero_BNF));
        Repeats : Big_Integer := Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 8;
            Repeats := Repeats - 1;
        end loop;

        return Result;
    end NUM_8;

    function NUM_9 (This : BNF) return Datatype'Class is
        Result : BNF := This;
    begin
        Result.Value := Result.Value * 94 + 9;

        return Result;
    end NUM_9;

    function NUM_9 (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : BNF := This;
        Counter : constant BNF := BNF (Block.all (Zero_BNF));
        Repeats : Big_Integer := Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 9;
            Repeats := Repeats - 1;
        end loop;

        return Result;
    end NUM_9;

    function A_MAJ (This : BNF) return Datatype'Class is
        Result : BNF := This;
    begin
        Result.Value := Result.Value * 94 + 10;

        return Result;
    end A_MAJ;

    function A_MAJ (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : BNF := This;
        Counter : constant BNF := BNF (Block.all (Zero_BNF));
        Repeats : Big_Integer := Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 10;
            Repeats := Repeats - 1;
        end loop;

        return Result;
    end A_MAJ;

    function B_MAJ (This : BNF) return Datatype'Class is
        Result : BNF := This;
    begin
        Result.Value := Result.Value * 94 + 11;

        return Result;
    end B_MAJ;

    function B_MAJ (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : BNF := This;
        Counter : constant BNF := BNF (Block.all (Zero_BNF));
        Repeats : Big_Integer := Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 11;
            Repeats := Repeats - 1;
        end loop;

        return Result;
    end B_MAJ;

    function C_MAJ (This : BNF) return Datatype'Class is
        Result : BNF := This;
    begin
        Result.Value := Result.Value * 94 + 12;

        return Result;
    end C_MAJ;

    function C_MAJ (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : BNF := This;
        Counter : constant BNF := BNF (Block.all (Zero_BNF));
        Repeats : Big_Integer := Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 12;
            Repeats := Repeats - 1;
        end loop;

        return Result;
    end C_MAJ;

    function D_MAJ (This : BNF) return Datatype'Class is
        Result : BNF := This;
    begin
        Result.Value := Result.Value * 94 + 13;

        return Result;
    end D_MAJ;

    function D_MAJ (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : BNF := This;
        Counter : constant BNF := BNF (Block.all (Zero_BNF));
        Repeats : Big_Integer := Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 13;
            Repeats := Repeats - 1;
        end loop;

        return Result;
    end D_MAJ;

    function E_MAJ (This : BNF) return Datatype'Class is
        Result : BNF := This;
    begin
        Result.Value := Result.Value * 94 + 14;

        return Result;
    end E_MAJ;

    function E_MAJ (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : BNF := This;
        Counter : constant BNF := BNF (Block.all (Zero_BNF));
        Repeats : Big_Integer := Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 14;
            Repeats := Repeats - 1;
        end loop;

        return Result;
    end E_MAJ;

    function F_MAJ (This : BNF) return Datatype'Class is
        Result : BNF := This;
    begin
        Result.Value := Result.Value * 94 + 15;

        return Result;
    end F_MAJ;

    function F_MAJ (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : BNF := This;
        Counter : constant BNF := BNF (Block.all (Zero_BNF));
        Repeats : Big_Integer := Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 15;
            Repeats := Repeats - 1;
        end loop;

        return Result;
    end F_MAJ;

    function G_MAJ (This : BNF) return Datatype'Class is
        Result : BNF := This;
    begin
        Result.Value := Result.Value * 94 + 16;

        return Result;
    end G_MAJ;

    function G_MAJ (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : BNF := This;
        Counter : constant BNF := BNF (Block.all (Zero_BNF));
        Repeats : Big_Integer := Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 16;
            Repeats := Repeats - 1;
        end loop;

        return Result;
    end G_MAJ;

    function H_MAJ (This : BNF) return Datatype'Class is
        Result : BNF := This;
    begin
        Result.Value := Result.Value * 94 + 17;

        return Result;
    end H_MAJ;

    function H_MAJ (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : BNF := This;
        Counter : constant BNF := BNF (Block.all (Zero_BNF));
        Repeats : Big_Integer := Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 17;
            Repeats := Repeats - 1;
        end loop;

        return Result;
    end H_MAJ;

    function I_MAJ (This : BNF) return Datatype'Class is
        Result : BNF := This;
    begin
        Result.Value := Result.Value * 94 + 18;

        return Result;
    end I_MAJ;

    function I_MAJ (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : BNF := This;
        Counter : constant BNF := BNF (Block.all (Zero_BNF));
        Repeats : Big_Integer := Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 18;
            Repeats := Repeats - 1;
        end loop;

        return Result;
    end I_MAJ;

    function J_MAJ (This : BNF) return Datatype'Class is
        Result : BNF := This;
    begin
        Result.Value := Result.Value * 94 + 19;

        return Result;
    end J_MAJ;

    function J_MAJ (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : BNF := This;
        Counter : constant BNF := BNF (Block.all (Zero_BNF));
        Repeats : Big_Integer := Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 19;
            Repeats := Repeats - 1;
        end loop;

        return Result;
    end J_MAJ;

    function K_MAJ (This : BNF) return Datatype'Class is
        Result : BNF := This;
    begin
        Result.Value := Result.Value * 94 + 20;

        return Result;
    end K_MAJ;

    function K_MAJ (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : BNF := This;
        Counter : constant BNF := BNF (Block.all (Zero_BNF));
        Repeats : Big_Integer := Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 20;
            Repeats := Repeats - 1;
        end loop;

        return Result;
    end K_MAJ;

    function L_MAJ (This : BNF) return Datatype'Class is
        Result : BNF := This;
    begin
        Result.Value := Result.Value * 94 + 21;

        return Result;
    end L_MAJ;

    function L_MAJ (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : BNF := This;
        Counter : constant BNF := BNF (Block.all (Zero_BNF));
        Repeats : Big_Integer := Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 21;
            Repeats := Repeats - 1;
        end loop;

        return Result;
    end L_MAJ;

    function M_MAJ (This : BNF) return Datatype'Class is
        Result : BNF := This;
    begin
        Result.Value := Result.Value * 94 + 22;

        return Result;
    end M_MAJ;

    function M_MAJ (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : BNF := This;
        Counter : constant BNF := BNF (Block.all (Zero_BNF));
        Repeats : Big_Integer := Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 22;
            Repeats := Repeats - 1;
        end loop;

        return Result;
    end M_MAJ;

    function N_MAJ (This : BNF) return Datatype'Class is
        Result : BNF := This;
    begin
        Result.Value := Result.Value * 94 + 23;

        return Result;
    end N_MAJ;

    function N_MAJ (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : BNF := This;
        Counter : constant BNF := BNF (Block.all (Zero_BNF));
        Repeats : Big_Integer := Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 23;
            Repeats := Repeats - 1;
        end loop;

        return Result;
    end N_MAJ;

    function O_MAJ (This : BNF) return Datatype'Class is
        Result : BNF := This;
    begin
        Result.Value := Result.Value * 94 + 24;

        return Result;
    end O_MAJ;

    function O_MAJ (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : BNF := This;
        Counter : constant BNF := BNF (Block.all (Zero_BNF));
        Repeats : Big_Integer := Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 24;
            Repeats := Repeats - 1;
        end loop;

        return Result;
    end O_MAJ;

    function P_MAJ (This : BNF) return Datatype'Class is
        Result : BNF := This;
    begin
        Result.Value := Result.Value * 94 + 25;

        return Result;
    end P_MAJ;

    function P_MAJ (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : BNF := This;
        Counter : constant BNF := BNF (Block.all (Zero_BNF));
        Repeats : Big_Integer := Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 25;
            Repeats := Repeats - 1;
        end loop;

        return Result;
    end P_MAJ;

    function Q_MAJ (This : BNF) return Datatype'Class is
        Result : BNF := This;
    begin
        Result.Value := Result.Value * 94 + 26;

        return Result;
    end Q_MAJ;

    function Q_MAJ (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : BNF := This;
        Counter : constant BNF := BNF (Block.all (Zero_BNF));
        Repeats : Big_Integer := Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 26;
            Repeats := Repeats - 1;
        end loop;

        return Result;
    end Q_MAJ;

    function R_MAJ (This : BNF) return Datatype'Class is
        Result : BNF := This;
    begin
        Result.Value := Result.Value * 94 + 27;

        return Result;
    end R_MAJ;

    function R_MAJ (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : BNF := This;
        Counter : constant BNF := BNF (Block.all (Zero_BNF));
        Repeats : Big_Integer := Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 27;
            Repeats := Repeats - 1;
        end loop;

        return Result;
    end R_MAJ;

    function S_MAJ (This : BNF) return Datatype'Class is
        Result : BNF := This;
    begin
        Result.Value := Result.Value * 94 + 28;

        return Result;
    end S_MAJ;

    function S_MAJ (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : BNF := This;
        Counter : constant BNF := BNF (Block.all (Zero_BNF));
        Repeats : Big_Integer := Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 28;
            Repeats := Repeats - 1;
        end loop;

        return Result;
    end S_MAJ;

    function T_MAJ (This : BNF) return Datatype'Class is
        Result : BNF := This;
    begin
        Result.Value := Result.Value * 94 + 29;

        return Result;
    end T_MAJ;

    function T_MAJ (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : BNF := This;
        Counter : constant BNF := BNF (Block.all (Zero_BNF));
        Repeats : Big_Integer := Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 29;
            Repeats := Repeats - 1;
        end loop;

        return Result;
    end T_MAJ;

    function U_MAJ (This : BNF) return Datatype'Class is
        Result : BNF := This;
    begin
        Result.Value := Result.Value * 94 + 30;

        return Result;
    end U_MAJ;

    function U_MAJ (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : BNF := This;
        Counter : constant BNF := BNF (Block.all (Zero_BNF));
        Repeats : Big_Integer := Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 30;
            Repeats := Repeats - 1;
        end loop;

        return Result;
    end U_MAJ;

    function V_MAJ (This : BNF) return Datatype'Class is
        Result : BNF := This;
    begin
        Result.Value := Result.Value * 94 + 31;

        return Result;
    end V_MAJ;

    function V_MAJ (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : BNF := This;
        Counter : constant BNF := BNF (Block.all (Zero_BNF));
        Repeats : Big_Integer := Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 31;
            Repeats := Repeats - 1;
        end loop;

        return Result;
    end V_MAJ;

    function W_MAJ (This : BNF) return Datatype'Class is
        Result : BNF := This;
    begin
        Result.Value := Result.Value * 94 + 32;

        return Result;
    end W_MAJ;

    function W_MAJ (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : BNF := This;
        Counter : constant BNF := BNF (Block.all (Zero_BNF));
        Repeats : Big_Integer := Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 32;
            Repeats := Repeats - 1;
        end loop;

        return Result;
    end W_MAJ;

    function X_MAJ (This : BNF) return Datatype'Class is
        Result : BNF := This;
    begin
        Result.Value := Result.Value * 94 + 33;

        return Result;
    end X_MAJ;

    function X_MAJ (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : BNF := This;
        Counter : constant BNF := BNF (Block.all (Zero_BNF));
        Repeats : Big_Integer := Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 33;
            Repeats := Repeats - 1;
        end loop;

        return Result;
    end X_MAJ;

    function Y_MAJ (This : BNF) return Datatype'Class is
        Result : BNF := This;
    begin
        Result.Value := Result.Value * 94 + 34;

        return Result;
    end Y_MAJ;

    function Y_MAJ (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : BNF := This;
        Counter : constant BNF := BNF (Block.all (Zero_BNF));
        Repeats : Big_Integer := Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 34;
            Repeats := Repeats - 1;
        end loop;

        return Result;
    end Y_MAJ;

    function Z_MAJ (This : BNF) return Datatype'Class is
        Result : BNF := This;
    begin
        Result.Value := Result.Value * 94 + 35;

        return Result;
    end Z_MAJ;

    function Z_MAJ (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : BNF := This;
        Counter : constant BNF := BNF (Block.all (Zero_BNF));
        Repeats : Big_Integer := Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 35;
            Repeats := Repeats - 1;
        end loop;

        return Result;
    end Z_MAJ;

    function A_MIN (This : BNF) return Datatype'Class is
        Result : BNF := This;
    begin
        Result.Value := Result.Value * 94 + 36;

        return Result;
    end A_MIN;

    function A_MIN (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : BNF := This;
        Counter : constant BNF := BNF (Block.all (Zero_BNF));
        Repeats : Big_Integer := Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 36;
            Repeats := Repeats - 1;
        end loop;

        return Result;
    end A_MIN;

    function B_MIN (This : BNF) return Datatype'Class is
        Result : BNF := This;
    begin
        Result.Value := Result.Value * 94 + 37;

        return Result;
    end B_MIN;

    function B_MIN (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : BNF := This;
        Counter : constant BNF := BNF (Block.all (Zero_BNF));
        Repeats : Big_Integer := Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 37;
            Repeats := Repeats - 1;
        end loop;

        return Result;
    end B_MIN;

    function C_MIN (This : BNF) return Datatype'Class is
        Result : BNF := This;
    begin
        Result.Value := Result.Value * 94 + 38;

        return Result;
    end C_MIN;

    function C_MIN (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : BNF := This;
        Counter : constant BNF := BNF (Block.all (Zero_BNF));
        Repeats : Big_Integer := Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 38;
            Repeats := Repeats - 1;
        end loop;

        return Result;
    end C_MIN;

    function D_MIN (This : BNF) return Datatype'Class is
        Result : BNF := This;
    begin
        Result.Value := Result.Value * 94 + 39;

        return Result;
    end D_MIN;

    function D_MIN (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : BNF := This;
        Counter : constant BNF := BNF (Block.all (Zero_BNF));
        Repeats : Big_Integer := Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 39;
            Repeats := Repeats - 1;
        end loop;

        return Result;
    end D_MIN;

    function E_MIN (This : BNF) return Datatype'Class is
        Result : BNF := This;
    begin
        Result.Value := Result.Value * 94 + 40;

        return Result;
    end E_MIN;

    function E_MIN (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : BNF := This;
        Counter : constant BNF := BNF (Block.all (Zero_BNF));
        Repeats : Big_Integer := Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 40;
            Repeats := Repeats - 1;
        end loop;

        return Result;
    end E_MIN;

    function F_MIN (This : BNF) return Datatype'Class is
        Result : BNF := This;
    begin
        Result.Value := Result.Value * 94 + 41;

        return Result;
    end F_MIN;

    function F_MIN (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : BNF := This;
        Counter : constant BNF := BNF (Block.all (Zero_BNF));
        Repeats : Big_Integer := Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 41;
            Repeats := Repeats - 1;
        end loop;

        return Result;
    end F_MIN;

    function G_MIN (This : BNF) return Datatype'Class is
        Result : BNF := This;
    begin
        Result.Value := Result.Value * 94 + 42;

        return Result;
    end G_MIN;

    function G_MIN (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : BNF := This;
        Counter : constant BNF := BNF (Block.all (Zero_BNF));
        Repeats : Big_Integer := Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 42;
            Repeats := Repeats - 1;
        end loop;

        return Result;
    end G_MIN;

    function H_MIN (This : BNF) return Datatype'Class is
        Result : BNF := This;
    begin
        Result.Value := Result.Value * 94 + 43;

        return Result;
    end H_MIN;

    function H_MIN (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : BNF := This;
        Counter : constant BNF := BNF (Block.all (Zero_BNF));
        Repeats : Big_Integer := Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 43;
            Repeats := Repeats - 1;
        end loop;

        return Result;
    end H_MIN;

    function I_MIN (This : BNF) return Datatype'Class is
        Result : BNF := This;
    begin
        Result.Value := Result.Value * 94 + 44;

        return Result;
    end I_MIN;

    function I_MIN (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : BNF := This;
        Counter : constant BNF := BNF (Block.all (Zero_BNF));
        Repeats : Big_Integer := Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 44;
            Repeats := Repeats - 1;
        end loop;

        return Result;
    end I_MIN;

    function J_MIN (This : BNF) return Datatype'Class is
        Result : BNF := This;
    begin
        Result.Value := Result.Value * 94 + 45;

        return Result;
    end J_MIN;

    function J_MIN (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : BNF := This;
        Counter : constant BNF := BNF (Block.all (Zero_BNF));
        Repeats : Big_Integer := Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 45;
            Repeats := Repeats - 1;
        end loop;

        return Result;
    end J_MIN;

    function K_MIN (This : BNF) return Datatype'Class is
        Result : BNF := This;
    begin
        Result.Value := Result.Value * 94 + 46;

        return Result;
    end K_MIN;

    function K_MIN (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : BNF := This;
        Counter : constant BNF := BNF (Block.all (Zero_BNF));
        Repeats : Big_Integer := Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 46;
            Repeats := Repeats - 1;
        end loop;

        return Result;
    end K_MIN;

    function L_MIN (This : BNF) return Datatype'Class is
        Result : BNF := This;
    begin
        Result.Value := Result.Value * 94 + 47;

        return Result;
    end L_MIN;

    function L_MIN (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : BNF := This;
        Counter : constant BNF := BNF (Block.all (Zero_BNF));
        Repeats : Big_Integer := Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 47;
            Repeats := Repeats - 1;
        end loop;

        return Result;
    end L_MIN;

    function M_MIN (This : BNF) return Datatype'Class is
        Result : BNF := This;
    begin
        Result.Value := Result.Value * 94 + 48;

        return Result;
    end M_MIN;

    function M_MIN (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : BNF := This;
        Counter : constant BNF := BNF (Block.all (Zero_BNF));
        Repeats : Big_Integer := Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 48;
            Repeats := Repeats - 1;
        end loop;

        return Result;
    end M_MIN;

    function N_MIN (This : BNF) return Datatype'Class is
        Result : BNF := This;
    begin
        Result.Value := Result.Value * 94 + 49;

        return Result;
    end N_MIN;

    function N_MIN (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : BNF := This;
        Counter : constant BNF := BNF (Block.all (Zero_BNF));
        Repeats : Big_Integer := Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 49;
            Repeats := Repeats - 1;
        end loop;

        return Result;
    end N_MIN;

    function O_MIN (This : BNF) return Datatype'Class is
        Result : BNF := This;
    begin
        Result.Value := Result.Value * 94 + 50;

        return Result;
    end O_MIN;

    function O_MIN (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : BNF := This;
        Counter : constant BNF := BNF (Block.all (Zero_BNF));
        Repeats : Big_Integer := Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 50;
            Repeats := Repeats - 1;
        end loop;

        return Result;
    end O_MIN;

    function P_MIN (This : BNF) return Datatype'Class is
        Result : BNF := This;
    begin
        Result.Value := Result.Value * 94 + 51;

        return Result;
    end P_MIN;

    function P_MIN (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : BNF := This;
        Counter : constant BNF := BNF (Block.all (Zero_BNF));
        Repeats : Big_Integer := Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 51;
            Repeats := Repeats - 1;
        end loop;

        return Result;
    end P_MIN;

    function Q_MIN (This : BNF) return Datatype'Class is
        Result : BNF := This;
    begin
        Result.Value := Result.Value * 94 + 52;

        return Result;
    end Q_MIN;

    function Q_MIN (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : BNF := This;
        Counter : constant BNF := BNF (Block.all (Zero_BNF));
        Repeats : Big_Integer := Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 52;
            Repeats := Repeats - 1;
        end loop;

        return Result;
    end Q_MIN;

    function R_MIN (This : BNF) return Datatype'Class is
        Result : BNF := This;
    begin
        Result.Value := Result.Value * 94 + 53;

        return Result;
    end R_MIN;

    function R_MIN (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : BNF := This;
        Counter : constant BNF := BNF (Block.all (Zero_BNF));
        Repeats : Big_Integer := Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 53;
            Repeats := Repeats - 1;
        end loop;

        return Result;
    end R_MIN;

    function S_MIN (This : BNF) return Datatype'Class is
        Result : BNF := This;
    begin
        Result.Value := Result.Value * 94 + 54;

        return Result;
    end S_MIN;

    function S_MIN (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : BNF := This;
        Counter : constant BNF := BNF (Block.all (Zero_BNF));
        Repeats : Big_Integer := Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 54;
            Repeats := Repeats - 1;
        end loop;

        return Result;
    end S_MIN;

    function T_MIN (This : BNF) return Datatype'Class is
        Result : BNF := This;
    begin
        Result.Value := Result.Value * 94 + 55;

        return Result;
    end T_MIN;

    function T_MIN (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : BNF := This;
        Counter : constant BNF := BNF (Block.all (Zero_BNF));
        Repeats : Big_Integer := Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 55;
            Repeats := Repeats - 1;
        end loop;

        return Result;
    end T_MIN;

    function U_MIN (This : BNF) return Datatype'Class is
        Result : BNF := This;
    begin
        Result.Value := Result.Value * 94 + 56;

        return Result;
    end U_MIN;

    function U_MIN (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : BNF := This;
        Counter : constant BNF := BNF (Block.all (Zero_BNF));
        Repeats : Big_Integer := Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 56;
            Repeats := Repeats - 1;
        end loop;

        return Result;
    end U_MIN;

    function V_MIN (This : BNF) return Datatype'Class is
        Result : BNF := This;
    begin
        Result.Value := Result.Value * 94 + 57;

        return Result;
    end V_MIN;

    function V_MIN (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : BNF := This;
        Counter : constant BNF := BNF (Block.all (Zero_BNF));
        Repeats : Big_Integer := Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 57;
            Repeats := Repeats - 1;
        end loop;

        return Result;
    end V_MIN;

    function W_MIN (This : BNF) return Datatype'Class is
        Result : BNF := This;
    begin
        Result.Value := Result.Value * 94 + 58;

        return Result;
    end W_MIN;

    function W_MIN (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : BNF := This;
        Counter : constant BNF := BNF (Block.all (Zero_BNF));
        Repeats : Big_Integer := Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 58;
            Repeats := Repeats - 1;
        end loop;

        return Result;
    end W_MIN;

    function X_MIN (This : BNF) return Datatype'Class is
        Result : BNF := This;
    begin
        Result.Value := Result.Value * 94 + 59;

        return Result;
    end X_MIN;

    function X_MIN (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : BNF := This;
        Counter : constant BNF := BNF (Block.all (Zero_BNF));
        Repeats : Big_Integer := Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 59;
            Repeats := Repeats - 1;
        end loop;

        return Result;
    end X_MIN;

    function Y_MIN (This : BNF) return Datatype'Class is
        Result : BNF := This;
    begin
        Result.Value := Result.Value * 94 + 60;

        return Result;
    end Y_MIN;

    function Y_MIN (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : BNF := This;
        Counter : constant BNF := BNF (Block.all (Zero_BNF));
        Repeats : Big_Integer := Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 60;
            Repeats := Repeats - 1;
        end loop;

        return Result;
    end Y_MIN;

    function Z_MIN (This : BNF) return Datatype'Class is
        Result : BNF := This;
    begin
        Result.Value := Result.Value * 94 + 61;

        return Result;
    end Z_MIN;

    function Z_MIN (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : BNF := This;
        Counter : constant BNF := BNF (Block.all (Zero_BNF));
        Repeats : Big_Integer := Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 61;
            Repeats := Repeats - 1;
        end loop;

        return Result;
    end Z_MIN;

    function ADDIT (This : BNF) return Datatype'Class is
        Result : BNF := This;
    begin
        Result.Value := Result.Value * 94 + 62;

        return Result;
    end ADDIT;

    function ADDIT (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : BNF := This;
        Counter : constant BNF := BNF (Block.all (Zero_BNF));
        Repeats : Big_Integer := Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 62;
            Repeats := Repeats - 1;
        end loop;

        return Result;
    end ADDIT;

    function SLASH (This : BNF) return Datatype'Class is
        Result : BNF := This;
    begin
        Result.Value := Result.Value * 94 + 63;

        return Result;
    end SLASH;

    function SLASH (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : BNF := This;
        Counter : constant BNF := BNF (Block.all (Zero_BNF));
        Repeats : Big_Integer := Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 63;
            Repeats := Repeats - 1;
        end loop;

        return Result;
    end SLASH;

    function MINUS (This : BNF) return Datatype'Class is
        Result : BNF := This;
    begin
        Result.Value := Result.Value * 94 + 64;

        return Result;
    end MINUS;

    function MINUS (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : BNF := This;
        Counter : constant BNF := BNF (Block.all (Zero_BNF));
        Repeats : Big_Integer := Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 64;
            Repeats := Repeats - 1;
        end loop;

        return Result;
    end MINUS;

    function TIMES (This : BNF) return Datatype'Class is
        Result : BNF := This;
    begin
        Result.Value := Result.Value * 94 + 65;

        return Result;
    end TIMES;

    function TIMES (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : BNF := This;
        Counter : constant BNF := BNF (Block.all (Zero_BNF));
        Repeats : Big_Integer := Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 65;
            Repeats := Repeats - 1;
        end loop;

        return Result;
    end TIMES;

    function VPIPE (This : BNF) return Datatype'Class is
        Result : BNF := This;
    begin
        Result.Value := Result.Value * 94 + 66;

        return Result;
    end VPIPE;

    function VPIPE (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : BNF := This;
        Counter : constant BNF := BNF (Block.all (Zero_BNF));
        Repeats : Big_Integer := Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 66;
            Repeats := Repeats - 1;
        end loop;

        return Result;
    end VPIPE;

    function CARET (This : BNF) return Datatype'Class is
        Result : BNF := This;
    begin
        Result.Value := Result.Value * 94 + 67;

        return Result;
    end CARET;

    function CARET (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : BNF := This;
        Counter : constant BNF := BNF (Block.all (Zero_BNF));
        Repeats : Big_Integer := Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 67;
            Repeats := Repeats - 1;
        end loop;

        return Result;
    end CARET;

    function AMPER (This : BNF) return Datatype'Class is
        Result : BNF := This;
    begin
        Result.Value := Result.Value * 94 + 68;

        return Result;
    end AMPER;

    function AMPER (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : BNF := This;
        Counter : constant BNF := BNF (Block.all (Zero_BNF));
        Repeats : Big_Integer := Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 68;
            Repeats := Repeats - 1;
        end loop;

        return Result;
    end AMPER;

    function PRCNT (This : BNF) return Datatype'Class is
        Result : BNF := This;
    begin
        Result.Value := Result.Value * 94 + 69;

        return Result;
    end PRCNT;

    function PRCNT (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : BNF := This;
        Counter : constant BNF := BNF (Block.all (Zero_BNF));
        Repeats : Big_Integer := Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 69;
            Repeats := Repeats - 1;
        end loop;

        return Result;
    end PRCNT;

    function LESST (This : BNF) return Datatype'Class is
        Result : BNF := This;
    begin
        Result.Value := Result.Value * 94 + 70;

        return Result;
    end LESST;

    function LESST (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : BNF := This;
        Counter : constant BNF := BNF (Block.all (Zero_BNF));
        Repeats : Big_Integer := Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 70;
            Repeats := Repeats - 1;
        end loop;

        return Result;
    end LESST;

    function MORET (This : BNF) return Datatype'Class is
        Result : BNF := This;
    begin
        Result.Value := Result.Value * 94 + 71;

        return Result;
    end MORET;

    function MORET (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : BNF := This;
        Counter : constant BNF := BNF (Block.all (Zero_BNF));
        Repeats : Big_Integer := Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 71;
            Repeats := Repeats - 1;
        end loop;

        return Result;
    end MORET;

    function PAR_L (This : BNF) return Datatype'Class is
        Result : BNF := This;
    begin
        Result.Value := Result.Value * 94 + 72;

        return Result;
    end PAR_L;

    function PAR_L (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : BNF := This;
        Counter : constant BNF := BNF (Block.all (Zero_BNF));
        Repeats : Big_Integer := Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 72;
            Repeats := Repeats - 1;
        end loop;

        return Result;
    end PAR_L;

    function PAR_R (This : BNF) return Datatype'Class is
        Result : BNF := This;
    begin
        Result.Value := Result.Value * 94 + 73;

        return Result;
    end PAR_R;

    function PAR_R (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : BNF := This;
        Counter : constant BNF := BNF (Block.all (Zero_BNF));
        Repeats : Big_Integer := Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 73;
            Repeats := Repeats - 1;
        end loop;

        return Result;
    end PAR_R;

    function BRA_L (This : BNF) return Datatype'Class is
        Result : BNF := This;
    begin
        Result.Value := Result.Value * 94 + 74;

        return Result;
    end BRA_L;

    function BRA_L (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : BNF := This;
        Counter : constant BNF := BNF (Block.all (Zero_BNF));
        Repeats : Big_Integer := Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 74;
            Repeats := Repeats - 1;
        end loop;

        return Result;
    end BRA_L;

    function BRA_R (This : BNF) return Datatype'Class is
        Result : BNF := This;
    begin
        Result.Value := Result.Value * 94 + 75;

        return Result;
    end BRA_R;

    function BRA_R (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : BNF := This;
        Counter : constant BNF := BNF (Block.all (Zero_BNF));
        Repeats : Big_Integer := Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 75;
            Repeats := Repeats - 1;
        end loop;

        return Result;
    end BRA_R;

    function CUR_L (This : BNF) return Datatype'Class is
        Result : BNF := This;
    begin
        Result.Value := Result.Value * 94 + 76;

        return Result;
    end CUR_L;

    function CUR_L (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : BNF := This;
        Counter : constant BNF := BNF (Block.all (Zero_BNF));
        Repeats : Big_Integer := Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 76;
            Repeats := Repeats - 1;
        end loop;

        return Result;
    end CUR_L;

    function CUR_R (This : BNF) return Datatype'Class is
        Result : BNF := This;
    begin
        Result.Value := Result.Value * 94 + 77;

        return Result;
    end CUR_R;

    function CUR_R (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : BNF := This;
        Counter : constant BNF := BNF (Block.all (Zero_BNF));
        Repeats : Big_Integer := Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 77;
            Repeats := Repeats - 1;
        end loop;

        return Result;
    end CUR_R;

    function O_DOT (This : BNF) return Datatype'Class is
        Result : BNF := This;
    begin
        Result.Value := Result.Value * 94 + 78;

        return Result;
    end O_DOT;

    function O_DOT (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : BNF := This;
        Counter : constant BNF := BNF (Block.all (Zero_BNF));
        Repeats : Big_Integer := Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 78;
            Repeats := Repeats - 1;
        end loop;

        return Result;
    end O_DOT;

    function COMMA (This : BNF) return Datatype'Class is
        Result : BNF := This;
    begin
        Result.Value := Result.Value * 94 + 79;

        return Result;
    end COMMA;

    function COMMA (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : BNF := This;
        Counter : constant BNF := BNF (Block.all (Zero_BNF));
        Repeats : Big_Integer := Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 79;
            Repeats := Repeats - 1;
        end loop;

        return Result;
    end COMMA;

    function SEMIC (This : BNF) return Datatype'Class is
        Result : BNF := This;
    begin
        Result.Value := Result.Value * 94 + 80;

        return Result;
    end SEMIC;

    function SEMIC (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : BNF := This;
        Counter : constant BNF := BNF (Block.all (Zero_BNF));
        Repeats : Big_Integer := Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 80;
            Repeats := Repeats - 1;
        end loop;

        return Result;
    end SEMIC;

    function COLON (This : BNF) return Datatype'Class is
        Result : BNF := This;
    begin
        Result.Value := Result.Value * 94 + 81;

        return Result;
    end COLON;

    function COLON (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : BNF := This;
        Counter : constant BNF := BNF (Block.all (Zero_BNF));
        Repeats : Big_Integer := Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 81;
            Repeats := Repeats - 1;
        end loop;

        return Result;
    end COLON;

    function APOST (This : BNF) return Datatype'Class is
        Result : BNF := This;
    begin
        Result.Value := Result.Value * 94 + 82;

        return Result;
    end APOST;

    function APOST (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : BNF := This;
        Counter : constant BNF := BNF (Block.all (Zero_BNF));
        Repeats : Big_Integer := Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 82;
            Repeats := Repeats - 1;
        end loop;

        return Result;
    end APOST;

    function QUOTE (This : BNF) return Datatype'Class is
        Result : BNF := This;
    begin
        Result.Value := Result.Value * 94 + 83;

        return Result;
    end QUOTE;

    function QUOTE (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : BNF := This;
        Counter : constant BNF := BNF (Block.all (Zero_BNF));
        Repeats : Big_Integer := Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 83;
            Repeats := Repeats - 1;
        end loop;

        return Result;
    end QUOTE;

    function BTICK (This : BNF) return Datatype'Class is
        Result : BNF := This;
    begin
        Result.Value := Result.Value * 94 + 84;

        return Result;
    end BTICK;

    function BTICK (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : BNF := This;
        Counter : constant BNF := BNF (Block.all (Zero_BNF));
        Repeats : Big_Integer := Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 84;
            Repeats := Repeats - 1;
        end loop;

        return Result;
    end BTICK;

    function TILDE (This : BNF) return Datatype'Class is
        Result : BNF := This;
    begin
        Result.Value := Result.Value * 94 + 85;

        return Result;
    end TILDE;

    function TILDE (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : BNF := This;
        Counter : constant BNF := BNF (Block.all (Zero_BNF));
        Repeats : Big_Integer := Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 85;
            Repeats := Repeats - 1;
        end loop;

        return Result;
    end TILDE;

    function EXCLA (This : BNF) return Datatype'Class is
        Result : BNF := This;
    begin
        Result.Value := Result.Value * 94 + 86;

        return Result;
    end EXCLA;

    function EXCLA (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : BNF := This;
        Counter : constant BNF := BNF (Block.all (Zero_BNF));
        Repeats : Big_Integer := Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 86;
            Repeats := Repeats - 1;
        end loop;

        return Result;
    end EXCLA;

    function ATSGN (This : BNF) return Datatype'Class is
        Result : BNF := This;
    begin
        Result.Value := Result.Value * 94 + 87;

        return Result;
    end ATSGN;

    function ATSGN (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : BNF := This;
        Counter : constant BNF := BNF (Block.all (Zero_BNF));
        Repeats : Big_Integer := Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 87;
            Repeats := Repeats - 1;
        end loop;

        return Result;
    end ATSGN;

    function POUND (This : BNF) return Datatype'Class is
        Result : BNF := This;
    begin
        Result.Value := Result.Value * 94 + 88;

        return Result;
    end POUND;

    function POUND (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : BNF := This;
        Counter : constant BNF := BNF (Block.all (Zero_BNF));
        Repeats : Big_Integer := Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 88;
            Repeats := Repeats - 1;
        end loop;

        return Result;
    end POUND;

    function MONEY (This : BNF) return Datatype'Class is
        Result : BNF := This;
    begin
        Result.Value := Result.Value * 94 + 89;

        return Result;
    end MONEY;

    function MONEY (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : BNF := This;
        Counter : constant BNF := BNF (Block.all (Zero_BNF));
        Repeats : Big_Integer := Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 89;
            Repeats := Repeats - 1;
        end loop;

        return Result;
    end MONEY;

    function QUERY (This : BNF) return Datatype'Class is
        Result : BNF := This;
    begin
        Result.Value := Result.Value * 94 + 90;

        return Result;
    end QUERY;

    function QUERY (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : BNF := This;
        Counter : constant BNF := BNF (Block.all (Zero_BNF));
        Repeats : Big_Integer := Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 90;
            Repeats := Repeats - 1;
        end loop;

        return Result;
    end QUERY;

    function UNDER (This : BNF) return Datatype'Class is
        Result : BNF := This;
    begin
        Result.Value := Result.Value * 94 + 91;

        return Result;
    end UNDER;

    function UNDER (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : BNF := This;
        Counter : constant BNF := BNF (Block.all (Zero_BNF));
        Repeats : Big_Integer := Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 91;
            Repeats := Repeats - 1;
        end loop;

        return Result;
    end UNDER;

    function EQUAL (This : BNF) return Datatype'Class is
        Result : BNF := This;
    begin
        Result.Value := Result.Value * 94 + 92;

        return Result;
    end EQUAL;

    function EQUAL (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : BNF := This;
        Counter : constant BNF := BNF (Block.all (Zero_BNF));
        Repeats : Big_Integer := Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 92;
            Repeats := Repeats - 1;
        end loop;

        return Result;
    end EQUAL;

    function HSALS (This : BNF) return Datatype'Class is
        Result : BNF := This;
    begin
        Result.Value := Result.Value * 94 + 93;

        return Result;
    end HSALS;

    function HSALS (
        This : BNF;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : BNF := This;
        Counter : constant BNF := BNF (Block.all (Zero_BNF));
        Repeats : Big_Integer := Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 93;
            Repeats := Repeats - 1;
        end loop;

        return Result;
    end HSALS;
    -- LCOV_EXCL_STOP }}}

    function Read (From : not null access Inputter) return BNF is
        pragma Unreferenced (From);
    begin
        return Zero_BNF;
    end Read;

    procedure Write (This : BNF; To : in out Outputter) is
    begin
        To.Put_Int (This.Value);
    end Write;

    function Image (This : BNF) return String is
    begin
        return Image (This.Value);
    end Image;

    function Type_Image (This : BNF) return String is
        pragma Unreferenced (This);
    begin
        return "BNF";
    end Type_Image;
end Sbit.BNFs;
