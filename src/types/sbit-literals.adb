with Big_Integers; use Big_Integers;
with GMP;
with Sbit.BNFs;

package body Sbit.Literals is
    use type GMP.Polarity;

    function Create (Ignore : not null access Nothing) return Literal is
        Result : Literal;
    begin
        Result.Start := Create (Ignore);
        Result.Initial := True;
        Result.Size := Create (1);
        Result.Value := Create (0);
        return Result;
    end Create;

    function Empty_Literal (From : Input) return Literal is
        Result : constant Literal := (Empty_Datum with
            Start => From,
            Initial => True,
            Size => Create (1),
            Value => Create (0));
    begin
        return Result;
    end Empty_Literal;

    -- LCOV_EXCL_START: Don't compute coverage here {{{
    function NUM_0 (This : Literal) return Datatype'Class is
        Result : Literal := This;
    begin
        Result.Value := Result.Value * 94 + 0;
        Result.Size := Result.Size - 1;
        Result.Initial := False;

        if Sign (Result.Size) = GMP.Neutral then
            return Apply (Result.Start, Result.Value);
        else
            return Result;
        end if;
    end NUM_0;

    function NUM_0 (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : Literal := This;
        Counter : constant BNFs.BNF := BNFs.BNF (Block.all (BNFs.Zero_BNF));
        Repeats : Big_Integer := BNFs.Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 0;
            Repeats := Repeats - 1;
        end loop;

        Result.Size := Result.Size - 1;
        Result.Initial := False;

        if Sign (Result.Size) = GMP.Neutral then
            return Apply (Result.Start, Result.Value);
        else
            return Result;
        end if;
    end NUM_0;

    function NUM_1 (This : Literal) return Datatype'Class is
        Result : Literal := This;
    begin
        Result.Value := Result.Value * 94 + 1;
        Result.Size := Result.Size - 1;
        Result.Initial := False;

        if Sign (Result.Size) = GMP.Neutral then
            return Apply (Result.Start, Result.Value);
        else
            return Result;
        end if;
    end NUM_1;

    function NUM_1 (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : Literal := This;
        Counter : constant BNFs.BNF := BNFs.BNF (Block.all (BNFs.Zero_BNF));
        Repeats : Big_Integer := BNFs.Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 1;
            Repeats := Repeats - 1;
        end loop;

        Result.Size := Result.Size - 1;
        Result.Initial := False;

        if Sign (Result.Size) = GMP.Neutral then
            return Apply (Result.Start, Result.Value);
        else
            return Result;
        end if;
    end NUM_1;

    function NUM_2 (This : Literal) return Datatype'Class is
        Result : Literal := This;
    begin
        Result.Value := Result.Value * 94 + 2;
        Result.Size := Result.Size - 1;
        Result.Initial := False;

        if Sign (Result.Size) = GMP.Neutral then
            return Apply (Result.Start, Result.Value);
        else
            return Result;
        end if;
    end NUM_2;

    function NUM_2 (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : Literal := This;
        Counter : constant BNFs.BNF := BNFs.BNF (Block.all (BNFs.Zero_BNF));
        Repeats : Big_Integer := BNFs.Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 2;
            Repeats := Repeats - 1;
        end loop;

        Result.Size := Result.Size - 1;
        Result.Initial := False;

        if Sign (Result.Size) = GMP.Neutral then
            return Apply (Result.Start, Result.Value);
        else
            return Result;
        end if;
    end NUM_2;

    function NUM_3 (This : Literal) return Datatype'Class is
        Result : Literal := This;
    begin
        Result.Value := Result.Value * 94 + 3;
        Result.Size := Result.Size - 1;
        Result.Initial := False;

        if Sign (Result.Size) = GMP.Neutral then
            return Apply (Result.Start, Result.Value);
        else
            return Result;
        end if;
    end NUM_3;

    function NUM_3 (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : Literal := This;
        Counter : constant BNFs.BNF := BNFs.BNF (Block.all (BNFs.Zero_BNF));
        Repeats : Big_Integer := BNFs.Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 3;
            Repeats := Repeats - 1;
        end loop;

        Result.Size := Result.Size - 1;
        Result.Initial := False;

        if Sign (Result.Size) = GMP.Neutral then
            return Apply (Result.Start, Result.Value);
        else
            return Result;
        end if;
    end NUM_3;

    function NUM_4 (This : Literal) return Datatype'Class is
        Result : Literal := This;
    begin
        Result.Value := Result.Value * 94 + 4;
        Result.Size := Result.Size - 1;
        Result.Initial := False;

        if Sign (Result.Size) = GMP.Neutral then
            return Apply (Result.Start, Result.Value);
        else
            return Result;
        end if;
    end NUM_4;

    function NUM_4 (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : Literal := This;
        Counter : constant BNFs.BNF := BNFs.BNF (Block.all (BNFs.Zero_BNF));
        Repeats : Big_Integer := BNFs.Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 4;
            Repeats := Repeats - 1;
        end loop;

        Result.Size := Result.Size - 1;
        Result.Initial := False;

        if Sign (Result.Size) = GMP.Neutral then
            return Apply (Result.Start, Result.Value);
        else
            return Result;
        end if;
    end NUM_4;

    function NUM_5 (This : Literal) return Datatype'Class is
        Result : Literal := This;
    begin
        Result.Value := Result.Value * 94 + 5;
        Result.Size := Result.Size - 1;
        Result.Initial := False;

        if Sign (Result.Size) = GMP.Neutral then
            return Apply (Result.Start, Result.Value);
        else
            return Result;
        end if;
    end NUM_5;

    function NUM_5 (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : Literal := This;
        Counter : constant BNFs.BNF := BNFs.BNF (Block.all (BNFs.Zero_BNF));
        Repeats : Big_Integer := BNFs.Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 5;
            Repeats := Repeats - 1;
        end loop;

        Result.Size := Result.Size - 1;
        Result.Initial := False;

        if Sign (Result.Size) = GMP.Neutral then
            return Apply (Result.Start, Result.Value);
        else
            return Result;
        end if;
    end NUM_5;

    function NUM_6 (This : Literal) return Datatype'Class is
        Result : Literal := This;
    begin
        Result.Value := Result.Value * 94 + 6;
        Result.Size := Result.Size - 1;
        Result.Initial := False;

        if Sign (Result.Size) = GMP.Neutral then
            return Apply (Result.Start, Result.Value);
        else
            return Result;
        end if;
    end NUM_6;

    function NUM_6 (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : Literal := This;
        Counter : constant BNFs.BNF := BNFs.BNF (Block.all (BNFs.Zero_BNF));
        Repeats : Big_Integer := BNFs.Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 6;
            Repeats := Repeats - 1;
        end loop;

        Result.Size := Result.Size - 1;
        Result.Initial := False;

        if Sign (Result.Size) = GMP.Neutral then
            return Apply (Result.Start, Result.Value);
        else
            return Result;
        end if;
    end NUM_6;

    function NUM_7 (This : Literal) return Datatype'Class is
        Result : Literal := This;
    begin
        Result.Value := Result.Value * 94 + 7;
        Result.Size := Result.Size - 1;
        Result.Initial := False;

        if Sign (Result.Size) = GMP.Neutral then
            return Apply (Result.Start, Result.Value);
        else
            return Result;
        end if;
    end NUM_7;

    function NUM_7 (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : Literal := This;
        Counter : constant BNFs.BNF := BNFs.BNF (Block.all (BNFs.Zero_BNF));
        Repeats : Big_Integer := BNFs.Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 7;
            Repeats := Repeats - 1;
        end loop;

        Result.Size := Result.Size - 1;
        Result.Initial := False;

        if Sign (Result.Size) = GMP.Neutral then
            return Apply (Result.Start, Result.Value);
        else
            return Result;
        end if;
    end NUM_7;

    function NUM_8 (This : Literal) return Datatype'Class is
        Result : Literal := This;
    begin
        Result.Value := Result.Value * 94 + 8;
        Result.Size := Result.Size - 1;
        Result.Initial := False;

        if Sign (Result.Size) = GMP.Neutral then
            return Apply (Result.Start, Result.Value);
        else
            return Result;
        end if;
    end NUM_8;

    function NUM_8 (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : Literal := This;
        Counter : constant BNFs.BNF := BNFs.BNF (Block.all (BNFs.Zero_BNF));
        Repeats : Big_Integer := BNFs.Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 8;
            Repeats := Repeats - 1;
        end loop;

        Result.Size := Result.Size - 1;
        Result.Initial := False;

        if Sign (Result.Size) = GMP.Neutral then
            return Apply (Result.Start, Result.Value);
        else
            return Result;
        end if;
    end NUM_8;

    function NUM_9 (This : Literal) return Datatype'Class is
        Result : Literal := This;
    begin
        Result.Value := Result.Value * 94 + 9;
        Result.Size := Result.Size - 1;
        Result.Initial := False;

        if Sign (Result.Size) = GMP.Neutral then
            return Apply (Result.Start, Result.Value);
        else
            return Result;
        end if;
    end NUM_9;

    function NUM_9 (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : Literal := This;
        Counter : constant BNFs.BNF := BNFs.BNF (Block.all (BNFs.Zero_BNF));
        Repeats : Big_Integer := BNFs.Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 9;
            Repeats := Repeats - 1;
        end loop;

        Result.Size := Result.Size - 1;
        Result.Initial := False;

        if Sign (Result.Size) = GMP.Neutral then
            return Apply (Result.Start, Result.Value);
        else
            return Result;
        end if;
    end NUM_9;

    function A_MAJ (This : Literal) return Datatype'Class is
        Result : Literal := This;
    begin
        Result.Value := Result.Value * 94 + 10;
        Result.Size := Result.Size - 1;
        Result.Initial := False;

        if Sign (Result.Size) = GMP.Neutral then
            return Apply (Result.Start, Result.Value);
        else
            return Result;
        end if;
    end A_MAJ;

    function A_MAJ (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : Literal := This;
        Counter : constant BNFs.BNF := BNFs.BNF (Block.all (BNFs.Zero_BNF));
        Repeats : Big_Integer := BNFs.Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 10;
            Repeats := Repeats - 1;
        end loop;

        Result.Size := Result.Size - 1;
        Result.Initial := False;

        if Sign (Result.Size) = GMP.Neutral then
            return Apply (Result.Start, Result.Value);
        else
            return Result;
        end if;
    end A_MAJ;

    function B_MAJ (This : Literal) return Datatype'Class is
        Result : Literal := This;
    begin
        Result.Value := Result.Value * 94 + 11;
        Result.Size := Result.Size - 1;
        Result.Initial := False;

        if Sign (Result.Size) = GMP.Neutral then
            return Apply (Result.Start, Result.Value);
        else
            return Result;
        end if;
    end B_MAJ;

    function B_MAJ (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : Literal := This;
        Counter : constant BNFs.BNF := BNFs.BNF (Block.all (BNFs.Zero_BNF));
        Repeats : Big_Integer := BNFs.Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 11;
            Repeats := Repeats - 1;
        end loop;

        Result.Size := Result.Size - 1;
        Result.Initial := False;

        if Sign (Result.Size) = GMP.Neutral then
            return Apply (Result.Start, Result.Value);
        else
            return Result;
        end if;
    end B_MAJ;

    function C_MAJ (This : Literal) return Datatype'Class is
        Result : Literal := This;
    begin
        Result.Value := Result.Value * 94 + 12;
        Result.Size := Result.Size - 1;
        Result.Initial := False;

        if Sign (Result.Size) = GMP.Neutral then
            return Apply (Result.Start, Result.Value);
        else
            return Result;
        end if;
    end C_MAJ;

    function C_MAJ (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : Literal := This;
        Counter : constant BNFs.BNF := BNFs.BNF (Block.all (BNFs.Zero_BNF));
        Repeats : Big_Integer := BNFs.Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 12;
            Repeats := Repeats - 1;
        end loop;

        Result.Size := Result.Size - 1;
        Result.Initial := False;

        if Sign (Result.Size) = GMP.Neutral then
            return Apply (Result.Start, Result.Value);
        else
            return Result;
        end if;
    end C_MAJ;

    function D_MAJ (This : Literal) return Datatype'Class is
        Result : Literal := This;
    begin
        Result.Value := Result.Value * 94 + 13;
        Result.Size := Result.Size - 1;
        Result.Initial := False;

        if Sign (Result.Size) = GMP.Neutral then
            return Apply (Result.Start, Result.Value);
        else
            return Result;
        end if;
    end D_MAJ;

    function D_MAJ (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : Literal := This;
        Counter : constant BNFs.BNF := BNFs.BNF (Block.all (BNFs.Zero_BNF));
        Repeats : Big_Integer := BNFs.Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 13;
            Repeats := Repeats - 1;
        end loop;

        Result.Size := Result.Size - 1;
        Result.Initial := False;

        if Sign (Result.Size) = GMP.Neutral then
            return Apply (Result.Start, Result.Value);
        else
            return Result;
        end if;
    end D_MAJ;

    function E_MAJ (This : Literal) return Datatype'Class is
        Result : Literal := This;
    begin
        Result.Value := Result.Value * 94 + 14;
        Result.Size := Result.Size - 1;
        Result.Initial := False;

        if Sign (Result.Size) = GMP.Neutral then
            return Apply (Result.Start, Result.Value);
        else
            return Result;
        end if;
    end E_MAJ;

    function E_MAJ (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : Literal := This;
        Counter : constant BNFs.BNF := BNFs.BNF (Block.all (BNFs.Zero_BNF));
        Repeats : Big_Integer := BNFs.Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 14;
            Repeats := Repeats - 1;
        end loop;

        Result.Size := Result.Size - 1;
        Result.Initial := False;

        if Sign (Result.Size) = GMP.Neutral then
            return Apply (Result.Start, Result.Value);
        else
            return Result;
        end if;
    end E_MAJ;

    function F_MAJ (This : Literal) return Datatype'Class is
        Result : Literal := This;
    begin
        Result.Value := Result.Value * 94 + 15;
        Result.Size := Result.Size - 1;
        Result.Initial := False;

        if Sign (Result.Size) = GMP.Neutral then
            return Apply (Result.Start, Result.Value);
        else
            return Result;
        end if;
    end F_MAJ;

    function F_MAJ (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : Literal := This;
        Counter : constant BNFs.BNF := BNFs.BNF (Block.all (BNFs.Zero_BNF));
        Repeats : Big_Integer := BNFs.Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 15;
            Repeats := Repeats - 1;
        end loop;

        Result.Size := Result.Size - 1;
        Result.Initial := False;

        if Sign (Result.Size) = GMP.Neutral then
            return Apply (Result.Start, Result.Value);
        else
            return Result;
        end if;
    end F_MAJ;

    function G_MAJ (This : Literal) return Datatype'Class is
        Result : Literal := This;
    begin
        Result.Value := Result.Value * 94 + 16;
        Result.Size := Result.Size - 1;
        Result.Initial := False;

        if Sign (Result.Size) = GMP.Neutral then
            return Apply (Result.Start, Result.Value);
        else
            return Result;
        end if;
    end G_MAJ;

    function G_MAJ (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : Literal := This;
        Counter : constant BNFs.BNF := BNFs.BNF (Block.all (BNFs.Zero_BNF));
        Repeats : Big_Integer := BNFs.Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 16;
            Repeats := Repeats - 1;
        end loop;

        Result.Size := Result.Size - 1;
        Result.Initial := False;

        if Sign (Result.Size) = GMP.Neutral then
            return Apply (Result.Start, Result.Value);
        else
            return Result;
        end if;
    end G_MAJ;

    function H_MAJ (This : Literal) return Datatype'Class is
        Result : Literal := This;
    begin
        Result.Value := Result.Value * 94 + 17;
        Result.Size := Result.Size - 1;
        Result.Initial := False;

        if Sign (Result.Size) = GMP.Neutral then
            return Apply (Result.Start, Result.Value);
        else
            return Result;
        end if;
    end H_MAJ;

    function H_MAJ (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : Literal := This;
        Counter : constant BNFs.BNF := BNFs.BNF (Block.all (BNFs.Zero_BNF));
        Repeats : Big_Integer := BNFs.Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 17;
            Repeats := Repeats - 1;
        end loop;

        Result.Size := Result.Size - 1;
        Result.Initial := False;

        if Sign (Result.Size) = GMP.Neutral then
            return Apply (Result.Start, Result.Value);
        else
            return Result;
        end if;
    end H_MAJ;

    function I_MAJ (This : Literal) return Datatype'Class is
        Result : Literal := This;
    begin
        Result.Value := Result.Value * 94 + 18;
        Result.Size := Result.Size - 1;
        Result.Initial := False;

        if Sign (Result.Size) = GMP.Neutral then
            return Apply (Result.Start, Result.Value);
        else
            return Result;
        end if;
    end I_MAJ;

    function I_MAJ (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : Literal := This;
        Counter : constant BNFs.BNF := BNFs.BNF (Block.all (BNFs.Zero_BNF));
        Repeats : Big_Integer := BNFs.Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 18;
            Repeats := Repeats - 1;
        end loop;

        Result.Size := Result.Size - 1;
        Result.Initial := False;

        if Sign (Result.Size) = GMP.Neutral then
            return Apply (Result.Start, Result.Value);
        else
            return Result;
        end if;
    end I_MAJ;

    function J_MAJ (This : Literal) return Datatype'Class is
        Result : Literal := This;
    begin
        Result.Value := Result.Value * 94 + 19;
        Result.Size := Result.Size - 1;
        Result.Initial := False;

        if Sign (Result.Size) = GMP.Neutral then
            return Apply (Result.Start, Result.Value);
        else
            return Result;
        end if;
    end J_MAJ;

    function J_MAJ (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : Literal := This;
        Counter : constant BNFs.BNF := BNFs.BNF (Block.all (BNFs.Zero_BNF));
        Repeats : Big_Integer := BNFs.Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 19;
            Repeats := Repeats - 1;
        end loop;

        Result.Size := Result.Size - 1;
        Result.Initial := False;

        if Sign (Result.Size) = GMP.Neutral then
            return Apply (Result.Start, Result.Value);
        else
            return Result;
        end if;
    end J_MAJ;

    function K_MAJ (This : Literal) return Datatype'Class is
        Result : Literal := This;
    begin
        Result.Value := Result.Value * 94 + 20;
        Result.Size := Result.Size - 1;
        Result.Initial := False;

        if Sign (Result.Size) = GMP.Neutral then
            return Apply (Result.Start, Result.Value);
        else
            return Result;
        end if;
    end K_MAJ;

    function K_MAJ (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : Literal := This;
        Counter : constant BNFs.BNF := BNFs.BNF (Block.all (BNFs.Zero_BNF));
        Repeats : Big_Integer := BNFs.Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 20;
            Repeats := Repeats - 1;
        end loop;

        Result.Size := Result.Size - 1;
        Result.Initial := False;

        if Sign (Result.Size) = GMP.Neutral then
            return Apply (Result.Start, Result.Value);
        else
            return Result;
        end if;
    end K_MAJ;

    function L_MAJ (This : Literal) return Datatype'Class is
        Result : Literal := This;
    begin
        Result.Value := Result.Value * 94 + 21;
        Result.Size := Result.Size - 1;
        Result.Initial := False;

        if Sign (Result.Size) = GMP.Neutral then
            return Apply (Result.Start, Result.Value);
        else
            return Result;
        end if;
    end L_MAJ;

    function L_MAJ (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : Literal := This;
        Counter : constant BNFs.BNF := BNFs.BNF (Block.all (BNFs.Zero_BNF));
        Repeats : Big_Integer := BNFs.Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 21;
            Repeats := Repeats - 1;
        end loop;

        Result.Size := Result.Size - 1;
        Result.Initial := False;

        if Sign (Result.Size) = GMP.Neutral then
            return Apply (Result.Start, Result.Value);
        else
            return Result;
        end if;
    end L_MAJ;

    function M_MAJ (This : Literal) return Datatype'Class is
        Result : Literal := This;
    begin
        Result.Value := Result.Value * 94 + 22;
        Result.Size := Result.Size - 1;
        Result.Initial := False;

        if Sign (Result.Size) = GMP.Neutral then
            return Apply (Result.Start, Result.Value);
        else
            return Result;
        end if;
    end M_MAJ;

    function M_MAJ (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : Literal := This;
        Counter : constant BNFs.BNF := BNFs.BNF (Block.all (BNFs.Zero_BNF));
        Repeats : Big_Integer := BNFs.Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 22;
            Repeats := Repeats - 1;
        end loop;

        Result.Size := Result.Size - 1;
        Result.Initial := False;

        if Sign (Result.Size) = GMP.Neutral then
            return Apply (Result.Start, Result.Value);
        else
            return Result;
        end if;
    end M_MAJ;

    function N_MAJ (This : Literal) return Datatype'Class is
        Result : Literal := This;
    begin
        Result.Value := Result.Value * 94 + 23;
        Result.Size := Result.Size - 1;
        Result.Initial := False;

        if Sign (Result.Size) = GMP.Neutral then
            return Apply (Result.Start, Result.Value);
        else
            return Result;
        end if;
    end N_MAJ;

    function N_MAJ (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : Literal := This;
        Counter : constant BNFs.BNF := BNFs.BNF (Block.all (BNFs.Zero_BNF));
        Repeats : Big_Integer := BNFs.Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 23;
            Repeats := Repeats - 1;
        end loop;

        Result.Size := Result.Size - 1;
        Result.Initial := False;

        if Sign (Result.Size) = GMP.Neutral then
            return Apply (Result.Start, Result.Value);
        else
            return Result;
        end if;
    end N_MAJ;

    function O_MAJ (This : Literal) return Datatype'Class is
        Result : Literal := This;
    begin
        Result.Value := Result.Value * 94 + 24;
        Result.Size := Result.Size - 1;
        Result.Initial := False;

        if Sign (Result.Size) = GMP.Neutral then
            return Apply (Result.Start, Result.Value);
        else
            return Result;
        end if;
    end O_MAJ;

    function O_MAJ (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : Literal := This;
        Counter : constant BNFs.BNF := BNFs.BNF (Block.all (BNFs.Zero_BNF));
        Repeats : Big_Integer := BNFs.Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 24;
            Repeats := Repeats - 1;
        end loop;

        Result.Size := Result.Size - 1;
        Result.Initial := False;

        if Sign (Result.Size) = GMP.Neutral then
            return Apply (Result.Start, Result.Value);
        else
            return Result;
        end if;
    end O_MAJ;

    function P_MAJ (This : Literal) return Datatype'Class is
        Result : Literal := This;
    begin
        Result.Value := Result.Value * 94 + 25;
        Result.Size := Result.Size - 1;
        Result.Initial := False;

        if Sign (Result.Size) = GMP.Neutral then
            return Apply (Result.Start, Result.Value);
        else
            return Result;
        end if;
    end P_MAJ;

    function P_MAJ (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : Literal := This;
        Counter : constant BNFs.BNF := BNFs.BNF (Block.all (BNFs.Zero_BNF));
        Repeats : Big_Integer := BNFs.Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 25;
            Repeats := Repeats - 1;
        end loop;

        Result.Size := Result.Size - 1;
        Result.Initial := False;

        if Sign (Result.Size) = GMP.Neutral then
            return Apply (Result.Start, Result.Value);
        else
            return Result;
        end if;
    end P_MAJ;

    function Q_MAJ (This : Literal) return Datatype'Class is
        Result : Literal := This;
    begin
        Result.Value := Result.Value * 94 + 26;
        Result.Size := Result.Size - 1;
        Result.Initial := False;

        if Sign (Result.Size) = GMP.Neutral then
            return Apply (Result.Start, Result.Value);
        else
            return Result;
        end if;
    end Q_MAJ;

    function Q_MAJ (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : Literal := This;
        Counter : constant BNFs.BNF := BNFs.BNF (Block.all (BNFs.Zero_BNF));
        Repeats : Big_Integer := BNFs.Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 26;
            Repeats := Repeats - 1;
        end loop;

        Result.Size := Result.Size - 1;
        Result.Initial := False;

        if Sign (Result.Size) = GMP.Neutral then
            return Apply (Result.Start, Result.Value);
        else
            return Result;
        end if;
    end Q_MAJ;

    function R_MAJ (This : Literal) return Datatype'Class is
        Result : Literal := This;
    begin
        Result.Value := Result.Value * 94 + 27;
        Result.Size := Result.Size - 1;
        Result.Initial := False;

        if Sign (Result.Size) = GMP.Neutral then
            return Apply (Result.Start, Result.Value);
        else
            return Result;
        end if;
    end R_MAJ;

    function R_MAJ (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : Literal := This;
        Counter : constant BNFs.BNF := BNFs.BNF (Block.all (BNFs.Zero_BNF));
        Repeats : Big_Integer := BNFs.Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 27;
            Repeats := Repeats - 1;
        end loop;

        Result.Size := Result.Size - 1;
        Result.Initial := False;

        if Sign (Result.Size) = GMP.Neutral then
            return Apply (Result.Start, Result.Value);
        else
            return Result;
        end if;
    end R_MAJ;

    function S_MAJ (This : Literal) return Datatype'Class is
        Result : Literal := This;
    begin
        Result.Value := Result.Value * 94 + 28;
        Result.Size := Result.Size - 1;
        Result.Initial := False;

        if Sign (Result.Size) = GMP.Neutral then
            return Apply (Result.Start, Result.Value);
        else
            return Result;
        end if;
    end S_MAJ;

    function S_MAJ (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : Literal := This;
        Counter : constant BNFs.BNF := BNFs.BNF (Block.all (BNFs.Zero_BNF));
        Repeats : Big_Integer := BNFs.Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 28;
            Repeats := Repeats - 1;
        end loop;

        Result.Size := Result.Size - 1;
        Result.Initial := False;

        if Sign (Result.Size) = GMP.Neutral then
            return Apply (Result.Start, Result.Value);
        else
            return Result;
        end if;
    end S_MAJ;

    function T_MAJ (This : Literal) return Datatype'Class is
        Result : Literal := This;
    begin
        Result.Value := Result.Value * 94 + 29;
        Result.Size := Result.Size - 1;
        Result.Initial := False;

        if Sign (Result.Size) = GMP.Neutral then
            return Apply (Result.Start, Result.Value);
        else
            return Result;
        end if;
    end T_MAJ;

    function T_MAJ (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : Literal := This;
        Counter : constant BNFs.BNF := BNFs.BNF (Block.all (BNFs.Zero_BNF));
        Repeats : Big_Integer := BNFs.Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 29;
            Repeats := Repeats - 1;
        end loop;

        Result.Size := Result.Size - 1;
        Result.Initial := False;

        if Sign (Result.Size) = GMP.Neutral then
            return Apply (Result.Start, Result.Value);
        else
            return Result;
        end if;
    end T_MAJ;

    function U_MAJ (This : Literal) return Datatype'Class is
        Result : Literal := This;
    begin
        Result.Value := Result.Value * 94 + 30;
        Result.Size := Result.Size - 1;
        Result.Initial := False;

        if Sign (Result.Size) = GMP.Neutral then
            return Apply (Result.Start, Result.Value);
        else
            return Result;
        end if;
    end U_MAJ;

    function U_MAJ (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : Literal := This;
        Counter : constant BNFs.BNF := BNFs.BNF (Block.all (BNFs.Zero_BNF));
        Repeats : Big_Integer := BNFs.Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 30;
            Repeats := Repeats - 1;
        end loop;

        Result.Size := Result.Size - 1;
        Result.Initial := False;

        if Sign (Result.Size) = GMP.Neutral then
            return Apply (Result.Start, Result.Value);
        else
            return Result;
        end if;
    end U_MAJ;

    function V_MAJ (This : Literal) return Datatype'Class is
        Result : Literal := This;
    begin
        Result.Value := Result.Value * 94 + 31;
        Result.Size := Result.Size - 1;
        Result.Initial := False;

        if Sign (Result.Size) = GMP.Neutral then
            return Apply (Result.Start, Result.Value);
        else
            return Result;
        end if;
    end V_MAJ;

    function V_MAJ (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : Literal := This;
        Counter : constant BNFs.BNF := BNFs.BNF (Block.all (BNFs.Zero_BNF));
        Repeats : Big_Integer := BNFs.Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 31;
            Repeats := Repeats - 1;
        end loop;

        Result.Size := Result.Size - 1;
        Result.Initial := False;

        if Sign (Result.Size) = GMP.Neutral then
            return Apply (Result.Start, Result.Value);
        else
            return Result;
        end if;
    end V_MAJ;

    function W_MAJ (This : Literal) return Datatype'Class is
        Result : Literal := This;
    begin
        Result.Value := Result.Value * 94 + 32;
        Result.Size := Result.Size - 1;
        Result.Initial := False;

        if Sign (Result.Size) = GMP.Neutral then
            return Apply (Result.Start, Result.Value);
        else
            return Result;
        end if;
    end W_MAJ;

    function W_MAJ (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : Literal := This;
        Counter : constant BNFs.BNF := BNFs.BNF (Block.all (BNFs.Zero_BNF));
        Repeats : Big_Integer := BNFs.Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 32;
            Repeats := Repeats - 1;
        end loop;

        Result.Size := Result.Size - 1;
        Result.Initial := False;

        if Sign (Result.Size) = GMP.Neutral then
            return Apply (Result.Start, Result.Value);
        else
            return Result;
        end if;
    end W_MAJ;

    function X_MAJ (This : Literal) return Datatype'Class is
        Result : Literal := This;
    begin
        Result.Value := Result.Value * 94 + 33;
        Result.Size := Result.Size - 1;
        Result.Initial := False;

        if Sign (Result.Size) = GMP.Neutral then
            return Apply (Result.Start, Result.Value);
        else
            return Result;
        end if;
    end X_MAJ;

    function X_MAJ (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : Literal := This;
        Counter : constant BNFs.BNF := BNFs.BNF (Block.all (BNFs.Zero_BNF));
        Repeats : Big_Integer := BNFs.Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 33;
            Repeats := Repeats - 1;
        end loop;

        Result.Size := Result.Size - 1;
        Result.Initial := False;

        if Sign (Result.Size) = GMP.Neutral then
            return Apply (Result.Start, Result.Value);
        else
            return Result;
        end if;
    end X_MAJ;

    function Y_MAJ (This : Literal) return Datatype'Class is
        Result : Literal := This;
    begin
        Result.Value := Result.Value * 94 + 34;
        Result.Size := Result.Size - 1;
        Result.Initial := False;

        if Sign (Result.Size) = GMP.Neutral then
            return Apply (Result.Start, Result.Value);
        else
            return Result;
        end if;
    end Y_MAJ;

    function Y_MAJ (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : Literal := This;
        Counter : constant BNFs.BNF := BNFs.BNF (Block.all (BNFs.Zero_BNF));
        Repeats : Big_Integer := BNFs.Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 34;
            Repeats := Repeats - 1;
        end loop;

        Result.Size := Result.Size - 1;
        Result.Initial := False;

        if Sign (Result.Size) = GMP.Neutral then
            return Apply (Result.Start, Result.Value);
        else
            return Result;
        end if;
    end Y_MAJ;

    function Z_MAJ (This : Literal) return Datatype'Class is
        Result : Literal := This;
    begin
        Result.Value := Result.Value * 94 + 35;
        Result.Size := Result.Size - 1;
        Result.Initial := False;

        if Sign (Result.Size) = GMP.Neutral then
            return Apply (Result.Start, Result.Value);
        else
            return Result;
        end if;
    end Z_MAJ;

    function Z_MAJ (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : Literal := This;
        Counter : constant BNFs.BNF := BNFs.BNF (Block.all (BNFs.Zero_BNF));
        Repeats : Big_Integer := BNFs.Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 35;
            Repeats := Repeats - 1;
        end loop;

        Result.Size := Result.Size - 1;
        Result.Initial := False;

        if Sign (Result.Size) = GMP.Neutral then
            return Apply (Result.Start, Result.Value);
        else
            return Result;
        end if;
    end Z_MAJ;

    function A_MIN (This : Literal) return Datatype'Class is
        Result : Literal := This;
    begin
        Result.Value := Result.Value * 94 + 36;
        Result.Size := Result.Size - 1;
        Result.Initial := False;

        if Sign (Result.Size) = GMP.Neutral then
            return Apply (Result.Start, Result.Value);
        else
            return Result;
        end if;
    end A_MIN;

    function A_MIN (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : Literal := This;
        Counter : constant BNFs.BNF := BNFs.BNF (Block.all (BNFs.Zero_BNF));
        Repeats : Big_Integer := BNFs.Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 36;
            Repeats := Repeats - 1;
        end loop;

        Result.Size := Result.Size - 1;
        Result.Initial := False;

        if Sign (Result.Size) = GMP.Neutral then
            return Apply (Result.Start, Result.Value);
        else
            return Result;
        end if;
    end A_MIN;

    function B_MIN (This : Literal) return Datatype'Class is
        Result : Literal := This;
    begin
        Result.Value := Result.Value * 94 + 37;
        Result.Size := Result.Size - 1;
        Result.Initial := False;

        if Sign (Result.Size) = GMP.Neutral then
            return Apply (Result.Start, Result.Value);
        else
            return Result;
        end if;
    end B_MIN;

    function B_MIN (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : Literal := This;
        Counter : constant BNFs.BNF := BNFs.BNF (Block.all (BNFs.Zero_BNF));
        Repeats : Big_Integer := BNFs.Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 37;
            Repeats := Repeats - 1;
        end loop;

        Result.Size := Result.Size - 1;
        Result.Initial := False;

        if Sign (Result.Size) = GMP.Neutral then
            return Apply (Result.Start, Result.Value);
        else
            return Result;
        end if;
    end B_MIN;

    function C_MIN (This : Literal) return Datatype'Class is
        Result : Literal := This;
    begin
        Result.Value := Result.Value * 94 + 38;
        Result.Size := Result.Size - 1;
        Result.Initial := False;

        if Sign (Result.Size) = GMP.Neutral then
            return Apply (Result.Start, Result.Value);
        else
            return Result;
        end if;
    end C_MIN;

    function C_MIN (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : Literal := This;
        Counter : constant BNFs.BNF := BNFs.BNF (Block.all (BNFs.Zero_BNF));
        Repeats : Big_Integer := BNFs.Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 38;
            Repeats := Repeats - 1;
        end loop;

        Result.Size := Result.Size - 1;
        Result.Initial := False;

        if Sign (Result.Size) = GMP.Neutral then
            return Apply (Result.Start, Result.Value);
        else
            return Result;
        end if;
    end C_MIN;

    function D_MIN (This : Literal) return Datatype'Class is
        Result : Literal := This;
    begin
        Result.Value := Result.Value * 94 + 39;
        Result.Size := Result.Size - 1;
        Result.Initial := False;

        if Sign (Result.Size) = GMP.Neutral then
            return Apply (Result.Start, Result.Value);
        else
            return Result;
        end if;
    end D_MIN;

    function D_MIN (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : Literal := This;
        Counter : constant BNFs.BNF := BNFs.BNF (Block.all (BNFs.Zero_BNF));
        Repeats : Big_Integer := BNFs.Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 39;
            Repeats := Repeats - 1;
        end loop;

        Result.Size := Result.Size - 1;
        Result.Initial := False;

        if Sign (Result.Size) = GMP.Neutral then
            return Apply (Result.Start, Result.Value);
        else
            return Result;
        end if;
    end D_MIN;

    function E_MIN (This : Literal) return Datatype'Class is
        Result : Literal := This;
    begin
        Result.Value := Result.Value * 94 + 40;
        Result.Size := Result.Size - 1;
        Result.Initial := False;

        if Sign (Result.Size) = GMP.Neutral then
            return Apply (Result.Start, Result.Value);
        else
            return Result;
        end if;
    end E_MIN;

    function E_MIN (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : Literal := This;
        Counter : constant BNFs.BNF := BNFs.BNF (Block.all (BNFs.Zero_BNF));
        Repeats : Big_Integer := BNFs.Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 40;
            Repeats := Repeats - 1;
        end loop;

        Result.Size := Result.Size - 1;
        Result.Initial := False;

        if Sign (Result.Size) = GMP.Neutral then
            return Apply (Result.Start, Result.Value);
        else
            return Result;
        end if;
    end E_MIN;

    function F_MIN (This : Literal) return Datatype'Class is
        Result : Literal := This;
    begin
        Result.Value := Result.Value * 94 + 41;
        Result.Size := Result.Size - 1;
        Result.Initial := False;

        if Sign (Result.Size) = GMP.Neutral then
            return Apply (Result.Start, Result.Value);
        else
            return Result;
        end if;
    end F_MIN;

    function F_MIN (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : Literal := This;
        Counter : constant BNFs.BNF := BNFs.BNF (Block.all (BNFs.Zero_BNF));
        Repeats : Big_Integer := BNFs.Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 41;
            Repeats := Repeats - 1;
        end loop;

        Result.Size := Result.Size - 1;
        Result.Initial := False;

        if Sign (Result.Size) = GMP.Neutral then
            return Apply (Result.Start, Result.Value);
        else
            return Result;
        end if;
    end F_MIN;

    function G_MIN (This : Literal) return Datatype'Class is
        Result : Literal := This;
    begin
        Result.Value := Result.Value * 94 + 42;
        Result.Size := Result.Size - 1;
        Result.Initial := False;

        if Sign (Result.Size) = GMP.Neutral then
            return Apply (Result.Start, Result.Value);
        else
            return Result;
        end if;
    end G_MIN;

    function G_MIN (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : Literal := This;
        Counter : constant BNFs.BNF := BNFs.BNF (Block.all (BNFs.Zero_BNF));
        Repeats : Big_Integer := BNFs.Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 42;
            Repeats := Repeats - 1;
        end loop;

        Result.Size := Result.Size - 1;
        Result.Initial := False;

        if Sign (Result.Size) = GMP.Neutral then
            return Apply (Result.Start, Result.Value);
        else
            return Result;
        end if;
    end G_MIN;

    function H_MIN (This : Literal) return Datatype'Class is
        Result : Literal := This;
    begin
        Result.Value := Result.Value * 94 + 43;
        Result.Size := Result.Size - 1;
        Result.Initial := False;

        if Sign (Result.Size) = GMP.Neutral then
            return Apply (Result.Start, Result.Value);
        else
            return Result;
        end if;
    end H_MIN;

    function H_MIN (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : Literal := This;
        Counter : constant BNFs.BNF := BNFs.BNF (Block.all (BNFs.Zero_BNF));
        Repeats : Big_Integer := BNFs.Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 43;
            Repeats := Repeats - 1;
        end loop;

        Result.Size := Result.Size - 1;
        Result.Initial := False;

        if Sign (Result.Size) = GMP.Neutral then
            return Apply (Result.Start, Result.Value);
        else
            return Result;
        end if;
    end H_MIN;

    function I_MIN (This : Literal) return Datatype'Class is
        Result : Literal := This;
    begin
        Result.Value := Result.Value * 94 + 44;
        Result.Size := Result.Size - 1;
        Result.Initial := False;

        if Sign (Result.Size) = GMP.Neutral then
            return Apply (Result.Start, Result.Value);
        else
            return Result;
        end if;
    end I_MIN;

    function I_MIN (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : Literal := This;
        Counter : constant BNFs.BNF := BNFs.BNF (Block.all (BNFs.Zero_BNF));
        Repeats : Big_Integer := BNFs.Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 44;
            Repeats := Repeats - 1;
        end loop;

        Result.Size := Result.Size - 1;
        Result.Initial := False;

        if Sign (Result.Size) = GMP.Neutral then
            return Apply (Result.Start, Result.Value);
        else
            return Result;
        end if;
    end I_MIN;

    function J_MIN (This : Literal) return Datatype'Class is
        Result : Literal := This;
    begin
        Result.Value := Result.Value * 94 + 45;
        Result.Size := Result.Size - 1;
        Result.Initial := False;

        if Sign (Result.Size) = GMP.Neutral then
            return Apply (Result.Start, Result.Value);
        else
            return Result;
        end if;
    end J_MIN;

    function J_MIN (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : Literal := This;
        Counter : constant BNFs.BNF := BNFs.BNF (Block.all (BNFs.Zero_BNF));
        Repeats : Big_Integer := BNFs.Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 45;
            Repeats := Repeats - 1;
        end loop;

        Result.Size := Result.Size - 1;
        Result.Initial := False;

        if Sign (Result.Size) = GMP.Neutral then
            return Apply (Result.Start, Result.Value);
        else
            return Result;
        end if;
    end J_MIN;

    function K_MIN (This : Literal) return Datatype'Class is
        Result : Literal := This;
    begin
        Result.Value := Result.Value * 94 + 46;
        Result.Size := Result.Size - 1;
        Result.Initial := False;

        if Sign (Result.Size) = GMP.Neutral then
            return Apply (Result.Start, Result.Value);
        else
            return Result;
        end if;
    end K_MIN;

    function K_MIN (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : Literal := This;
        Counter : constant BNFs.BNF := BNFs.BNF (Block.all (BNFs.Zero_BNF));
        Repeats : Big_Integer := BNFs.Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 46;
            Repeats := Repeats - 1;
        end loop;

        Result.Size := Result.Size - 1;
        Result.Initial := False;

        if Sign (Result.Size) = GMP.Neutral then
            return Apply (Result.Start, Result.Value);
        else
            return Result;
        end if;
    end K_MIN;

    function L_MIN (This : Literal) return Datatype'Class is
        Result : Literal := This;
    begin
        Result.Value := Result.Value * 94 + 47;
        Result.Size := Result.Size - 1;
        Result.Initial := False;

        if Sign (Result.Size) = GMP.Neutral then
            return Apply (Result.Start, Result.Value);
        else
            return Result;
        end if;
    end L_MIN;

    function L_MIN (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : Literal := This;
        Counter : constant BNFs.BNF := BNFs.BNF (Block.all (BNFs.Zero_BNF));
        Repeats : Big_Integer := BNFs.Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 47;
            Repeats := Repeats - 1;
        end loop;

        Result.Size := Result.Size - 1;
        Result.Initial := False;

        if Sign (Result.Size) = GMP.Neutral then
            return Apply (Result.Start, Result.Value);
        else
            return Result;
        end if;
    end L_MIN;

    function M_MIN (This : Literal) return Datatype'Class is
        Result : Literal := This;
    begin
        Result.Value := Result.Value * 94 + 48;
        Result.Size := Result.Size - 1;
        Result.Initial := False;

        if Sign (Result.Size) = GMP.Neutral then
            return Apply (Result.Start, Result.Value);
        else
            return Result;
        end if;
    end M_MIN;

    function M_MIN (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : Literal := This;
        Counter : constant BNFs.BNF := BNFs.BNF (Block.all (BNFs.Zero_BNF));
        Repeats : Big_Integer := BNFs.Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 48;
            Repeats := Repeats - 1;
        end loop;

        Result.Size := Result.Size - 1;
        Result.Initial := False;

        if Sign (Result.Size) = GMP.Neutral then
            return Apply (Result.Start, Result.Value);
        else
            return Result;
        end if;
    end M_MIN;

    function N_MIN (This : Literal) return Datatype'Class is
        Result : Literal := This;
    begin
        Result.Value := Result.Value * 94 + 49;
        Result.Size := Result.Size - 1;
        Result.Initial := False;

        if Sign (Result.Size) = GMP.Neutral then
            return Apply (Result.Start, Result.Value);
        else
            return Result;
        end if;
    end N_MIN;

    function N_MIN (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : Literal := This;
        Counter : constant BNFs.BNF := BNFs.BNF (Block.all (BNFs.Zero_BNF));
        Repeats : Big_Integer := BNFs.Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 49;
            Repeats := Repeats - 1;
        end loop;

        Result.Size := Result.Size - 1;
        Result.Initial := False;

        if Sign (Result.Size) = GMP.Neutral then
            return Apply (Result.Start, Result.Value);
        else
            return Result;
        end if;
    end N_MIN;

    function O_MIN (This : Literal) return Datatype'Class is
        Result : Literal := This;
    begin
        Result.Value := Result.Value * 94 + 50;
        Result.Size := Result.Size - 1;
        Result.Initial := False;

        if Sign (Result.Size) = GMP.Neutral then
            return Apply (Result.Start, Result.Value);
        else
            return Result;
        end if;
    end O_MIN;

    function O_MIN (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : Literal := This;
        Counter : constant BNFs.BNF := BNFs.BNF (Block.all (BNFs.Zero_BNF));
        Repeats : Big_Integer := BNFs.Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 50;
            Repeats := Repeats - 1;
        end loop;

        Result.Size := Result.Size - 1;
        Result.Initial := False;

        if Sign (Result.Size) = GMP.Neutral then
            return Apply (Result.Start, Result.Value);
        else
            return Result;
        end if;
    end O_MIN;

    function P_MIN (This : Literal) return Datatype'Class is
        Result : Literal := This;
    begin
        Result.Value := Result.Value * 94 + 51;
        Result.Size := Result.Size - 1;
        Result.Initial := False;

        if Sign (Result.Size) = GMP.Neutral then
            return Apply (Result.Start, Result.Value);
        else
            return Result;
        end if;
    end P_MIN;

    function P_MIN (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : Literal := This;
        Counter : constant BNFs.BNF := BNFs.BNF (Block.all (BNFs.Zero_BNF));
        Repeats : Big_Integer := BNFs.Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 51;
            Repeats := Repeats - 1;
        end loop;

        Result.Size := Result.Size - 1;
        Result.Initial := False;

        if Sign (Result.Size) = GMP.Neutral then
            return Apply (Result.Start, Result.Value);
        else
            return Result;
        end if;
    end P_MIN;

    function Q_MIN (This : Literal) return Datatype'Class is
        Result : Literal := This;
    begin
        Result.Value := Result.Value * 94 + 52;
        Result.Size := Result.Size - 1;
        Result.Initial := False;

        if Sign (Result.Size) = GMP.Neutral then
            return Apply (Result.Start, Result.Value);
        else
            return Result;
        end if;
    end Q_MIN;

    function Q_MIN (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : Literal := This;
        Counter : constant BNFs.BNF := BNFs.BNF (Block.all (BNFs.Zero_BNF));
        Repeats : Big_Integer := BNFs.Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 52;
            Repeats := Repeats - 1;
        end loop;

        Result.Size := Result.Size - 1;
        Result.Initial := False;

        if Sign (Result.Size) = GMP.Neutral then
            return Apply (Result.Start, Result.Value);
        else
            return Result;
        end if;
    end Q_MIN;

    function R_MIN (This : Literal) return Datatype'Class is
        Result : Literal := This;
    begin
        Result.Value := Result.Value * 94 + 53;
        Result.Size := Result.Size - 1;
        Result.Initial := False;

        if Sign (Result.Size) = GMP.Neutral then
            return Apply (Result.Start, Result.Value);
        else
            return Result;
        end if;
    end R_MIN;

    function R_MIN (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : Literal := This;
        Counter : constant BNFs.BNF := BNFs.BNF (Block.all (BNFs.Zero_BNF));
        Repeats : Big_Integer := BNFs.Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 53;
            Repeats := Repeats - 1;
        end loop;

        Result.Size := Result.Size - 1;
        Result.Initial := False;

        if Sign (Result.Size) = GMP.Neutral then
            return Apply (Result.Start, Result.Value);
        else
            return Result;
        end if;
    end R_MIN;

    function S_MIN (This : Literal) return Datatype'Class is
        Result : Literal := This;
    begin
        Result.Value := Result.Value * 94 + 54;
        Result.Size := Result.Size - 1;
        Result.Initial := False;

        if Sign (Result.Size) = GMP.Neutral then
            return Apply (Result.Start, Result.Value);
        else
            return Result;
        end if;
    end S_MIN;

    function S_MIN (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : Literal := This;
        Counter : constant BNFs.BNF := BNFs.BNF (Block.all (BNFs.Zero_BNF));
        Repeats : Big_Integer := BNFs.Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 54;
            Repeats := Repeats - 1;
        end loop;

        Result.Size := Result.Size - 1;
        Result.Initial := False;

        if Sign (Result.Size) = GMP.Neutral then
            return Apply (Result.Start, Result.Value);
        else
            return Result;
        end if;
    end S_MIN;

    function T_MIN (This : Literal) return Datatype'Class is
        Result : Literal := This;
    begin
        Result.Value := Result.Value * 94 + 55;
        Result.Size := Result.Size - 1;
        Result.Initial := False;

        if Sign (Result.Size) = GMP.Neutral then
            return Apply (Result.Start, Result.Value);
        else
            return Result;
        end if;
    end T_MIN;

    function T_MIN (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : Literal := This;
        Counter : constant BNFs.BNF := BNFs.BNF (Block.all (BNFs.Zero_BNF));
        Repeats : Big_Integer := BNFs.Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 55;
            Repeats := Repeats - 1;
        end loop;

        Result.Size := Result.Size - 1;
        Result.Initial := False;

        if Sign (Result.Size) = GMP.Neutral then
            return Apply (Result.Start, Result.Value);
        else
            return Result;
        end if;
    end T_MIN;

    function U_MIN (This : Literal) return Datatype'Class is
        Result : Literal := This;
    begin
        Result.Value := Result.Value * 94 + 56;
        Result.Size := Result.Size - 1;
        Result.Initial := False;

        if Sign (Result.Size) = GMP.Neutral then
            return Apply (Result.Start, Result.Value);
        else
            return Result;
        end if;
    end U_MIN;

    function U_MIN (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : Literal := This;
        Counter : constant BNFs.BNF := BNFs.BNF (Block.all (BNFs.Zero_BNF));
        Repeats : Big_Integer := BNFs.Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 56;
            Repeats := Repeats - 1;
        end loop;

        Result.Size := Result.Size - 1;
        Result.Initial := False;

        if Sign (Result.Size) = GMP.Neutral then
            return Apply (Result.Start, Result.Value);
        else
            return Result;
        end if;
    end U_MIN;

    function V_MIN (This : Literal) return Datatype'Class is
        Result : Literal := This;
    begin
        Result.Value := Result.Value * 94 + 57;
        Result.Size := Result.Size - 1;
        Result.Initial := False;

        if Sign (Result.Size) = GMP.Neutral then
            return Apply (Result.Start, Result.Value);
        else
            return Result;
        end if;
    end V_MIN;

    function V_MIN (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : Literal := This;
        Counter : constant BNFs.BNF := BNFs.BNF (Block.all (BNFs.Zero_BNF));
        Repeats : Big_Integer := BNFs.Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 57;
            Repeats := Repeats - 1;
        end loop;

        Result.Size := Result.Size - 1;
        Result.Initial := False;

        if Sign (Result.Size) = GMP.Neutral then
            return Apply (Result.Start, Result.Value);
        else
            return Result;
        end if;
    end V_MIN;

    function W_MIN (This : Literal) return Datatype'Class is
        Result : Literal := This;
    begin
        Result.Value := Result.Value * 94 + 58;
        Result.Size := Result.Size - 1;
        Result.Initial := False;

        if Sign (Result.Size) = GMP.Neutral then
            return Apply (Result.Start, Result.Value);
        else
            return Result;
        end if;
    end W_MIN;

    function W_MIN (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : Literal := This;
        Counter : constant BNFs.BNF := BNFs.BNF (Block.all (BNFs.Zero_BNF));
        Repeats : Big_Integer := BNFs.Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 58;
            Repeats := Repeats - 1;
        end loop;

        Result.Size := Result.Size - 1;
        Result.Initial := False;

        if Sign (Result.Size) = GMP.Neutral then
            return Apply (Result.Start, Result.Value);
        else
            return Result;
        end if;
    end W_MIN;

    function X_MIN (This : Literal) return Datatype'Class is
        Result : Literal := This;
    begin
        Result.Value := Result.Value * 94 + 59;
        Result.Size := Result.Size - 1;
        Result.Initial := False;

        if Sign (Result.Size) = GMP.Neutral then
            return Apply (Result.Start, Result.Value);
        else
            return Result;
        end if;
    end X_MIN;

    function X_MIN (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : Literal := This;
        Counter : constant BNFs.BNF := BNFs.BNF (Block.all (BNFs.Zero_BNF));
        Repeats : Big_Integer := BNFs.Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 59;
            Repeats := Repeats - 1;
        end loop;

        Result.Size := Result.Size - 1;
        Result.Initial := False;

        if Sign (Result.Size) = GMP.Neutral then
            return Apply (Result.Start, Result.Value);
        else
            return Result;
        end if;
    end X_MIN;

    function Y_MIN (This : Literal) return Datatype'Class is
        Result : Literal := This;
    begin
        Result.Value := Result.Value * 94 + 60;
        Result.Size := Result.Size - 1;
        Result.Initial := False;

        if Sign (Result.Size) = GMP.Neutral then
            return Apply (Result.Start, Result.Value);
        else
            return Result;
        end if;
    end Y_MIN;

    function Y_MIN (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : Literal := This;
        Counter : constant BNFs.BNF := BNFs.BNF (Block.all (BNFs.Zero_BNF));
        Repeats : Big_Integer := BNFs.Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 60;
            Repeats := Repeats - 1;
        end loop;

        Result.Size := Result.Size - 1;
        Result.Initial := False;

        if Sign (Result.Size) = GMP.Neutral then
            return Apply (Result.Start, Result.Value);
        else
            return Result;
        end if;
    end Y_MIN;

    function Z_MIN (This : Literal) return Datatype'Class is
        Result : Literal := This;
    begin
        Result.Value := Result.Value * 94 + 61;
        Result.Size := Result.Size - 1;
        Result.Initial := False;

        if Sign (Result.Size) = GMP.Neutral then
            return Apply (Result.Start, Result.Value);
        else
            return Result;
        end if;
    end Z_MIN;

    function Z_MIN (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : Literal := This;
        Counter : constant BNFs.BNF := BNFs.BNF (Block.all (BNFs.Zero_BNF));
        Repeats : Big_Integer := BNFs.Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 61;
            Repeats := Repeats - 1;
        end loop;

        Result.Size := Result.Size - 1;
        Result.Initial := False;

        if Sign (Result.Size) = GMP.Neutral then
            return Apply (Result.Start, Result.Value);
        else
            return Result;
        end if;
    end Z_MIN;

    function ADDIT (This : Literal) return Datatype'Class is
        Result : Literal := This;
    begin
        Result.Value := Result.Value * 94 + 62;
        Result.Size := Result.Size - 1;
        Result.Initial := False;

        if Sign (Result.Size) = GMP.Neutral then
            return Apply (Result.Start, Result.Value);
        else
            return Result;
        end if;
    end ADDIT;

    function ADDIT (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : Literal := This;
        Counter : constant BNFs.BNF := BNFs.BNF (Block.all (BNFs.Zero_BNF));
        Repeats : Big_Integer := BNFs.Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 62;
            Repeats := Repeats - 1;
        end loop;

        Result.Size := Result.Size - 1;
        Result.Initial := False;

        if Sign (Result.Size) = GMP.Neutral then
            return Apply (Result.Start, Result.Value);
        else
            return Result;
        end if;
    end ADDIT;

    function SLASH (This : Literal) return Datatype'Class is
        Result : Literal := This;
    begin
        Result.Value := Result.Value * 94 + 63;
        Result.Size := Result.Size - 1;
        Result.Initial := False;

        if Sign (Result.Size) = GMP.Neutral then
            return Apply (Result.Start, Result.Value);
        else
            return Result;
        end if;
    end SLASH;

    function SLASH (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : Literal := This;
        Counter : constant BNFs.BNF := BNFs.BNF (Block.all (BNFs.Zero_BNF));
        Repeats : Big_Integer := BNFs.Get_Value (Counter) + 4;
    begin
        while Sign (Repeats) /= GMP.Neutral loop
            Result.Value := Result.Value * 94 + 63;
            Repeats := Repeats - 1;
        end loop;

        Result.Size := Result.Size - 1;
        Result.Initial := False;

        if Sign (Result.Size) = GMP.Neutral then
            return Apply (Result.Start, Result.Value);
        else
            return Result;
        end if;
    end SLASH;

    function MINUS (This : Literal) return Datatype'Class is
        Result : Literal := This;
    begin
        if Result.Initial then
            if Result.Size = 1 then
                Result.Size := Create (0);
            else
                Result.Size := Result.Size - 2;
            end if;

            Result.Size := Result.Size * 30 + 0;
            Result.Size := Result.Size + 2;

            return Result;
        else
            Result.Value := Result.Value * 94 + 64;
            Result.Size := Result.Size - 1;

            if Sign (Result.Size) = GMP.Neutral then
                return Apply (Result.Start, Result.Value);
            else
                return Result;
            end if;
        end if;
    end MINUS;

    function MINUS (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : Literal := This;
        Counter : constant BNFs.BNF := BNFs.BNF (Block.all (BNFs.Zero_BNF));
        Repeats : Big_Integer := BNFs.Get_Value (Counter) + 4;
    begin
        if Result.Initial then
            if Result.Size = 1 then
                Result.Size := Create (0);
            else
                Result.Size := Result.Size - 2;
            end if;

            while Sign (Repeats) /= GMP.Neutral loop
                Result.Size := Result.Size * 30 + 0;
                Repeats := Repeats - 1;
            end loop;

            Result.Size := Result.Size + 2;

            return Result;
        else
            while Sign (Repeats) /= GMP.Neutral loop
                Result.Value := Result.Value * 94 + 64;
                Repeats := Repeats - 1;
            end loop;

            Result.Size := Result.Size - 1;

            if Sign (Result.Size) = GMP.Neutral then
                return Apply (Result.Start, Result.Value);
            else
                return Result;
            end if;
        end if;
    end MINUS;

    function TIMES (This : Literal) return Datatype'Class is
        Result : Literal := This;
    begin
        if Result.Initial then
            if Result.Size = 1 then
                Result.Size := Create (0);
            else
                Result.Size := Result.Size - 2;
            end if;

            Result.Size := Result.Size * 30 + 1;
            Result.Size := Result.Size + 2;

            return Result;
        else
            Result.Value := Result.Value * 94 + 65;
            Result.Size := Result.Size - 1;

            if Sign (Result.Size) = GMP.Neutral then
                return Apply (Result.Start, Result.Value);
            else
                return Result;
            end if;
        end if;
    end TIMES;

    function TIMES (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : Literal := This;
        Counter : constant BNFs.BNF := BNFs.BNF (Block.all (BNFs.Zero_BNF));
        Repeats : Big_Integer := BNFs.Get_Value (Counter) + 4;
    begin
        if Result.Initial then
            if Result.Size = 1 then
                Result.Size := Create (0);
            else
                Result.Size := Result.Size - 2;
            end if;

            while Sign (Repeats) /= GMP.Neutral loop
                Result.Size := Result.Size * 30 + 1;
                Repeats := Repeats - 1;
            end loop;

            Result.Size := Result.Size + 2;

            return Result;
        else
            while Sign (Repeats) /= GMP.Neutral loop
                Result.Value := Result.Value * 94 + 65;
                Repeats := Repeats - 1;
            end loop;

            Result.Size := Result.Size - 1;

            if Sign (Result.Size) = GMP.Neutral then
                return Apply (Result.Start, Result.Value);
            else
                return Result;
            end if;
        end if;
    end TIMES;

    function VPIPE (This : Literal) return Datatype'Class is
        Result : Literal := This;
    begin
        if Result.Initial then
            if Result.Size = 1 then
                Result.Size := Create (0);
            else
                Result.Size := Result.Size - 2;
            end if;

            Result.Size := Result.Size * 30 + 2;
            Result.Size := Result.Size + 2;

            return Result;
        else
            Result.Value := Result.Value * 94 + 66;
            Result.Size := Result.Size - 1;

            if Sign (Result.Size) = GMP.Neutral then
                return Apply (Result.Start, Result.Value);
            else
                return Result;
            end if;
        end if;
    end VPIPE;

    function VPIPE (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : Literal := This;
        Counter : constant BNFs.BNF := BNFs.BNF (Block.all (BNFs.Zero_BNF));
        Repeats : Big_Integer := BNFs.Get_Value (Counter) + 4;
    begin
        if Result.Initial then
            if Result.Size = 1 then
                Result.Size := Create (0);
            else
                Result.Size := Result.Size - 2;
            end if;

            while Sign (Repeats) /= GMP.Neutral loop
                Result.Size := Result.Size * 30 + 2;
                Repeats := Repeats - 1;
            end loop;

            Result.Size := Result.Size + 2;

            return Result;
        else
            while Sign (Repeats) /= GMP.Neutral loop
                Result.Value := Result.Value * 94 + 66;
                Repeats := Repeats - 1;
            end loop;

            Result.Size := Result.Size - 1;

            if Sign (Result.Size) = GMP.Neutral then
                return Apply (Result.Start, Result.Value);
            else
                return Result;
            end if;
        end if;
    end VPIPE;

    function CARET (This : Literal) return Datatype'Class is
        Result : Literal := This;
    begin
        if Result.Initial then
            if Result.Size = 1 then
                Result.Size := Create (0);
            else
                Result.Size := Result.Size - 2;
            end if;

            Result.Size := Result.Size * 30 + 3;
            Result.Size := Result.Size + 2;

            return Result;
        else
            Result.Value := Result.Value * 94 + 67;
            Result.Size := Result.Size - 1;

            if Sign (Result.Size) = GMP.Neutral then
                return Apply (Result.Start, Result.Value);
            else
                return Result;
            end if;
        end if;
    end CARET;

    function CARET (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : Literal := This;
        Counter : constant BNFs.BNF := BNFs.BNF (Block.all (BNFs.Zero_BNF));
        Repeats : Big_Integer := BNFs.Get_Value (Counter) + 4;
    begin
        if Result.Initial then
            if Result.Size = 1 then
                Result.Size := Create (0);
            else
                Result.Size := Result.Size - 2;
            end if;

            while Sign (Repeats) /= GMP.Neutral loop
                Result.Size := Result.Size * 30 + 3;
                Repeats := Repeats - 1;
            end loop;

            Result.Size := Result.Size + 2;

            return Result;
        else
            while Sign (Repeats) /= GMP.Neutral loop
                Result.Value := Result.Value * 94 + 67;
                Repeats := Repeats - 1;
            end loop;

            Result.Size := Result.Size - 1;

            if Sign (Result.Size) = GMP.Neutral then
                return Apply (Result.Start, Result.Value);
            else
                return Result;
            end if;
        end if;
    end CARET;

    function AMPER (This : Literal) return Datatype'Class is
        Result : Literal := This;
    begin
        if Result.Initial then
            if Result.Size = 1 then
                Result.Size := Create (0);
            else
                Result.Size := Result.Size - 2;
            end if;

            Result.Size := Result.Size * 30 + 4;
            Result.Size := Result.Size + 2;

            return Result;
        else
            Result.Value := Result.Value * 94 + 68;
            Result.Size := Result.Size - 1;

            if Sign (Result.Size) = GMP.Neutral then
                return Apply (Result.Start, Result.Value);
            else
                return Result;
            end if;
        end if;
    end AMPER;

    function AMPER (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : Literal := This;
        Counter : constant BNFs.BNF := BNFs.BNF (Block.all (BNFs.Zero_BNF));
        Repeats : Big_Integer := BNFs.Get_Value (Counter) + 4;
    begin
        if Result.Initial then
            if Result.Size = 1 then
                Result.Size := Create (0);
            else
                Result.Size := Result.Size - 2;
            end if;

            while Sign (Repeats) /= GMP.Neutral loop
                Result.Size := Result.Size * 30 + 4;
                Repeats := Repeats - 1;
            end loop;

            Result.Size := Result.Size + 2;

            return Result;
        else
            while Sign (Repeats) /= GMP.Neutral loop
                Result.Value := Result.Value * 94 + 68;
                Repeats := Repeats - 1;
            end loop;

            Result.Size := Result.Size - 1;

            if Sign (Result.Size) = GMP.Neutral then
                return Apply (Result.Start, Result.Value);
            else
                return Result;
            end if;
        end if;
    end AMPER;

    function PRCNT (This : Literal) return Datatype'Class is
        Result : Literal := This;
    begin
        if Result.Initial then
            if Result.Size = 1 then
                Result.Size := Create (0);
            else
                Result.Size := Result.Size - 2;
            end if;

            Result.Size := Result.Size * 30 + 5;
            Result.Size := Result.Size + 2;

            return Result;
        else
            Result.Value := Result.Value * 94 + 69;
            Result.Size := Result.Size - 1;

            if Sign (Result.Size) = GMP.Neutral then
                return Apply (Result.Start, Result.Value);
            else
                return Result;
            end if;
        end if;
    end PRCNT;

    function PRCNT (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : Literal := This;
        Counter : constant BNFs.BNF := BNFs.BNF (Block.all (BNFs.Zero_BNF));
        Repeats : Big_Integer := BNFs.Get_Value (Counter) + 4;
    begin
        if Result.Initial then
            if Result.Size = 1 then
                Result.Size := Create (0);
            else
                Result.Size := Result.Size - 2;
            end if;

            while Sign (Repeats) /= GMP.Neutral loop
                Result.Size := Result.Size * 30 + 5;
                Repeats := Repeats - 1;
            end loop;

            Result.Size := Result.Size + 2;

            return Result;
        else
            while Sign (Repeats) /= GMP.Neutral loop
                Result.Value := Result.Value * 94 + 69;
                Repeats := Repeats - 1;
            end loop;

            Result.Size := Result.Size - 1;

            if Sign (Result.Size) = GMP.Neutral then
                return Apply (Result.Start, Result.Value);
            else
                return Result;
            end if;
        end if;
    end PRCNT;

    function LESST (This : Literal) return Datatype'Class is
        Result : Literal := This;
    begin
        if Result.Initial then
            if Result.Size = 1 then
                Result.Size := Create (0);
            else
                Result.Size := Result.Size - 2;
            end if;

            Result.Size := Result.Size * 30 + 6;
            Result.Size := Result.Size + 2;

            return Result;
        else
            Result.Value := Result.Value * 94 + 70;
            Result.Size := Result.Size - 1;

            if Sign (Result.Size) = GMP.Neutral then
                return Apply (Result.Start, Result.Value);
            else
                return Result;
            end if;
        end if;
    end LESST;

    function LESST (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : Literal := This;
        Counter : constant BNFs.BNF := BNFs.BNF (Block.all (BNFs.Zero_BNF));
        Repeats : Big_Integer := BNFs.Get_Value (Counter) + 4;
    begin
        if Result.Initial then
            if Result.Size = 1 then
                Result.Size := Create (0);
            else
                Result.Size := Result.Size - 2;
            end if;

            while Sign (Repeats) /= GMP.Neutral loop
                Result.Size := Result.Size * 30 + 6;
                Repeats := Repeats - 1;
            end loop;

            Result.Size := Result.Size + 2;

            return Result;
        else
            while Sign (Repeats) /= GMP.Neutral loop
                Result.Value := Result.Value * 94 + 70;
                Repeats := Repeats - 1;
            end loop;

            Result.Size := Result.Size - 1;

            if Sign (Result.Size) = GMP.Neutral then
                return Apply (Result.Start, Result.Value);
            else
                return Result;
            end if;
        end if;
    end LESST;

    function MORET (This : Literal) return Datatype'Class is
        Result : Literal := This;
    begin
        if Result.Initial then
            if Result.Size = 1 then
                Result.Size := Create (0);
            else
                Result.Size := Result.Size - 2;
            end if;

            Result.Size := Result.Size * 30 + 7;
            Result.Size := Result.Size + 2;

            return Result;
        else
            Result.Value := Result.Value * 94 + 71;
            Result.Size := Result.Size - 1;

            if Sign (Result.Size) = GMP.Neutral then
                return Apply (Result.Start, Result.Value);
            else
                return Result;
            end if;
        end if;
    end MORET;

    function MORET (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : Literal := This;
        Counter : constant BNFs.BNF := BNFs.BNF (Block.all (BNFs.Zero_BNF));
        Repeats : Big_Integer := BNFs.Get_Value (Counter) + 4;
    begin
        if Result.Initial then
            if Result.Size = 1 then
                Result.Size := Create (0);
            else
                Result.Size := Result.Size - 2;
            end if;

            while Sign (Repeats) /= GMP.Neutral loop
                Result.Size := Result.Size * 30 + 7;
                Repeats := Repeats - 1;
            end loop;

            Result.Size := Result.Size + 2;

            return Result;
        else
            while Sign (Repeats) /= GMP.Neutral loop
                Result.Value := Result.Value * 94 + 71;
                Repeats := Repeats - 1;
            end loop;

            Result.Size := Result.Size - 1;

            if Sign (Result.Size) = GMP.Neutral then
                return Apply (Result.Start, Result.Value);
            else
                return Result;
            end if;
        end if;
    end MORET;

    function PAR_L (This : Literal) return Datatype'Class is
        Result : Literal := This;
    begin
        if Result.Initial then
            if Result.Size = 1 then
                Result.Size := Create (0);
            else
                Result.Size := Result.Size - 2;
            end if;

            Result.Size := Result.Size * 30 + 8;
            Result.Size := Result.Size + 2;

            return Result;
        else
            Result.Value := Result.Value * 94 + 72;
            Result.Size := Result.Size - 1;

            if Sign (Result.Size) = GMP.Neutral then
                return Apply (Result.Start, Result.Value);
            else
                return Result;
            end if;
        end if;
    end PAR_L;

    function PAR_L (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : Literal := This;
        Counter : constant BNFs.BNF := BNFs.BNF (Block.all (BNFs.Zero_BNF));
        Repeats : Big_Integer := BNFs.Get_Value (Counter) + 4;
    begin
        if Result.Initial then
            if Result.Size = 1 then
                Result.Size := Create (0);
            else
                Result.Size := Result.Size - 2;
            end if;

            while Sign (Repeats) /= GMP.Neutral loop
                Result.Size := Result.Size * 30 + 8;
                Repeats := Repeats - 1;
            end loop;

            Result.Size := Result.Size + 2;

            return Result;
        else
            while Sign (Repeats) /= GMP.Neutral loop
                Result.Value := Result.Value * 94 + 72;
                Repeats := Repeats - 1;
            end loop;

            Result.Size := Result.Size - 1;

            if Sign (Result.Size) = GMP.Neutral then
                return Apply (Result.Start, Result.Value);
            else
                return Result;
            end if;
        end if;
    end PAR_L;

    function PAR_R (This : Literal) return Datatype'Class is
        Result : Literal := This;
    begin
        if Result.Initial then
            if Result.Size = 1 then
                Result.Size := Create (0);
            else
                Result.Size := Result.Size - 2;
            end if;

            Result.Size := Result.Size * 30 + 9;
            Result.Size := Result.Size + 2;

            return Result;
        else
            Result.Value := Result.Value * 94 + 73;
            Result.Size := Result.Size - 1;

            if Sign (Result.Size) = GMP.Neutral then
                return Apply (Result.Start, Result.Value);
            else
                return Result;
            end if;
        end if;
    end PAR_R;

    function PAR_R (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : Literal := This;
        Counter : constant BNFs.BNF := BNFs.BNF (Block.all (BNFs.Zero_BNF));
        Repeats : Big_Integer := BNFs.Get_Value (Counter) + 4;
    begin
        if Result.Initial then
            if Result.Size = 1 then
                Result.Size := Create (0);
            else
                Result.Size := Result.Size - 2;
            end if;

            while Sign (Repeats) /= GMP.Neutral loop
                Result.Size := Result.Size * 30 + 9;
                Repeats := Repeats - 1;
            end loop;

            Result.Size := Result.Size + 2;

            return Result;
        else
            while Sign (Repeats) /= GMP.Neutral loop
                Result.Value := Result.Value * 94 + 73;
                Repeats := Repeats - 1;
            end loop;

            Result.Size := Result.Size - 1;

            if Sign (Result.Size) = GMP.Neutral then
                return Apply (Result.Start, Result.Value);
            else
                return Result;
            end if;
        end if;
    end PAR_R;

    function BRA_L (This : Literal) return Datatype'Class is
        Result : Literal := This;
    begin
        if Result.Initial then
            if Result.Size = 1 then
                Result.Size := Create (0);
            else
                Result.Size := Result.Size - 2;
            end if;

            Result.Size := Result.Size * 30 + 10;
            Result.Size := Result.Size + 2;

            return Result;
        else
            Result.Value := Result.Value * 94 + 74;
            Result.Size := Result.Size - 1;

            if Sign (Result.Size) = GMP.Neutral then
                return Apply (Result.Start, Result.Value);
            else
                return Result;
            end if;
        end if;
    end BRA_L;

    function BRA_L (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : Literal := This;
        Counter : constant BNFs.BNF := BNFs.BNF (Block.all (BNFs.Zero_BNF));
        Repeats : Big_Integer := BNFs.Get_Value (Counter) + 4;
    begin
        if Result.Initial then
            if Result.Size = 1 then
                Result.Size := Create (0);
            else
                Result.Size := Result.Size - 2;
            end if;

            while Sign (Repeats) /= GMP.Neutral loop
                Result.Size := Result.Size * 30 + 10;
                Repeats := Repeats - 1;
            end loop;

            Result.Size := Result.Size + 2;

            return Result;
        else
            while Sign (Repeats) /= GMP.Neutral loop
                Result.Value := Result.Value * 94 + 74;
                Repeats := Repeats - 1;
            end loop;

            Result.Size := Result.Size - 1;

            if Sign (Result.Size) = GMP.Neutral then
                return Apply (Result.Start, Result.Value);
            else
                return Result;
            end if;
        end if;
    end BRA_L;

    function BRA_R (This : Literal) return Datatype'Class is
        Result : Literal := This;
    begin
        if Result.Initial then
            if Result.Size = 1 then
                Result.Size := Create (0);
            else
                Result.Size := Result.Size - 2;
            end if;

            Result.Size := Result.Size * 30 + 11;
            Result.Size := Result.Size + 2;

            return Result;
        else
            Result.Value := Result.Value * 94 + 75;
            Result.Size := Result.Size - 1;

            if Sign (Result.Size) = GMP.Neutral then
                return Apply (Result.Start, Result.Value);
            else
                return Result;
            end if;
        end if;
    end BRA_R;

    function BRA_R (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : Literal := This;
        Counter : constant BNFs.BNF := BNFs.BNF (Block.all (BNFs.Zero_BNF));
        Repeats : Big_Integer := BNFs.Get_Value (Counter) + 4;
    begin
        if Result.Initial then
            if Result.Size = 1 then
                Result.Size := Create (0);
            else
                Result.Size := Result.Size - 2;
            end if;

            while Sign (Repeats) /= GMP.Neutral loop
                Result.Size := Result.Size * 30 + 11;
                Repeats := Repeats - 1;
            end loop;

            Result.Size := Result.Size + 2;

            return Result;
        else
            while Sign (Repeats) /= GMP.Neutral loop
                Result.Value := Result.Value * 94 + 75;
                Repeats := Repeats - 1;
            end loop;

            Result.Size := Result.Size - 1;

            if Sign (Result.Size) = GMP.Neutral then
                return Apply (Result.Start, Result.Value);
            else
                return Result;
            end if;
        end if;
    end BRA_R;

    function CUR_L (This : Literal) return Datatype'Class is
        Result : Literal := This;
    begin
        if Result.Initial then
            if Result.Size = 1 then
                Result.Size := Create (0);
            else
                Result.Size := Result.Size - 2;
            end if;

            Result.Size := Result.Size * 30 + 12;
            Result.Size := Result.Size + 2;

            return Result;
        else
            Result.Value := Result.Value * 94 + 76;
            Result.Size := Result.Size - 1;

            if Sign (Result.Size) = GMP.Neutral then
                return Apply (Result.Start, Result.Value);
            else
                return Result;
            end if;
        end if;
    end CUR_L;

    function CUR_L (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : Literal := This;
        Counter : constant BNFs.BNF := BNFs.BNF (Block.all (BNFs.Zero_BNF));
        Repeats : Big_Integer := BNFs.Get_Value (Counter) + 4;
    begin
        if Result.Initial then
            if Result.Size = 1 then
                Result.Size := Create (0);
            else
                Result.Size := Result.Size - 2;
            end if;

            while Sign (Repeats) /= GMP.Neutral loop
                Result.Size := Result.Size * 30 + 12;
                Repeats := Repeats - 1;
            end loop;

            Result.Size := Result.Size + 2;

            return Result;
        else
            while Sign (Repeats) /= GMP.Neutral loop
                Result.Value := Result.Value * 94 + 76;
                Repeats := Repeats - 1;
            end loop;

            Result.Size := Result.Size - 1;

            if Sign (Result.Size) = GMP.Neutral then
                return Apply (Result.Start, Result.Value);
            else
                return Result;
            end if;
        end if;
    end CUR_L;

    function CUR_R (This : Literal) return Datatype'Class is
        Result : Literal := This;
    begin
        if Result.Initial then
            if Result.Size = 1 then
                Result.Size := Create (0);
            else
                Result.Size := Result.Size - 2;
            end if;

            Result.Size := Result.Size * 30 + 13;
            Result.Size := Result.Size + 2;

            return Result;
        else
            Result.Value := Result.Value * 94 + 77;
            Result.Size := Result.Size - 1;

            if Sign (Result.Size) = GMP.Neutral then
                return Apply (Result.Start, Result.Value);
            else
                return Result;
            end if;
        end if;
    end CUR_R;

    function CUR_R (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : Literal := This;
        Counter : constant BNFs.BNF := BNFs.BNF (Block.all (BNFs.Zero_BNF));
        Repeats : Big_Integer := BNFs.Get_Value (Counter) + 4;
    begin
        if Result.Initial then
            if Result.Size = 1 then
                Result.Size := Create (0);
            else
                Result.Size := Result.Size - 2;
            end if;

            while Sign (Repeats) /= GMP.Neutral loop
                Result.Size := Result.Size * 30 + 13;
                Repeats := Repeats - 1;
            end loop;

            Result.Size := Result.Size + 2;

            return Result;
        else
            while Sign (Repeats) /= GMP.Neutral loop
                Result.Value := Result.Value * 94 + 77;
                Repeats := Repeats - 1;
            end loop;

            Result.Size := Result.Size - 1;

            if Sign (Result.Size) = GMP.Neutral then
                return Apply (Result.Start, Result.Value);
            else
                return Result;
            end if;
        end if;
    end CUR_R;

    function O_DOT (This : Literal) return Datatype'Class is
        Result : Literal := This;
    begin
        if Result.Initial then
            if Result.Size = 1 then
                Result.Size := Create (0);
            else
                Result.Size := Result.Size - 2;
            end if;

            Result.Size := Result.Size * 30 + 14;
            Result.Size := Result.Size + 2;

            return Result;
        else
            Result.Value := Result.Value * 94 + 78;
            Result.Size := Result.Size - 1;

            if Sign (Result.Size) = GMP.Neutral then
                return Apply (Result.Start, Result.Value);
            else
                return Result;
            end if;
        end if;
    end O_DOT;

    function O_DOT (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : Literal := This;
        Counter : constant BNFs.BNF := BNFs.BNF (Block.all (BNFs.Zero_BNF));
        Repeats : Big_Integer := BNFs.Get_Value (Counter) + 4;
    begin
        if Result.Initial then
            if Result.Size = 1 then
                Result.Size := Create (0);
            else
                Result.Size := Result.Size - 2;
            end if;

            while Sign (Repeats) /= GMP.Neutral loop
                Result.Size := Result.Size * 30 + 14;
                Repeats := Repeats - 1;
            end loop;

            Result.Size := Result.Size + 2;

            return Result;
        else
            while Sign (Repeats) /= GMP.Neutral loop
                Result.Value := Result.Value * 94 + 78;
                Repeats := Repeats - 1;
            end loop;

            Result.Size := Result.Size - 1;

            if Sign (Result.Size) = GMP.Neutral then
                return Apply (Result.Start, Result.Value);
            else
                return Result;
            end if;
        end if;
    end O_DOT;

    function COMMA (This : Literal) return Datatype'Class is
        Result : Literal := This;
    begin
        if Result.Initial then
            if Result.Size = 1 then
                Result.Size := Create (0);
            else
                Result.Size := Result.Size - 2;
            end if;

            Result.Size := Result.Size * 30 + 15;
            Result.Size := Result.Size + 2;

            return Result;
        else
            Result.Value := Result.Value * 94 + 79;
            Result.Size := Result.Size - 1;

            if Sign (Result.Size) = GMP.Neutral then
                return Apply (Result.Start, Result.Value);
            else
                return Result;
            end if;
        end if;
    end COMMA;

    function COMMA (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : Literal := This;
        Counter : constant BNFs.BNF := BNFs.BNF (Block.all (BNFs.Zero_BNF));
        Repeats : Big_Integer := BNFs.Get_Value (Counter) + 4;
    begin
        if Result.Initial then
            if Result.Size = 1 then
                Result.Size := Create (0);
            else
                Result.Size := Result.Size - 2;
            end if;

            while Sign (Repeats) /= GMP.Neutral loop
                Result.Size := Result.Size * 30 + 15;
                Repeats := Repeats - 1;
            end loop;

            Result.Size := Result.Size + 2;

            return Result;
        else
            while Sign (Repeats) /= GMP.Neutral loop
                Result.Value := Result.Value * 94 + 79;
                Repeats := Repeats - 1;
            end loop;

            Result.Size := Result.Size - 1;

            if Sign (Result.Size) = GMP.Neutral then
                return Apply (Result.Start, Result.Value);
            else
                return Result;
            end if;
        end if;
    end COMMA;

    function SEMIC (This : Literal) return Datatype'Class is
        Result : Literal := This;
    begin
        if Result.Initial then
            if Result.Size = 1 then
                Result.Size := Create (0);
            else
                Result.Size := Result.Size - 2;
            end if;

            Result.Size := Result.Size * 30 + 16;
            Result.Size := Result.Size + 2;

            return Result;
        else
            Result.Value := Result.Value * 94 + 80;
            Result.Size := Result.Size - 1;

            if Sign (Result.Size) = GMP.Neutral then
                return Apply (Result.Start, Result.Value);
            else
                return Result;
            end if;
        end if;
    end SEMIC;

    function SEMIC (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : Literal := This;
        Counter : constant BNFs.BNF := BNFs.BNF (Block.all (BNFs.Zero_BNF));
        Repeats : Big_Integer := BNFs.Get_Value (Counter) + 4;
    begin
        if Result.Initial then
            if Result.Size = 1 then
                Result.Size := Create (0);
            else
                Result.Size := Result.Size - 2;
            end if;

            while Sign (Repeats) /= GMP.Neutral loop
                Result.Size := Result.Size * 30 + 16;
                Repeats := Repeats - 1;
            end loop;

            Result.Size := Result.Size + 2;

            return Result;
        else
            while Sign (Repeats) /= GMP.Neutral loop
                Result.Value := Result.Value * 94 + 80;
                Repeats := Repeats - 1;
            end loop;

            Result.Size := Result.Size - 1;

            if Sign (Result.Size) = GMP.Neutral then
                return Apply (Result.Start, Result.Value);
            else
                return Result;
            end if;
        end if;
    end SEMIC;

    function COLON (This : Literal) return Datatype'Class is
        Result : Literal := This;
    begin
        if Result.Initial then
            if Result.Size = 1 then
                Result.Size := Create (0);
            else
                Result.Size := Result.Size - 2;
            end if;

            Result.Size := Result.Size * 30 + 17;
            Result.Size := Result.Size + 2;

            return Result;
        else
            Result.Value := Result.Value * 94 + 81;
            Result.Size := Result.Size - 1;

            if Sign (Result.Size) = GMP.Neutral then
                return Apply (Result.Start, Result.Value);
            else
                return Result;
            end if;
        end if;
    end COLON;

    function COLON (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : Literal := This;
        Counter : constant BNFs.BNF := BNFs.BNF (Block.all (BNFs.Zero_BNF));
        Repeats : Big_Integer := BNFs.Get_Value (Counter) + 4;
    begin
        if Result.Initial then
            if Result.Size = 1 then
                Result.Size := Create (0);
            else
                Result.Size := Result.Size - 2;
            end if;

            while Sign (Repeats) /= GMP.Neutral loop
                Result.Size := Result.Size * 30 + 17;
                Repeats := Repeats - 1;
            end loop;

            Result.Size := Result.Size + 2;

            return Result;
        else
            while Sign (Repeats) /= GMP.Neutral loop
                Result.Value := Result.Value * 94 + 81;
                Repeats := Repeats - 1;
            end loop;

            Result.Size := Result.Size - 1;

            if Sign (Result.Size) = GMP.Neutral then
                return Apply (Result.Start, Result.Value);
            else
                return Result;
            end if;
        end if;
    end COLON;

    function APOST (This : Literal) return Datatype'Class is
        Result : Literal := This;
    begin
        if Result.Initial then
            if Result.Size = 1 then
                Result.Size := Create (0);
            else
                Result.Size := Result.Size - 2;
            end if;

            Result.Size := Result.Size * 30 + 18;
            Result.Size := Result.Size + 2;

            return Result;
        else
            Result.Value := Result.Value * 94 + 82;
            Result.Size := Result.Size - 1;

            if Sign (Result.Size) = GMP.Neutral then
                return Apply (Result.Start, Result.Value);
            else
                return Result;
            end if;
        end if;
    end APOST;

    function APOST (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : Literal := This;
        Counter : constant BNFs.BNF := BNFs.BNF (Block.all (BNFs.Zero_BNF));
        Repeats : Big_Integer := BNFs.Get_Value (Counter) + 4;
    begin
        if Result.Initial then
            if Result.Size = 1 then
                Result.Size := Create (0);
            else
                Result.Size := Result.Size - 2;
            end if;

            while Sign (Repeats) /= GMP.Neutral loop
                Result.Size := Result.Size * 30 + 18;
                Repeats := Repeats - 1;
            end loop;

            Result.Size := Result.Size + 2;

            return Result;
        else
            while Sign (Repeats) /= GMP.Neutral loop
                Result.Value := Result.Value * 94 + 82;
                Repeats := Repeats - 1;
            end loop;

            Result.Size := Result.Size - 1;

            if Sign (Result.Size) = GMP.Neutral then
                return Apply (Result.Start, Result.Value);
            else
                return Result;
            end if;
        end if;
    end APOST;

    function QUOTE (This : Literal) return Datatype'Class is
        Result : Literal := This;
    begin
        if Result.Initial then
            if Result.Size = 1 then
                Result.Size := Create (0);
            else
                Result.Size := Result.Size - 2;
            end if;

            Result.Size := Result.Size * 30 + 19;
            Result.Size := Result.Size + 2;

            return Result;
        else
            Result.Value := Result.Value * 94 + 83;
            Result.Size := Result.Size - 1;

            if Sign (Result.Size) = GMP.Neutral then
                return Apply (Result.Start, Result.Value);
            else
                return Result;
            end if;
        end if;
    end QUOTE;

    function QUOTE (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : Literal := This;
        Counter : constant BNFs.BNF := BNFs.BNF (Block.all (BNFs.Zero_BNF));
        Repeats : Big_Integer := BNFs.Get_Value (Counter) + 4;
    begin
        if Result.Initial then
            if Result.Size = 1 then
                Result.Size := Create (0);
            else
                Result.Size := Result.Size - 2;
            end if;

            while Sign (Repeats) /= GMP.Neutral loop
                Result.Size := Result.Size * 30 + 19;
                Repeats := Repeats - 1;
            end loop;

            Result.Size := Result.Size + 2;

            return Result;
        else
            while Sign (Repeats) /= GMP.Neutral loop
                Result.Value := Result.Value * 94 + 83;
                Repeats := Repeats - 1;
            end loop;

            Result.Size := Result.Size - 1;

            if Sign (Result.Size) = GMP.Neutral then
                return Apply (Result.Start, Result.Value);
            else
                return Result;
            end if;
        end if;
    end QUOTE;

    function BTICK (This : Literal) return Datatype'Class is
        Result : Literal := This;
    begin
        if Result.Initial then
            if Result.Size = 1 then
                Result.Size := Create (0);
            else
                Result.Size := Result.Size - 2;
            end if;

            Result.Size := Result.Size * 30 + 20;
            Result.Size := Result.Size + 2;

            return Result;
        else
            Result.Value := Result.Value * 94 + 84;
            Result.Size := Result.Size - 1;

            if Sign (Result.Size) = GMP.Neutral then
                return Apply (Result.Start, Result.Value);
            else
                return Result;
            end if;
        end if;
    end BTICK;

    function BTICK (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : Literal := This;
        Counter : constant BNFs.BNF := BNFs.BNF (Block.all (BNFs.Zero_BNF));
        Repeats : Big_Integer := BNFs.Get_Value (Counter) + 4;
    begin
        if Result.Initial then
            if Result.Size = 1 then
                Result.Size := Create (0);
            else
                Result.Size := Result.Size - 2;
            end if;

            while Sign (Repeats) /= GMP.Neutral loop
                Result.Size := Result.Size * 30 + 20;
                Repeats := Repeats - 1;
            end loop;

            Result.Size := Result.Size + 2;

            return Result;
        else
            while Sign (Repeats) /= GMP.Neutral loop
                Result.Value := Result.Value * 94 + 84;
                Repeats := Repeats - 1;
            end loop;

            Result.Size := Result.Size - 1;

            if Sign (Result.Size) = GMP.Neutral then
                return Apply (Result.Start, Result.Value);
            else
                return Result;
            end if;
        end if;
    end BTICK;

    function TILDE (This : Literal) return Datatype'Class is
        Result : Literal := This;
    begin
        if Result.Initial then
            if Result.Size = 1 then
                Result.Size := Create (0);
            else
                Result.Size := Result.Size - 2;
            end if;

            Result.Size := Result.Size * 30 + 21;
            Result.Size := Result.Size + 2;

            return Result;
        else
            Result.Value := Result.Value * 94 + 85;
            Result.Size := Result.Size - 1;

            if Sign (Result.Size) = GMP.Neutral then
                return Apply (Result.Start, Result.Value);
            else
                return Result;
            end if;
        end if;
    end TILDE;

    function TILDE (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : Literal := This;
        Counter : constant BNFs.BNF := BNFs.BNF (Block.all (BNFs.Zero_BNF));
        Repeats : Big_Integer := BNFs.Get_Value (Counter) + 4;
    begin
        if Result.Initial then
            if Result.Size = 1 then
                Result.Size := Create (0);
            else
                Result.Size := Result.Size - 2;
            end if;

            while Sign (Repeats) /= GMP.Neutral loop
                Result.Size := Result.Size * 30 + 21;
                Repeats := Repeats - 1;
            end loop;

            Result.Size := Result.Size + 2;

            return Result;
        else
            while Sign (Repeats) /= GMP.Neutral loop
                Result.Value := Result.Value * 94 + 85;
                Repeats := Repeats - 1;
            end loop;

            Result.Size := Result.Size - 1;

            if Sign (Result.Size) = GMP.Neutral then
                return Apply (Result.Start, Result.Value);
            else
                return Result;
            end if;
        end if;
    end TILDE;

    function EXCLA (This : Literal) return Datatype'Class is
        Result : Literal := This;
    begin
        if Result.Initial then
            if Result.Size = 1 then
                Result.Size := Create (0);
            else
                Result.Size := Result.Size - 2;
            end if;

            Result.Size := Result.Size * 30 + 22;
            Result.Size := Result.Size + 2;

            return Result;
        else
            Result.Value := Result.Value * 94 + 86;
            Result.Size := Result.Size - 1;

            if Sign (Result.Size) = GMP.Neutral then
                return Apply (Result.Start, Result.Value);
            else
                return Result;
            end if;
        end if;
    end EXCLA;

    function EXCLA (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : Literal := This;
        Counter : constant BNFs.BNF := BNFs.BNF (Block.all (BNFs.Zero_BNF));
        Repeats : Big_Integer := BNFs.Get_Value (Counter) + 4;
    begin
        if Result.Initial then
            if Result.Size = 1 then
                Result.Size := Create (0);
            else
                Result.Size := Result.Size - 2;
            end if;

            while Sign (Repeats) /= GMP.Neutral loop
                Result.Size := Result.Size * 30 + 22;
                Repeats := Repeats - 1;
            end loop;

            Result.Size := Result.Size + 2;

            return Result;
        else
            while Sign (Repeats) /= GMP.Neutral loop
                Result.Value := Result.Value * 94 + 86;
                Repeats := Repeats - 1;
            end loop;

            Result.Size := Result.Size - 1;

            if Sign (Result.Size) = GMP.Neutral then
                return Apply (Result.Start, Result.Value);
            else
                return Result;
            end if;
        end if;
    end EXCLA;

    function ATSGN (This : Literal) return Datatype'Class is
        Result : Literal := This;
    begin
        if Result.Initial then
            if Result.Size = 1 then
                Result.Size := Create (0);
            else
                Result.Size := Result.Size - 2;
            end if;

            Result.Size := Result.Size * 30 + 23;
            Result.Size := Result.Size + 2;

            return Result;
        else
            Result.Value := Result.Value * 94 + 87;
            Result.Size := Result.Size - 1;

            if Sign (Result.Size) = GMP.Neutral then
                return Apply (Result.Start, Result.Value);
            else
                return Result;
            end if;
        end if;
    end ATSGN;

    function ATSGN (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : Literal := This;
        Counter : constant BNFs.BNF := BNFs.BNF (Block.all (BNFs.Zero_BNF));
        Repeats : Big_Integer := BNFs.Get_Value (Counter) + 4;
    begin
        if Result.Initial then
            if Result.Size = 1 then
                Result.Size := Create (0);
            else
                Result.Size := Result.Size - 2;
            end if;

            while Sign (Repeats) /= GMP.Neutral loop
                Result.Size := Result.Size * 30 + 23;
                Repeats := Repeats - 1;
            end loop;

            Result.Size := Result.Size + 2;

            return Result;
        else
            while Sign (Repeats) /= GMP.Neutral loop
                Result.Value := Result.Value * 94 + 87;
                Repeats := Repeats - 1;
            end loop;

            Result.Size := Result.Size - 1;

            if Sign (Result.Size) = GMP.Neutral then
                return Apply (Result.Start, Result.Value);
            else
                return Result;
            end if;
        end if;
    end ATSGN;

    function POUND (This : Literal) return Datatype'Class is
        Result : Literal := This;
    begin
        if Result.Initial then
            if Result.Size = 1 then
                Result.Size := Create (0);
            else
                Result.Size := Result.Size - 2;
            end if;

            Result.Size := Result.Size * 30 + 24;
            Result.Size := Result.Size + 2;

            return Result;
        else
            Result.Value := Result.Value * 94 + 88;
            Result.Size := Result.Size - 1;

            if Sign (Result.Size) = GMP.Neutral then
                return Apply (Result.Start, Result.Value);
            else
                return Result;
            end if;
        end if;
    end POUND;

    function POUND (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : Literal := This;
        Counter : constant BNFs.BNF := BNFs.BNF (Block.all (BNFs.Zero_BNF));
        Repeats : Big_Integer := BNFs.Get_Value (Counter) + 4;
    begin
        if Result.Initial then
            if Result.Size = 1 then
                Result.Size := Create (0);
            else
                Result.Size := Result.Size - 2;
            end if;

            while Sign (Repeats) /= GMP.Neutral loop
                Result.Size := Result.Size * 30 + 24;
                Repeats := Repeats - 1;
            end loop;

            Result.Size := Result.Size + 2;

            return Result;
        else
            while Sign (Repeats) /= GMP.Neutral loop
                Result.Value := Result.Value * 94 + 88;
                Repeats := Repeats - 1;
            end loop;

            Result.Size := Result.Size - 1;

            if Sign (Result.Size) = GMP.Neutral then
                return Apply (Result.Start, Result.Value);
            else
                return Result;
            end if;
        end if;
    end POUND;

    function MONEY (This : Literal) return Datatype'Class is
        Result : Literal := This;
    begin
        if Result.Initial then
            if Result.Size = 1 then
                Result.Size := Create (0);
            else
                Result.Size := Result.Size - 2;
            end if;

            Result.Size := Result.Size * 30 + 25;
            Result.Size := Result.Size + 2;

            return Result;
        else
            Result.Value := Result.Value * 94 + 89;
            Result.Size := Result.Size - 1;

            if Sign (Result.Size) = GMP.Neutral then
                return Apply (Result.Start, Result.Value);
            else
                return Result;
            end if;
        end if;
    end MONEY;

    function MONEY (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : Literal := This;
        Counter : constant BNFs.BNF := BNFs.BNF (Block.all (BNFs.Zero_BNF));
        Repeats : Big_Integer := BNFs.Get_Value (Counter) + 4;
    begin
        if Result.Initial then
            if Result.Size = 1 then
                Result.Size := Create (0);
            else
                Result.Size := Result.Size - 2;
            end if;

            while Sign (Repeats) /= GMP.Neutral loop
                Result.Size := Result.Size * 30 + 25;
                Repeats := Repeats - 1;
            end loop;

            Result.Size := Result.Size + 2;

            return Result;
        else
            while Sign (Repeats) /= GMP.Neutral loop
                Result.Value := Result.Value * 94 + 89;
                Repeats := Repeats - 1;
            end loop;

            Result.Size := Result.Size - 1;

            if Sign (Result.Size) = GMP.Neutral then
                return Apply (Result.Start, Result.Value);
            else
                return Result;
            end if;
        end if;
    end MONEY;

    function QUERY (This : Literal) return Datatype'Class is
        Result : Literal := This;
    begin
        if Result.Initial then
            if Result.Size = 1 then
                Result.Size := Create (0);
            else
                Result.Size := Result.Size - 2;
            end if;

            Result.Size := Result.Size * 30 + 26;
            Result.Size := Result.Size + 2;

            return Result;
        else
            Result.Value := Result.Value * 94 + 90;
            Result.Size := Result.Size - 1;

            if Sign (Result.Size) = GMP.Neutral then
                return Apply (Result.Start, Result.Value);
            else
                return Result;
            end if;
        end if;
    end QUERY;

    function QUERY (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : Literal := This;
        Counter : constant BNFs.BNF := BNFs.BNF (Block.all (BNFs.Zero_BNF));
        Repeats : Big_Integer := BNFs.Get_Value (Counter) + 4;
    begin
        if Result.Initial then
            if Result.Size = 1 then
                Result.Size := Create (0);
            else
                Result.Size := Result.Size - 2;
            end if;

            while Sign (Repeats) /= GMP.Neutral loop
                Result.Size := Result.Size * 30 + 26;
                Repeats := Repeats - 1;
            end loop;

            Result.Size := Result.Size + 2;

            return Result;
        else
            while Sign (Repeats) /= GMP.Neutral loop
                Result.Value := Result.Value * 94 + 90;
                Repeats := Repeats - 1;
            end loop;

            Result.Size := Result.Size - 1;

            if Sign (Result.Size) = GMP.Neutral then
                return Apply (Result.Start, Result.Value);
            else
                return Result;
            end if;
        end if;
    end QUERY;

    function UNDER (This : Literal) return Datatype'Class is
        Result : Literal := This;
    begin
        if Result.Initial then
            if Result.Size = 1 then
                Result.Size := Create (0);
            else
                Result.Size := Result.Size - 2;
            end if;

            Result.Size := Result.Size * 30 + 27;
            Result.Size := Result.Size + 2;

            return Result;
        else
            Result.Value := Result.Value * 94 + 91;
            Result.Size := Result.Size - 1;

            if Sign (Result.Size) = GMP.Neutral then
                return Apply (Result.Start, Result.Value);
            else
                return Result;
            end if;
        end if;
    end UNDER;

    function UNDER (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : Literal := This;
        Counter : constant BNFs.BNF := BNFs.BNF (Block.all (BNFs.Zero_BNF));
        Repeats : Big_Integer := BNFs.Get_Value (Counter) + 4;
    begin
        if Result.Initial then
            if Result.Size = 1 then
                Result.Size := Create (0);
            else
                Result.Size := Result.Size - 2;
            end if;

            while Sign (Repeats) /= GMP.Neutral loop
                Result.Size := Result.Size * 30 + 27;
                Repeats := Repeats - 1;
            end loop;

            Result.Size := Result.Size + 2;

            return Result;
        else
            while Sign (Repeats) /= GMP.Neutral loop
                Result.Value := Result.Value * 94 + 91;
                Repeats := Repeats - 1;
            end loop;

            Result.Size := Result.Size - 1;

            if Sign (Result.Size) = GMP.Neutral then
                return Apply (Result.Start, Result.Value);
            else
                return Result;
            end if;
        end if;
    end UNDER;

    function EQUAL (This : Literal) return Datatype'Class is
        Result : Literal := This;
    begin
        if Result.Initial then
            if Result.Size = 1 then
                Result.Size := Create (0);
            else
                Result.Size := Result.Size - 2;
            end if;

            Result.Size := Result.Size * 30 + 28;
            Result.Size := Result.Size + 2;

            return Result;
        else
            Result.Value := Result.Value * 94 + 92;
            Result.Size := Result.Size - 1;

            if Sign (Result.Size) = GMP.Neutral then
                return Apply (Result.Start, Result.Value);
            else
                return Result;
            end if;
        end if;
    end EQUAL;

    function EQUAL (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : Literal := This;
        Counter : constant BNFs.BNF := BNFs.BNF (Block.all (BNFs.Zero_BNF));
        Repeats : Big_Integer := BNFs.Get_Value (Counter) + 4;
    begin
        if Result.Initial then
            if Result.Size = 1 then
                Result.Size := Create (0);
            else
                Result.Size := Result.Size - 2;
            end if;

            while Sign (Repeats) /= GMP.Neutral loop
                Result.Size := Result.Size * 30 + 28;
                Repeats := Repeats - 1;
            end loop;

            Result.Size := Result.Size + 2;

            return Result;
        else
            while Sign (Repeats) /= GMP.Neutral loop
                Result.Value := Result.Value * 94 + 92;
                Repeats := Repeats - 1;
            end loop;

            Result.Size := Result.Size - 1;

            if Sign (Result.Size) = GMP.Neutral then
                return Apply (Result.Start, Result.Value);
            else
                return Result;
            end if;
        end if;
    end EQUAL;

    function HSALS (This : Literal) return Datatype'Class is
        Result : Literal := This;
    begin
        if Result.Initial then
            if Result.Size = 1 then
                Result.Size := Create (0);
            else
                Result.Size := Result.Size - 2;
            end if;

            Result.Size := Result.Size * 30 + 29;
            Result.Size := Result.Size + 2;

            return Result;
        else
            Result.Value := Result.Value * 94 + 93;
            Result.Size := Result.Size - 1;

            if Sign (Result.Size) = GMP.Neutral then
                return Apply (Result.Start, Result.Value);
            else
                return Result;
            end if;
        end if;
    end HSALS;

    function HSALS (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : Literal := This;
        Counter : constant BNFs.BNF := BNFs.BNF (Block.all (BNFs.Zero_BNF));
        Repeats : Big_Integer := BNFs.Get_Value (Counter) + 4;
    begin
        if Result.Initial then
            if Result.Size = 1 then
                Result.Size := Create (0);
            else
                Result.Size := Result.Size - 2;
            end if;

            while Sign (Repeats) /= GMP.Neutral loop
                Result.Size := Result.Size * 30 + 29;
                Repeats := Repeats - 1;
            end loop;

            Result.Size := Result.Size + 2;

            return Result;
        else
            while Sign (Repeats) /= GMP.Neutral loop
                Result.Value := Result.Value * 94 + 93;
                Repeats := Repeats - 1;
            end loop;

            Result.Size := Result.Size - 1;

            if Sign (Result.Size) = GMP.Neutral then
                return Apply (Result.Start, Result.Value);
            else
                return Result;
            end if;
        end if;
    end HSALS;
    -- LCOV_EXCL_STOP }}}

    function Read (From : not null access Inputter) return Literal is
    begin
        return (Empty_Datum with
            Start => Read (From),
            Initial => True,
            Size => Create (1),
            Value => Create (0));
    end Read;

    procedure Write (This : Literal; To : in out Outputter) is
    begin
        This.Start.Write (To);
    end Write;

    function Image (This : Literal) return String is
    begin
        return This.Start.Image;
    end Image;

    function Type_Image (This : Literal) return String is
    begin
        return This.Start.Type_Image & "_literal(" &
            Image (This.Size) & ", " & Image (This.Value) & ")";
    end Type_Image;
end Sbit.Literals;
