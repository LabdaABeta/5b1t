package Sbit.Tuples is
    type Tuple is new Datatype with private;

    function Create (Ignore : not null access Nothing) return Tuple;
    function Extract (From : not null access Tuple) return Tuple;

    -- May return any child class of tuple
    function Child_Reduce (From : Tuple) return Tuple'Class;

    function Empty_Tuple return Tuple;

    procedure Append (Into : in out Tuple; Item : Datatype'Class);
    procedure Prepend (Into : in out Tuple; Item : Datatype'Class);
    function Get (
        From : Tuple;
        Index : Positive)
        return Datatype'Class;
    function Size (From : Tuple) return Natural;

    -- Direct operators
    function NUM_0 (This : Tuple) return Datatype'Class;
    function NUM_1 (This : Tuple) return Datatype'Class;
    function NUM_1 (
        This : Tuple;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function NUM_2 (This : Tuple) return Datatype'Class;
    function NUM_2 (
        This : Tuple;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function NUM_3 (This : Tuple) return Datatype'Class;
    function NUM_3 (
        This : Tuple;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function NUM_4 (This : Tuple) return Datatype'Class;
    function NUM_4 (
        This : Tuple;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function NUM_5 (This : Tuple) return Datatype'Class;
    function NUM_5 (
        This : Tuple;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function NUM_6 (This : Tuple) return Datatype'Class;
    function NUM_6 (
        This : Tuple;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function NUM_7 (This : Tuple) return Datatype'Class;
    function NUM_7 (
        This : Tuple;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function NUM_8 (This : Tuple) return Datatype'Class;
    function NUM_8 (
        This : Tuple;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function NUM_9 (This : Tuple) return Datatype'Class;
    function NUM_9 (
        This : Tuple;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;

    function Read (From : not null access Inputter) return Tuple;
    procedure Write (This : Tuple; To : in out Outputter);

    -- Debugging and utility functions
    function Image (This : Tuple) return String;
    function Type_Image (This : Tuple) return String;

private
    type Tuple is new Datatype with
        record
            Value : Data_Vectors.Vector;
        end record;
end Sbit.Tuples;
