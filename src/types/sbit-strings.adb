with Ada.Strings.UTF_Encoding.Wide_Wide_Strings;
with Ada.Wide_Wide_Characters.Unicode;
with Type_Registration;

package body Sbit.Strings is
    function Create (Ignore : not null access Nothing) return String is
        Result : constant String := (Empty_Datum with Size => 0, Value => "");
    begin
        return Result;
    end Create;

    function Create (From : Standard.String) return String is
        Result : constant String := (Size => From'Length, Value => From);
    begin
        return Result;
    end Create;

    -- Direct operators
    function TILDE (This : String) return Datatype'Class is
        WWRep : Wide_Wide_String :=
            Ada.Strings.UTF_Encoding.Wide_Wide_Strings.Decode (This.Value);

        function Is_Upper (X : Wide_Wide_Character) return Boolean is (
            Ada.Wide_Wide_Characters.Unicode.To_Upper_Case (X) = X);
    begin
        for I in WWRep'Range loop
            if Is_Upper (WWRep (I)) then
                WWRep (I) :=
                    Ada.Wide_Wide_Characters.Unicode.To_Lower_Case (WWRep (I));
            else
                WWRep (I) :=
                    Ada.Wide_Wide_Characters.Unicode.To_Upper_Case (WWRep (I));
            end if;
        end loop;

        declare
            U8Rep : constant Standard.String :=
                Ada.Strings.UTF_Encoding.Wide_Wide_Strings.Encode (WWRep);
            Result : constant String := (Empty_Datum with
                Size => U8Rep'Length,
                Value => U8Rep);
        begin
            return Result;
        end;
    end TILDE;

    function Read (From : not null access Inputter) return String is
        Line : constant Standard.String := From.Get_Line;
    begin
        return (Empty_Datum with Size => Line'Length, Value => Line);
    end Read;

    procedure Write (This : String; To : in out Outputter) is
    begin
        To.Put_String (This.Value);
    end Write;

    -- Debugging and utility functions
    function Image (This : String) return Standard.String is
    begin
        return This.Value;
    end Image;

    function Type_Image (This : String) return Standard.String is
        pragma Unreferenced (This);
    begin
        return "S";
    end Type_Image;
begin
    Type_Registration.Register ("Strings", String'Tag);
end Sbit.Strings;
