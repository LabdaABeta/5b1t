with Big_Integers;

generic
    type Input is new Datatype with private;
    with function Apply (
        This : Input;
        Argument : Big_Integers.Big_Integer)
        return Datatype'Class;
package Sbit.Literals is
    pragma Elaborate_Body; -- Elaborate body to ensure Apply is elaborated

    type Literal is new Datatype with private;

    function Create (Ignore : not null access Nothing) return Literal;
    function Empty_Literal (From : Input) return Literal;

    -- Direct operators
    function NUM_0 (This : Literal) return Datatype'Class;
    function NUM_0 (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function NUM_1 (This : Literal) return Datatype'Class;
    function NUM_1 (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function NUM_2 (This : Literal) return Datatype'Class;
    function NUM_2 (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function NUM_3 (This : Literal) return Datatype'Class;
    function NUM_3 (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function NUM_4 (This : Literal) return Datatype'Class;
    function NUM_4 (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function NUM_5 (This : Literal) return Datatype'Class;
    function NUM_5 (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function NUM_6 (This : Literal) return Datatype'Class;
    function NUM_6 (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function NUM_7 (This : Literal) return Datatype'Class;
    function NUM_7 (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function NUM_8 (This : Literal) return Datatype'Class;
    function NUM_8 (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function NUM_9 (This : Literal) return Datatype'Class;
    function NUM_9 (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function A_MAJ (This : Literal) return Datatype'Class;
    function A_MAJ (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function B_MAJ (This : Literal) return Datatype'Class;
    function B_MAJ (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function C_MAJ (This : Literal) return Datatype'Class;
    function C_MAJ (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function D_MAJ (This : Literal) return Datatype'Class;
    function D_MAJ (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function E_MAJ (This : Literal) return Datatype'Class;
    function E_MAJ (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function F_MAJ (This : Literal) return Datatype'Class;
    function F_MAJ (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function G_MAJ (This : Literal) return Datatype'Class;
    function G_MAJ (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function H_MAJ (This : Literal) return Datatype'Class;
    function H_MAJ (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function I_MAJ (This : Literal) return Datatype'Class;
    function I_MAJ (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function J_MAJ (This : Literal) return Datatype'Class;
    function J_MAJ (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function K_MAJ (This : Literal) return Datatype'Class;
    function K_MAJ (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function L_MAJ (This : Literal) return Datatype'Class;
    function L_MAJ (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function M_MAJ (This : Literal) return Datatype'Class;
    function M_MAJ (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function N_MAJ (This : Literal) return Datatype'Class;
    function N_MAJ (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function O_MAJ (This : Literal) return Datatype'Class;
    function O_MAJ (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function P_MAJ (This : Literal) return Datatype'Class;
    function P_MAJ (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function Q_MAJ (This : Literal) return Datatype'Class;
    function Q_MAJ (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function R_MAJ (This : Literal) return Datatype'Class;
    function R_MAJ (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function S_MAJ (This : Literal) return Datatype'Class;
    function S_MAJ (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function T_MAJ (This : Literal) return Datatype'Class;
    function T_MAJ (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function U_MAJ (This : Literal) return Datatype'Class;
    function U_MAJ (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function V_MAJ (This : Literal) return Datatype'Class;
    function V_MAJ (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function W_MAJ (This : Literal) return Datatype'Class;
    function W_MAJ (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function X_MAJ (This : Literal) return Datatype'Class;
    function X_MAJ (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function Y_MAJ (This : Literal) return Datatype'Class;
    function Y_MAJ (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function Z_MAJ (This : Literal) return Datatype'Class;
    function Z_MAJ (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function A_MIN (This : Literal) return Datatype'Class;
    function A_MIN (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function B_MIN (This : Literal) return Datatype'Class;
    function B_MIN (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function C_MIN (This : Literal) return Datatype'Class;
    function C_MIN (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function D_MIN (This : Literal) return Datatype'Class;
    function D_MIN (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function E_MIN (This : Literal) return Datatype'Class;
    function E_MIN (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function F_MIN (This : Literal) return Datatype'Class;
    function F_MIN (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function G_MIN (This : Literal) return Datatype'Class;
    function G_MIN (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function H_MIN (This : Literal) return Datatype'Class;
    function H_MIN (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function I_MIN (This : Literal) return Datatype'Class;
    function I_MIN (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function J_MIN (This : Literal) return Datatype'Class;
    function J_MIN (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function K_MIN (This : Literal) return Datatype'Class;
    function K_MIN (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function L_MIN (This : Literal) return Datatype'Class;
    function L_MIN (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function M_MIN (This : Literal) return Datatype'Class;
    function M_MIN (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function N_MIN (This : Literal) return Datatype'Class;
    function N_MIN (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function O_MIN (This : Literal) return Datatype'Class;
    function O_MIN (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function P_MIN (This : Literal) return Datatype'Class;
    function P_MIN (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function Q_MIN (This : Literal) return Datatype'Class;
    function Q_MIN (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function R_MIN (This : Literal) return Datatype'Class;
    function R_MIN (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function S_MIN (This : Literal) return Datatype'Class;
    function S_MIN (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function T_MIN (This : Literal) return Datatype'Class;
    function T_MIN (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function U_MIN (This : Literal) return Datatype'Class;
    function U_MIN (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function V_MIN (This : Literal) return Datatype'Class;
    function V_MIN (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function W_MIN (This : Literal) return Datatype'Class;
    function W_MIN (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function X_MIN (This : Literal) return Datatype'Class;
    function X_MIN (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function Y_MIN (This : Literal) return Datatype'Class;
    function Y_MIN (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function Z_MIN (This : Literal) return Datatype'Class;
    function Z_MIN (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function ADDIT (This : Literal) return Datatype'Class;
    function ADDIT (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function SLASH (This : Literal) return Datatype'Class;
    function SLASH (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function MINUS (This : Literal) return Datatype'Class;
    function MINUS (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function TIMES (This : Literal) return Datatype'Class;
    function TIMES (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function VPIPE (This : Literal) return Datatype'Class;
    function VPIPE (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function CARET (This : Literal) return Datatype'Class;
    function CARET (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function AMPER (This : Literal) return Datatype'Class;
    function AMPER (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function PRCNT (This : Literal) return Datatype'Class;
    function PRCNT (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function LESST (This : Literal) return Datatype'Class;
    function LESST (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function MORET (This : Literal) return Datatype'Class;
    function MORET (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function PAR_L (This : Literal) return Datatype'Class;
    function PAR_L (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function PAR_R (This : Literal) return Datatype'Class;
    function PAR_R (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function BRA_L (This : Literal) return Datatype'Class;
    function BRA_L (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function BRA_R (This : Literal) return Datatype'Class;
    function BRA_R (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function CUR_L (This : Literal) return Datatype'Class;
    function CUR_L (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function CUR_R (This : Literal) return Datatype'Class;
    function CUR_R (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function O_DOT (This : Literal) return Datatype'Class;
    function O_DOT (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function COMMA (This : Literal) return Datatype'Class;
    function COMMA (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function SEMIC (This : Literal) return Datatype'Class;
    function SEMIC (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function COLON (This : Literal) return Datatype'Class;
    function COLON (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function APOST (This : Literal) return Datatype'Class;
    function APOST (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function QUOTE (This : Literal) return Datatype'Class;
    function QUOTE (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function BTICK (This : Literal) return Datatype'Class;
    function BTICK (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function TILDE (This : Literal) return Datatype'Class;
    function TILDE (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function EXCLA (This : Literal) return Datatype'Class;
    function EXCLA (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function ATSGN (This : Literal) return Datatype'Class;
    function ATSGN (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function POUND (This : Literal) return Datatype'Class;
    function POUND (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function MONEY (This : Literal) return Datatype'Class;
    function MONEY (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function QUERY (This : Literal) return Datatype'Class;
    function QUERY (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function UNDER (This : Literal) return Datatype'Class;
    function UNDER (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function EQUAL (This : Literal) return Datatype'Class;
    function EQUAL (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function HSALS (This : Literal) return Datatype'Class;
    function HSALS (
        This : Literal;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;

    function Read (From : not null access Inputter) return Literal;
    procedure Write (This : Literal; To : in out Outputter);

    -- Debugging and utility functions
    function Image (This : Literal) return String;
    function Type_Image (This : Literal) return String;

private
    type Literal is new Datatype with
        record
            Start : Input;
            Initial : Boolean;
            Size : Big_Integers.Big_Integer;
            Value : Big_Integers.Big_Integer;
        end record;
end Sbit.Literals;
