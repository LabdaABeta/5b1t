with Sbit.Lists;

package Sbit.Integer_Lists is
    type Integer_List is new Lists.List with private;

    function Create (Ignore : not null access Nothing) return Integer_List;
    function Extract (From : not null access Lists.List) return Integer_List;
    function From_List (From : Lists.List) return Integer_List;

    function Empty_List return Integer_List;

    -- Direct operators
    function M_MAJ (This : Integer_List) return Datatype'Class;
    function M_MIN (This : Integer_List) return Datatype'Class;
    function ADDIT (This : Integer_List) return Datatype'Class;
    function SLASH (This : Integer_List) return Datatype'Class;
    function HSALS (This : Integer_List) return Datatype'Class;

    function Read (From : not null access Inputter) return Integer_List;
    procedure Write (This : Integer_List; To : in out Outputter);

    -- Debugging and utility functions
    function Image (This : Integer_List) return String;
    function Type_Image (This : Integer_List) return String;
private
    type Integer_List is new Lists.List with null record;
end Sbit.Integer_Lists;
