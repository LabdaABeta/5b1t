with Big_Integers; use Big_Integers;
with Sbit.Integers;
with List_Registration;
with Type_Registration;
with Regex;

package body Sbit.Integer_Lists is
    function Less_Than (
        Left, Right : Datatype'Class)
        return Boolean
    is
    begin
        return Sbit.Integers.Get_Value (Sbit.Integers.Integer (Left)) <
            Sbit.Integers.Get_Value (Sbit.Integers.Integer (Right));
    end Less_Than;

    function Greater_Than (
        Left, Right : Datatype'Class)
        return Boolean
    is
    begin
        return Sbit.Integers.Get_Value (Sbit.Integers.Integer (Left)) >
            Sbit.Integers.Get_Value (Sbit.Integers.Integer (Right));
    end Greater_Than;

    package Ascending_Sorting is new Data_Lists.Generic_Sorting (Less_Than);

    package Descending_Sorting is new Data_Lists.Generic_Sorting (Greater_Than);

    function Create (Ignore : not null access Nothing) return Integer_List is
    begin
        return Empty_List;
    end Create;

    function Extract (From : not null access Lists.List) return Integer_List is
    begin
        return From_List (From.all);
    end Extract;

    function From_List (From : Sbit.Lists.List) return Integer_List is
        Result : constant Integer_List := (From with null record);
    begin
        return Result;
    end From_List;

    function Empty_List return Integer_List is
        Result : constant Integer_List :=
            (Sbit.Lists.Empty_List with null record);
    begin
        return Result;
    end Empty_List;

    -- Direct operators
    function M_MAJ (This : Integer_List) return Datatype'Class is
        Max : Big_Integer := Create (0);
        Curr : Big_Integer;
        Vec : constant Data_Lists.List := This.Get_Value;
    begin
        if This.Length = 0 then
            return Sbit.Integers.Create (Max);
        else
            Max := Sbit.Integers.Get_Value (
                Sbit.Integers.Integer (Vec.First_Element));
        end if;

        for Elem of Vec loop
            Curr := Sbit.Integers.Get_Value (Sbit.Integers.Integer (Elem));
            if Curr > Max then
                Max := Curr;
            end if;
        end loop;

        return Sbit.Integers.Create (Max);
    end M_MAJ;

    function M_MIN (This : Integer_List) return Datatype'Class is
        Min : Big_Integer := Create (0);
        Curr : Big_Integer;
        Vec : constant Data_Lists.List := This.Get_Value;
    begin
        if This.Length = 0 then
            return Sbit.Integers.Create (Min);
        else
            Min := Sbit.Integers.Get_Value (
                Sbit.Integers.Integer (Vec.First_Element));
        end if;

        for Elem of Vec loop
            Curr := Sbit.Integers.Get_Value (Sbit.Integers.Integer (Elem));
            if Curr < Min then
                Min := Curr;
            end if;
        end loop;

        return Sbit.Integers.Create (Min);
    end M_MIN;

    function ADDIT (This : Integer_List) return Datatype'Class is
        Sum : Big_Integer := Create (0);
        Result : Integers.Integer;
    begin
        for I of This.Get_Value loop
            Sum := Sum + Sbit.Integers.Get_Value (Integers.Integer (I));
        end loop;

        Result := Sbit.Integers.Create (Sum);
        return Result;
    end ADDIT;

    function SLASH (This : Integer_List) return Datatype'Class is
        Data : Data_Lists.List := This.Get_Value;
        Result : Integer_List;
    begin
        Ascending_Sorting.Sort (Data);

        Result := (Sbit.Lists.As_List (Data) with null record);
        return Result;
    end SLASH;

    function HSALS (This : Integer_List) return Datatype'Class is
        Data : Data_Lists.List := This.Get_Value;
        Result : Integer_List;
    begin
        Descending_Sorting.Sort (Data);

        Result := (Sbit.Lists.As_List (Data) with null record);
        return Result;
    end HSALS;

    function Read (From : not null access Inputter) return Integer_List is
        Line : constant String := From.Get_Line;
        Ignore_Regex : constant String := "[^[:alnum:] ,-|\t]+";
        Sep_Regex : constant String := "[ ,|\t]+";
        Tokens : constant Regex.Match_List := Regex.Tokenize (
            Input => Regex.Remove (Line, Ignore_Regex),
            Pattern => Sep_Regex);
        Result : Sbit.Lists.List := Sbit.Lists.Empty_List;
    begin
        for T of Tokens loop
            Result.Append (
                Integers.Create (
                    Create (
                        Line (T.Start .. T.Finish)
                    )
                )
            );
        end loop;

        return From_List (Result);
    end Read;

    procedure Write (This : Integer_List; To : in out Outputter) is
    begin
        Sbit.Lists.Write (Sbit.Lists.List (This), To);
    end Write;

    -- Debugging and utility functions
    function Image (This : Integer_List) return String is
    begin
        return Sbit.Lists.Image (Sbit.Lists.List (This));
    end Image;

    function Type_Image (This : Integer_List) return String is
    begin
        return Sbit.Lists.Type_Image (Sbit.Lists.List (This));
    end Type_Image;
begin
    List_Registration.Register (Integers.Integer'Tag, Integer_List'Tag);
    Type_Registration.Register ("Integer_Lists", Integer_List'Tag);
end Sbit.Integer_Lists;
