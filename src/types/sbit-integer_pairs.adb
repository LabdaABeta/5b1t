with Ada.Tags;
with Big_Integers;
with Tuple_Registration;
with Type_Registration;

package body Sbit.Integer_Pairs is
    use type Big_Integers.Big_Integer;

    function Create (Ignore : not null access Nothing) return Integer_Pair is
    begin
        return From_Tuple (Tuples.Empty_Tuple);
    end Create;

    function Extract (From : not null access Tuples.Tuple) return Integer_Pair
    is
    begin
        return From_Tuple (From.all);
    end Extract;

    function From_Tuple (From : Tuples.Tuple) return Integer_Pair is
        Result : constant Integer_Pair := (From with null record);
    begin
        return Result;
    end From_Tuple;

    function Create (Left, Right : Integers.Integer) return Integer_Pair is
        Base : Tuples.Tuple := Tuples.Empty_Tuple;
    begin
        Base.Append (Left);
        Base.Append (Right);

        return From_Tuple (Base);
    end Create;

    function Get_Left (From : Integer_Pair) return Integers.Integer is
    begin
        return Integers.Integer (From.Get (1));
    end Get_Left;

    function Get_Right (From : Integer_Pair) return Integers.Integer is
    begin
        return Integers.Integer (From.Get (2));
    end Get_Right;

    -- Direct operators
    function ADDIT (This : Integer_Pair) return Datatype'Class is
        Result : constant Datatype'Class := Integers.Create (
            This.Get_Left.Get_Value + This.Get_Right.Get_Value);
    begin
        return Result;
    end ADDIT;

    function Read (From : not null access Inputter) return Integer_Pair is
        Result : constant Integer_Pair := Create (
            Left => Integers.Create (From.Get_Int),
            Right => Integers.Create (From.Get_Int));
    begin
        return Result;
    end Read;

    procedure Write (This : Integer_Pair; To : in out Outputter) is
    begin
        Tuples.Write (Tuples.Tuple (This), To);
    end Write;

    -- Debugging and utility functions
    function Image (This : Integer_Pair) return String is
    begin
        return Tuples.Image (Tuples.Tuple (This));
    end Image;

    function Type_Image (This : Integer_Pair) return String is
    begin
        return Tuples.Type_Image (Tuples.Tuple (This));
    end Type_Image;

    Tuple_Tags : constant Ada.Tags.Tag_Array (1 .. 2) := (
        1 => Integers.Integer'Tag,
        2 => Integers.Integer'Tag);
begin
    Tuple_Registration.Register (Tuple_Tags, Integer_Pair'Tag);
    Type_Registration.Register ("Integer_Pairs", Integer_Pair'Tag);
end Sbit.Integer_Pairs;
