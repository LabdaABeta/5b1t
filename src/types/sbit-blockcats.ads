-- Blockcats concatenate the source code for blocks
package Sbit.Blockcats is
    type Blockcat (<>) is new Datatype with private;

    function Create (Ignore : not null access Nothing) return Blockcat;
    function Create return Blockcat;
    function Extract (From : Blockcat) return String;

    -- Direct operators
    function NUM_0 (This : Blockcat) return Datatype'Class;
    function NUM_0 (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function NUM_1 (This : Blockcat) return Datatype'Class;
    function NUM_1 (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function NUM_2 (This : Blockcat) return Datatype'Class;
    function NUM_2 (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function NUM_3 (This : Blockcat) return Datatype'Class;
    function NUM_3 (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function NUM_4 (This : Blockcat) return Datatype'Class;
    function NUM_4 (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function NUM_5 (This : Blockcat) return Datatype'Class;
    function NUM_5 (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function NUM_6 (This : Blockcat) return Datatype'Class;
    function NUM_6 (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function NUM_7 (This : Blockcat) return Datatype'Class;
    function NUM_7 (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function NUM_8 (This : Blockcat) return Datatype'Class;
    function NUM_8 (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function NUM_9 (This : Blockcat) return Datatype'Class;
    function NUM_9 (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function A_MAJ (This : Blockcat) return Datatype'Class;
    function A_MAJ (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function B_MAJ (This : Blockcat) return Datatype'Class;
    function B_MAJ (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function C_MAJ (This : Blockcat) return Datatype'Class;
    function C_MAJ (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function D_MAJ (This : Blockcat) return Datatype'Class;
    function D_MAJ (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function E_MAJ (This : Blockcat) return Datatype'Class;
    function E_MAJ (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function F_MAJ (This : Blockcat) return Datatype'Class;
    function F_MAJ (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function G_MAJ (This : Blockcat) return Datatype'Class;
    function G_MAJ (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function H_MAJ (This : Blockcat) return Datatype'Class;
    function H_MAJ (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function I_MAJ (This : Blockcat) return Datatype'Class;
    function I_MAJ (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function J_MAJ (This : Blockcat) return Datatype'Class;
    function J_MAJ (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function K_MAJ (This : Blockcat) return Datatype'Class;
    function K_MAJ (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function L_MAJ (This : Blockcat) return Datatype'Class;
    function L_MAJ (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function M_MAJ (This : Blockcat) return Datatype'Class;
    function M_MAJ (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function N_MAJ (This : Blockcat) return Datatype'Class;
    function N_MAJ (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function O_MAJ (This : Blockcat) return Datatype'Class;
    function O_MAJ (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function P_MAJ (This : Blockcat) return Datatype'Class;
    function P_MAJ (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function Q_MAJ (This : Blockcat) return Datatype'Class;
    function Q_MAJ (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function R_MAJ (This : Blockcat) return Datatype'Class;
    function R_MAJ (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function S_MAJ (This : Blockcat) return Datatype'Class;
    function S_MAJ (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function T_MAJ (This : Blockcat) return Datatype'Class;
    function T_MAJ (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function U_MAJ (This : Blockcat) return Datatype'Class;
    function U_MAJ (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function V_MAJ (This : Blockcat) return Datatype'Class;
    function V_MAJ (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function W_MAJ (This : Blockcat) return Datatype'Class;
    function W_MAJ (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function X_MAJ (This : Blockcat) return Datatype'Class;
    function X_MAJ (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function Y_MAJ (This : Blockcat) return Datatype'Class;
    function Y_MAJ (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function Z_MAJ (This : Blockcat) return Datatype'Class;
    function Z_MAJ (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function A_MIN (This : Blockcat) return Datatype'Class;
    function A_MIN (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function B_MIN (This : Blockcat) return Datatype'Class;
    function B_MIN (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function C_MIN (This : Blockcat) return Datatype'Class;
    function C_MIN (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function D_MIN (This : Blockcat) return Datatype'Class;
    function D_MIN (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function E_MIN (This : Blockcat) return Datatype'Class;
    function E_MIN (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function F_MIN (This : Blockcat) return Datatype'Class;
    function F_MIN (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function G_MIN (This : Blockcat) return Datatype'Class;
    function G_MIN (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function H_MIN (This : Blockcat) return Datatype'Class;
    function H_MIN (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function I_MIN (This : Blockcat) return Datatype'Class;
    function I_MIN (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function J_MIN (This : Blockcat) return Datatype'Class;
    function J_MIN (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function K_MIN (This : Blockcat) return Datatype'Class;
    function K_MIN (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function L_MIN (This : Blockcat) return Datatype'Class;
    function L_MIN (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function M_MIN (This : Blockcat) return Datatype'Class;
    function M_MIN (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function N_MIN (This : Blockcat) return Datatype'Class;
    function N_MIN (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function O_MIN (This : Blockcat) return Datatype'Class;
    function O_MIN (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function P_MIN (This : Blockcat) return Datatype'Class;
    function P_MIN (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function Q_MIN (This : Blockcat) return Datatype'Class;
    function Q_MIN (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function R_MIN (This : Blockcat) return Datatype'Class;
    function R_MIN (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function S_MIN (This : Blockcat) return Datatype'Class;
    function S_MIN (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function T_MIN (This : Blockcat) return Datatype'Class;
    function T_MIN (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function U_MIN (This : Blockcat) return Datatype'Class;
    function U_MIN (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function V_MIN (This : Blockcat) return Datatype'Class;
    function V_MIN (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function W_MIN (This : Blockcat) return Datatype'Class;
    function W_MIN (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function X_MIN (This : Blockcat) return Datatype'Class;
    function X_MIN (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function Y_MIN (This : Blockcat) return Datatype'Class;
    function Y_MIN (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function Z_MIN (This : Blockcat) return Datatype'Class;
    function Z_MIN (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function ADDIT (This : Blockcat) return Datatype'Class;
    function ADDIT (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function SLASH (This : Blockcat) return Datatype'Class;
    function SLASH (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function MINUS (This : Blockcat) return Datatype'Class;
    function MINUS (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function TIMES (This : Blockcat) return Datatype'Class;
    function TIMES (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function VPIPE (This : Blockcat) return Datatype'Class;
    function VPIPE (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function CARET (This : Blockcat) return Datatype'Class;
    function CARET (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function AMPER (This : Blockcat) return Datatype'Class;
    function AMPER (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function PRCNT (This : Blockcat) return Datatype'Class;
    function PRCNT (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function LESST (This : Blockcat) return Datatype'Class;
    function LESST (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function MORET (This : Blockcat) return Datatype'Class;
    function MORET (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function PAR_L (This : Blockcat) return Datatype'Class;
    function PAR_L (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function PAR_R (This : Blockcat) return Datatype'Class;
    function PAR_R (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function BRA_L (This : Blockcat) return Datatype'Class;
    function BRA_L (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function BRA_R (This : Blockcat) return Datatype'Class;
    function BRA_R (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function CUR_L (This : Blockcat) return Datatype'Class;
    function CUR_L (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function CUR_R (This : Blockcat) return Datatype'Class;
    function CUR_R (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function O_DOT (This : Blockcat) return Datatype'Class;
    function O_DOT (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function COMMA (This : Blockcat) return Datatype'Class;
    function COMMA (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function SEMIC (This : Blockcat) return Datatype'Class;
    function SEMIC (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function COLON (This : Blockcat) return Datatype'Class;
    function COLON (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function APOST (This : Blockcat) return Datatype'Class;
    function APOST (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function QUOTE (This : Blockcat) return Datatype'Class;
    function QUOTE (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function BTICK (This : Blockcat) return Datatype'Class;
    function BTICK (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function TILDE (This : Blockcat) return Datatype'Class;
    function TILDE (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function EXCLA (This : Blockcat) return Datatype'Class;
    function EXCLA (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function ATSGN (This : Blockcat) return Datatype'Class;
    function ATSGN (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function POUND (This : Blockcat) return Datatype'Class;
    function POUND (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function MONEY (This : Blockcat) return Datatype'Class;
    function MONEY (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function QUERY (This : Blockcat) return Datatype'Class;
    function QUERY (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function UNDER (This : Blockcat) return Datatype'Class;
    function UNDER (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function EQUAL (This : Blockcat) return Datatype'Class;
    function EQUAL (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function HSALS (This : Blockcat) return Datatype'Class;
    function HSALS (
        This : Blockcat;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;

    function Read (From : not null access Inputter) return Blockcat;
    procedure Write (This : Blockcat; To : in out Outputter);

    -- Debugging and utility functions
    function Image (This : Blockcat) return String;
    function Type_Image (This : Blockcat) return String;

private
    type Blockcat (Length : Natural) is new Datatype with
        record
            Code : String (1 .. Length);
        end record;
end Sbit.Blockcats;
