with Big_Integers; use Big_Integers;
with List_Registration;
with Type_Registration;
with Sbit.Naturals;
with Regex;

package body Sbit.Natural_Lists is
    function Create (Ignore : not null access Nothing) return Natural_List is
    begin
        return Empty_List;
    end Create;

    function Extract (From : not null access Lists.List) return Natural_List is
    begin
        return From_List (From.all);
    end Extract;

    function From_List (From : Lists.List) return Natural_List is
        Result : constant Natural_List :=
            (Integer_Lists.From_List (From) with null record);
    begin
        return Result;
    end From_List;

    function Empty_List return Natural_List is
        Result : constant Natural_List :=
            (Integer_Lists.Empty_List with null record);
    begin
        return Result;
    end Empty_List;

    -- Direct operators
    function P_MAJ (This : Natural_List) return Datatype'Class is
        Current_Prime : Big_Integer := Create (2);
        Result_Value : Big_Integer := Create (1);
    begin
        for I of This.Get_Value loop
            Result_Value := Result_Value * (Current_Prime **
                Naturals.Get_Value (Sbit.Naturals.Natural (I)).As_Integer);
            Current_Prime := Next_Prime (Current_Prime);
        end loop;

        return Sbit.Naturals.Create (Result_Value);
    end P_MAJ;

    function ADDIT (This : Natural_List) return Datatype'Class is
        Sum : Big_Integer := Create (0);
    begin
        for I of This.Get_Value loop
            Sum := Sum + Naturals.Get_Value (Sbit.Naturals.Natural (I));
        end loop;

        return Naturals.Create (Sum);
    end ADDIT;

    function Read (From : not null access Inputter) return Natural_List is
        Line : constant String := From.Get_Line;
        Ignore_Regex : constant String := "[^[:alnum:] ,|\t]+";
        Sep_Regex : constant String := "[ ,|\t]+";
        Tokens : constant Regex.Match_List := Regex.Tokenize (
            Input => Regex.Remove (Line, Ignore_Regex),
            Pattern => Sep_Regex);
        Result : Sbit.Lists.List := Sbit.Lists.Empty_List;
    begin
        for T of Tokens loop
            Result.Append (
                Naturals.Create (
                    Create (
                        Line (T.Start .. T.Finish)
                    )
                )
            );
        end loop;

        return From_List (Result);
    end Read;

    procedure Write (This : Natural_List; To : in out Outputter) is
    begin
        Lists.Write (Sbit.Lists.List (This), To);
    end Write;

    -- Debugging and utility functions
    function Image (This : Natural_List) return String is
    begin
        return Lists.Image (Sbit.Lists.List (This));
    end Image;

    function Type_Image (This : Natural_List) return String is
    begin
        return Lists.Type_Image (Sbit.Lists.List (This));
    end Type_Image;
begin
    List_Registration.Register (Naturals.Natural'Tag, Natural_List'Tag);
    Type_Registration.Register ("Natural_Lists", Natural_List'Tag);
end Sbit.Natural_Lists;
