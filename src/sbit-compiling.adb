with Ada.Strings.Unbounded; use Ada.Strings.Unbounded;
with Ada.Containers.Vectors;
with Sbit.Typenames;

package body Sbit.Compiling is
    use type Ada.Containers.Count_Type;

    New_Line : constant Character := Character'Val (10);
    Tab_Char : constant Character := Character'Val (9);

    function Block_Name (Block : Natural) return String is
        Result : String := Natural'Image (Block);
    begin
        Result (Result'First) := '_';
        return "B" & Result;
    end Block_Name;

    package String_Vectors is new Ada.Containers.Vectors (
        Index_Type => Natural,
        Element_Type => Unbounded_String);

    package Nat_Vectors is new Ada.Containers.Vectors (
        Index_Type => Natural,
        Element_Type => Natural);

    function Compile (
        From : Listing;
        Share : String;
        Start : Listing := (1 => 0);
        Main : String := "Main";
        Debug : Debug_Depth := 0;
        Seed : String := "")
        return String
    is
        function Debug_Include return String is
        begin
            if Debug > 0 then
                return "with Ada.Exceptions;" & New_Line &
                    "with Ada.Text_IO;" & New_Line;
            else
                return "";
            end if;
        end Debug_Include;

        function Verbosity return String is (
            case Debug is
                when 0 | 1 => "Sbit.Low",
                when 2 => "Sbit.Medium",
                when 3 => "Sbit.High");

        function Debug_End return String is
        begin
            if Debug > 0 then
                return "exception" & New_Line &
                    Tab_Char & "when E : others =>" & New_Line &
                    Tab_Char & Tab_Char & "Ada.Text_IO.Put_Line (" &
                    "Ada.Exceptions.Exception_Information (E));" & New_Line;
            else
                return "";
            end if;
        end Debug_End;

        Pack : constant String :=
            Sbit.Typenames.To_Nearest_Package (Start);
        Header : constant String :=
            "pragma Style_Checks (Off);" & New_Line &
            "with Sbit;" & New_Line &
            "with Required_Units; pragma Unreferenced (Required_Units);" &
            New_Line & Debug_Include &
            "with Random;" & New_Line &
            "with Sbit.Resources;" & New_Line &
            "with Interaction;" & New_Line &
            "with Sbit." & Pack & ";" & New_Line & New_Line &
            "procedure " & Main & " is" & New_Line;
        Middle : constant String :=
            "begin" & New_Line &
            Tab_Char & "Sbit.Resources.Initialize (""" & Share & """);" &
                New_Line &
            Tab_Char & "Random.Initialize" & Seed & ";" & New_Line &
            Tab_Char & "Sbit." & Pack & ".Read (Interaction.Input)" &
            New_Line;
        Footer : constant String := Debug_End & "end " & Main & ";" & New_Line;

        Blocks : String_Vectors.Vector := String_Vectors.Empty_Vector;
        Stack : Nat_Vectors.Vector := Nat_Vectors.Empty_Vector;

        Text : Unbounded_String := Null_Unbounded_String;

        procedure Add (Line : String) is
            procedure Do_Append (Element : in out Unbounded_String) is begin
                Append (Element, Tab_Char & Tab_Char & Line & New_Line);
            end Do_Append;
        begin
            Blocks.Update_Element (Stack.Last_Element, Do_Append'Access);
        end Add;

        procedure Cap_Off (Closure : String) is
            procedure Do_Append (Element : in out Unbounded_String) is begin
                Append (Element, Tab_Char & Closure & New_Line);
            end Do_Append;
        begin
            Blocks.Update_Element (Stack.Last_Element, Do_Append'Access);
        end Cap_Off;
    begin
        -- Setup the 'main' function
        Blocks.Append (To_Unbounded_String (Middle));
        Stack.Append (0);

        for I in From'Range loop
            if From (I) = To_Code (' ') then
                Add ("(" & Block_Name (Blocks.Last_Index) & "'Access)");
                Blocks.Append (
                    To_Unbounded_String (
                        Tab_Char & "function " &
                        Block_Name (Blocks.Last_Index) &
                        " (X : Sbit.Datatype'Class) " &
                        "return Sbit.Datatype'Class is (X" &
                        New_Line));
                Stack.Append (Blocks.Last_Index);
            elsif From (I) = To_Code (New_Line) then
                if Stack.Length > 1 then
                    Cap_Off (");");
                    Stack.Delete_Last;
                end if;
            else
                if Debug > 0 then
                    Add (".Debug (" & From (I)'Img);
                    Add (", " & Verbosity);

                    if I /= From'Last and then From (I + 1) = To_Code (' ') then
                        Add (", " & Block_Name (Blocks.Last_Index) & "'Access");
                    end if;

                    Add (")");
                end if;

                Add ("." & String (Canonical_Name (From (I))));
            end if;
        end loop;

        while not Stack.Is_Empty loop
            if Stack.Last_Element > 0 then
                Cap_Off (");");
            else
                Cap_Off (".Write (Interaction.Output.all);");
            end if;
            Stack.Delete_Last;
        end loop;

        Append (Text, Header);

        for I of reverse Blocks loop
            Append (Text, I & New_Line);
        end loop;

        Append (Text, Footer);

        return To_String (Text);
    end Compile;
end Sbit.Compiling;
