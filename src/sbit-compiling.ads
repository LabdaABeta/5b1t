package Sbit.Compiling is
    type Debug_Depth is mod 4;

    function Compile (
        From : Listing;
        Share : String;
        Start : Listing := (1 => 0);
        Main : String := "Main";
        Debug : Debug_Depth := 0;
        Seed : String := "") -- or " (seed)"
        return String;
end Sbit.Compiling;
