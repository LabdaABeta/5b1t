with Ada.Tags.Generic_Dispatching_Constructor;
with Ada.Unchecked_Deallocation;
with Ada.Strings.Hash;
with Sbit.Blockcats;
with Sbit.Tuples;
with Sbit.Lists;
with Sbit.Strings;
with Sbit.Empties;
with Sbit.Sets;
with Ada.Strings.Unbounded;
with Opinfo; use Opinfo;
with Ada.Text_IO;

package body Sbit is
    use type Ada.Strings.Unbounded.Unbounded_String;

    package Variable_Maps is new Ada.Containers.Indefinite_Hashed_Maps (
        Key_Type => String,
        Element_Type => Datatype'Class,
        Hash => Ada.Strings.Hash,
        Equivalent_Keys => "=");
    Environment : Variable_Maps.Map;
    Stack : Data_Lists.List;

    type Loop_State is access Datatype'Class;

    -- Most subprograms of the root class raise not implemented messages.
    -- These will only be accessed if some type does not override the default.

    function Not_Implemented_Message (This : Datatype'Class) return String is
    begin
        return " called on " & This.Type_Image &
            " (" & Ada.Tags.Expanded_Name (This'Tag) & ")" &
            " which does not implement it.";
    end Not_Implemented_Message;

    -- A datum's hash is calculated from its representation
    function Data_Hash (Item : Datatype'Class) return Ada.Containers.Hash_Type
    is
    begin
        return Ada.Strings.Hash (Representation (Item));
    end Data_Hash;

    -- Data are equivalent if their representations are
    function Data_Equivalence (Left, Right : Datatype'Class) return Boolean is
    begin
        return Representation (Left) = Representation (Right);
    end Data_Equivalence;

    -- LCOV_EXCL_START: Don't compute coverage here {{{
    function Create (Ignore : not null access Nothing) return Datatype is
        pragma Unreferenced (Ignore);
    begin
        return Empty_Datum;
    end Create;

    function NUM_0 (This : Datatype) return Datatype'Class is
    begin
        raise Not_Implemented with "0 (NUM_0)" & Not_Implemented_Message (This);
        return This;
    end NUM_0;

    function NUM_0 (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        pragma Unreferenced (Block);
    begin
        raise Not_Implemented with "0 (NUM_0) functionally" &
            Not_Implemented_Message (This);
        return This;
    end NUM_0;

    function NUM_1 (This : Datatype) return Datatype'Class is
    begin
        raise Not_Implemented with "1 (NUM_1)" & Not_Implemented_Message (This);
        return This;
    end NUM_1;

    function NUM_1 (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        pragma Unreferenced (Block);
    begin
        raise Not_Implemented with "1 (NUM_1) functionally" &
            Not_Implemented_Message (This);
        return This;
    end NUM_1;

    function NUM_2 (This : Datatype) return Datatype'Class is
    begin
        raise Not_Implemented with "2 (NUM_2)" & Not_Implemented_Message (This);
        return This;
    end NUM_2;

    function NUM_2 (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        pragma Unreferenced (Block);
    begin
        raise Not_Implemented with "2 (NUM_2) functionally" &
            Not_Implemented_Message (This);
        return This;
    end NUM_2;

    function NUM_3 (This : Datatype) return Datatype'Class is
    begin
        raise Not_Implemented with "3 (NUM_3)" & Not_Implemented_Message (This);
        return This;
    end NUM_3;

    function NUM_3 (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        pragma Unreferenced (Block);
    begin
        raise Not_Implemented with "3 (NUM_3) functionally" &
            Not_Implemented_Message (This);
        return This;
    end NUM_3;

    function NUM_4 (This : Datatype) return Datatype'Class is
    begin
        raise Not_Implemented with "4 (NUM_4)" & Not_Implemented_Message (This);
        return This;
    end NUM_4;

    function NUM_4 (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        pragma Unreferenced (Block);
    begin
        raise Not_Implemented with "4 (NUM_4) functionally" &
            Not_Implemented_Message (This);
        return This;
    end NUM_4;

    function NUM_5 (This : Datatype) return Datatype'Class is
    begin
        raise Not_Implemented with "5 (NUM_5)" & Not_Implemented_Message (This);
        return This;
    end NUM_5;

    function NUM_5 (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        pragma Unreferenced (Block);
    begin
        raise Not_Implemented with "5 (NUM_5) functionally" &
            Not_Implemented_Message (This);
        return This;
    end NUM_5;

    function NUM_6 (This : Datatype) return Datatype'Class is
    begin
        raise Not_Implemented with "6 (NUM_6)" & Not_Implemented_Message (This);
        return This;
    end NUM_6;

    function NUM_6 (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        pragma Unreferenced (Block);
    begin
        raise Not_Implemented with "6 (NUM_6) functionally" &
            Not_Implemented_Message (This);
        return This;
    end NUM_6;

    function NUM_7 (This : Datatype) return Datatype'Class is
    begin
        raise Not_Implemented with "7 (NUM_7)" & Not_Implemented_Message (This);
        return This;
    end NUM_7;

    function NUM_7 (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        pragma Unreferenced (Block);
    begin
        raise Not_Implemented with "7 (NUM_7) functionally" &
            Not_Implemented_Message (This);
        return This;
    end NUM_7;

    function NUM_8 (This : Datatype) return Datatype'Class is
    begin
        raise Not_Implemented with "8 (NUM_8)" & Not_Implemented_Message (This);
        return This;
    end NUM_8;

    function NUM_8 (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        pragma Unreferenced (Block);
    begin
        raise Not_Implemented with "8 (NUM_8) functionally" &
            Not_Implemented_Message (This);
        return This;
    end NUM_8;

    function NUM_9 (This : Datatype) return Datatype'Class is
    begin
        raise Not_Implemented with "9 (NUM_9)" & Not_Implemented_Message (This);
        return This;
    end NUM_9;

    function NUM_9 (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        pragma Unreferenced (Block);
    begin
        raise Not_Implemented with "9 (NUM_9) functionally" &
            Not_Implemented_Message (This);
        return This;
    end NUM_9;

    function A_MAJ (This : Datatype) return Datatype'Class is
    begin
        raise Not_Implemented with "A (A_MAJ)" & Not_Implemented_Message (This);
        return This;
    end A_MAJ;

    function A_MAJ (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        pragma Unreferenced (Block);
    begin
        raise Not_Implemented with "A (A_MAJ) functionally" &
            Not_Implemented_Message (This);
        return This;
    end A_MAJ;

    function B_MAJ (This : Datatype) return Datatype'Class is
    begin
        raise Not_Implemented with "B (B_MAJ)" & Not_Implemented_Message (This);
        return This;
    end B_MAJ;

    function B_MAJ (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        pragma Unreferenced (Block);
    begin
        raise Not_Implemented with "B (B_MAJ) functionally" &
            Not_Implemented_Message (This);
        return This;
    end B_MAJ;

    function C_MAJ (This : Datatype) return Datatype'Class is
    begin
        raise Not_Implemented with "C (C_MAJ)" & Not_Implemented_Message (This);
        return This;
    end C_MAJ;

    function C_MAJ (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        pragma Unreferenced (Block);
    begin
        raise Not_Implemented with "C (C_MAJ) functionally" &
            Not_Implemented_Message (This);
        return This;
    end C_MAJ;

    function D_MAJ (This : Datatype) return Datatype'Class is
    begin
        raise Not_Implemented with "D (D_MAJ)" & Not_Implemented_Message (This);
        return This;
    end D_MAJ;

    function D_MAJ (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        pragma Unreferenced (Block);
    begin
        raise Not_Implemented with "D (D_MAJ) functionally" &
            Not_Implemented_Message (This);
        return This;
    end D_MAJ;

    function E_MAJ (This : Datatype) return Datatype'Class is
    begin
        raise Not_Implemented with "E (E_MAJ)" & Not_Implemented_Message (This);
        return This;
    end E_MAJ;

    function E_MAJ (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        pragma Unreferenced (Block);
    begin
        raise Not_Implemented with "E (E_MAJ) functionally" &
            Not_Implemented_Message (This);
        return This;
    end E_MAJ;

    function F_MAJ (This : Datatype) return Datatype'Class is
    begin
        raise Not_Implemented with "F (F_MAJ)" & Not_Implemented_Message (This);
        return This;
    end F_MAJ;

    function F_MAJ (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        pragma Unreferenced (Block);
    begin
        raise Not_Implemented with "F (F_MAJ) functionally" &
            Not_Implemented_Message (This);
        return This;
    end F_MAJ;

    function G_MAJ (This : Datatype) return Datatype'Class is
    begin
        raise Not_Implemented with "G (G_MAJ)" & Not_Implemented_Message (This);
        return This;
    end G_MAJ;

    function G_MAJ (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        pragma Unreferenced (Block);
    begin
        raise Not_Implemented with "G (G_MAJ) functionally" &
            Not_Implemented_Message (This);
        return This;
    end G_MAJ;

    function H_MAJ (This : Datatype) return Datatype'Class is
    begin
        raise Not_Implemented with "H (H_MAJ)" & Not_Implemented_Message (This);
        return This;
    end H_MAJ;

    function H_MAJ (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        pragma Unreferenced (Block);
    begin
        raise Not_Implemented with "H (H_MAJ) functionally" &
            Not_Implemented_Message (This);
        return This;
    end H_MAJ;

    function I_MAJ (This : Datatype) return Datatype'Class is
    begin
        raise Not_Implemented with "I (I_MAJ)" & Not_Implemented_Message (This);
        return This;
    end I_MAJ;

    function I_MAJ (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        pragma Unreferenced (Block);
    begin
        raise Not_Implemented with "I (I_MAJ) functionally" &
            Not_Implemented_Message (This);
        return This;
    end I_MAJ;

    function J_MAJ (This : Datatype) return Datatype'Class is
    begin
        raise Not_Implemented with "J (J_MAJ)" & Not_Implemented_Message (This);
        return This;
    end J_MAJ;

    function J_MAJ (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        pragma Unreferenced (Block);
    begin
        raise Not_Implemented with "J (J_MAJ) functionally" &
            Not_Implemented_Message (This);
        return This;
    end J_MAJ;

    function K_MAJ (This : Datatype) return Datatype'Class is
    begin
        raise Not_Implemented with "K (K_MAJ)" & Not_Implemented_Message (This);
        return This;
    end K_MAJ;

    function K_MAJ (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        pragma Unreferenced (Block);
    begin
        raise Not_Implemented with "K (K_MAJ) functionally" &
            Not_Implemented_Message (This);
        return This;
    end K_MAJ;

    function L_MAJ (This : Datatype) return Datatype'Class is
    begin
        raise Not_Implemented with "L (L_MAJ)" & Not_Implemented_Message (This);
        return This;
    end L_MAJ;

    function L_MAJ (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        pragma Unreferenced (Block);
    begin
        raise Not_Implemented with "L (L_MAJ) functionally" &
            Not_Implemented_Message (This);
        return This;
    end L_MAJ;

    function M_MAJ (This : Datatype) return Datatype'Class is
    begin
        raise Not_Implemented with "M (M_MAJ)" & Not_Implemented_Message (This);
        return This;
    end M_MAJ;

    function M_MAJ (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        pragma Unreferenced (Block);
    begin
        raise Not_Implemented with "M (M_MAJ) functionally" &
            Not_Implemented_Message (This);
        return This;
    end M_MAJ;

    function N_MAJ (This : Datatype) return Datatype'Class is
    begin
        raise Not_Implemented with "N (N_MAJ)" & Not_Implemented_Message (This);
        return This;
    end N_MAJ;

    function N_MAJ (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        pragma Unreferenced (Block);
    begin
        raise Not_Implemented with "N (N_MAJ) functionally" &
            Not_Implemented_Message (This);
        return This;
    end N_MAJ;

    function O_MAJ (This : Datatype) return Datatype'Class is
    begin
        raise Not_Implemented with "O (O_MAJ)" & Not_Implemented_Message (This);
        return This;
    end O_MAJ;

    function O_MAJ (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        pragma Unreferenced (Block);
    begin
        raise Not_Implemented with "O (O_MAJ) functionally" &
            Not_Implemented_Message (This);
        return This;
    end O_MAJ;

    function P_MAJ (This : Datatype) return Datatype'Class is
    begin
        raise Not_Implemented with "P (P_MAJ)" & Not_Implemented_Message (This);
        return This;
    end P_MAJ;

    function P_MAJ (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        pragma Unreferenced (Block);
    begin
        raise Not_Implemented with "P (P_MAJ) functionally" &
            Not_Implemented_Message (This);
        return This;
    end P_MAJ;

    function Q_MAJ (This : Datatype) return Datatype'Class is
    begin
        raise Not_Implemented with "Q (Q_MAJ)" & Not_Implemented_Message (This);
        return This;
    end Q_MAJ;

    function Q_MAJ (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        pragma Unreferenced (Block);
    begin
        raise Not_Implemented with "Q (Q_MAJ) functionally" &
            Not_Implemented_Message (This);
        return This;
    end Q_MAJ;

    function R_MAJ (This : Datatype) return Datatype'Class is
    begin
        raise Not_Implemented with "R (R_MAJ)" & Not_Implemented_Message (This);
        return This;
    end R_MAJ;

    function R_MAJ (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        pragma Unreferenced (Block);
    begin
        raise Not_Implemented with "R (R_MAJ) functionally" &
            Not_Implemented_Message (This);
        return This;
    end R_MAJ;

    function S_MAJ (This : Datatype) return Datatype'Class is
    begin
        raise Not_Implemented with "S (S_MAJ)" & Not_Implemented_Message (This);
        return This;
    end S_MAJ;

    function S_MAJ (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        pragma Unreferenced (Block);
    begin
        raise Not_Implemented with "S (S_MAJ) functionally" &
            Not_Implemented_Message (This);
        return This;
    end S_MAJ;

    function T_MAJ (This : Datatype) return Datatype'Class is
    begin
        raise Not_Implemented with "T (T_MAJ)" & Not_Implemented_Message (This);
        return This;
    end T_MAJ;

    function T_MAJ (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        pragma Unreferenced (Block);
    begin
        raise Not_Implemented with "T (T_MAJ) functionally" &
            Not_Implemented_Message (This);
        return This;
    end T_MAJ;

    function U_MAJ (This : Datatype) return Datatype'Class is
    begin
        raise Not_Implemented with "U (U_MAJ)" & Not_Implemented_Message (This);
        return This;
    end U_MAJ;

    function U_MAJ (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        pragma Unreferenced (Block);
    begin
        raise Not_Implemented with "U (U_MAJ) functionally" &
            Not_Implemented_Message (This);
        return This;
    end U_MAJ;

    function V_MAJ (This : Datatype) return Datatype'Class is
    begin
        raise Not_Implemented with "V (V_MAJ)" & Not_Implemented_Message (This);
        return This;
    end V_MAJ;

    function V_MAJ (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        pragma Unreferenced (Block);
    begin
        raise Not_Implemented with "V (V_MAJ) functionally" &
            Not_Implemented_Message (This);
        return This;
    end V_MAJ;

    -- LCOV_EXCL_STOP }}}

    function W_MAJ (This : Datatype) return Datatype'Class is
        Result : Tuples.Tuple := Tuples.Empty_Tuple;
    begin
        Result.Append (This);
        Result.Append (This);

        return Result;
    end W_MAJ;

    -- LCOV_EXCL_START: Don't compute coverage here {{{

    function W_MAJ (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        pragma Unreferenced (Block);
    begin
        raise Not_Implemented with "W (W_MAJ) functionally" &
            Not_Implemented_Message (This);
        return This;
    end W_MAJ;

    function X_MAJ (This : Datatype) return Datatype'Class is
    begin
        raise Not_Implemented with "X (X_MAJ)" & Not_Implemented_Message (This);
        return This;
    end X_MAJ;

    function X_MAJ (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        pragma Unreferenced (Block);
    begin
        raise Not_Implemented with "X (X_MAJ) functionally" &
            Not_Implemented_Message (This);
        return This;
    end X_MAJ;

    function Y_MAJ (This : Datatype) return Datatype'Class is
    begin
        raise Not_Implemented with "Y (Y_MAJ)" & Not_Implemented_Message (This);
        return This;
    end Y_MAJ;

    -- LCOV_EXCL_STOP }}}

    function Y_MAJ (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        State : Loop_State := new Datatype'Class'(Datatype'Class (This));

        procedure Free_State is new Ada.Unchecked_Deallocation (
            Datatype'Class, Loop_State);
    begin
        loop
            declare
                Next : constant Datatype'Class := Block (State.all);
            begin
                if Representation (Next) = Representation (State.all) then
                    Free_State (State);
                    return Next;
                end if;

                Free_State (State);

                State := new Datatype'Class'(Next);
            end;
        end loop;
    end Y_MAJ;

    -- LCOV_EXCL_START: Don't compute coverage here {{{

    function Z_MAJ (This : Datatype) return Datatype'Class is
    begin
        raise Not_Implemented with "Z (Z_MAJ)" & Not_Implemented_Message (This);
        return This;
    end Z_MAJ;

    function Z_MAJ (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        pragma Unreferenced (Block);
    begin
        raise Not_Implemented with "Z (Z_MAJ) functionally" &
            Not_Implemented_Message (This);
        return This;
    end Z_MAJ;

    function A_MIN (This : Datatype) return Datatype'Class is
    begin
        raise Not_Implemented with "a (A_MIN)" & Not_Implemented_Message (This);
        return This;
    end A_MIN;

    function A_MIN (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        pragma Unreferenced (Block);
    begin
        raise Not_Implemented with "a (A_MIN) functionally" &
            Not_Implemented_Message (This);
        return This;
    end A_MIN;

    function B_MIN (This : Datatype) return Datatype'Class is
    begin
        raise Not_Implemented with "b (B_MIN)" & Not_Implemented_Message (This);
        return This;
    end B_MIN;

    function B_MIN (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        pragma Unreferenced (Block);
    begin
        raise Not_Implemented with "b (B_MIN) functionally" &
            Not_Implemented_Message (This);
        return This;
    end B_MIN;

    function C_MIN (This : Datatype) return Datatype'Class is
    begin
        raise Not_Implemented with "c (C_MIN)" & Not_Implemented_Message (This);
        return This;
    end C_MIN;

    function C_MIN (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        pragma Unreferenced (Block);
    begin
        raise Not_Implemented with "c (C_MIN) functionally" &
            Not_Implemented_Message (This);
        return This;
    end C_MIN;

    function D_MIN (This : Datatype) return Datatype'Class is
    begin
        raise Not_Implemented with "d (D_MIN)" & Not_Implemented_Message (This);
        return This;
    end D_MIN;

    function D_MIN (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        pragma Unreferenced (Block);
    begin
        raise Not_Implemented with "d (D_MIN) functionally" &
            Not_Implemented_Message (This);
        return This;
    end D_MIN;

    function E_MIN (This : Datatype) return Datatype'Class is
    begin
        raise Not_Implemented with "e (E_MIN)" & Not_Implemented_Message (This);
        return This;
    end E_MIN;

    function E_MIN (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        pragma Unreferenced (Block);
    begin
        raise Not_Implemented with "e (E_MIN) functionally" &
            Not_Implemented_Message (This);
        return This;
    end E_MIN;

    function F_MIN (This : Datatype) return Datatype'Class is
    begin
        raise Not_Implemented with "f (F_MIN)" & Not_Implemented_Message (This);
        return This;
    end F_MIN;

    function F_MIN (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        pragma Unreferenced (Block);
    begin
        raise Not_Implemented with "f (F_MIN) functionally" &
            Not_Implemented_Message (This);
        return This;
    end F_MIN;

    function G_MIN (This : Datatype) return Datatype'Class is
    begin
        raise Not_Implemented with "g (G_MIN)" & Not_Implemented_Message (This);
        return This;
    end G_MIN;

    function G_MIN (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        pragma Unreferenced (Block);
    begin
        raise Not_Implemented with "g (G_MIN) functionally" &
            Not_Implemented_Message (This);
        return This;
    end G_MIN;

    function H_MIN (This : Datatype) return Datatype'Class is
    begin
        raise Not_Implemented with "h (H_MIN)" & Not_Implemented_Message (This);
        return This;
    end H_MIN;

    function H_MIN (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        pragma Unreferenced (Block);
    begin
        raise Not_Implemented with "h (H_MIN) functionally" &
            Not_Implemented_Message (This);
        return This;
    end H_MIN;

    function I_MIN (This : Datatype) return Datatype'Class is
    begin
        raise Not_Implemented with "i (I_MIN)" & Not_Implemented_Message (This);
        return This;
    end I_MIN;

    function I_MIN (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        pragma Unreferenced (Block);
    begin
        raise Not_Implemented with "i (I_MIN) functionally" &
            Not_Implemented_Message (This);
        return This;
    end I_MIN;

    function J_MIN (This : Datatype) return Datatype'Class is
    begin
        raise Not_Implemented with "j (J_MIN)" & Not_Implemented_Message (This);
        return This;
    end J_MIN;

    function J_MIN (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        pragma Unreferenced (Block);
    begin
        raise Not_Implemented with "j (J_MIN) functionally" &
            Not_Implemented_Message (This);
        return This;
    end J_MIN;

    function K_MIN (This : Datatype) return Datatype'Class is
    begin
        raise Not_Implemented with "k (K_MIN)" & Not_Implemented_Message (This);
        return This;
    end K_MIN;

    function K_MIN (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        pragma Unreferenced (Block);
    begin
        raise Not_Implemented with "k (K_MIN) functionally" &
            Not_Implemented_Message (This);
        return This;
    end K_MIN;

    function L_MIN (This : Datatype) return Datatype'Class is
    begin
        raise Not_Implemented with "l (L_MIN)" & Not_Implemented_Message (This);
        return This;
    end L_MIN;

    function L_MIN (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        pragma Unreferenced (Block);
    begin
        raise Not_Implemented with "l (L_MIN) functionally" &
            Not_Implemented_Message (This);
        return This;
    end L_MIN;

    function M_MIN (This : Datatype) return Datatype'Class is
    begin
        raise Not_Implemented with "m (M_MIN)" & Not_Implemented_Message (This);
        return This;
    end M_MIN;

    function M_MIN (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        pragma Unreferenced (Block);
    begin
        raise Not_Implemented with "m (M_MIN) functionally" &
            Not_Implemented_Message (This);
        return This;
    end M_MIN;

    function N_MIN (This : Datatype) return Datatype'Class is
    begin
        raise Not_Implemented with "n (N_MIN)" & Not_Implemented_Message (This);
        return This;
    end N_MIN;

    function N_MIN (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        pragma Unreferenced (Block);
    begin
        raise Not_Implemented with "n (N_MIN) functionally" &
            Not_Implemented_Message (This);
        return This;
    end N_MIN;

    -- LCOV_EXCL_STOP }}}

    function O_MIN (This : Datatype) return Datatype'Class is
        Self : constant Datatype'Class := This;
    begin
        Self.Write (Output.all);

        return Self;
    end O_MIN;

    function O_MIN (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        State : Loop_State := new Datatype'Class'(Datatype'Class (This));

        procedure Free_State is new Ada.Unchecked_Deallocation (
            Datatype'Class, Loop_State);
    begin
        loop
            declare
                Next : constant Datatype'Class := Block (State.all);
            begin
                Free_State (State);

                State := new Datatype'Class'(Next);
            end;
        end loop;

        pragma Warnings (Off, "unreachable code");
        return State.all;
        pragma Warnings (On, "unreachable code");
    end O_MIN;

    -- LCOV_EXCL_START: Don't compute coverage here {{{

    function P_MIN (This : Datatype) return Datatype'Class is
    begin
        raise Not_Implemented with "p (P_MIN)" & Not_Implemented_Message (This);
        return This;
    end P_MIN;

    function P_MIN (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        pragma Unreferenced (Block);
    begin
        raise Not_Implemented with "p (P_MIN) functionally" &
            Not_Implemented_Message (This);
        return This;
    end P_MIN;

    function Q_MIN (This : Datatype) return Datatype'Class is
    begin
        raise Not_Implemented with "q (Q_MIN)" & Not_Implemented_Message (This);
        return This;
    end Q_MIN;

    function Q_MIN (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        pragma Unreferenced (Block);
    begin
        raise Not_Implemented with "q (Q_MIN) functionally" &
            Not_Implemented_Message (This);
        return This;
    end Q_MIN;

    function R_MIN (This : Datatype) return Datatype'Class is
    begin
        raise Not_Implemented with "r (R_MIN)" & Not_Implemented_Message (This);
        return This;
    end R_MIN;

    function R_MIN (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        pragma Unreferenced (Block);
    begin
        raise Not_Implemented with "r (R_MIN) functionally" &
            Not_Implemented_Message (This);
        return This;
    end R_MIN;

    function S_MIN (This : Datatype) return Datatype'Class is
    begin
        raise Not_Implemented with "s (S_MIN)" & Not_Implemented_Message (This);
        return This;
    end S_MIN;

    function S_MIN (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        pragma Unreferenced (Block);
    begin
        raise Not_Implemented with "s (S_MIN) functionally" &
            Not_Implemented_Message (This);
        return This;
    end S_MIN;

    function T_MIN (This : Datatype) return Datatype'Class is
    begin
        raise Not_Implemented with "t (T_MIN)" & Not_Implemented_Message (This);
        return This;
    end T_MIN;

    function T_MIN (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        pragma Unreferenced (Block);
    begin
        raise Not_Implemented with "t (T_MIN) functionally" &
            Not_Implemented_Message (This);
        return This;
    end T_MIN;

    function U_MIN (This : Datatype) return Datatype'Class is
    begin
        raise Not_Implemented with "u (U_MIN)" & Not_Implemented_Message (This);
        return This;
    end U_MIN;

    function U_MIN (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        pragma Unreferenced (Block);
    begin
        raise Not_Implemented with "u (U_MIN) functionally" &
            Not_Implemented_Message (This);
        return This;
    end U_MIN;

    -- LCOV_EXCL_STOP }}}

    function V_MIN (This : Datatype) return Datatype'Class is
    begin
        Stack.Append (Datatype'Class (This));

        return Datatype'Class (This);
    end V_MIN;

    function V_MIN (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Name : constant String :=
            Block.all (Blockcats.Create).Image;
    begin
        Environment.Include (Name, Datatype'Class (This));

        return Datatype'Class (This);
    end V_MIN;

    -- LCOV_EXCL_START: Don't compute coverage here {{{

    function W_MIN (This : Datatype) return Datatype'Class is
    begin
        raise Not_Implemented with "w (W_MIN)" & Not_Implemented_Message (This);
        return This;
    end W_MIN;

    function W_MIN (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        pragma Unreferenced (Block);
    begin
        raise Not_Implemented with "w (W_MIN) functionally" &
            Not_Implemented_Message (This);
        return This;
    end W_MIN;

    function X_MIN (This : Datatype) return Datatype'Class is
    begin
        raise Not_Implemented with "x (X_MIN)" & Not_Implemented_Message (This);
        return This;
    end X_MIN;

    function X_MIN (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        pragma Unreferenced (Block);
    begin
        raise Not_Implemented with "x (X_MIN) functionally" &
            Not_Implemented_Message (This);
        return This;
    end X_MIN;

    function Y_MIN (This : Datatype) return Datatype'Class is
    begin
        raise Not_Implemented with "y (Y_MIN)" & Not_Implemented_Message (This);
        return This;
    end Y_MIN;

    function Y_MIN (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        pragma Unreferenced (Block);
    begin
        raise Not_Implemented with "y (Y_MIN) functionally" &
            Not_Implemented_Message (This);
        return This;
    end Y_MIN;

    function Z_MIN (This : Datatype) return Datatype'Class is
    begin
        raise Not_Implemented with "z (Z_MIN)" & Not_Implemented_Message (This);
        return This;
    end Z_MIN;

    function Z_MIN (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        pragma Unreferenced (Block);
    begin
        raise Not_Implemented with "z (Z_MIN) functionally" &
            Not_Implemented_Message (This);
        return This;
    end Z_MIN;

    function ADDIT (This : Datatype) return Datatype'Class is
    begin
        raise Not_Implemented with "+ (ADDIT)" & Not_Implemented_Message (This);
        return This;
    end ADDIT;

    function ADDIT (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        pragma Unreferenced (Block);
    begin
        raise Not_Implemented with "+ (ADDIT) functionally" &
            Not_Implemented_Message (This);
        return This;
    end ADDIT;

    function SLASH (This : Datatype) return Datatype'Class is
    begin
        raise Not_Implemented with "/ (SLASH)" & Not_Implemented_Message (This);
        return This;
    end SLASH;

    function SLASH (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        pragma Unreferenced (Block);
    begin
        raise Not_Implemented with "/ (SLASH) functionally" &
            Not_Implemented_Message (This);
        return This;
    end SLASH;

    function MINUS (This : Datatype) return Datatype'Class is
    begin
        raise Not_Implemented with "- (MINUS)" & Not_Implemented_Message (This);
        return This;
    end MINUS;

    function MINUS (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        pragma Unreferenced (Block);
    begin
        raise Not_Implemented with "- (MINUS) functionally" &
            Not_Implemented_Message (This);
        return This;
    end MINUS;

    function TIMES (This : Datatype) return Datatype'Class is
    begin
        raise Not_Implemented with "* (TIMES)" & Not_Implemented_Message (This);
        return This;
    end TIMES;

    function TIMES (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        pragma Unreferenced (Block);
    begin
        raise Not_Implemented with "* (TIMES) functionally" &
            Not_Implemented_Message (This);
        return This;
    end TIMES;

    function VPIPE (This : Datatype) return Datatype'Class is
    begin
        raise Not_Implemented with "| (VPIPE)" & Not_Implemented_Message (This);
        return This;
    end VPIPE;

    function VPIPE (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        pragma Unreferenced (Block);
    begin
        raise Not_Implemented with "| (VPIPE) functionally" &
            Not_Implemented_Message (This);
        return This;
    end VPIPE;

    -- LCOV_EXCL_STOP }}}

    function CARET (This : Datatype) return Datatype'Class is
    begin
        if Stack.Is_Empty then
            return This;
        else
            declare
                Result : constant Datatype'Class := Stack.Last_Element;
            begin
                Stack.Delete_Last;
                return Result;
            end;
        end if;
    end CARET;

    function CARET (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Name : constant String :=
            Block.all (Blockcats.Create).Image;
    begin
        if Environment.Contains (Name) then
            declare
                Result : constant Datatype'Class := Environment.Element (Name);
            begin
                return Result;
            end;
        else
            Environment.Insert (Name, This);

            return This;
        end if;
    end CARET;

    -- LCOV_EXCL_START: Don't compute coverage here {{{

    function AMPER (This : Datatype) return Datatype'Class is
    begin
        raise Not_Implemented with "& (AMPER)" & Not_Implemented_Message (This);
        return This;
    end AMPER;

    function AMPER (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        pragma Unreferenced (Block);
    begin
        raise Not_Implemented with "& (AMPER) functionally" &
            Not_Implemented_Message (This);
        return This;
    end AMPER;

    function PRCNT (This : Datatype) return Datatype'Class is
    begin
        raise Not_Implemented with "% (PRCNT)" & Not_Implemented_Message (This);
        return This;
    end PRCNT;

    function PRCNT (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        pragma Unreferenced (Block);
    begin
        raise Not_Implemented with "% (PRCNT) functionally" &
            Not_Implemented_Message (This);
        return This;
    end PRCNT;

    function LESST (This : Datatype) return Datatype'Class is
    begin
        raise Not_Implemented with "< (LESST)" & Not_Implemented_Message (This);
        return This;
    end LESST;

    function LESST (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        pragma Unreferenced (Block);
    begin
        raise Not_Implemented with "< (LESST) functionally" &
            Not_Implemented_Message (This);
        return This;
    end LESST;

    function MORET (This : Datatype) return Datatype'Class is
    begin
        raise Not_Implemented with "> (MORET)" & Not_Implemented_Message (This);
        return This;
    end MORET;

    function MORET (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        pragma Unreferenced (Block);
    begin
        raise Not_Implemented with "> (MORET) functionally" &
            Not_Implemented_Message (This);
        return This;
    end MORET;

    function PAR_L (This : Datatype) return Datatype'Class is
    begin
        raise Not_Implemented with "( (PAR_L)" & Not_Implemented_Message (This);
        return This;
    end PAR_L;

    function PAR_L (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        pragma Unreferenced (Block);
    begin
        raise Not_Implemented with "( (PAR_L) functionally" &
            Not_Implemented_Message (This);
        return This;
    end PAR_L;

    -- LCOV_EXCL_STOP }}}

    function PAR_R (This : Datatype) return Datatype'Class is
        Result : Tuples.Tuple := Tuples.Empty_Tuple;
    begin
        Result.Append (This);

        return Result;
    end PAR_R;

    -- LCOV_EXCL_START: Don't compute coverage here {{{

    function PAR_R (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        pragma Unreferenced (Block);
    begin
        raise Not_Implemented with ") (PAR_R) functionally" &
            Not_Implemented_Message (This);
        return This;
    end PAR_R;

    function BRA_L (This : Datatype) return Datatype'Class is
    begin
        raise Not_Implemented with "[ (BRA_L)" & Not_Implemented_Message (This);
        return This;
    end BRA_L;

    function BRA_L (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        pragma Unreferenced (Block);
    begin
        raise Not_Implemented with "[ (BRA_L) functionally" &
            Not_Implemented_Message (This);
        return This;
    end BRA_L;

    -- LCOV_EXCL_STOP }}}

    function BRA_R (This : Datatype) return Datatype'Class is
        Result : Lists.List := Lists.Empty_List;
    begin
        Result.Append (This);
        return Result;
    end BRA_R;

    -- LCOV_EXCL_START: Don't compute coverage here {{{

    function BRA_R (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        pragma Unreferenced (Block);
    begin
        raise Not_Implemented with "] (BRA_R) functionally" &
            Not_Implemented_Message (This);
        return This;
    end BRA_R;

    function CUR_L (This : Datatype) return Datatype'Class is
    begin
        raise Not_Implemented with "{ (CUR_L)" & Not_Implemented_Message (This);
        return This;
    end CUR_L;

    function CUR_L (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        pragma Unreferenced (Block);
    begin
        raise Not_Implemented with "{ (CUR_L) functionally" &
            Not_Implemented_Message (This);
        return This;
    end CUR_L;

    -- LCOV_EXCL_STOP }}}

    function CUR_R (This : Datatype) return Datatype'Class is
        Result : Sets.Set := Sets.Empty_Set;
    begin
        Result.Insert (This);

        return Result;
    end CUR_R;

    -- LCOV_EXCL_START: Don't compute coverage here {{{

    function CUR_R (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        pragma Unreferenced (Block);
    begin
        raise Not_Implemented with "} (CUR_R) functionally" &
            Not_Implemented_Message (This);
        return This;
    end CUR_R;

    function O_DOT (This : Datatype) return Datatype'Class is
    begin
        raise Not_Implemented with ". (O_DOT)" & Not_Implemented_Message (This);
        return This;
    end O_DOT;

    -- LCOV_EXCL_STOP }}}

    function O_DOT (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        Result : constant Datatype'Class := Block.all (This);
    begin
        return Result;
    end O_DOT;

    -- LCOV_EXCL_START: Don't compute coverage here {{{

    function COMMA (This : Datatype) return Datatype'Class is
    begin
        raise Not_Implemented with ", (COMMA)" & Not_Implemented_Message (This);
        return This;
    end COMMA;

    -- LCOV_EXCL_STOP }}}

    function COMMA (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        New_Item : constant Datatype'Class := Block.all (Empties.Instance);
        Result : Tuples.Tuple := Tuples.Empty_Tuple;
    begin
        Result.Append (This);
        Result.Append (New_Item);

        declare
            Final : constant Tuples.Tuple'Class := Tuples.Child_Reduce (Result);
        begin
            return Final;
        end;
    end COMMA;

    -- LCOV_EXCL_START: Don't compute coverage here {{{

    function SEMIC (This : Datatype) return Datatype'Class is
    begin
        raise Not_Implemented with "; (SEMIC)" & Not_Implemented_Message (This);
        return This;
    end SEMIC;

    -- LCOV_EXCL_STOP }}}

    function SEMIC (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        New_Item : constant Datatype'Class := Block.all (Empties.Instance);
        Result : Tuples.Tuple := Tuples.Empty_Tuple;
    begin
        Result.Append (New_Item);
        Result.Append (This);

        declare
            Final : constant Tuples.Tuple'Class := Tuples.Child_Reduce (Result);
        begin
            return Final;
        end;
    end SEMIC;

    -- LCOV_EXCL_START: Don't compute coverage here {{{

    function COLON (This : Datatype) return Datatype'Class is
    begin
        raise Not_Implemented with ": (COLON)" & Not_Implemented_Message (This);
        return This;
    end COLON;

    function COLON (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        pragma Unreferenced (Block);
    begin
        raise Not_Implemented with ": (COLON) functionally" &
            Not_Implemented_Message (This);
        return This;
    end COLON;

    function APOST (This : Datatype) return Datatype'Class is
    begin
        raise Not_Implemented with "' (APOST)" & Not_Implemented_Message (This);
        return This;
    end APOST;

    function APOST (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        pragma Unreferenced (Block);
    begin
        raise Not_Implemented with "' (APOST) functionally" &
            Not_Implemented_Message (This);
        return This;
    end APOST;

    function QUOTE (This : Datatype) return Datatype'Class is
    begin
        raise Not_Implemented with """ (QUOTE)" &
            Not_Implemented_Message (This);
        return This;
    end QUOTE;

    function QUOTE (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        pragma Unreferenced (Block);
    begin
        raise Not_Implemented with " functionally" &
            Not_Implemented_Message (This);
        return This;
    end QUOTE;

    -- LCOV_EXCL_STOP }}}

    function BTICK (This : Datatype) return Datatype'Class is
        Result : constant Strings.String := Strings.Create (This.Image);
    begin
        return Datatype'Class (Result);
    end BTICK;

    -- LCOV_EXCL_START: Don't compute coverage here {{{

    function BTICK (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        pragma Unreferenced (Block);
    begin
        raise Not_Implemented with "` (BTICK) functionally" &
            Not_Implemented_Message (This);
        return This;
    end BTICK;

    function TILDE (This : Datatype) return Datatype'Class is
    begin
        raise Not_Implemented with "~ (TILDE)" & Not_Implemented_Message (This);
        return This;
    end TILDE;

    function TILDE (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        pragma Unreferenced (Block);
    begin
        raise Not_Implemented with "~ (TILDE) functionally" &
            Not_Implemented_Message (This);
        return This;
    end TILDE;

    function EXCLA (This : Datatype) return Datatype'Class is
    begin
        raise Not_Implemented with "! (EXCLA)" & Not_Implemented_Message (This);
        return This;
    end EXCLA;

    function EXCLA (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        pragma Unreferenced (Block);
    begin
        raise Not_Implemented with "! (EXCLA) functionally" &
            Not_Implemented_Message (This);
        return This;
    end EXCLA;

    function ATSGN (This : Datatype) return Datatype'Class is
    begin
        raise Not_Implemented with "@ (ATSGN)" & Not_Implemented_Message (This);
        return This;
    end ATSGN;

    function ATSGN (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        pragma Unreferenced (Block);
    begin
        raise Not_Implemented with "@ (ATSGN) functionally" &
            Not_Implemented_Message (This);
        return This;
    end ATSGN;

    function POUND (This : Datatype) return Datatype'Class is
    begin
        raise Not_Implemented with "# (POUND)" & Not_Implemented_Message (This);
        return This;
    end POUND;

    function POUND (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        pragma Unreferenced (Block);
    begin
        raise Not_Implemented with "# (POUND) functionally" &
            Not_Implemented_Message (This);
        return This;
    end POUND;

    function MONEY (This : Datatype) return Datatype'Class is
    begin
        raise Not_Implemented with "$ (MONEY)" & Not_Implemented_Message (This);
        return This;
    end MONEY;

    function MONEY (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        pragma Unreferenced (Block);
    begin
        raise Not_Implemented with "$ (MONEY) functionally" &
            Not_Implemented_Message (This);
        return This;
    end MONEY;

    function QUERY (This : Datatype) return Datatype'Class is
    begin
        raise Not_Implemented with "? (QUERY)" & Not_Implemented_Message (This);
        return This;
    end QUERY;

    function QUERY (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        pragma Unreferenced (Block);
    begin
        raise Not_Implemented with "? (QUERY) functionally" &
            Not_Implemented_Message (This);
        return This;
    end QUERY;

    function UNDER (This : Datatype) return Datatype'Class is
    begin
        raise Not_Implemented with "_ (UNDER)" & Not_Implemented_Message (This);
        return This;
    end UNDER;

    function UNDER (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        pragma Unreferenced (Block);
    begin
        raise Not_Implemented with "_ (UNDER) functionally" &
            Not_Implemented_Message (This);
        return This;
    end UNDER;

    function EQUAL (This : Datatype) return Datatype'Class is
    begin
        raise Not_Implemented with "= (EQUAL)" & Not_Implemented_Message (This);
        return This;
    end EQUAL;

    function EQUAL (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        pragma Unreferenced (Block);
    begin
        raise Not_Implemented with "= (EQUAL) functionally" &
            Not_Implemented_Message (This);
        return This;
    end EQUAL;

    function HSALS (This : Datatype) return Datatype'Class is
    begin
        raise Not_Implemented with "\ (HSALS)" & Not_Implemented_Message (This);
        return This;
    end HSALS;

    function HSALS (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class
    is
        pragma Unreferenced (Block);
    begin
        raise Not_Implemented with "\ (HSALS) functionally" &
            Not_Implemented_Message (This);
        return This;
    end HSALS;

    -- LCOV_EXCL_STOP }}}

    function Read (From : not null access Inputter) return Datatype is
        pragma Unreferenced (From);
    begin
        raise Not_Implemented
            with "Read" & Not_Implemented_Message (Empty_Datum);
        return Empty_Datum;
    end Read;

    procedure Write (This : Datatype; To : in out Outputter) is
        pragma Unreferenced (To);
    begin
        raise Not_Implemented with "Write" & Not_Implemented_Message (This);
    end Write;

    function "-" (Right : Ada.Strings.Unbounded.Unbounded_String) return String
        renames Ada.Strings.Unbounded.To_String;

    -- Debugging is disabled when recursively blockcatting
    Debugging_Enabled : Boolean := True;

    function Debug (
        This : Datatype'Class;
        Op : Code_Point;
        Verbose : Debug_Verbosity;
        Block : access function (This : Datatype'Class) return Datatype'Class
            := null) return Sbit.Datatype'Class
    is
        Ops : constant Sbit_Type_Maps.Map := Get_Operations;
        Source_Rep : constant Sbit_Representation := +This.Type_Image;

        procedure Err_Line (Item : String) is
        begin
            Ada.Text_IO.Put_Line (Ada.Text_IO.Standard_Error, Item);
        end Err_Line;

        procedure Err (Item : String) is
        begin
            Ada.Text_IO.Put (Ada.Text_IO.Standard_Error, Item);
        end Err;
    begin
        if not Debugging_Enabled then
            return This;
        end if;

        Err (This.Image);
        Err ("." & String (Sbit.Canonical_Name (Op)));

        if Block /= null then
            Debugging_Enabled := False;
            Err ("[" & Block.all (Sbit.Blockcats.Create).Image & "]");
            Debugging_Enabled := True;
        end if;

        if not Ops.Contains (Source_Rep) then
            Err_Line ("?");
            Err_Line ("Could not find type '" & (-Source_Rep) & "'!");
            Err_Line ("Cannot debug this operation. Sorry.");
            Err_Line ("");
            Err_Line ("");
            return This;
        end if;

        if not Ops.Element (Source_Rep).Operations (
            (if Block /= null then Blocked else No_Block), Op).Exists
        then
            Err_Line ("?");
            Err_Line ("This operator is undocumented!");
            Err_Line ("Cannot debug this operation. Sorry.");
            Err_Line ("");
            Err_Line ("");
            return This;
        end if;

        declare
            Source : constant Sbit_Type := Ops.Element (Source_Rep);
            The_Op : constant Present_Operator := Source.Operations (
                (if Block /= null then Blocked else No_Block), Op);
            Start : Boolean := True;
        begin
            case Verbose is
                when Low =>
                    Err_Line (" => ");
                when Medium =>
                    Err (" (" & (-The_Op.Name) & ") => ");

                    for Ret of The_Op.Return_Types loop
                        if not Start then
                            Err (", ");
                        end if;
                        Start := False;

                        -- TODO: Intelligently handle the 'generic' types

                        if Ops.Contains (Ret) then
                            Err (-Ops.Element (Ret).Name);
                        else
                            Err ("???");
                        end if;
                    end loop;

                    Err_Line ("");
                    Err_Line ("");
                when High =>
                    Err ((-The_Op.Name) & ": " & (-Source_Rep) & " -> ");

                    for Ret of The_Op.Return_Types loop
                        if not Start then
                            Err (", ");
                        end if;
                        Start := False;

                        Err (-Ret);
                    end loop;

                    Err_Line (-The_Op.Description);

                    for Ret of The_Op.Return_Types loop
                        if Ops.Contains (Ret) then
                            Err_Line (" => " & (-Ops.Element (Ret).Name));
                            Err_Line (-Ops.Element (Ret).Description);
                        else
                            Err_Line (" => Unknown Type");
                            Err_Line ("This type is undocumented.");
                            Err_Line ("No information is available for it.");
                        end if;
                    end loop;

                    Err_Line ("");
                    Err_Line ("");
            end case;
        end;

        return This;
    end Debug;

    function Image (This : Datatype) return String is
        pragma Unreferenced (This);
    begin
        return "";
    end Image;

    function Type_Image (This : Datatype) return String is
        pragma Unreferenced (This);
    begin
        return "";
    end Type_Image;

    function Representation (This : Datatype'Class) return String is
    begin
        return "<" & This.Type_Image & ">: " & This.Image;
    end Representation;

    function Type_Image (This : Ada.Tags.Tag) return String is
        function Constructor is new Ada.Tags.Generic_Dispatching_Constructor (
            T => Datatype,
            Parameters => Nothing,
            Constructor => Create);
        Ignored : aliased Nothing;
    begin
        return Constructor (This, Ignored'Access).Type_Image;
    end Type_Image;
end Sbit;
