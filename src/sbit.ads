with Ada.Tags;
with Ada.Containers.Indefinite_Hashed_Maps;
with Ada.Containers.Indefinite_Hashed_Sets;
with Ada.Containers.Indefinite_Doubly_Linked_Lists;
with Ada.Containers.Indefinite_Vectors;
with Interaction; use Interaction;

package Sbit is
    type Code_Point is mod 96;

    Code_Page : constant array (Code_Point) of Character := (
        0  => '0', 1  => '1', 2  => '2', 3  => '3', 4  => '4', 5  => '5',
        6  => '6', 7  => '7', 8  => '8', 9  => '9', 10 => 'A', 11 => 'B',
        12 => 'C', 13 => 'D', 14 => 'E', 15 => 'F', 16 => 'G', 17 => 'H',
        18 => 'I', 19 => 'J', 20 => 'K', 21 => 'L', 22 => 'M', 23 => 'N',
        24 => 'O', 25 => 'P', 26 => 'Q', 27 => 'R', 28 => 'S', 29 => 'T',
        30 => 'U', 31 => 'V', 32 => 'W', 33 => 'X', 34 => 'Y', 35 => 'Z',
        36 => 'a', 37 => 'b', 38 => 'c', 39 => 'd', 40 => 'e', 41 => 'f',
        42 => 'g', 43 => 'h', 44 => 'i', 45 => 'j', 46 => 'k', 47 => 'l',
        48 => 'm', 49 => 'n', 50 => 'o', 51 => 'p', 52 => 'q', 53 => 'r',
        54 => 's', 55 => 't', 56 => 'u', 57 => 'v', 58 => 'w', 59 => 'x',
        60 => 'y', 61 => 'z', 62 => '+', 63 => '/', 64 => '-', 65 => '*',
        66 => '|', 67 => '^', 68 => '&', 69 => '%', 70 => '<', 71 => '>',
        72 => '(', 73 => ')', 74 => '[', 75 => ']', 76 => '{', 77 => '}',
        78 => '.', 79 => ',', 80 => ';', 81 => ':', 82 => ''', 83 => '"',
        84 => '`', 85 => '~', 86 => '!', 87 => '@', 88 => '#', 89 => '$',
        90 => '?', 91 => '_', 92 => '=', 93 => '\', 94 => ' ',
        95 => Character'Val (10));

    To_Code : constant array (Character) of Code_Point := (
        '0' => 0, '1' => 1, '2' => 2, '3' => 3, '4' => 4, '5' => 5,
        '6' => 6, '7' => 7, '8' => 8, '9' => 9, 'A' => 10, 'B' => 11,
        'C' => 12, 'D' => 13, 'E' => 14, 'F' => 15, 'G' => 16, 'H' => 17,
        'I' => 18, 'J' => 19, 'K' => 20, 'L' => 21, 'M' => 22, 'N' => 23,
        'O' => 24, 'P' => 25, 'Q' => 26, 'R' => 27, 'S' => 28, 'T' => 29,
        'U' => 30, 'V' => 31, 'W' => 32, 'X' => 33, 'Y' => 34, 'Z' => 35,
        'a' => 36, 'b' => 37, 'c' => 38, 'd' => 39, 'e' => 40, 'f' => 41,
        'g' => 42, 'h' => 43, 'i' => 44, 'j' => 45, 'k' => 46, 'l' => 47,
        'm' => 48, 'n' => 49, 'o' => 50, 'p' => 51, 'q' => 52, 'r' => 53,
        's' => 54, 't' => 55, 'u' => 56, 'v' => 57, 'w' => 58, 'x' => 59,
        'y' => 60, 'z' => 61, '+' => 62, '/' => 63, '-' => 64, '*' => 65,
        '|' => 66, '^' => 67, '&' => 68, '%' => 69, '<' => 70, '>' => 71,
        '(' => 72, ')' => 73, '[' => 74, ']' => 75, '{' => 76, '}' => 77,
        '.' => 78, ',' => 79, ';' => 80, ':' => 81, ''' => 82, '"' => 83,
        '`' => 84, '~' => 85, '!' => 86, '@' => 87, '#' => 88, '$' => 89,
        '?' => 90, '_' => 91, '=' => 92, '\' => 93, ' ' => 94,
        Character'Val (10) => 95, others => 0);

    type Code_Name is new String (1 .. 5);

    Canonical_Name : constant array (Code_Point) of Code_Name := (
        0  => "NUM_0", 1  => "NUM_1", 2  => "NUM_2", 3  => "NUM_3",
        4  => "NUM_4", 5  => "NUM_5", 6  => "NUM_6", 7  => "NUM_7",
        8  => "NUM_8", 9  => "NUM_9", 10 => "A_MAJ", 11 => "B_MAJ",
        12 => "C_MAJ", 13 => "D_MAJ", 14 => "E_MAJ", 15 => "F_MAJ",
        16 => "G_MAJ", 17 => "H_MAJ", 18 => "I_MAJ", 19 => "J_MAJ",
        20 => "K_MAJ", 21 => "L_MAJ", 22 => "M_MAJ", 23 => "N_MAJ",
        24 => "O_MAJ", 25 => "P_MAJ", 26 => "Q_MAJ", 27 => "R_MAJ",
        28 => "S_MAJ", 29 => "T_MAJ", 30 => "U_MAJ", 31 => "V_MAJ",
        32 => "W_MAJ", 33 => "X_MAJ", 34 => "Y_MAJ", 35 => "Z_MAJ",
        36 => "A_MIN", 37 => "B_MIN", 38 => "C_MIN", 39 => "D_MIN",
        40 => "E_MIN", 41 => "F_MIN", 42 => "G_MIN", 43 => "H_MIN",
        44 => "I_MIN", 45 => "J_MIN", 46 => "K_MIN", 47 => "L_MIN",
        48 => "M_MIN", 49 => "N_MIN", 50 => "O_MIN", 51 => "P_MIN",
        52 => "Q_MIN", 53 => "R_MIN", 54 => "S_MIN", 55 => "T_MIN",
        56 => "U_MIN", 57 => "V_MIN", 58 => "W_MIN", 59 => "X_MIN",
        60 => "Y_MIN", 61 => "Z_MIN", 62 => "ADDIT", 63 => "SLASH",
        64 => "MINUS", 65 => "TIMES", 66 => "VPIPE", 67 => "CARET",
        68 => "AMPER", 69 => "PRCNT", 70 => "LESST", 71 => "MORET",
        72 => "PAR_L", 73 => "PAR_R", 74 => "BRA_L", 75 => "BRA_R",
        76 => "CUR_L", 77 => "CUR_R", 78 => "O_DOT", 79 => "COMMA",
        80 => "SEMIC", 81 => "COLON", 82 => "APOST", 83 => "QUOTE",
        84 => "BTICK", 85 => "TILDE", 86 => "EXCLA", 87 => "ATSGN",
        88 => "POUND", 89 => "MONEY", 90 => "QUERY", 91 => "UNDER",
        92 => "EQUAL", 93 => "HSALS", 94 => "SPACE", 95 => "ENTER");

    type Listing is array (Positive range <>) of Code_Point;

    type Datatype is tagged null record;
    type Nothing is null record;

    -- A datum's hash is calculated from its representation
    function Data_Hash (Item : Datatype'Class) return Ada.Containers.Hash_Type;

    -- Data are equivalent if their representations are
    function Data_Equivalence (Left, Right : Datatype'Class) return Boolean;

    Not_Implemented : exception;

    Empty_Datum : constant Datatype;

    function Create (Ignore : not null access Nothing) return Datatype;

    -- Direct operators
    function NUM_0 (This : Datatype) return Datatype'Class;
    function NUM_0 (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function NUM_1 (This : Datatype) return Datatype'Class;
    function NUM_1 (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function NUM_2 (This : Datatype) return Datatype'Class;
    function NUM_2 (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function NUM_3 (This : Datatype) return Datatype'Class;
    function NUM_3 (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function NUM_4 (This : Datatype) return Datatype'Class;
    function NUM_4 (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function NUM_5 (This : Datatype) return Datatype'Class;
    function NUM_5 (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function NUM_6 (This : Datatype) return Datatype'Class;
    function NUM_6 (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function NUM_7 (This : Datatype) return Datatype'Class;
    function NUM_7 (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function NUM_8 (This : Datatype) return Datatype'Class;
    function NUM_8 (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function NUM_9 (This : Datatype) return Datatype'Class;
    function NUM_9 (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function A_MAJ (This : Datatype) return Datatype'Class;
    function A_MAJ (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function B_MAJ (This : Datatype) return Datatype'Class;
    function B_MAJ (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function C_MAJ (This : Datatype) return Datatype'Class;
    function C_MAJ (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function D_MAJ (This : Datatype) return Datatype'Class;
    function D_MAJ (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function E_MAJ (This : Datatype) return Datatype'Class;
    function E_MAJ (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function F_MAJ (This : Datatype) return Datatype'Class;
    function F_MAJ (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function G_MAJ (This : Datatype) return Datatype'Class;
    function G_MAJ (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function H_MAJ (This : Datatype) return Datatype'Class;
    function H_MAJ (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function I_MAJ (This : Datatype) return Datatype'Class;
    function I_MAJ (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function J_MAJ (This : Datatype) return Datatype'Class;
    function J_MAJ (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function K_MAJ (This : Datatype) return Datatype'Class;
    function K_MAJ (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function L_MAJ (This : Datatype) return Datatype'Class;
    function L_MAJ (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function M_MAJ (This : Datatype) return Datatype'Class;
    function M_MAJ (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function N_MAJ (This : Datatype) return Datatype'Class;
    function N_MAJ (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function O_MAJ (This : Datatype) return Datatype'Class;
    function O_MAJ (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function P_MAJ (This : Datatype) return Datatype'Class;
    function P_MAJ (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function Q_MAJ (This : Datatype) return Datatype'Class;
    function Q_MAJ (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function R_MAJ (This : Datatype) return Datatype'Class;
    function R_MAJ (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function S_MAJ (This : Datatype) return Datatype'Class;
    function S_MAJ (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function T_MAJ (This : Datatype) return Datatype'Class;
    function T_MAJ (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function U_MAJ (This : Datatype) return Datatype'Class;
    function U_MAJ (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function V_MAJ (This : Datatype) return Datatype'Class;
    function V_MAJ (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function W_MAJ (This : Datatype) return Datatype'Class;
    function W_MAJ (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function X_MAJ (This : Datatype) return Datatype'Class;
    function X_MAJ (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function Y_MAJ (This : Datatype) return Datatype'Class;
    function Y_MAJ (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function Z_MAJ (This : Datatype) return Datatype'Class;
    function Z_MAJ (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function A_MIN (This : Datatype) return Datatype'Class;
    function A_MIN (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function B_MIN (This : Datatype) return Datatype'Class;
    function B_MIN (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function C_MIN (This : Datatype) return Datatype'Class;
    function C_MIN (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function D_MIN (This : Datatype) return Datatype'Class;
    function D_MIN (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function E_MIN (This : Datatype) return Datatype'Class;
    function E_MIN (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function F_MIN (This : Datatype) return Datatype'Class;
    function F_MIN (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function G_MIN (This : Datatype) return Datatype'Class;
    function G_MIN (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function H_MIN (This : Datatype) return Datatype'Class;
    function H_MIN (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function I_MIN (This : Datatype) return Datatype'Class;
    function I_MIN (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function J_MIN (This : Datatype) return Datatype'Class;
    function J_MIN (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function K_MIN (This : Datatype) return Datatype'Class;
    function K_MIN (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function L_MIN (This : Datatype) return Datatype'Class;
    function L_MIN (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function M_MIN (This : Datatype) return Datatype'Class;
    function M_MIN (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function N_MIN (This : Datatype) return Datatype'Class;
    function N_MIN (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function O_MIN (This : Datatype) return Datatype'Class;
    function O_MIN (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function P_MIN (This : Datatype) return Datatype'Class;
    function P_MIN (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function Q_MIN (This : Datatype) return Datatype'Class;
    function Q_MIN (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function R_MIN (This : Datatype) return Datatype'Class;
    function R_MIN (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function S_MIN (This : Datatype) return Datatype'Class;
    function S_MIN (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function T_MIN (This : Datatype) return Datatype'Class;
    function T_MIN (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function U_MIN (This : Datatype) return Datatype'Class;
    function U_MIN (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function V_MIN (This : Datatype) return Datatype'Class;
    function V_MIN (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function W_MIN (This : Datatype) return Datatype'Class;
    function W_MIN (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function X_MIN (This : Datatype) return Datatype'Class;
    function X_MIN (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function Y_MIN (This : Datatype) return Datatype'Class;
    function Y_MIN (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function Z_MIN (This : Datatype) return Datatype'Class;
    function Z_MIN (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function ADDIT (This : Datatype) return Datatype'Class;
    function ADDIT (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function SLASH (This : Datatype) return Datatype'Class;
    function SLASH (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function MINUS (This : Datatype) return Datatype'Class;
    function MINUS (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function TIMES (This : Datatype) return Datatype'Class;
    function TIMES (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function VPIPE (This : Datatype) return Datatype'Class;
    function VPIPE (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function CARET (This : Datatype) return Datatype'Class;
    function CARET (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function AMPER (This : Datatype) return Datatype'Class;
    function AMPER (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function PRCNT (This : Datatype) return Datatype'Class;
    function PRCNT (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function LESST (This : Datatype) return Datatype'Class;
    function LESST (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function MORET (This : Datatype) return Datatype'Class;
    function MORET (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function PAR_L (This : Datatype) return Datatype'Class;
    function PAR_L (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function PAR_R (This : Datatype) return Datatype'Class;
    function PAR_R (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function BRA_L (This : Datatype) return Datatype'Class;
    function BRA_L (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function BRA_R (This : Datatype) return Datatype'Class;
    function BRA_R (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function CUR_L (This : Datatype) return Datatype'Class;
    function CUR_L (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function CUR_R (This : Datatype) return Datatype'Class;
    function CUR_R (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function O_DOT (This : Datatype) return Datatype'Class;
    function O_DOT (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function COMMA (This : Datatype) return Datatype'Class;
    function COMMA (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function SEMIC (This : Datatype) return Datatype'Class;
    function SEMIC (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function COLON (This : Datatype) return Datatype'Class;
    function COLON (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function APOST (This : Datatype) return Datatype'Class;
    function APOST (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function QUOTE (This : Datatype) return Datatype'Class;
    function QUOTE (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function BTICK (This : Datatype) return Datatype'Class;
    function BTICK (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function TILDE (This : Datatype) return Datatype'Class;
    function TILDE (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function EXCLA (This : Datatype) return Datatype'Class;
    function EXCLA (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function ATSGN (This : Datatype) return Datatype'Class;
    function ATSGN (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function POUND (This : Datatype) return Datatype'Class;
    function POUND (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function MONEY (This : Datatype) return Datatype'Class;
    function MONEY (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function QUERY (This : Datatype) return Datatype'Class;
    function QUERY (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function UNDER (This : Datatype) return Datatype'Class;
    function UNDER (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function EQUAL (This : Datatype) return Datatype'Class;
    function EQUAL (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;
    function HSALS (This : Datatype) return Datatype'Class;
    function HSALS (
        This : Datatype;
        Block : access function (This : Datatype'Class) return Datatype'Class)
        return Datatype'Class;

    function Read (From : not null access Inputter) return Datatype;
    procedure Write (This : Datatype; To : in out Outputter);

    -- Debugging and utility functions
    type Debug_Verbosity is (
        Low,    -- val.OP_OF[block] =>
        Medium, -- val.OP_OF[block] (Operator Name) => (Type Name, Other Type)
        High);  -- val.OP_OF[block] Operator Name: SrcRep -> DstRep
                --     Operator description spanning
                --     multiple lines.
                -- => Type Name
                --     Type description spanning
                --     multiple lines.
                -- => Other Type
                --     Type description spanning
                --     multiple lines.

    -- Prints information to stderr, returning the input type
    -- If Block is null then the operator isn't taking a block
    function Debug (
        This : Datatype'Class;
        Op : Code_Point;
        Verbose : Debug_Verbosity;
        Block : access function (This : Datatype'Class) return Datatype'Class
            := null) return Sbit.Datatype'Class;

    function Image (This : Datatype) return String;

    -- Must return the same value used in documentation/operations
    function Type_Image (This : Datatype) return String;

    function Representation (This : Datatype'Class) return String;
    function Type_Image (This : Ada.Tags.Tag) return String;

    package Data_Maps is new Ada.Containers.Indefinite_Hashed_Maps (
        Key_Type => Datatype'Class,
        Element_Type => Datatype'Class,
        Hash => Data_Hash,
        Equivalent_Keys => Data_Equivalence);

    package Data_Sets is new Ada.Containers.Indefinite_Hashed_Sets (
        Element_Type => Datatype'Class,
        Hash => Data_Hash,
        Equivalent_Elements => Data_Equivalence);

    package Data_Vectors is new Ada.Containers.Indefinite_Vectors (
        Index_Type => Positive,
        Element_Type => Datatype'Class);

    package Data_Lists is new Ada.Containers.Indefinite_Doubly_Linked_Lists (
        Element_Type => Datatype'Class);

    type Collection is abstract new Datatype with null record;
    function Elements (From : Collection) return Data_Lists.List is abstract;
private
    Empty_Datum : constant Datatype := (null record);
end Sbit;
