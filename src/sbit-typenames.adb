with Ada.Strings.Unbounded;
with Ada.Strings.Maps;
with Opinfo; use Opinfo;

package body Sbit.Typenames is
    function "-" (Right : Ada.Strings.Unbounded.Unbounded_String) return String
        renames Ada.Strings.Unbounded.To_String;

    Plural_To_Package : constant Ada.Strings.Maps.Character_Mapping :=
        Ada.Strings.Maps.To_Mapping (
            From => " ",
            To => "_");

    function To_Nearest_Package (Code : Listing) return String
    is
        Distance : Natural := Natural'Last;
        Best : Raw_Text := +"Empties";
        Order : Natural := Natural'Last;

        function To_Listing (From : String) return Listing is
            Empty_Listing : constant Listing (1 .. 0) := (others => 0);
        begin
            if From'Length = 0 then
                return Empty_Listing;
            else
                return To_Code (From (From'First)) &
                    To_Listing (From (From'First + 1 .. From'Last));
            end if;
        end To_Listing;

        function DL_Distance (A, B : Listing) return Natural is
            Dists : array (0 .. A'Length, 0 .. B'Length) of Natural := (
                others => (others => Natural'Last));
        begin
            Dists (0, 0) := 0;

            for I in Natural range 0 .. A'Length loop
                for J in Natural range 0 .. B'Length loop
                    if I > 0 then
                        Dists (I, J) := Natural'Min (Dists (I, J),
                            Dists (I - 1, J) + 1);
                    end if;

                    if J > 0 then
                        Dists (I, J) := Natural'Min (Dists (I, J),
                            Dists (I, J - 1) + 1);
                    end if;

                    if I > 0 and J > 0 then
                        if A (A'First + I - 1) = B (B'First + J - 1) then
                            Dists (I, J) := Natural'Min (Dists (I, J),
                                Dists (I - 1, J - 1));
                        else
                            Dists (I, J) := Natural'Min (Dists (I, J),
                                Dists (I - 1, J - 1) + 1);
                        end if;
                    end if;

                    if (I > 1 and J > 1) and then
                        (A (A'First + I - 1) = B (B'First + J - 2) and
                         A (A'First + I - 2) = B (B'First + J - 1))
                    then
                        Dists (I, J) := Natural'Min (Dists (I, J),
                            Dists (I - 2, J - 2) + 1);
                    end if;
                end loop;
            end loop;

            return Dists (A'Length, B'Length);
        end DL_Distance;

        procedure Do_Check (On : Sbit_Type_Maps.Cursor) is
            Type_Repr : constant Sbit_Representation := Sbit_Type_Maps.Key (On);
            Type_List : constant Listing := To_Listing (-Type_Repr);
            New_Distance : constant Natural := DL_Distance (Code, Type_List);
            Elem : constant Sbit_Type := Sbit_Type_Maps.Element (On);
        begin
            if New_Distance < Distance or (
                New_Distance = Distance and Elem.Order < Order)
            then
                Distance := New_Distance;
                Best := Elem.Plural;
                Order := Elem.Order;
            end if;
        end Do_Check;
    begin
        Opinfo.Get_Operations.Iterate (Do_Check'Access);

        Ada.Strings.Unbounded.Translate (Best, Plural_To_Package);

        return Ada.Strings.Unbounded.To_String (Best);
    end To_Nearest_Package;
end Sbit.Typenames;
