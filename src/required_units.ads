pragma Warnings (Off);

with Sbit.Blockcats; pragma Unreferenced (Sbit.Blockcats);
with Sbit.BNFs; pragma Unreferenced (Sbit.BNFs);
with Sbit.Boolean_Lists; pragma Unreferenced (Sbit.Boolean_Lists);
with Sbit.Booleans; pragma Unreferenced (Sbit.Booleans);
with Sbit.Empties; pragma Unreferenced (Sbit.Empties);
with Sbit.Integer_Lists; pragma Unreferenced (Sbit.Integer_Lists);
with Sbit.Integer_Pairs; pragma Unreferenced (Sbit.Integer_Pairs);
with Sbit.Integers; pragma Unreferenced (Sbit.Integers);
with Sbit.Lists; pragma Unreferenced (Sbit.Lists);
with Sbit.Literals; pragma Unreferenced (Sbit.Literals);
with Sbit.Natural_Lists; pragma Unreferenced (Sbit.Natural_Lists);
with Sbit.Naturals; pragma Unreferenced (Sbit.Naturals);
with Sbit.Radigits; pragma Unreferenced (Sbit.Radigits);
with Sbit.Rationals; pragma Unreferenced (Sbit.Rationals);
with Sbit.Sets; pragma Unreferenced (Sbit.Sets);
with Sbit.Strings; pragma Unreferenced (Sbit.Strings);
with Sbit.Tuples; pragma Unreferenced (Sbit.Tuples);

package Required_Units is
    procedure Keep;
end Required_Units;
pragma Warnings (On);
