with Sbit;
with Sbit.Compiling;
with Sbit.Resources;
with Ada.Text_IO;
with Ada.Command_Line;

procedure Compiler is
    Main_Name : constant String := Ada.Command_Line.Argument (1);
    Type_Name : constant String := Ada.Command_Line.Argument (2);
    Debug_Mode : constant String := Ada.Command_Line.Argument (3);
    Seed_Text : constant String := Ada.Command_Line.Argument (4);
    Share_Dir : constant String := Ada.Command_Line.Argument (5);
    Type_List : Sbit.Listing (Type_Name'Range);

    function Read_All_Input return Sbit.Listing is
        Empty : Sbit.Listing (1 .. 0);
        use type Sbit.Listing;
    begin
        if Ada.Text_IO.End_Of_File then
            return Empty;
        else
            declare
                Line : constant String := Ada.Text_IO.Get_Line;
                Result : Sbit.Listing (Line'Range) := (others => 0);
            begin
                for I in Line'Range loop
                    if Line (I) = Character'Val (9) then
                        return Result (Result'First .. I - 1) & Read_All_Input;
                    end if;

                    Result (I) := Sbit.To_Code (Line (I));
                end loop;

                return Result & 95 & Read_All_Input;
            end;
        end if;
    end Read_All_Input;

    Input : constant Sbit.Listing := Read_All_Input;
begin
    Sbit.Resources.Initialize (Share_Dir);

    for I in Type_Name'Range loop
        Type_List (I) := Sbit.To_Code (Type_Name (I));
    end loop;

    Ada.Text_IO.Put_Line (Sbit.Compiling.Compile (
        From => Input,
        Share => Share_Dir,
        Start => Type_List,
        Main => Main_Name,
        Debug => Sbit.Compiling.Debug_Depth'Value (Debug_Mode),
        Seed => (if Seed_Text = "x" then "" else "(" & Seed_Text & ")")));
end Compiler;
