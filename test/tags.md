The tags used by 5b1t are as follows:

 - `net`: for tests that make use of networking
 - `pcg`: for tests that implement a programming code golf submission