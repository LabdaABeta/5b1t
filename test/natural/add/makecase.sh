#!/bin/bash

NAME=$1
VALUE=$2
CODE=$(../../../tools/to_literal $VALUE)

mkdir $NAME
echo "+$CODE" > $NAME/test.sbit
5b1t -e $NAME/test.sbit > $NAME/test
touch $NAME/tags
echo -n 'n' > $NAME/type
mkdir $NAME/cases
echo '0' > $NAME/cases/zero.in
echo "$VALUE" > $NAME/cases/zero.out
