# 5b1t Test Format

A test is any directory found within this directory containing the following:

 - test.sbit: The source code of a 5b1t program
 - test: The expected encoded version of test.sbit
 - type: A file containing *just* the type to be used as input
 - tags: A file listing tags applicable to the test
 - cases/: A directory containing test cases

## Test Source Format

The source code supports TAB comments as usual, these should be used where
applicable to retain legibility.

## Test Binary Format

The binary should match byte-for-byte with the compiled source, if it does not
all test cases in the suite fail.

## Type Format

The type file's contents are passed directly as the value of the `-i` switch to
`5b1t`. As such it should not have a newline unless the type itself has
a newline.

## Tags format

The tags file is a list of whitespace-separated tags.

The tags used by 5b1t are as follows:

 - `net`: for tests that make use of networking
 - `pcg`: for tests that implement a programming code golf submission

## Cases Format

The following test case formats are supported.

### Input/Output

Input/Output cases have two files with the same name. One has the `.in`
extension while the other has the `.out` extension.

The input file is given to the compiled executable as input and the output is
compared against the output file. If the contents match exactly then the test
passes.

### Failure

Failure cases have the extension `.bad`. The test case is given to the compiled
executable as input and the return code is expected to be non-zero indicating
a failure.

### Input/Check

Input/Check cases have two files with the same name. One has the `.in` extension
while the other has the `.ch` extension and must be executable. The check file
is typically a bash script.

The input file is given to the compiled executable as input and the output is
piped to the input of the check executable. If the check executable returns
a non-zero status then the test case fails.
