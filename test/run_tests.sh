#!/bin/bash

# run_tests.sh 5b1t < [test_name]*
#  - Runs the test(s) in the test_name(s) directory
#  - Can pass additional arguments to the 5b1t executable
#
# Each directory should contain:
#  test.sbit    The ascii-coded source code
#  test         The 5b1t encoded source code
#  type         The ascii-coded typename to use as input
#  input        The input file
#  output       The exected output
#  tags         The relevant test tags
#
# Tests are filtered based on matching the tags file against the environment variables:
#   GREP_5B1T_TEST_FILTER - Matching files are not run
#   GREP_5B1T_TEST_SELECT - Matching files are run
#
# The default values of these environment variables are such that every test will run:
#   GREP_5B1T_TEST_FILTER="$a"
#   GREP_5B1T_TEST_SELECT=""
#
# A log is written by default to log.txt. This can be overwritten with the
# environment variable: FILE_5B1T_TEST_LOG
#
# Results are written by default to results.json. This can be overwritten with
# the environment variable: FILE_5B1T_TEST_RESULTS

SBIT_EXE="$*"

LOG_FILE=${FILE_5B1T_TEST_LOG-log.txt}
RESULTS_FILE=${FILE_5B1T_TEST_RESULTS-results.json}

shopt -s nullglob

# Colouring
TERM_RED=$(tput setaf 1)
TERM_GREEN=$(tput setaf 2)
TERM_YELLOW=$(tput setaf 3)
TERM_CYAN=$(tput setaf 6)
TERM_UNDER=$(tput smul)
TERM_NO_UNDER=$(tput rmul)
TERM_NO_COL=$(echo -e '\x1b[39m')

TEST_5B1T=
TEST_OUTPUT=
TEST_EXE=
ENC_DIFF=
RUN_DIFF=

# Counters for problems
declare -i T_SKIP
declare -i E_PASS
declare -i E_FAIL
declare -i C_PASS
declare -i C_FAIL
declare -i R_PASS
declare -i R_FAIL
declare -i R_LEAK
declare -i CNT_COMP
declare -i CNT_RUN

T_SKIP=0
E_PASS=0
E_FAIL=0
C_PASS=0
C_FAIL=0
R_PASS=0
R_FAIL=0
R_LEAK=0

MIN_COMP=1600000000
TOT_COMP=0
CNT_COMP=0
MAX_COMP=0

MIN_RUN=1600000000
TOT_RUN=0
CNT_RUN=0
MAX_RUN=0

json_a() {
    jq -cMaS "$@" "$RESULTS_FILE" | sponge "$RESULTS_FILE"
}

run_test() {
    TEST_NAME=$1

    echo "Running test $1..." >>"$LOG_FILE"
    json_a '.tests |= . + [{}]'
    json_a --arg name "${TEST_NAME#test/}" '.tests[-1].name |= $name'

    if [ ! -d "$TEST_NAME" ]; then
        echo "Skipping non-existant ${TEST_NAME#test/}." | tee -a "$LOG_FILE"
        json_a '.tests[-1].skipped |= true'
        json_a '.tests[-1].reason |= "missing"'
        return
    fi

    if [ ! -e "$TEST_NAME/tags" ]; then
        echo "Generating empty tags for ${TEST_NAME#test/}..." >> "$LOG_FILE"
        echo > "$TEST_NAME/tags"
    fi

    if grep -v -q "${GREP_5B1T_TEST_SELECT-}" < "$TEST_NAME/tags"; then
        T_SKIP+=1
        echo "${TEST_NAME#test/} ${TERM_CYAN}SKIP${TERM_NO_COL}"
        echo "Skipping unselected ${TEST_NAME#test/}." >> "$LOG_FILE"
        json_a '.tests[-1].skipped |= true'
        json_a '.tests[-1].reason |= "unselected"'
        return
    fi

    if grep -q "${GREP_5B1T_TEST_FILTER-\$a}" < "$TEST_NAME/tags"; then
        T_SKIP+=1
        echo "${TEST_NAME#test/} ${TERM_CYAN}SKIP${TERM_NO_COL}"
        echo "Skipping filtered ${TEST_NAME#test/}." >> "$LOG_FILE"
        json_a '.tests[-1].skipped |= true'
        json_a '.tests[-1].reason |= "filtered"'
        return
    fi

    TEST_5B1T=$(mktemp sXXXXXX.bt)
    TEST_OUTPUT=$(mktemp oXXXXXX.out)
    TEST_EXE=$(mktemp rXXXXXX.exe)
    HAS_FAIL=false

    echo "${TEST_NAME#test/}"

    echo "Encoding..." >> "$LOG_FILE"
    $SBIT_EXE -e "$TEST_NAME/test.sbit" > "$TEST_5B1T" 2>> "$LOG_FILE"

    echo "Expected:" >> "$LOG_FILE"
    xxd < "$TEST_NAME/test" >> "$LOG_FILE"
    echo "Got:" >> "$LOG_FILE"
    xxd < "$TEST_5B1T" >> "$LOG_FILE"

    echo -n " ENCODING                     "

    if diff "$TEST_NAME/test" "$TEST_5B1T" >/dev/null; then
        E_PASS+=1
        echo "${TERM_GREEN}PASS${TERM_NO_COL}"
        echo "ENCODING PASSED" >> "$LOG_FILE"
        json_a '.tests[-1].encoding |= "PASS"'
    else
        E_FAIL+=1
        HAS_FAIL=true
        echo "${TERM_RED}FAIL${TERM_NO_COL}  "
        echo "ENCODING FAILED" >> "$LOG_FILE"
        json_a '.tests[-1].encoding |= "FAIL"'
    fi

    echo -n " COMPILING                    "

    TYPE="$(cat "$TEST_NAME/type")"

    echo "Compiling..." >> "$LOG_FILE"

    BEFORE="$(date +%s.%N)"
    $SBIT_EXE -i "$TYPE" "$TEST_NAME/test" -c -o "$TEST_EXE" 1>>"$LOG_FILE" 2>&1
    STATUS=$?
    AFTER="$(date +%s.%N)"

    COMP_TIME=$(echo "$AFTER-$BEFORE" | bc)

    if [ "$STATUS" -eq '0' ]; then
        C_PASS+=1

        [ "$(echo "$COMP_TIME > $MAX_COMP" | bc)" -eq '1' ] && MAX_COMP=$COMP_TIME
        [ "$(echo "$COMP_TIME < $MIN_COMP" | bc)" -eq '1' ] && MIN_COMP=$COMP_TIME

        TOT_COMP=$(echo "$TOT_COMP + $COMP_TIME" | bc)
        CNT_COMP+=1

        echo "${TERM_GREEN}PASS${TERM_NO_COL} ($(printf %f "$COMP_TIME"))"
        echo "COMPILING PASSED" >> "$LOG_FILE"
        json_a '.tests[-1].compiling |= "PASS"'
        json_a --arg time "$(printf %f "$COMP_TIME")" '.tests[-1].compiled_in |= $time'
    else
        C_FAIL+=1
        HAS_FAIL=true
        echo "${TERM_RED}FAIL${TERM_NO_COL} ($(printf %f "$COMP_TIME"))"
        echo "COMPILING FAILED" >> "$LOG_FILE"
        json_a '.tests[-1].compiling |= "FAIL"'
        json_a --arg time "$(printf %f "$COMP_TIME")" '.tests[-1].compiled_in |= $time'
    fi

    if ! $HAS_FAIL; then
        echo "TEST CASES" >> "$LOG_FILE"
        json_a '.tests[-1].cases |= []'

        for tc in $TEST_NAME/cases/*.in; do
            TCNAME=$(basename "$tc")
            printf "%-30s" " - ${TCNAME%.in}"
            echo "Case $TCNAME:" >> "$LOG_FILE"
            json_a '.tests[-1].cases |= . + [{}]'
            json_a --arg name "${TCNAME%.in}" '.tests[-1].cases[-1].name |= $name'

            BEFORE="$(date +%s.%N)"
            "./$TEST_EXE" < "$tc" 2>>"$LOG_FILE" >"$TEST_OUTPUT"
            STATUS=$?
            AFTER="$(date +%s.%N)"

            RUN_TIME=$(echo "$AFTER-$BEFORE" | bc)

            echo "Result:" >> "$LOG_FILE"
            cat "$TEST_OUTPUT" >> "$LOG_FILE"

            if [ -f "${tc%.in}.out" ]; then
                echo "Expected:" >> "$LOG_FILE"
                cat "${tc%.in}.out" >> "$LOG_FILE"
                diff "${tc%.in}.out" "$TEST_OUTPUT" >/dev/null
                STATUS=$?
            elif [ -x "${tc%.in}.ch" ]; then
                echo "Verifying with:" >> "$LOG_FILE"
                cat "${tc%in}.ch" >> "$LOG_FILE"
                echo "Verifying..." >> "$LOG_FILE"
                "${tc%in}.ch" < "$TEST_OUTPUT" >>"$LOG_FILE" 2>&1
                STATUS=$?
            fi

            if [ "$STATUS" -eq "0" ]; then
                echo "Success, checking for leaks..." >>"$LOG_FILE"
                if valgrind -q --error-exitcode=1 "./$TEST_EXE" < "$tc" >>"$LOG_FILE" 2>&1; then
                    R_PASS+=1

                    [ "$(echo "$RUN_TIME > $MAX_RUN" | bc)" -eq '1' ] && MAX_RUN=$RUN_TIME
                    [ "$(echo "$RUN_TIME < $MIN_RUN" | bc)" -eq '1' ] && MIN_RUN=$RUN_TIME
                    TOT_RUN=$(echo "$TOT_RUN + $RUN_TIME" | bc)
                    CNT_RUN+=1

                    echo -n "${TERM_GREEN}PASS${TERM_NO_COL}"
                    echo "NO LEAKS FOUND" >> "$LOG_FILE"
                    json_a '.tests[-1].cases[-1].status |= "PASS"'
                else
                    R_LEAK+=1
                    echo -n "${TERM_YELLOW}LEAK${TERM_NO_COL}"
                    echo "LEAK FOUND" >> "$LOG_FILE"
                    json_a '.tests[-1].cases[-1].status |= "LEAK"'
                fi
            else
                R_FAIL+=1
                HAS_FAIL=true
                echo -n "${TERM_RED}FAIL${TERM_NO_COL}"
                echo "Failure" >> "$LOG_FILE"
                json_a '.tests[-1].cases[-1].status |= "FAIL"'
            fi

            echo " ($(printf %f "$RUN_TIME"))"
            json_a --arg time "$(printf %f "$RUN_TIME")" '.tests[-1].cases[-1].time |= $time'
        done

        for tc in $TEST_NAME/cases/*.bad; do
            TCNAME=$(basename "$tc")
            printf "%-30s" " - ${TCNAME%.bad}"
            echo "Fail Case $TCNAME:" >> "$LOG_FILE"
            json_a '.tests[-1].cases |= . + [{}]'
            json_a --arg name "${TCNAME%.bad}" '.tests[-1].cases[-1].name |= $name'

            BEFORE="$(date +%s.%N)"
            "./$TEST_EXE" < "$tc" 2>>"$LOG_FILE" >"$TEST_OUTPUT"
            STATUS=$?
            AFTER="$(date +%s.%N)"

            if [ "$STATUS" -eq "0" ]; then
                R_FAIL+=1
                HAS_FAIL=true
                echo "${TERM_RED}FAIL${TERM_NO_COL} "

                echo "SHOULD HAVE FAILED!" >> "$LOG_FILE"
                json_a '.tests[-1].cases[-1].status |= "FAIL"'
            else
                R_PASS+=1
                echo "${TERM_GREEN}PASS${TERM_NO_COL} "

                echo "PASSED" >>  "$LOG_FILE"
                json_a '.tests[-1].cases[-1].status |= "PASS"'
            fi

            echo "($(printf %f "$RUN_TIME"))"
            json_a --arg time "$(printf %f "$RUN_TIME")" '.tests[-1].cases[-1].time |= $time'
        done
    fi

    if $HAS_FAIL; then
        echo " Result                       ${TERM_RED}FAIL${TERM_NO_COL}"
        json_a '.tests[-1].result |= "FAIL"'
    else
        echo " Result                       ${TERM_GREEN}PASS${TERM_NO_COL}"
        json_a '.tests[-1].result |= "PASS"'
    fi

    rm "$TEST_5B1T" "$TEST_OUTPUT" "$TEST_EXE"
    TEST_5B1T=
    TEST_OUTPUT=
    TEST_EXE=
    ENC_DIFF=
    RUN_DIFF=
}

# Print setup information
echo "LOG: $LOG_FILE  RESULTS: $RESULTS_FILE" | tee "$LOG_FILE"
echo "Run date: $(date)" >>"$LOG_FILE"
echo "{}" > "$RESULTS_FILE"
json_a --arg date "$(date)" '.date |= $date'

# Delete temp files on exit
trap 'rm -rf $TEST_5B1T $TEST_OUTPUT $TEST_EXE $ENC_DIFF $RUN_DIFF' EXIT

# Print filter information
echo "SELECT: ${GREP_5B1T_TEST_SELECT-all}  FILTER: ${GREP_5B1T_TEST_FILTER-none}" | tee -a "$LOG_FILE"
json_a --arg select "${GREP_5B1T_TEST_SELECT-all}" '.select |= $select'
json_a --arg filter "${GREP_5B1T_TEST_FILTER-none}" '.filter |= $filter'

# Prepare for test data
json_a '.tests |= []'

while read -r t; do
    run_test "$t"
done

if [ "$CNT_COMP" -eq '0' ]; then
    CNT_COMP=1
fi

if [ "$CNT_RUN" -eq '0' ]; then
    CNT_RUN=1
fi

MIN_COMP_SEC=$(echo "scale=0;$MIN_COMP * 1000/1" | bc)
AVG_COMP_SEC=$(echo "scale=0;($TOT_COMP * 1000)/$CNT_COMP" | bc)
MAX_COMP_SEC=$(echo "scale=0;$MAX_COMP * 1000/1" | bc)
TOT_COMP_SEC=$(echo "scale=0;$TOT_COMP * 1000/1" | bc)
MIN_RUN_SEC=$(echo "scale=0;$MIN_RUN * 1000/1" | bc)
AVG_RUN_SEC=$(echo "scale=0;($TOT_RUN * 1000)/$CNT_RUN" | bc)
MAX_RUN_SEC=$(echo "scale=0;$MAX_RUN * 1000/1" | bc)
TOT_RUN_SEC=$(echo "scale=0;$TOT_RUN * 1000/1" | bc)

echo
echo "ENCODING:  ${TERM_GREEN}$E_PASS${TERM_NO_COL} PASS  ${TERM_RED}$E_FAIL${TERM_NO_COL} FAIL"
echo "COMPILING: ${TERM_GREEN}$C_PASS${TERM_NO_COL} PASS  ${TERM_RED}$C_FAIL${TERM_NO_COL} FAIL"
echo "RUNNING:   ${TERM_GREEN}$R_PASS${TERM_NO_COL} PASS  ${TERM_RED}$R_FAIL${TERM_NO_COL} FAIL  ${TERM_YELLOW}$R_LEAK${TERM_NO_COL} LEAK"
echo "SKIPPED:   ${TERM_CYAN}$T_SKIP${TERM_NO_COL}"
echo
echo "${TERM_UNDER} Timing  |    MEAN    |    MAX     |    MIN     |   TOTAL    ${TERM_NO_UNDER}"
echo "Compiling|\
$(printf '%02d:%02d:%02d.%03d' $((AVG_COMP_SEC/3600000)) $((AVG_COMP_SEC%3600000/60000)) $((AVG_COMP_SEC%60000/1000)) $((AVG_COMP_SEC%1000)))|\
$(printf '%02d:%02d:%02d.%03d' $((MAX_COMP_SEC/3600000)) $((MAX_COMP_SEC%3600000/60000)) $((MAX_COMP_SEC%60000/1000)) $((MAX_COMP_SEC%1000)))|\
$(printf '%02d:%02d:%02d.%03d' $((MIN_COMP_SEC/3600000)) $((MIN_COMP_SEC%3600000/60000)) $((MIN_COMP_SEC%60000/1000)) $((MIN_COMP_SEC%1000)))|\
$(printf '%02d:%02d:%02d.%03d' $((TOT_COMP_SEC/3600000)) $((TOT_COMP_SEC%3600000/60000)) $((TOT_COMP_SEC%60000/1000)) $((TOT_COMP_SEC%1000)))"
echo " Running |\
$(printf '%02d:%02d:%02d.%03d' $((AVG_RUN_SEC/3600000)) $((AVG_RUN_SEC%3600000/60000)) $((AVG_RUN_SEC%60000/1000)) $((AVG_RUN_SEC%1000)))|\
$(printf '%02d:%02d:%02d.%03d' $((MAX_RUN_SEC/3600000)) $((MAX_RUN_SEC%3600000/60000)) $((MAX_RUN_SEC%60000/1000)) $((MAX_RUN_SEC%1000)))|\
$(printf '%02d:%02d:%02d.%03d' $((MIN_RUN_SEC/3600000)) $((MIN_RUN_SEC%3600000/60000)) $((MIN_RUN_SEC%60000/1000)) $((MIN_RUN_SEC%1000)))|\
$(printf '%02d:%02d:%02d.%03d' $((TOT_RUN_SEC/3600000)) $((TOT_RUN_SEC%3600000/60000)) $((TOT_RUN_SEC%60000/1000)) $((TOT_RUN_SEC%1000)))"

json_a '.results |= {}'
json_a '.results.compile |= {}'
json_a '.results.run |= {}'
json_a '.results.encoding |= {}'
json_a '.results.compile.time |= {}'
json_a '.results.run.time |= {}'
json_a ".results.compile.time.min |= $MIN_COMP_SEC"
json_a ".results.compile.time.max |= $MAX_COMP_SEC"
json_a ".results.compile.time.avg |= $AVG_COMP_SEC"
json_a ".results.compile.time.total |= $TOT_COMP_SEC"
json_a ".results.compile.pass |= $C_PASS"
json_a ".results.compile.fail |= $C_FAIL"
json_a ".results.run.time.min |= $MIN_RUN_SEC"
json_a ".results.run.time.max |= $MAX_RUN_SEC"
json_a ".results.run.time.avg |= $AVG_RUN_SEC"
json_a ".results.run.time.total |= $TOT_RUN_SEC"
json_a ".results.run.pass |= $R_PASS"
json_a ".results.run.fail |= $R_FAIL"
json_a ".results.run.leak |= $R_LEAK"
json_a ".results.encoding.pass |= $E_PASS"
json_a ".results.encoding.fail |= $E_FAIL"

if [ $((E_FAIL+C_FAIL+R_FAIL)) -eq 0 ]; then
    json_a '.result |= "FAIL"'
else
    json_a '.result |= "PASS"'
fi

[ $((E_FAIL+C_FAIL+R_FAIL)) -eq 0 ]
