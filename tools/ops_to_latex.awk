#!/usr/bin/gawk -f

@include "tools/parse_ops.awk"

function mode_to_command(mode) {
    if (mode == "?") return "\\unimplemented"
    if (mode == "!") return "\\obsoleted"
    if (mode == ".") return "\\partially"
    return ""
}

function labelize(lbl) {
    out = "op:"
    rep = repr_of(lbl)
    opr = op_of(lbl)
    for (i = 1; i <= length(rep); ++i) {
        char = substr(rep,i,1)
        if (char in opreprs)
            out = out opreprs[char]
        else
            out = out char
    }
    out = out "-" opreprs[opr]

    if (has_block(lbl))
        out = out "+"
    return out
}

function reprize(str) {
    out = ""
    for (i = 1; i <= length(str); ++i) {
        char = substr(str,i,1)
        if (char == "#")
            out = out "\\#"
        else if (char == "$")
            out = out "\\$"
        else if (char == "%")
            out = out "\\%"
        else if (char == "&")
            out = out "\\&"
        else if (char == "\\")
            out = out "\\textbackslash{}"
        else if (char == "^")
            out = out "\\textasciicircum{}"
        else if (char == "_")
            out = out "\\_"
        else if (char == "{")
            out = out "\\{"
        else if (char == "}")
            out = out "\\}"
        else if (char == "~")
            out = out "\\textasciitilde{}"
        else
            out = out char
    }
    return out
}

function add_op(repr, opr, arg) {
    opname = repr ":" opr arg
    printf "%s \\refstepcounter{operatorcounter} \\label{%s} ", mode_to_command(opmode[opname]), labelize(opname)

    if (opsource[opname] != repr) printf "\\inherited "

    printf "\\sbit{%s} ", opreprs[opr]

    if (arg != "") printf "\\blockArgument{} "

    if (opliteral[opname]) printf "\\literal{} "

    ttype = optypes[opname]
    gsub(/𝔸/, "\\circled{A}", ttype)
    gsub(/𝔹/, "\\circled{B}", ttype)
    gsub(/ℂ/, "\\circled{C}", ttype)
    gsub(/𝟙/, "\\circled{1}", ttype)
    gsub(/𝟚/, "\\circled{1}", ttype)
    gsub(/𝟛/, "\\circled{1}", ttype)
    gsub(/⊙/, "\\circled{*}", ttype)
    gsub(/∪/, "$\\lor{}$", ttype)

    printf "& "

    if (opsource[opname] != repr) printf "\\inherited "

    printf "%s & ", ttype

    if (opsource[opname] != repr) printf "\\inherited "

    printf "%s: ", opnames[opname]

    if (opsource[opname] != repr) {
        odesc = "Inherited from " names[opsource[opname]] " on page \\pageref{" labelize(opsource[opname] ":" opr arg) "}."
    } else {
        odesc = opdescs[opname]

        gsub(/𝔸/, "\\circled{A}", odesc)
        gsub(/𝔹/, "\\circled{B}", odesc)
        gsub(/ℂ/, "\\circled{C}", odesc)
        gsub(/𝟙/, "\\circled{1}", odesc)
        gsub(/𝟚/, "\\circled{1}", odesc)
        gsub(/𝟛/, "\\circled{1}", odesc)
        gsub(/⊙/, "\\circled{*}", odesc)
        gsub(/λ/, "\\literal{}", odesc)
        gsub(/Λ/, "\\blockArgument{}", odesc)
    }

    printf "%s \\\\\n", odesc
}

END {
    for (typeind = 0; typeind < num_types; ++typeind) {
        repr = order[typeind]
        printf "\\settype"
        if (parents[repr] != "") printf "[%s]", names[parents[repr]]
        printf "{%s}{%s}\n", names[repr], reprize(repr)

        printf "%s\n\n", descriptions[repr]

        printf "\\begin{typetab}\n"

        for (op in opreprs) {
            if (repr ":" op in optypes) {
                add_op(repr, op, "")
            }

            if (repr ":" op "Λ" in optypes) {
                add_op(repr, op, "Λ")
            }
        }

        printf "\\end{typetab}\n\n"
    }
}
