#!/bin/bash

DBFILE=$1

join -t- -j1 -e 'UNIMPLEMENTED' -a 1 -a 2 \
    <(./tools/ls_defined.awk < share/operations | sort) \
    <(./tools/ls_implemented.sh "$DBFILE" | sort) | \
    sed 's/\(.*\):\(.*\):\(.*\):-\(.*\)/\1.\2 \3: \4/' | \
    sed 's/-X/x/' | \
    grep -v \
        -e '^Datatype.* X$' \
        -e '^String.Quote'

