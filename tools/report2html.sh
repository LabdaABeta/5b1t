#!/bin/bash

REPORT_FILE=$1

cat \
    tools/htmlprefix.html \
    <(tail -8 "$REPORT_FILE" | ./tools/summary_html.sh) \
    <(head --lines=-9 "$REPORT_FILE" | ./tools/progress_html.awk) \
    tools/htmlsuffix.html
