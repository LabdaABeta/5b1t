#!/usr/bin/gawk -f

@include "tools/parse_ops.awk"

END {
    OFS = ":"
    for (idx in opsource) {
        rep = repr_of(idx)
        op = op_of(idx)
        mode = opmode[idx]
        if (opsource[idx] == rep) {
            status = "-?"
            if (mode == " ")
                status = "-I"
            if (mode == "?")
                status = "-U"
            if (mode == "!")
                status = "-O"
            if (mode == ".")
                status = "-P"

            gsub(/ /, "_", names[rep])
            print names[rep], opreprs[op], has_block(idx), status
        }
    }
}
