#!/usr/bin/gawk -f

function status_of(x) {
    if (x ~ "PASS")
        return "PASS"
    else if (x ~ "FAIL")
        return "FAIL"
    else if (x ~ "LEAK")
        return "LEAK"
    else if (x ~ "SKIP")
        return "SKIP"
    else
        return "????"
}

function cell_of(x) {
    if (x == "PASS")
        return "<td class=\"pass\">PASS</td>"
    else if (x == "FAIL")
        return "<td class=\"fail\">FAIL</td>"
    else if (x == "LEAK")
        return "<td class=\"leak\">LEAK</td>"
    else if (x == "SKIP")
        return "<td class=\"skip\">SKIP</td>"
    else
        return "<td class=\"what\">????</td>"
}

BEGIN {

    suite="unknown"
    suite_count=0
}
{
    print "<tr>"
    if (NF == 1) {
        # Suite start
        suite = $1;
        suites[suite_count++] = suite;
        suite_case_count[suite] = 0;
    } else if (NF == 2) {
        if ($1 ~ /ENCODING/) {
            # Encoding
            suite_encoding[suite] = status_of($2);
        } else {
            suite_status[suite] = status_of($2);
        }
    } else if (NF == 3) {
        # Compiling
        suite_compiling[suite] = status_of($2);
        suite_comptime[suite] = $3;
    } else if (NF == 4) {
        # Test case
        suite_casenames[suite][suite_case_count[suite]] = $2
        suite_cases[suite][suite_case_count[suite]] = status_of($3);
        suite_case_times[suite][suite_case_count[suite]++] = $4;
    }
}
END {
    print "<div class=\"tprogress-wrap\">";
    print "<table class=\"tprogress\">";
    print "<thead>"
    print "<tr>"
    print "    <th>Test-Suite</th>"
    print "    <th>Test-Case</th>"
    print "    <th>Status</th>"
    print "    <th>(time)</th>"
    print "</tr>"
    print "</thead>"
    print "<tbody>"

    for (s in suites) {
        su = suites[s]
        # print "<tr>"
        # print "    <td colspan=\"2\">" su "</td>"
        # print "    "  cell_of(suite_status[su])
        # print "    <td></td>"
        # print "</tr>"

        for (c in suite_cases[su]) {
            print "<tr>"
            print "    <td>" su "</td>"
            print "    <td>" suite_casenames[su][c] "</td>"
            print "    " cell_of(suite_cases[su][c])
            print "    <td>" suite_case_times[su][c] "</td>"
            print "</tr>"
        }
    }

    print "</tbody>"
    print "</table>"
    print "</div>"
}
