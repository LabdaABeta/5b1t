#!/bin/bash

# Shows all operations
awk -f tools/extract.awk documentation/documentation.tex | \
	sort | \
	join -t: -j1 tools/order - | \
	sort -t: -k2 | \
	cut -d: -f 2 --complement | \
	awk -f tools/expand.awk

# Shows all type declarations as "Derived Base", still have to format "Base"
find src/types -name '*.ads' -exec grep 'with private' {} \; | awk '{print $2, $(NF-2)}'
