#!/bin/bash

REMOVE_ANSI='s/\x1B\[([0-9]{1,2}(;[0-9]{1,2})?)?m//g'

read ENCODING_SUMMARY_LINE
ENCODING_SUMMARY=( $(echo "$ENCODING_SUMMARY_LINE" | sed -r "$REMOVE_ANSI") )
ENCODING_PASSES=${ENCODING_SUMMARY[1]}
ENCODING_FAILS=${ENCODING_SUMMARY[3]}

read COMPILING_SUMMARY_LINE
COMPILING_SUMMARY=( $(echo "$COMPILING_SUMMARY_LINE" | sed -r "$REMOVE_ANSI") )
COMPILING_PASSES=${COMPILING_SUMMARY[1]}
COMPILING_FAILS=${COMPILING_SUMMARY[3]}

read RUNNING_SUMMARY_LINE
RUNNING_SUMMARY=( $(echo "$RUNNING_SUMMARY_LINE" | sed -r "$REMOVE_ANSI") )
RUNNING_PASSES=${RUNNING_SUMMARY[1]}
RUNNING_FAILS=${RUNNING_SUMMARY[3]}
RUNNING_LEAKS=${RUNNING_SUMMARY[5]}

read SKIPPED_SUMMARY_LINE
SKIPPED_SUMMARY=( $(echo "$SKIPPED_SUMMARY_LINE" | sed -r "$REMOVE_ANSI") )
SKIPS=${SKIPPED_SUMMARY[1]}

read IGNORE
read IGNORE

read COMPILING_TIME_LINE
COMPILING_MEAN=${COMPILING_TIME_LINE:10:12}
COMPILING_MAX=${COMPILING_TIME_LINE:23:12}
COMPILING_MIN=${COMPILING_TIME_LINE:36:12}
COMPILING_TOTAL=${COMPILING_TIME_LINE:49:12}

read RUNNING_TIME_LINE
RUNNING_MEAN=${RUNNING_TIME_LINE:9:12}
RUNNING_MAX=${RUNNING_TIME_LINE:22:12}
RUNNING_MIN=${RUNNING_TIME_LINE:35:12}
RUNNING_TOTAL=${RUNNING_TIME_LINE:48:12}

cat <<EOF
<div class="tsummary-wrap">
<table class="tsummary">
<tr>
    <th colspan=7>SUMMARY</th>
</tr>
<tr>
    <th>Encoding:</th>
    <td class="pass">$ENCODING_PASSES</td>
    <td>PASS</td>
    <td class="fail">$ENCODING_FAILS</td>
    <td>FAIL</td>
    <td></td>
    <td></td>
</tr>
<tr>
    <th>Compiling:</th>
    <td class="pass">$COMPILING_PASSES</td>
    <td>PASS</td>
    <td class="fail">$COMPILING_FAILS</td>
    <td>FAIL</td>
    <td></td>
    <td></td>
</tr>
<tr>
    <th>Running:</th>
    <td class="pass">$RUNNING_PASSES</td>
    <td>PASS</td>
    <td class="fail">$RUNNING_FAILS</td>
    <td>FAIL</td>
    <td class="leak">$RUNNING_LEAKS</td>
    <td>LEAK</td>
</tr>
<tr>
    <th>Skipped:</th>
    <td class="skip">$SKIPS</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
</tr>
</table>
</div>
<br />
<br />
<div class="ttiming-wrap">
<table class="ttiming">
<tr>
    <th>Timing</th>
    <th>Mean</th>
    <th>Maximum</th>
    <th>Minimum</th>
    <th>Total</th>
</tr>
<tr>
    <th>Compiling</th>
    <td>$COMPILING_MEAN</td>
    <td>$COMPILING_MAX</td>
    <td>$COMPILING_MIN</td>
    <td>$COMPILING_TOTAL</td>
</tr>
<tr>
    <th>Running</th>
    <td>$RUNNING_MEAN</td>
    <td>$RUNNING_MAX</td>
    <td>$RUNNING_MIN</td>
    <td>$RUNNING_TOTAL</td>
</tr>
</table>
</div>
<br />
<br />
EOF
