#!/bin/bash

SOURCE_FILE="$1"
TYPENAME="$2"

declare -a OPNAMES=(
        "NUM_0" "NUM_1" "NUM_2" "NUM_3"
        "NUM_4" "NUM_5" "NUM_6" "NUM_7"
        "NUM_8" "NUM_9" "A_MAJ" "B_MAJ"
        "C_MAJ" "D_MAJ" "E_MAJ" "F_MAJ"
        "G_MAJ" "H_MAJ" "I_MAJ" "J_MAJ"
        "K_MAJ" "L_MAJ" "M_MAJ" "N_MAJ"
        "O_MAJ" "P_MAJ" "Q_MAJ" "R_MAJ"
        "S_MAJ" "T_MAJ" "U_MAJ" "V_MAJ"
        "W_MAJ" "X_MAJ" "Y_MAJ" "Z_MAJ"
        "A_MIN" "B_MIN" "C_MIN" "D_MIN"
        "E_MIN" "F_MIN" "G_MIN" "H_MIN"
        "I_MIN" "J_MIN" "K_MIN" "L_MIN"
        "M_MIN" "N_MIN" "O_MIN" "P_MIN"
        "Q_MIN" "R_MIN" "S_MIN" "T_MIN"
        "U_MIN" "V_MIN" "W_MIN" "X_MIN"
        "Y_MIN" "Z_MIN" "ADDIT" "SLASH"
        "MINUS" "TIMES" "VPIPE" "CARET"
        "AMPER" "PRCNT" "LESST" "MORET"
        "PAR_L" "PAR_R" "BRA_L" "BRA_R"
        "CUR_L" "CUR_R" "O_DOT" "COMMA"
        "SEMIC" "COLON" "APOST" "QUOTE"
        "BTICK" "TILDE" "EXCLA" "ATSGN"
        "POUND" "MONEY" "QUERY" "UNDER"
        "EQUAL" "HSALS" "SPACE" "ENTER"
    )

for op in "${OPNAMES[@]}"; do
    if grep -q $op "$SOURCE_FILE"; then
        echo "${TYPENAME//_/ }:$op:-X"
    fi
done
