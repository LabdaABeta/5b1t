#!/bin/bash

# Argument is 'Id' for the dictionary to get from ftp.gnu.org/gnu/aspell/dict

WHICHDICT=$1

INDEX_PAGE=$(mktemp)

wget https://ftp.gnu.org/gnu/aspell/dict/0index.html -O "$INDEX_PAGE" >/dev/null

DICT_TAR=$(grep "href=\"$WHICHDICT\"" < "$INDEX_PAGE" | head -1 | grep -o 'href="[^"]*"' | tail -1 | cut -d \" -f 2)

rm "$INDEX_PAGE"

TAR_FILE=$(mktemp)

wget "https://ftp.gnu.org/gnu/aspell/dict/$DICT_TAR" -O "$TAR_FILE" >/dev/null

TAR_DIR=$(mktemp -d)

tar -xf "$TAR_FILE" -C "$TAR_DIR" >/dev/null

cd "$TAR_DIR"
cd ./*

CHARSET=$(cat ./*.dat | grep charset | head -1 | sed 's/charset //')

precat -k ./*.cwl | iconv -f "${CHARSET:-utf-8}" -t utf-8 | aspell create master "./$WHICHDICT.rws" "--lang=$WHICHDICT" --local-data-dir=./ --encoding=utf-8 --normalize

aspell -d "./$WHICHDICT.rws" dump master --encoding=utf-8 | aspell -l "./$WHICHDICT" expand --encoding=utf-8 | tr " " "\n" | sort | uniq

rm "$TAR_FILE"
rm -rf "$TAR_DIR"
