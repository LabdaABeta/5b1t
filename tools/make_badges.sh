#!/bin/bash

# This is the results.json from tests
INPUT_FILE=${1-results.json}

mkdir -p public

# Move docs to public space as well
cp documentation/documentation.pdf public/

# Make the coverage badge
COVERAGE=$(grep 'Total:|' < coverage.summary | sed 's/.*|\([0-9]\+\.[0-9]\+\)\%.*/\1/')

COV_HUE=$(echo "$COVERAGE*3.0" | bc)

wget "https://img.shields.io/badge/coverage-${COVERAGE}%25-hsl(${COV_HUE%.*},100%25,50%25).svg" -O public/coverage.svg
rm bodies


# Make the operator badge
if [ ! -f refdb.db ]; then
    gnatinspect --db=refdb.db --force -P sbit.gpr --exit
fi
./tools/ls_ops.sh refdb.db > ops

# Positive findings
UNIMPLEMENTED=$(grep -wc U < ops)
OBSOLETED=$(grep -wc Ox < ops)
PARTIALLY_IMPLEMENTED=$(grep -wc Px < ops)
IMPLEMENTED=$(grep -wc Ix < ops)

# Negative findings
UNDOCUMENTED=$(grep -wc X < ops)
KEPT_SECRET=$(grep -wc Ux < ops)
PREMATURELY_DELETED=$(grep -wc O < ops)
PARTIALLY_IS_EMPTY=$(grep -wc P < ops)
LYING_DOCUMENTED=$(grep -wc I < ops)

# Accumulated findings
ALL_IMPLEMENTED=$((OBSOLETED + PARTIALLY_IMPLEMENTED + IMPLEMENTED))
ALL_VALID=$((UNIMPLEMENTED + OBSOLETED + PARTIALLY_IMPLEMENTED + IMPLEMENTED))
ALL_NEGATIVE=$((UNDOCUMENTED + KEPT_SECRET + PREMATURELY_DELETED + PARTIALLY_IS_EMPTY + LYING_DOCUMENTED))

if [ "$ALL_NEGATIVE" -gt "0" ]; then
    OPERATOR_COLOR=red
else
    OPERATOR_COLOR=green
fi

wget "https://img.shields.io/badge/operators%20(impl%2Ftotal--bad)-${ALL_IMPLEMENTED}%2F${ALL_VALID}--${ALL_NEGATIVE}-${OPERATOR_COLOR}.svg?colorA=blue" -O public/operators.svg

rm ops

# Make the test result badges
if [ "$(jq -r '.result' "$INPUT_FILE")" = "PASS" ]; then
    TEST_COLOUR=green
else
    TEST_COLOUR=red
fi

COMPILING_PASSES=$(jq '.results.compile.pass' "$INPUT_FILE")
COMPILING_FAILS=$(jq '.results.compile.fail' "$INPUT_FILE")

RUNNING_PASSES=$(jq '.results.run.pass' "$INPUT_FILE")
RUNNING_FAILS=$(jq '.results.run.fail' "$INPUT_FILE")

COMPILING_MAX=$(jq '.results.compile.time.max' "$INPUT_FILE")
RUNNING_MAX=$(jq '.results.run.time.max' "$INPUT_FILE")

wget "https://img.shields.io/badge/tests%20(comp--run)-${COMPILING_PASSES}%2F$((COMPILING_PASSES+COMPILING_FAILS))--${RUNNING_PASSES}%2F$((RUNNING_PASSES+RUNNING_FAILS))-${TEST_COLOUR}.svg?colorA=black" -O public/tests.svg
wget "https://img.shields.io/badge/time%20(comp--run)-${COMPILING_MAX}--${RUNNING_MAX}-blue.svg?colorA=blueviolet" -O public/time.svg

