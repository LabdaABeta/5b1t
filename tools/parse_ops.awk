#!/usr/bin/gawk -f

# Generates the following associative arrays:
# num_types - The number of types found
# order[#] - The representation of the next type in the order they appeared
# names[representation] - The full name of the type represented
# types[representation] - The type (set/tuple/single/etc) of the type represented
# parents[representation] - The parent type of the type represented
# plurals[representation] - The plural name of the type represented
# descriptions[representation] - The description of the type represented
# optypes[representation ":" op] - The representation of the return type of the operator of the type represented
# opnames[representation ":" op] - The names of the operator of the type represented
# opdescs[representation ":" op] - The descriptions of the operator of the type represented
# opsource[representation ":" op] - The source representation for the definition of the operator of the type represented.
# opmode[representation ":" op] - The mode (normal[ ], unimplemented[?], obsolete[!], partial[.]) of the operator of the type represented.
# opliteral[representation ":" op] - 1 if the operator defined takes a literal.
# opcodes[#] - Each of the 5b1t op-codes
# opreprs[*] - Each of the 5b1t representations

# NOTE: op above can be suffixed with Λ to represent the combinatorial variant.

# Also provided are has_block, repr_of, and op_of which break apart the representation ":" op syntax

BEGIN {
    FS = "\t"
    typekind = "unknown"

    opcodestr = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz+/-*|^&%<>()[]{}.,;:'\"`~!@#$?_=\\"
    opreprstr = "NUM_0NUM_1NUM_2NUM_3NUM_4NUM_5NUM_6NUM_7NUM_8NUM_9A_MAJB_MAJC_MAJD_MAJE_MAJF_MAJG_MAJH_MAJI_MAJJ_MAJK_MAJL_MAJM_MAJN_MAJO_MAJP_MAJQ_MAJR_MAJS_MAJT_MAJU_MAJV_MAJW_MAJX_MAJY_MAJZ_MAJA_MINB_MINC_MIND_MINE_MINF_MING_MINH_MINI_MINJ_MINK_MINL_MINM_MINN_MINO_MINP_MINQ_MINR_MINS_MINT_MINU_MINV_MINW_MINX_MINY_MINZ_MINADDITSLASHMINUSTIMESVPIPECARETAMPERPRCNTLESSTMORETPAR_LPAR_RBRA_LBRA_RCUR_LCUR_RO_DOTCOMMASEMICCOLONAPOSTQUOTEBTICKTILDEEXCLAATSGNPOUNDMONEYQUERYUNDEREQUALHSALS"

    for (i=1;i<=length(opcodestr);i++) {
        tmpstr = substr(opcodestr,i,1)
        opcodes[i-1] = tmpstr
        opreprs[tmpstr] = substr(opreprstr,(i-1)*5+1,5)
    }

    num_types = 0
    inop=0
}

/^##* .* ##*$/ {
    gsub(/#/, "")
    gsub(/^ */, "")
    gsub(/ *$/, "")
    typekind = $0
}

function has_block(idx) {
    if (idx ~ /.*Λ/)
        return 1
    return 0
}

function repr_of(idx) {
    match(idx, /(.*):.(Λ)?/, result)
    return result[1]
}

function op_of(idx) {
    match(idx, /(.*):(.)(Λ)?/, result)
    return result[2]
}

{
    if (NF == 4) { # Type Definition
        representation = $1
        parent = $2
        name = $3
        plural = $4

        if (representation in names) {
            print "ERROR: ", representation, " is a duplicate representation for ", names[representation]
            exit 1
        }

        order[num_types++] = representation
        names[representation] = name
        types[representation] = typekind
        parents[representation] = parent
        plurals[representation] = plural
        descriptions[representation] = ""

        for (op in opreprs) {
            if (parent ":" op in optypes) {
                optypes[representation ":" op] = optypes[parent ":" op]
                opnames[representation ":" op] = opnames[parent ":" op]
                opdescs[representation ":" op] = opdescs[parent ":" op]
                opsource[representation ":" op] = opsource[parent ":" op]
                opmode[representation ":" op] = opmode[parent ":" op]
                opliteral[representation ":" op] = opliteral[parent ":" op]
            }

            if (parent ":" op "Λ" in optypes) {
                optypes[representation ":" op "Λ"] = optypes[parent ":" op "Λ"]
                opnames[representation ":" op "Λ"] = opnames[parent ":" op "Λ"]
                opdescs[representation ":" op "Λ"] = opdescs[parent ":" op "Λ"]
                opsource[representation ":" op "Λ"] = opsource[parent ":" op "Λ"]
                opmode[representation ":" op "Λ"] = opmode[parent ":" op "Λ"]
                opliteral[representation ":" op "Λ"] = opliteral[parent ":" op "Λ"]
            }
        }

        inop = 0
    }

    if (NF == 3) { # Operator Definition
        operator = substr($1,1,1)
        argument = ""
        literal = 0
        mode = " "

        if (length ($1) == 2) {
            mode = substr($1,2,1)
        }

        if (length($1) == 3) {
            if (substr($1,2,2) == "Λ") {
                argument = "Λ"
            } else {
                literal = 1
            }
        }

        if (length($1) == 4) {
            if (substr($1,2,2) == "Λ") {
                argument = "Λ"
            } else {
                literal = 1
            }

            mode = substr($1,4,1)
        }

        if (length($1) == 5) {
            argument = "Λ"
            literal = 1
        }

        if (length($1) == 6) {
            argument = "Λ"
            literal = 1
            mode = substr($1,6,1)
        }

        optypes[representation ":" operator argument] = $2
        opnames[representation ":" operator argument] = $3
        opdescs[representation ":" operator argument] = ""
        opsource[representation ":" operator argument] = representation
        opmode[representation ":" operator argument] = mode
        opliteral[representation ":" operator argument] = literal

        inop = 1
    }

    if (NF == 2) { # Description
        if (inop) {
            opdescs[representation ":" operator argument] = opdescs[representation ":" operator argument] "\n" $2
        } else {
            descriptions[representation] = descriptions[representation] "\n" $2
        }
    }
}
