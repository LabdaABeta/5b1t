#!/usr/bin/gawk -f

@include "tools/parse_ops.awk"

END {
    result = plurals[search]
    gsub(/ /, "_", result)
    print result
}
