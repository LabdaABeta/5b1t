if exists("b:current_syntax")
    finish
endif

syntax match sbitComment "\v\t.*$"
syntax match sbitFunction "\v \w*"

highlight link sbitComment Comment
highlight link sbitFunction Statement
