#!/bin/bash

TAG="${1-latest}"

echo "Pushing images for tag :$TAG..."

echo 'Logging in to Docker Hub...'
docker login

echo 'Pushing base image...'
docker push "lambdabeta/5b1t-base:$TAG"

echo 'Pushing build image...'
docker push "lambdabeta/5b1t-build:$TAG"

echo 'Pushing test image...'
docker push "lambdabeta/5b1t-test:$TAG"

echo 'Pushing docs image...'
docker push "lambdabeta/5b1t-docs:$TAG"

echo 'Pushing image...'
docker push "lambdabeta/5b1t:$TAG"
