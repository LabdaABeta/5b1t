#!/bin/bash

TAG="${1-latest}"

echo "Building images for tag :$TAG..."

echo 'Logging in to Docker Hub...'
docker login

echo 'Building base image...'
docker build -t "lambdabeta/5b1t-base:$TAG" base

echo 'Building build image...'
docker build -t "lambdabeta/5b1t-build:$TAG" build

echo 'Building test image...'
docker build -t "lambdabeta/5b1t-test:$TAG" test

echo 'Building docs image...'
docker build -t "lambdabeta/5b1t-docs:$TAG" docs

echo 'Building image...'
docker build -t "lambdabeta/5b1t:$TAG" .
