% vi: wrap tabstop=4 shiftwidth=4 expandtab textwidth=0 spell spelllang=en_ca fdm=indent

\documentclass{article}

\usepackage{graphicx}
\usepackage{array}
\usepackage{hyperref}
\usepackage{booktabs}
\usepackage{titlesec}
\usepackage{longtable}
\usepackage{xcolor}
\usepackage{listings}
\usepackage{float}
\usepackage{lettrine}
\usepackage{xstring}
\usepackage{caption}
\usepackage{fancybox}
\usepackage{tikz}
\usepackage{xparse}
\usepackage{colortbl}
\usepackage{amsmath}
\usepackage{siunitx}
\usepackage{microtype}

\newcommand*{\circledtext}[1]{\tikz[baseline=(char.base)]{
    \node[shape=circle,draw,inner sep=2pt] (char) {#1};}}
\newcommand{\circled}[1]{\texorpdfstring{\protect\circledtext{#1}}{(#1)}}

\newcommand*{\squaredtext}[1]{\tikz[baseline=(char.base)]{
    \node[shape=rectangle,draw,inner sep=2pt] (char) {#1};}}
\renewcommand{\boxed}[1]{\texorpdfstring{\protect\squaredtext{#1}}{[#1]}}

\lstset{showspaces=true}

\definecolor{light-grey}{gray}{0.7}

\renewcommand{\listtablename}{Operators}

\usepackage[margin=1cm,footskip=0pt]{geometry}

\hypersetup{
    colorlinks,
    citecolor=black,
    filecolor=black,
    linkcolor=black,
    urlcolor=black,
    unicode
}

\def\scword #1 {\textsc{#1}}

\titleformat{name=\section}{}{\thetitle.}{0.8em}{\centering\scshape}
\titleformat{name=\section}[block]{}{\thetitle.}{0.8em}{\centering\scshape}
\titleformat{name=\subsection}[display]{}{\thetitle.}{0.5em}{\bfseries}
\titleformat{name=\subsubsection}[runin]{}{\thetitle.}{0.5em}{\itshape}[.]
\titleformat{name=\paragraph,numberless}[runin]{}{}{0em}{}[.]
\titlespacing{\paragraph}{0em}{0em}{0.5em}
\titleformat{name=\subparagraph,numberless}[runin]{}{}{0em}{}[.]
\titlespacing{\subparagraph}{0em}{0em}{0.5em}

% To unify and simplify source listings, each codepoint gets a def
% These should always appear in TT font!
\newcommand{\sbit}[1]{%
    \IfEqCase{#1}{%
        {NUM_0}{0}%
        {NUM_1}{1}%
        {NUM_2}{2}%
        {NUM_3}{3}%
        {NUM_4}{4}%
        {NUM_5}{5}%
        {NUM_6}{6}%
        {NUM_7}{7}%
        {NUM_8}{8}%
        {NUM_9}{9}%
        {A_MAJ}{A}%
        {B_MAJ}{B}%
        {C_MAJ}{C}%
        {D_MAJ}{D}%
        {E_MAJ}{E}%
        {F_MAJ}{F}%
        {G_MAJ}{G}%
        {H_MAJ}{H}%
        {I_MAJ}{I}%
        {J_MAJ}{J}%
        {K_MAJ}{K}%
        {L_MAJ}{L}%
        {M_MAJ}{M}%
        {N_MAJ}{N}%
        {O_MAJ}{O}%
        {P_MAJ}{P}%
        {Q_MAJ}{Q}%
        {R_MAJ}{R}%
        {S_MAJ}{S}%
        {T_MAJ}{T}%
        {U_MAJ}{U}%
        {V_MAJ}{V}%
        {W_MAJ}{W}%
        {X_MAJ}{X}%
        {Y_MAJ}{Y}%
        {Z_MAJ}{Z}%
        {A_MIN}{a}%
        {B_MIN}{b}%
        {C_MIN}{c}%
        {D_MIN}{d}%
        {E_MIN}{e}%
        {F_MIN}{f}%
        {G_MIN}{g}%
        {H_MIN}{h}%
        {I_MIN}{i}%
        {J_MIN}{j}%
        {K_MIN}{k}%
        {L_MIN}{l}%
        {M_MIN}{m}%
        {N_MIN}{n}%
        {O_MIN}{o}%
        {P_MIN}{p}%
        {Q_MIN}{q}%
        {R_MIN}{r}%
        {S_MIN}{s}%
        {T_MIN}{t}%
        {U_MIN}{u}%
        {V_MIN}{v}%
        {W_MIN}{w}%
        {X_MIN}{x}%
        {Y_MIN}{y}%
        {Z_MIN}{z}%
        {ADDIT}{+}%
        {SLASH}{/}%
        {MINUS}{-}%
        {TIMES}{*}%
        {VPIPE}{|}%
        {CARET}{\raisebox{-.3ex}{\char`^}}%
        {AMPER}{\&}%
        {PRCNT}{\%}%
        {LESST}{<}%
        {MORET}{>}%
        {PAR_L}{(}%
        {PAR_R}{)}%
        {BRA_L}{[}%
        {BRA_R}{]}%
        {CUR_L}{\char`\{}%
        {CUR_R}{\char`\}}%
        {O_DOT}{.}%
        {COMMA}{,}%
        {SEMIC}{;}%
        {COLON}{:}%
        {APOST}{'}%
        {QUOTE}{"}%
        {BTICK}{`}%
        {TILDE}{\raisebox{-.5ex}{\char`~}}%
        {EXCLA}{!}%
        {ATSGN}{@}%
        {POUND}{\#}%
        {MONEY}{\$}%
        {QUERY}{?}%
        {UNDER}{\char`\_}%
        {EQUAL}{=}%
        {HSALS}{\char`\\}%
        {SPACE}{\textvisiblespace}%
        {ENTER}{\textparagraph}}
}

\usepackage{fontspec}
\setmonofont{Unifont}

\setcounter{secnumdepth}{0} % Prevent numbers on sections

\graphicspath{{./share/}}

\begin{document}
\frenchspacing
\begin{titlepage}
    \vspace*{10em}
    {\centering\Huge 5b1t Documentation\par}
    \vspace{1em}
    {\centering Louis A. Burke\par}
    \vspace{5em}
    {\centering \includegraphics{logo}\par}
\end{titlepage}
\tableofcontents \newpage
\listoftables \newpage

\section{Introduction}

This document provides documentation and instruction for how to write programs in 5b1t.

5b1t is a programming language designed to produce extremely terse code.

5b1t is written using the 95 printable ASCII characters, plus the new line character. The name derives from the fact that these characters can be encoded across 5 bits and 1 trit ($96=2^5 3$). Since when terseness is measured it is often done in bytes, 5b1t has a standard encoding across bytes that retains efficiency.

5b1t code cares deeply about types. Every type in 5b1t overloads nearly every operator. Due to the nature of 5b1t and its dependence on types it is compiled. Due to the number of built-in features it is compiled to another high level language which must in turn be compiled down to an executable. This differs from many other terse languages in that 5b1t is not an interpreted language.

Also due to the typing system of 5b1t, the initial compilation stage must compile for a specific input type. For instance a program compiled to take an integer as input might not behave the same as a program compiled to take a pair of integers as input. The type of the argument can be listed with the source separately, or the source can be assumed to take as argument an empty type, in which case it must consume and parse input itself.

In summary, the process of writing a 5b1t program involves encoding it, while running it involves decoding, compiling with type specifiers, recompiling, and then finally executing the resulting executable. While this process is more involved than most terse languages' process, it can be automated easily and produce executables that can run orders of magnitude faster than most interpreted languages.

\section{Overview}

5b1t is a concatenative language, like \emph{Joy}. At all points the program maintains the `current data' which is passed to various functions. Each symbol represents an operation. Depending on whether it is followed by a block of code, each operation may take a function as an additional argument. All operations work on the common state of execution.

Additionally, many 5b1t types inherit operations from each other, thus there are certain operations that are common to many or even all input types.

Control flow is represented using whitespace. A space character introduces a block while a newline terminates a block. A newline outside of a block terminates parsing and is treated as the end of the input. It may be helpful to think of the control structures similarly to quotations in the Joy programming language. A space/newline pair can be thought of as a [, ] pair. If the program's whitespace is unbalanced, an appropriate number of balancing characters will be added. Thus if there are more spaces than newlines, that number of newlines will be added to the end. If there are more newlines than spaces, that number of spaces will be added to the start.

\subsection{Example}

An example program may help clarify control flow.

\begin{verbatim}
[1234m D
+
\end{verbatim}

When run on the nothing type this is interpreted as follows.

First the \texttt{[} indicates a list constructor. Each subsequent number in a list constructor merely adds that number to the list. The result is the list of the numbers 1, 2, 3, and 4.

The \texttt{m} followed by a space indicates the map function. This will apply the rest of the line to the input list. The rest of the line contains \texttt{D} which, when applied to natural numbers doubles them. Thus, after applying the map, the current state is a list containing 2, 4, 6, and 8.

Finally \texttt{+} when applied to a list of natural numbers indicates the sum operation. Therefore the result of this code is 20.

\subsection{Code Page}

Below is the code page for 5b1t programs.

\setlength\tabcolsep{1pt}
\texttt{%
\begin{tabular}{r|*{32}{c}}
Bits  & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 1 & 1 & 1 & 1 & 1 & 1 & 1 & 1 & 1 & 1 & 1 & 1 & 1 & 1 & 1 & 1 \\
      & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 1 & 1 & 1 & 1 & 1 & 1 & 1 & 1 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 1 & 1 & 1 & 1 & 1 & 1 & 1 & 1 \\
      & 0 & 0 & 0 & 0 & 1 & 1 & 1 & 1 & 0 & 0 & 0 & 0 & 1 & 1 & 1 & 1 & 0 & 0 & 0 & 0 & 1 & 1 & 1 & 1 & 0 & 0 & 0 & 0 & 1 & 1 & 1 & 1 \\
      & 0 & 0 & 1 & 1 & 0 & 0 & 1 & 1 & 0 & 0 & 1 & 1 & 0 & 0 & 1 & 1 & 0 & 0 & 1 & 1 & 0 & 0 & 1 & 1 & 0 & 0 & 1 & 1 & 0 & 0 & 1 & 1 \\
Trits & 0 & 1 & 0 & 1 & 0 & 1 & 0 & 1 & 0 & 1 & 0 & 1 & 0 & 1 & 0 & 1 & 0 & 1 & 0 & 1 & 0 & 1 & 0 & 1 & 0 & 1 & 0 & 1 & 0 & 1 & 0 & 1 \\ \midrule
0     & \sbit{NUM_0} & \sbit{NUM_1} & \sbit{NUM_2} & \sbit{NUM_3} & \sbit{NUM_4} & \sbit{NUM_5} & \sbit{NUM_6} & \sbit{NUM_7} & \sbit{NUM_8} & \sbit{NUM_9} & \sbit{A_MAJ} & \sbit{B_MAJ} & \sbit{C_MAJ} & \sbit{D_MAJ} & \sbit{E_MAJ} & \sbit{F_MAJ} & \sbit{G_MAJ} & \sbit{H_MAJ} & \sbit{I_MAJ} & \sbit{J_MAJ} & \sbit{K_MAJ} & \sbit{L_MAJ} & \sbit{M_MAJ} & \sbit{N_MAJ} & \sbit{O_MAJ} & \sbit{P_MAJ} & \sbit{Q_MAJ} & \sbit{R_MAJ} & \sbit{S_MAJ} & \sbit{T_MAJ} & \sbit{U_MAJ} & \sbit{V_MAJ} \\
1     & \sbit{W_MAJ} & \sbit{X_MAJ} & \sbit{Y_MAJ} & \sbit{Z_MAJ} & \sbit{A_MIN} & \sbit{B_MIN} & \sbit{C_MIN} & \sbit{D_MIN} & \sbit{E_MIN} & \sbit{F_MIN} & \sbit{G_MIN} & \sbit{H_MIN} & \sbit{I_MIN} & \sbit{J_MIN} & \sbit{K_MIN} & \sbit{L_MIN} & \sbit{M_MIN} & \sbit{N_MIN} & \sbit{O_MIN} & \sbit{P_MIN} & \sbit{Q_MIN} & \sbit{R_MIN} & \sbit{S_MIN} & \sbit{T_MIN} & \sbit{U_MIN} & \sbit{V_MIN} & \sbit{W_MIN} & \sbit{X_MIN} & \sbit{Y_MIN} & \sbit{Z_MIN} & \sbit{ADDIT} & \sbit{SLASH} \\
2     & \sbit{MINUS} & \sbit{TIMES} & \sbit{VPIPE} & \sbit{CARET} & \sbit{AMPER} & \sbit{PRCNT} & \sbit{LESST} & \sbit{MORET} & \sbit{PAR_L} & \sbit{PAR_R} & \sbit{BRA_L} & \sbit{BRA_R} & \sbit{CUR_L} & \sbit{CUR_R} & \sbit{O_DOT} & \sbit{COMMA} & \sbit{SEMIC} & \sbit{COLON} & \sbit{APOST} & \sbit{QUOTE} & \sbit{BTICK} & \sbit{TILDE} & \sbit{EXCLA} & \sbit{ATSGN} & \sbit{POUND} & \sbit{MONEY} & \sbit{QUERY} & \sbit{UNDER} & \sbit{EQUAL} & \sbit{HSALS} & \sbit{SPACE} & \sbit{ENTER} \\
\end{tabular}
}
\setlength\tabcolsep{6pt} % The default

\subsection{Encoding}

While normally written in any superset of the ASCII character encoding, the true size of a 5b1t program is measured based on its encoded size. The encoding method is to first convert the input to a base-96 number, appending a $1$ in front of it. This number is converted to binary and padded with leading zeroes to a multiple of 8 bits. The resulting bytes is the encoding.

\subsubsection{Encoding Example}

For example, consider the code \texttt{Hello, World!}. The code page expansion in base-96 is: \texttt{17 40 47 47 50 79 94 32 50 53 47 39 86}

After adding the leading $1$ this yields: \texttt{1 17 40 47 47 50 79 94 32 50 53 47 39 86} which, in decimal, is \texttt{69494655939676\allowbreak{}481327868662}.

Converting this number to binary gives: \texttt{11100101\allowbreak{}11110000\allowbreak{}01000011\allowbreak{}01100011\allowbreak{}10100001\allowbreak{}10110001\allowbreak{}01000100\allowbreak{}11111111\allowbreak{}11001000\allowbreak{}10101011\allowbreak{}110110}.

Padding this out to octets yields: \texttt{00111001 01111100 00010000 11011000 11101000 01101100 01010001 00111111 11110010 00101010 11110110}.

Therefore \texttt{Hello, World!} is encoded as \texttt{397C10D8E86C513FF22AF6} in hexadecimal.

\subsubsection{Decoding Example}

Another example, consider the hexdump of a program: \texttt{126F1B78\allowbreak{}09ED6F0B\allowbreak{}A8F7FB10\allowbreak{}AF90C516\allowbreak{}518F7D69\allowbreak{}665CB79E\allowbreak{}71589208\allowbreak{}55038D8A\allowbreak{}35CCA3F8\allowbreak{}EFE27EA6\allowbreak{}E668B3FF\allowbreak{}2EB08B15\allowbreak{}2B27C97D\allowbreak{}4877E3EA\allowbreak{}1}.

Converting the base-16 number to decimal yields \texttt{83740965\allowbreak{}08872589\allowbreak{}88666618\allowbreak{}07146599\allowbreak{}79936054\allowbreak{}26886483\allowbreak{}34154093\allowbreak{}26023212\allowbreak{}32594279\allowbreak{}79056986\allowbreak{}44464647\allowbreak{}75538517\allowbreak{}81735696\allowbreak{}16118603\allowbreak{}61920765\allowbreak{}02006956\allowbreak{}5447841}. Which in base-96 is \texttt{1 33 5 0 86 25 69 87 10 25 74 4 93 25 35 33 5 4 72 25 67 73 7 12 12 73 7 77 89 14 18 12 10 27 64 28 29 10 23 13 10 27 13 64 10 23 29 18 31 18 27 30 28 64 29 14 28 29 64 15 18 21 14 86 89 17 62 17 65}.

Stripping the 1 and reversing the code-page yields:

\texttt{X5O!P\%@AP[4\textbackslash PZX54(P\char`\^)7CC)7\}\$EICAR-STANDARD-ANTIVIRUS-TEST-FILE!\$H+H*}

Which is the source of the program.

\subsubsection{Ramifications}

The result of this encoding scheme is that the minimum number of bytes required to store a program are used. However, since each symbol takes over 6 bits ($\log_2 96 \approx 6.585$) most characters of input seem to take up a whole byte of output. The remaining bit and a half (approximately) of information accumulates until eventually an input character appears to be 'free' in that the total byte count does not increase. It may be useful to see a table of input length to output bytecounts highlighting the 'free' lengths.

\newcommand{\freebee}[1]{\colorbox{blue!30}{#1}}

\renewcommand{\arraystretch}{0.8}
\begin{table}[H]
    \centering
    \begin{tabular}{rl|rl|rl|rl|rl|rl}
    Input & Output & Input & Output & Input & Output & Input & Output & Input & Output & Input & Output\\ \midrule
    1 & 1 & 33 & 28 & 65 & 54 & \freebee{97} & 80 & 129 & 107 & 161 & 133 \\
    2 & 2 & \freebee{34} & 28 & 66 & 55 & 98 & 81 & 130 & 108 & 162 & 134 \\
    3 & 3 & 35 & 29 & 67 & 56 & 99 & 82 & \freebee{131} & 108 & 163 & 135 \\
    4 & 4 & 36 & 30 & \freebee{68} & 56 & 100 & 83 & 132 & 109 & \freebee{164} & 135 \\
    5 & 5 & 37 & 31 & 69 & 57 & 101 & 84 & 133 & 110 & 165 & 136 \\
    \freebee{6} & 5 & 38 & 32 & 70 & 58 & \freebee{102} & 84 & 134 & 111 & 166 & 137 \\
    7 & 6 & 39 & 33 & 71 & 59 & 103 & 85 & 135 & 112 & 167 & 138 \\
    8 & 7 & \freebee{40} & 33 & 72 & 60 & 104 & 86 & \freebee{136} & 112 & 168 & 139 \\
    9 & 8 & 41 & 34 & 73 & 61 & 105 & 87 & 137 & 113 & 169 & 140 \\
    10 & 9 & 42 & 35 & \freebee{74} & 61 & 106 & 88 & 138 & 114 & \freebee{170} & 140 \\
    11 & 10 & 43 & 36 & 75 & 62 & 107 & 89 & 139 & 115 & 171 & 141 \\
    \freebee{12} & 10 & 44 & 37 & 76 & 63 & \freebee{108} & 89 & 140 & 116 & 172 & 142 \\
    13 & 11 & 45 & 38 & 77 & 64 & 109 & 90 & 141 & 117 & 173 & 143 \\
    14 & 12 & \freebee{46} & 38 & 78 & 65 & 110 & 91 & \freebee{142} & 117 & 174 & 144 \\
    15 & 13 & 47 & 39 & 79 & 66 & 111 & 92 & 143 & 118 & 175 & 145 \\
    16 & 14 & 48 & 40 & \freebee{80} & 66 & 112 & 93 & 144 & 119 & \freebee{176} & 145 \\
    \freebee{17} & 14 & 49 & 41 & 81 & 67 & 113 & 94 & 145 & 120 & 177 & 146 \\
    18 & 15 & 50 & 42 & 82 & 68 & \freebee{114} & 94 & 146 & 121 & 178 & 147 \\
    19 & 16 & \freebee{51} & 42 & 83 & 69 & 115 & 95 & \freebee{147} & 121 & 179 & 148 \\
    20 & 17 & 52 & 43 & 84 & 70 & 116 & 96 & 148 & 122 & 180 & 149 \\
    21 & 18 & 53 & 44 & \freebee{85} & 70 & 117 & 97 & 149 & 123 & \freebee{181} & 149 \\
    22 & 19 & 54 & 45 & 86 & 71 & 118 & 98 & 150 & 124 & 182 & 150 \\
    \freebee{23} & 19 & 55 & 46 & 87 & 72 & \freebee{119} & 98 & 151 & 125 & 183 & 151 \\
    24 & 20 & 56 & 47 & 88 & 73 & 120 & 99 & 152 & 126 & 184 & 152 \\
    25 & 21 & \freebee{57} & 47 & 89 & 74 & 121 & 100 & \freebee{153} & 126 & 185 & 153 \\
    26 & 22 & 58 & 48 & 90 & 75 & 122 & 101 & 154 & 127 & 186 & 154 \\
    27 & 23 & 59 & 49 & \freebee{91} & 75 & 123 & 102 & 155 & 128 & \freebee{187} & 154 \\
    28 & 24 & 60 & 50 & 92 & 76 & 124 & 103 & 156 & 129 & 188 & 155 \\
    \freebee{29} & 24 & 51 & 51 & 93 & 77 & \freebee{125} & 103 & 157 & 130 & 189 & 156 \\
    30 & 25 & 62 & 52 & 94 & 78 & 126 & 104 & 158 & 131 & 190 & 157 \\
    31 & 26 & \freebee{63} & 52 & 95 & 79 & 127 & 105 & \freebee{159} & 131 & 191 & 158 \\
    32 & 27 & 64 & 53 & 96 & 80 & 128 & 106 & 160 & 132 & 192 & 159 \\
    \end{tabular}
\end{table}
\renewcommand{\arraystretch}{1.0}

\newcommand{\blockArgument}{Λ}
\newcommand{\literal}{λ}

\section{Language}

This section defines every operation of the language. Each operation operates on the state of execution. Depending on the type of the state of execution, an operator may perform differing actions. Some types inherit their operations from other types.

What follows is a summary of the operations of each type. When a type is generic (that is, it can take types as parameters) the generic parameters are marked with enclosed numerics (e.g. \circled{1} \circled{2}). When a returned type is generic, enclosed alphabetics are used instead (e.g. \circled{A} \circled{B}). When an operator takes a function as a block it is represented by \blockArgument{}.

The summaries are titled based on the type's name along with whatever type it inherits from, followed by its canonical representation.

Following the titles is a description of what a type represents and what it is used for. Often variable names are assigned in this section for future reference.

Finally a table summarizing the actions of each operator on the type is presented. The result type is shown in canonical representation and the description may make reference to the variables defined in the opening paragraph. Any as-yet unimplemented operations are coloured \colorbox{yellow!50}{yellow} while obsoleted or removed operations are coloured \colorbox{red!50}{red} and partially implemented operations are coloured \colorbox{blue!50}{blue}. Inherited operations appear with grey text and merely direct you to their source.

Occasionally, after the table there will be additional notes. These are often to point out unusual or particularly interesting behaviour of certain operations.

\subsection{Literals}

Sometimes a command would like to take a literal value of some kind, however there are no literal values in 5b1t. To work around this there are a class of partially completed types marked with \literal{} to indicate that they take a literal value.

The format of a literal value is an optional digit count followed by a base-94 number containing that many digits.

The digit count is made up of all symbols up to the first symbol in the first 64 codepoints. These have 64 subtracted from their values and are treated as a base-30 number. This number has $2$ added to it to become the full digit count of the literal. If the digit count is omitted, then the number is assumed to have $1$ digit. Note that this means the numbers from $64$ to $95$ take $3$ symbols (e.g. $88$ is \texttt{-0\#}).

Any symbol with a block will be repeated as many times as the base-94 value of the block. This also applies to the base-94 symbols within said blocks.

Literal values print their initial argument if the result of the program is a partial application thereof.

\subsubsection{Examples}

A few examples may be useful to understand literals.

\texttt{x}

The literal value is $59$ because it has a code-point of 59.

\begin{verbatim}
{Hello, World!
How are you?
Good.
Good.
\end{verbatim}

This begins with \texttt{\{} so it needs 14 base-94 digits. The \texttt{Hello} corresponds to $17:40:47:47:50$ in base-94, or $1360916352$. The \texttt{,} takes a block containing \texttt{World!}. \texttt{World!} corresponds to $32:50:53:47:39:86$ in base-94, or $238797471964$. The code-point of \texttt{,} is $79$ therefore the partial value at this point is $17:40:47:47:50:\underbrace{79:\dots:79}_{238787471968}$ in base-94, with $238787471968$ $79$s.

Returning to the consumption of the 14 base-94 digits, 8 are still missing. \texttt{Ho} corresponds to $17:50$ in base-94, or $1648$. The \texttt{w} takes a block whose value must be calculated.

This block starts with \texttt{ar} corresponding to $36:53$ in base-94, but then the \texttt{e} has another block to calculate. This block contains \texttt{you?} with a value of $60:50:56:90$ in base-94, or $50282194$. Thus the block of the \texttt{w} has a partial value of $36:53:\underbrace{40:\dots:40}_{50282198}$ in base-94. Now the remaining \texttt{Good.} is appended in base-94 to give the final repetition count on the \texttt{w} as $36:53:\underbrace{40:\dots:40}_{50282198}:16:50:50:39:78$ in base-94.

Applying this to the \texttt{w} yields a partial result of:

$$17:40:47:47:50:\underbrace{79:\dots:79}_{238787471968}:17:50:\underbrace{58:\dots:58}_{36:53:\underbrace{40:\dots:40}_{50282198}:16:50:50:39:78}$$

Finally, the remaining \texttt{Good.} is appended showing that the base-94 value of the above literal is:

$$17:40:47:47:50:\underbrace{79:\dots:79}_{238787471968}:17:50:\underbrace{58:\dots:58}_{36:53:\underbrace{40:\dots:40}_{50282198}:16:50:50:39:78}:16:50:50:39:78$$

This is a very contrived example, but shows just how massive literal values can be made with minimal code. In fact this number is far beyond the transcomputational limit, and thus would never actually be created in memory. It would take almost 100 million digits just to write out how many digits the number has. For now 5b1t uses GMP for bignum manipulation, which will fail to even store the result. In future it is hoped to use a more symbolic representation in order to enable the use of such large constants.

One can use the tool \texttt{to\_literal} to create literal values easily.

\subsection{Containers}

There exists a convention for certain commonly used containers. There are four containers used by 5b1t. They are parameterized based on whether they care about order and how they treat types.

These containers are:

\begin{itemize}
    \item Lists. Represented with square brackets.
    \item Sets. Represented with curly brackets.
    \item Tuples. Represented with round brackets.
    \item Groups. Represented with multiple datatypes in sequence with no bracketing.
\end{itemize}

Lists, sets, and tuples can all be created from the base datatype using the wrapping \texttt{\sbit{PAR_R}}, \texttt{\sbit{BRA_R}}, and \texttt{\sbit{CUR_R}} operators. Groups are far less common, arising from specific operations. Types created from groups are each effectively their own atomic type, as there is no means by which to index a group.

\subsubsection{Order}

Ordered containers have a well defined order. The elements can be indexed and re-ordered. Unordered containers are the opposite.

\subsubsection{Type}

Homogeneous containers are parameterized by a single type. This type is determined by finding the closest parent type that is a parent of all types in the list. In the base case these containers will contain objects so different that the only relevant parent type is the base datatype.

By contrast heterogeneous containers are parameterized by precisely which types are present. The container's type is determined by finding the closest type with the same number of elements and whose elements are each parents of their respective source elements.

\subsubsection{Summary}

With these definitions in mind, here are the containers in 5b1t.

\begin{table}[H]
    \begin{tabular}{c|cc}
                      & Ordered  & Unordered \\ \midrule
        Homogeneous   & List     & Set       \\
        Heterogeneous & Tuple    & Group     \\
    \end{tabular}
\end{table}

\subsubsection{Examples}

To determine the 'true' type can sometimes be difficult. As such, lets look at a few examples.

Lets pretend we only have four types other than the base datatype. Lets call these types A, B, C, and D. A and B will be direct descendants of the base datatype. Meanwhile C and D will be descendants of A.

Meanwhile lets pretend that the defined list types are A, B, and C lists (plus the base datatype list). Similarly lets pretend that the defined set types are A, B, and C sets (plus the base datatype set). Lets also pretend we have AA, AB, and AC tuples (plus the base datatype tuple). Similarly lets pretend that we have AA, AB, and AC groups (plus the base datatype group).

Now lets look at some examples. First lets consider a container with an A and a C. For each type this will yield the following.

\begin{table}[H]
    \begin{tabular}{c|cc}
                      & Ordered  & Unordered \\ \midrule
        Homogeneous   & A-List   & A-Set     \\
        Heterogeneous & AC-Tuple & AC-Group  \\
    \end{tabular}
\end{table}

This is pretty straightforward.

Now lets consider a container with an A and a D. For each type this will yield the following.

\begin{table}[H]
    \begin{tabular}{c|cc}
                      & Ordered  & Unordered \\ \midrule
        Homogeneous   & A-List   & A-Set     \\
        Heterogeneous & AA-Tuple & AA-Group  \\
    \end{tabular}
\end{table}

Finally lets consider a container with an A and a B. For each type this will yield the following.

\begin{table}[H]
    \begin{tabular}{c|cc}
                      & Ordered  & Unordered \\ \midrule
        Homogeneous   & List     & Set       \\
        Heterogeneous & AB-Tuple & AB-Group  \\
    \end{tabular}
\end{table}

\newcommand{\currenttype}{--}
\newcommand{\currentname}{--}
\newcommand{\currentbase}{--}
\newcommand{\typename}{\currentname\ $\bullet$\currenttype$\bullet$}

\newcommand{\settype}[3][]{%
    \renewcommand{\currenttype}{#3}
    \renewcommand{\currentname}{#2}
    \renewcommand{\currentbase}{#1}
\subsubsection{\currentname (\currentbase) \texorpdfstring{$\bullet$\currenttype$\bullet$}{\textbullet\currenttype\textbullet}}}

\newenvironment{typetab}
    {\begin{longtable}{>{ \ttfamily}r<{ } @{$\mapsto$} >{ \ttfamily}l<{ } p{0.65\textwidth}}
         \caption{Operators for \typename}\\
         OP & RES & Description \\ \midrule
    }{\end{longtable}}

\newcommand{\unimplemented}{\rowcolor{yellow!50}}
\newcommand{\obsoleted}{\rowcolor{red!50}}
\newcommand{\partially}{\rowcolor{blue!50}}
\newcommand{\inherited}{\color{gray}}

\subsection{Types} % TODO: Break into Single Types, List Types, Tuple Types, Set Types, Group Types somehow

\newcounter{operatorcounter}
\input{operations.tex}

\section{Execution}

\subsection{Installation}

The program can be installed by simply running the \texttt{make \&\& make install} idiom. This uses the environment variable \texttt{PREFIX} as the installation directory if not otherwise specified.

\subsection{Driver}

The \texttt{5b1t} executable is the main driver for 5b1t code. It has a basic \texttt{--help} option, but its behaviour will be more precisely explained.

The driver takes the input file as a positional argument. If no such argument is present, then it reads it from standard input.

By default, the inputted file is compiled and run. This can cause a subtle problem if the input is provided by standard input instead of a positional file. In this case the compilation phase will have already consumed all of the available standard input, leaving nothing for the compiled program to consume. This may not be an issue for programs which take no input, but for those that do it can lead to unusual behaviour.

The default behaviour of running the code can be modified via command-line arguments. The \texttt{-c} option causes the driver to only compile the source (by default creating an executable named ``a.out''). The \texttt{-d} option causes the driver to decode the input from 5b1t encoding to ASCII. The \texttt{-e} option causes the driver to encode the input from ASCII to 5b1t encoding. Both the \texttt{-d} and \texttt{-e} modes of operation dump their results to standard output, ignoring any provided output file. The \texttt{-c} options output executable can be modified by specifying an alternative using the \texttt{-o} option. If the \texttt{-o} option is present, then it is assumed that the desired behaviour is compilation, thus \texttt{-c} is not necessary.

Also by default, the inputted file is assumed to be encoded as per the above encoding. If the \texttt{-a} switch is passed then it is assumed to be ASCII encoded instead, treating tab characters as line comment initializers.

The initial type on which the program will operate (and which will determine how the initial object is parsed from input) can be specified using the \texttt{-i} option. If not specified the initial type defaults to the nothing type.

For debugging purposes the \texttt{-g} option can be specified, causing the resulting execution to print partial results to standard error. Similarly the \texttt{-s} option provides the capability to save the generated Ada source code to a known file for inspection.

The random number generator used by the code can be pre-seeded with an integer using the \texttt{-r} option. A seed of 0 (the default) will use a dynamic seed.

\appendix
\section{Dictionaries}
\label{sec:dictionaries}

Many dictionaries are accessible from within 5b1t programs. Most of them are based on the dictionaries at \url{https://ftp.gnu.org/gnu/aspell/dict/0index.html}.

These are found in the dictionary files in the share directory once they are downloaded.

The indices of the dictionaries, and their descriptions, are as follows:

\begin{longtable}{rlp{0.65\textwidth}}
    ID\# & Name & Description \\ \midrule
    0 & English & The English words as conglomerated from aspell dictionaries for multiple dialects. \\
    1 & Afrikaans & The Afrikaans words as conglomerated from the aspell dictionary. \\
    2 & Amharix & The Amharic words as conglomerated from the aspell dictionary. \\
    3 & Arabic & The Arabic words as conglomerated from the aspell dictionary. \\
    4 & Asturian & The Asturian words as conglomerated from the aspell dictionary. \\
    5 & Azerbaijani & The Azerbaijani words as conglomerated from the aspell dictionary. \\
    6 & Belarusian & The Belarusian words as conglomerated from the aspell dictionary. \\
    7 & Bulgarian & The Bulgarian words as conglomerated from the aspell dictionary. \\
    8 & Bengali & The Bengali words as conglomerated from the aspell dictionary. \\
    9 & Breton & The Breton words as conglomerated from the aspell dictionary. \\
    10 & Catalan & The Catalan words as conglomerated from the aspell dictionary. \\
    11 & Czech & The Czech words as conglomerated from the aspell dictionary. \\
    12 & Kashubian & The Kashubian words as conglomerated from the aspell dictionary. \\
    13 & Welsh & The Welsh words as conglomerated from the aspell dictionary. \\
    14 & Danish & The Danish words as conglomerated from the aspell dictionary. \\
    15 & German & The German words as conglomerated from the aspell dictionary. \\
    16 & German - Old Spelling & The German words in old spelling as conglomerated from the aspell dictionary. \\
    17 & Greek & The Greek words as conglomerated from the aspell dictionary. \\
    18 & Esperanto & The Esperanto words as conglomerated from the aspell dictionary. \\
    19 & Spanish & The Spanish words as conglomerated from the aspell dictionary. \\
    20 & Estonian & The Estonian words as conglomerated from the aspell dictionary. \\
    21 & Persian & The Persian words as conglomerated from the aspell dictionary. \\
    22 & Finnish & The Finnish words as conglomerated from the aspell dictionary. \\
    23 & Faroese & The Faroese words as conglomerated from the aspell dictionary. \\
    24 & French & The French words as conglomerated from the aspell dictionary. \\
    25 & Frisian & The Frisian words as conglomerated from the aspell dictionary. \\
    26 & Irish & The Irish words as conglomerated from the aspell dictionary. \\
    27 & Scottish Gaelic & The Scottish Gaelic words as conglomerated from the aspell dictionary. \\
    28 & Galician & The Galician words as conglomerated from the aspell dictionary. \\
    29 & Ancient Greek & The Ancient Greek words as conglomerated from the aspell dictionary. \\
    30 & Gujarati & The Gujarati words as conglomerated from the aspell dictionary. \\
    31 & Manx Gaelic & The Manx Gaelic words as conglomerated from the aspell dictionary. \\
    32 & Hebrew & The Hebrew words as conglomerated from the aspell dictionary. \\
    33 & Hindi & The Hindi words as conglomerated from the aspell dictionary. \\
    34 & Hiligaynon & The Hiligaynon words as conglomerated from the aspell dictionary. \\
    35 & Croatian & The Croatian words as conglomerated from the aspell dictionary. \\
    36 & Upper Sorbian & The Upper Sorbian words as conglomerated from the aspell dictionary. \\
    37 & Hungarian & The Hungarian words as conglomerated from the aspell dictionary. \\
    38 & Huastec & The Huastec words as conglomerated from the aspell dictionary. \\
    39 & Armenian & The Armenian words as conglomerated from the aspell dictionary. \\
    40 & Interlingua & The Interlingua words as conglomerated from the aspell dictionary. \\
    41 & Indonesian & The Indonesian words as conglomerated from the aspell dictionary. \\
    42 & Icelandic & The Icelandic words as conglomerated from the aspell dictionary. \\
    43 & Italian & The Italian words as conglomerated from the aspell dictionary. \\
    44 & Kannada & The Kannada words as conglomerated from the aspell dictionary. \\
    45 & Kurdi & The Kurdi words as conglomerated from the aspell dictionary. \\
    46 & Kirghiz & The Kirghiz words as conglomerated from the aspell dictionary. \\
    47 & Latin & The Latin words as conglomerated from the aspell dictionary. \\
    48 & Lithuanian & The Lithuanian words as conglomerated from the aspell dictionary. \\
    49 & Latvian & The Latvian words as conglomerated from the aspell dictionary. \\
    50 & Malagasy & The Malagasy words as conglomerated from the aspell dictionary. \\
    51 & Maori & The Maori words as conglomerated from the aspell dictionary. \\
    52 & Macedonian & The Macedonian words as conglomerated from the aspell dictionary. \\
    53 & Malayalam & The Malayalam words as conglomerated from the aspell dictionary. \\
    54 & Mongolian & The Mongolian words as conglomerated from the aspell dictionary. \\
    55 & Marathi & The Marathi words as conglomerated from the aspell dictionary. \\
    56 & Malay & The Malay words as conglomerated from the aspell dictionary. \\
    57 & Maltese & The Maltese words as conglomerated from the aspell dictionary. \\
    58 & Norwegian Bokmal & The Norwegian Bokmal words as conglomerated from the aspell dictionary. \\
    59 & Low Saxon & The Low Saxon words as conglomerated from the aspell dictionary. \\
    60 & Dutch & The Dutch words as conglomerated from the aspell dictionary. \\
    61 & Norwegian Nynorsk & The Norwegian Nynorsk words as conglomerated from the aspell dictionary. \\
    62 & Chichewa & The Chichewa words as conglomerated from the aspell dictionary. \\
    63 & Oriya & The Oriya words as conglomerated from the aspell dictionary. \\
    64 & Punjabi & The Punjabi words as conglomerated from the aspell dictionary. \\
    65 & Polish & The Polish words as conglomerated from the aspell dictionary. \\
    66 & Brazilian Portuguese & The Brazilian Portuguese words as conglomerated from the aspell dictionary. \\
    67 & Portuguese & The Portuguese words as conglomerated from the aspell dictionary. \\
    68 & Quechua & The Quechua words as conglomerated from the aspell dictionary. \\
    69 & Romanian & The Romanian words as conglomerated from the aspell dictionary. \\
    70 & Russian & The Russian words as conglomerated from the aspell dictionary. \\
    71 & Kinyarwanda & The Kinyarwanda words as conglomerated from the aspell dictionary. \\
    72 & Sardinian & The Sardinian words as conglomerated from the aspell dictionary. \\
    73 & Slovak & The Slovak words as conglomerated from the aspell dictionary. \\
    74 & Slovenian & The Slovenian words as conglomerated from the aspell dictionary. \\
    75 & Serbian & The Serbian words as conglomerated from the aspell dictionary. \\
    76 & Swedish & The Swedish words as conglomerated from the aspell dictionary. \\
    77 & Swahili & The Swahili words as conglomerated from the aspell dictionary. \\
    78 & Tamil & The Tamil words as conglomerated from the aspell dictionary. \\
    79 & Telugu & The Telugu words as conglomerated from the aspell dictionary. \\
    80 & Tetum & The Tetum words as conglomerated from the aspell dictionary. \\
    81 & Turkmen & The Turkmen words as conglomerated from the aspell dictionary. \\
    82 & Tagalog & The Tagalog words as conglomerated from the aspell dictionary. \\
    83 & Setswana & The Setswana words as conglomerated from the aspell dictionary. \\
    84 & Turkish & The Turkish words as conglomerated from the aspell dictionary. \\
    85 & Ukrainian & The Ukrainian words as conglomerated from the aspell dictionary. \\
    86 & Uzbek & The Uzbek words as conglomerated from the aspell dictionary. \\
    87 & Vietnamese & The Vietnamese words as conglomerated from the aspell dictionary. \\
    88 & Walloon & The Walloon words as conglomerated from the aspell dictionary. \\
    89 & Yiddish & The Yiddish words as conglomerated from the aspell dictionary. \\
    90 & Zulu & The Zulu words as conglomerated from the aspell dictionary. \\
\caption{5b1t Dictionaries}%
\end{longtable}

\section{FAQ}
\label{sec:faq}

\subsection{What is the 5b1t logo?}

The logo is a simplified vector rendition of a \emph{Mola} fry. The \emph{Mola} or sunfish is a fairly well-known fish. It is the largest known bony fish at over to \SI{3}{\meter} long and weighing over \SI{2}{\tonne}. However, being a \emph{tetraodontiform} closely related to boxfishes and puffer fish, it has very tiny fry (fish babies) measuring only \SI{2.5}{\milli\meter} long and weighing less than a gram. These fry look a lot like a tiny puffer fish, showing the \emph{Mola}'s ancestry.

A \emph{Mola} fry was chosen as the 5b1t logo due to the large growth it undergoes through its lifetime. The sunfish has a claim to be the animal with the largest growth over its lifetime, growing over 60 million times its size at birth by the time it is an adult. Similarly 5b1t programs expand tiny source into massive executables (the empty program expands to over 5 mb of executable) with enormous potential.

Despite the myth that sunfish diets consist mostly of jellyfish, 5b1t is intended to be a friendly competitor to Dennis Mitchell's Jelly and not a predator thereof.

\end{document}
