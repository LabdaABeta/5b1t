# 5b1t: 5 Bits, 1 Trit (or /ɛsbɪt/)

![Logo](https://assets.gitlab-static.net/uploads/-/system/project/avatar/7706110/bitmap.png)

Yet another code golf language. This language uses the 96 printable ASCII
characters as its functional character set (5 bits and 1 trit each - thus the
name). This makes it easy to write and when scored at ~6.585 bits/character (or
~0.823 bytes) it can compete with other golfing languages.

## Getting Started

These instructions explain how to both write code in 5b1t and how to add your
own features to the language.

### Prerequisites

In order to build 5b1t you will need a working Ada 12 compiler and a gnat
gprbuild executable. These are provided in many Linux distributions under names
like `gprbuild` and `gcc-ada` or you can get them from
[AdaCore](https://www.adacore.com/).

In order to compile the documentation you will need a LaTeX installation.

Additional tools which are required are:

 - aspell
 - wget
 - tar
 - bash
 - libcurl

If you have these dependencies you can build all the necessary files with
a simple `make` command. With priviledges you can also run `make install` with
an optionally set `PREFIX` environment variable to expose the `5b1t` executable
system wide.

### Building

To build 5b1t just run `make` from the command-line.

### Executing

To execute a 5b1t source file the command you use will depend on the encoding.
If encoded as an ASCII file then you can run `./5b1t -a <filename>`. If encoded
in natural encoding (which is compressed) run `./5b1t <filename>`.

You can set the input type using the `-i <typename>` flag.

For additional options and information run `./5b1t --help`.

## The Language

For documentation about the language see documentation/documentation.pdf which
can be generated from documentation/documentation.tex. For a tutorial go
[here](#) (coming soon!).

## Built With

This project uses Ada 12 as its core language. The build system is AdaCore's
GPRbuild. The documentation is written in LaTeX, currently targeted for XeLaTeX compilation.

![Ada mascot](http://www.getadanow.com/fsfgnat.png)
![Ada coin](https://people.cs.kuleuven.be/~dirk.craeynest/ada-belgium/pictures/ada-strong-305-color.png)
![AdaCore](https://upload.wikimedia.org/wikipedia/commons/c/cd/AdaCore_%28logo%29.jpg)

## Authors

 - [Louis A. Burke](http://www.labprogramming.net/)
