# Contributing

When contributing to this repository, please follow these guidelines.

## What to contribute

There are 10 ways to contribute to 5b1t. Any pull request should only update one of them. They are:

 - [Definition of New Operation(s)](#definition-of-new-operations)
 - [Definition of New Type](#definition-of-new-type)
 - [Definition of Operation Consolidation](#definition-of-operation-consolidation)
 - [Implementation of New Operation(s)](#implementation-of-new-operations)
 - [Implementation of New Type](#implementation-of-new-type)
 - [Implementation of New Tests](#implementation-of-new-tests)
 - [New Bug Report](#new-bug-report)
 - [New Issue](#new-issue)
 - [Bug Fix](#bug-fix)
 - [Issue Fix](#issue-fix)

### Definition of New Operation(s)

**Guideline:** Define new operations on existing types when they are well attributed to the type. Prefer creating new subtypes or related types if an operation would be better suited for them.

**Example:** The `P` operator on a natural number should not be extended to the integer type. The concept of primality is a fundamental property of natural numbers, but not of integers. For example, is -2 prime?

**How:** To add a new operation to a type, edit the [documentation.tex](documentation/documentation.tex) file. Find the typetab under the settype naming the type and add the new operation to it. Come up with a test case that proves the operator is working, add its source to the commit message.

**Rules:**

 1. Operation tables must be kept in code-page order (Operations with blocks come immediately after the unblocked operation of the same name.)
 2. New operators must be marked with `\unimplemented`
 3. New operators should have a descriptive name
 4. New operators' descriptions must take the form `Name: Description`
 5. New operators' descriptions must consist of full sentences, complete with capitalization, grammar, and punctuation
 6. New operators' descriptions should fully define the operation (If necessary, use paragraphs to maintain readability.)
 7. New operators should be submitted along with source and expected outputs for at least one sanity-level test
 8. Only operations for a single type should be submitted per request

**Sample Commit:** !2

### Definition of New Type

**Guideline:** Define new types when any of the following conditions are met:

 - There isn't an existing type to represent the same information
 - An existing type has a set of related operations that could be handled by a sub-type
 - An existing type needs more operations but is low on available code-points
 - An existing type admits a related derivative type with a different set of useful operations

**Example:** The integer type is useful, but fairly general. Creating the natural type relieves much of the load of the integer type by taking the natural-specific responsibilities.

**How:** To add a new type, edit the [documentation.tex](documentation/documentation.tex) file. Find the most relevant related type's settype section and create a new settype after its section. Ensure to use the optional argument to specify any inherited type.

**Rules:**

 1. New types must have at least a basic description
 2. New types that can be used as inputs should be added to the [types](types) file
 3. New types must be accessible, either as inputs or via an operation on another type
 4. When adding a new type only operations which result in an object of the new type may be added to other types
 5. Only one new type may be added in one request

**Sample Commit:** !3

### Definition of Operation Consolidation

**Guideline:** Define a consolidation of operations when a type has too many operations or poorly named operations.

**Example:** If the integer type had `+` and `A` both meaning "absolute value", one should be consolidated.

**How:** To consolidate operation(s) edit the [documentation.tex](documentation/documentation.tex) file. Find the typetab under the settype naming the type and mark the obsolete operation(s) with `\obsoleted`. Meanwhile follow the above guidelines to add a new type or new operation(s) to replace the obsoleted one(s) if necessary.

**Rules:**

 1. Consolidated operations should have a good reason for being consolidated
 2. Consolidated operations should not be removed without very good reason (move operations - don't remove them)

**Sample Commit:** Coming soon!

### Implementation of New Operation(s)

**Guideline:** Implement operations when they are already documented. Do not implement operations until their documentation has been pulled.

**Example:** If the integer type has an operation marked `\unimplemented`, it could be implemented.

**How:** To implement an operation find its parent type under the [types](src/types) directory. Add the function to be overriden to the specification file (.ads) and implement it in the body file (.adb). Add the sanity test from the definition commit to the [test](test) directory. Ensure all tests still pass. Remove the `\unimplemented` tag from the relevant line in the documentation, replacing it with `\partially` if necessary.

**Rules:**

 1. New operators must only be implemented *after* their definitions are committed
 2. New operators should be implemented with clean, easy-to-maintain code
 3. Any infrastructure created to implement a new operator should be exposed in an appropriate specification
 4. New operators should conform as closely as possible to their existing definitions
 5. Any discrepencies between a new operator's implementation and its definition should be resolved by updating the definition if necessary

**Sample Commit:** !4

### Implementation of New Type

**Guideline:** Implement types when they are already documented. Do not implement types until their documentation has been pulled.

**Example:** If the integer type doesn't exist yet, but has documentation committed, it could be implemented.

**How:** To implement a new type, create a specification/body pair under the [types](src/types) directory. The files should be named after a suitable plural form of the type itself. Implement the type's package, ensuring to override at least the `Create`, `Read`, `Write`, `Image`, and `Type_Image` subprograms. It is not necessary to implement all documented operations concerning a type. In fact, it is often useful to implement the type, and some infrastructure to work with it, without implementing any operations that reference it.

**Rules:**

 1. New types must be implemented *after* their definitions are committed
 2. New types should have reasonable images
 3. New types should have reasonable output formats
 4. New types that can be inputs should have reasonable input formats

**Sample Commit:** !5

### Implementation of New Tests

**Guideline:** Implement new tests judiciously. Just make sure to tag them appropriately.

**Example:** If there is an operator that only has its sanity test defined, it is useful to define a few more tests to investigate the implementation's reliability.

**How:** To implement a new test, create a new directory somewhere under the [test](test) directory. Add the relevant test code to the file `test.sbit`. Encode that file and save it as `test`. Add the name of the input type to the `type` file, ensuring that there are no trailing newlines (often done via `echo -n 'type' > type`). Add the input and output data to the `input` and `output` files respectively. Add any relevant tags to the `tags` directory. Read [tags.md](test/tags.md) for a list of tags.

**Rules:**

 1. New tests must pass when added (Tests which fail but should pass should be reported as bugs instead.)
 2. New tests should be well commented in the source code (Use the TAB character to add comments to sbit code)
 3. New tests should cover new test cases, do not duplicate tests
 4. New tests should be appropriately tagged
 5. New tests should appear in the appropriate test subdirectories (test/typename/...)

**Sample Commit:** !7

### New Bug Report

**Guideline:** Generate a new bug report if there is any instance of unexpected or undesired behaviour.

**Example:** If the `+` operator is documented to return the absolute value of an integer, but instead increments it by 1, this should be captured in a bug report.

**How:** Go to the gitlab repository (at https://gitlab.com/LabdaABeta/5b1t). Click on the [issues](https://gitlab.com/LabdaABeta/5b1t/issues) button in the left pane. Click on the `New issue` button. Fully describe the issue and set its label to ~bug. Alternatively, email [incoming+LabdaABeta/5b1t@incoming.gitlab.com](mailto:incoming+LabdaABeta/5b1t@incoming.gitlab.com) and another user can create a bug from a description in the email.

**Rules:**

 1. Do not create duplicate bugs
 2. Do not raise enhancements as bugs
 3. A bug report must contain a test case which produces incorrect behaviour

**Sample Bug:** #15

### New Issue

**Guideline:** Generate a new issue if there is anything that can be improved about 5b1t.

**Example:** If it would be useful to be able to calculate fibonacci numbers, but you aren't sure if such functionality exists or where it would be documented, create an enhancement issue for fibonacci numbers.

**How:** Go to the gitlab repository (at https://gitlab.com/LabdaABeta/5b1t). Click on the [issues](https://gitlab.com/LabdaABeta/5b1t/issues) button in the left pane. Click on the `New issue` button. Fully describe the issue and choose an appropriate label:

 - ~documentation: For documentation issues that should be fixed. If the documentation is wrong use a bug instead.
 - ~enhancement: For specific feature requests (e.g. Add a Fibonacci operator to Natural Numbers)
 - ~suggestion: For generic feature requests (e.g. Add a way to calculate Fibonacci numbers)
 - ~support: For features that would make writing 5b1t programs easier
 - ~project: For issues with the 5b1t project itself

 **Rules:**

  1. Do not create duplicate issues
  2. Do not create low-effort issues

**Sample Issue:** #13

### Bug Fix

**Guideline:** Create a bug fix for any open bug.

**Example:** If there was a bug for the above `+` operator issue, creating a fix would be helpful.

**How:** When looking at a bug, note its id#. Implement the fix, put the fixed test into the tests directory, and add the string `Fixes #?` to the commit message (with the ? representing the issue id).

**Rules:**

 1. Bug fixes must fix open bugs
 2. Bug fixes must pass all previous tests
 3. Bug fixes must contain the key `Fixes #?` auto-closing line in commit message
 4. Bug fixes must add the previously failing test to the (test)[test] directory

**Sample Commit:** !6

### Issue Fix

**Guideline:** Create issue fixes for ~enhancement/~documentation/~support/~project issues. ~suggestion issues should first be modified into ~enhancement issues before being fixed.

**Example:** If there was an enhancement request to add fibonacci numbers, adding the appropriate new operator definitions would fix the issue.

**How:** Typically ~enhancement issues are fixed with either a new operator or new type definition. This would follow the above guidelines with the additional `Fixes #?` tag added to the commit message. Note that the issue is fixed by the documentation update, the documentation itself acts as the 'issue' for implementing the feature(s).

**Rules:**

 1. Issues should be fixed by documentation updates, not implementations

**Sample Commit:** !3

## How to contribute

In order to contribute either raise an issue or create a merge request matching one of the above options.
